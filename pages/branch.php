<?php
include_once('../header.php');

include_once('../dbFunction.php');
$funObj = new dbFunction();
if($_POST['addBranch'])
{
	$branch = $_POST['branch'];
	$check = $funObj->checkeAllreadyExist('branch','branch',$branch);
	
	if($check)
	{
		echo "<script>alert('Branch aleady exist!')</script>";
	}
	else
	{
		$fieldvalue['branch'] = $branch;
		$tc = $funObj->addfunction('branch',$fieldvalue);	
	}
}
$branchs = $funObj->getTableData('branch');
?>
<div id="page-wrapper">
    <div class="container">
	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">Branch</h1>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-7">
			<div class="panel panel-default">
				<div class="panel-heading clearfix">
					Branch Detail
				</div>				
				<div class="panel-body">
					<div class="dataTable_wrapper">
						<table class="table table-striped table-bordered table-hover" id="dataTables-example">
							<thead>
								<tr>
									<th>Action</th>
									<th>Branch</th>
								</tr>
							</thead>
							<tbody>
								<?php 
									while($branch=mysql_fetch_object($branchs))
									{
										?>
											<tr class="odd gradeX">
												<td><a onclick="return confirm('Are you sure?');" href="<?php echo SITE_URL.'pages/delete.php?redirect=branch&table=branch&field=branch_id&id='.$branch->branch_id; ?>">Delete</a></td>
												<td><?php echo $branch->branch; ?></td>
											</tr>	
										<?php
									} 
								?>								
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
		<div class="col-lg-5">
			<div class="panel panel-default">
				<div class="panel-heading clearfix">
					Add Branch
				</div>
				<div style="margin:20px">	
				<form role="form" name="addBranch" method="POST">
					<div class="form-group">
						<label>Branch</label>
						<input type="text" name="branch" required class="form-control">
					</div>
					<input class="btn btn-success btn-block" type="submit" name="addBranch" value="Add Branch" />
				</form>
				</div>
			</div>
		</div>
	</div>
    </div>
</div>
<?php
include_once('../footer.php');
?>

