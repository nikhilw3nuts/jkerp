<?php
include_once('../header.php');
include_once('../dbFunction.php');
$funObj = new dbFunction();

if($_POST['adduser'])
{
	$usertype = $_POST['usertype'];
	$username = $_POST['username'];
	$phone = $_POST['phone'];
	$email = $_POST['email'];
	$pass = $_POST['pass'];
	$repass = $_POST['re-pass'];
	$checkemail = $funObj->checkemail($email);
	if($checkemail)
	{
		echo "<script>alert('User aleady exist with same email')</script>";
	}
	else
	{
		if($repass != $pass)
		{
			echo "<script>alert('Password Not match')</script>";
		}
		else
		{
			$adduer = $funObj->adduser($usertype,$username,$phone,$email,$pass);
			$url = SITE_URL.'pages/user.php';
			echo "<script>alert('Add User Successfully')</script>";
		}
	}
}
$usertype = $funObj->getTableData('usertype');
?>
<div id="page-wrapper">
    <div class="container">
	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">Add User</h1>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-default">
				<div class="panel-body">
					<div class="col-lg-3">
					</div>
					<div class="col-lg-6">
						<form role="form" name="adduser" method="POST">
							<div class="form-group">
								<label>User type</label>
								<select class="form-control" name="usertype">
									<?php
										while($usype=mysql_fetch_object($usertype))
										{
											?>
											<option value="<?php echo $usype->usertypeid; ?>"><?php echo $usype->usertype; ?></option>
											<?php
										}
									?>	
								</select>
							</div>
							<div class="form-group">
								<label>User Name</label>
								<input type="text" name="username" required class="form-control">
							</div>
							<div class="form-group">
								<label>Phone Number</label>
								<input type="text" name="phone" required class="form-control">
							</div>
							<div class="form-group">
								<label>Email</label>
								<input type="email" name="email" required class="form-control">
							</div>							
							<div class="form-group">
								<label>Password</label>
								<input type="password" name="pass" required class="form-control">
							</div>
							<div class="form-group">
								<label>Confirm Password</label>
								<input type="password" name="re-pass" required class="form-control">
							</div>
							<input class="btn btn-success btn-block" type="submit" name="adduser" value="Add User" />
						</form>	
					</div>
					<div class="col-lg-3">
					</div>
				</div>
			</div>
		</div>
	</div>
    </div>    
</div>
<?php
include_once('../footer.php');
?>
<script>
jQuery(document).ready(function() {
	jQuery('#dataTables-example').DataTable({
			responsive: true
	});
});
</script>
	
