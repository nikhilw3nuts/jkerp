<?php
include_once('../header.php');

include_once('../dbFunction.php');
$funObj = new dbFunction();
$users = $funObj->getTableData('user');
?>
<div id="page-wrapper">
    <div class="container">
	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">Users</h1>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-default">
				<div class="panel-heading clearfix">
					User Detail
					<a class="btn btn-primary" style="float:right" href="<?php echo SITE_URL ?>pages/adduser.php">Add User</a>
				</div>				
				<div class="panel-body">
					<div class="dataTable_wrapper">
						<table class="table table-striped table-bordered table-hover" id="dataTables-example">
							<thead>
								<tr>
									<th>Action</th>
									<th>User Name</th>
									<th>Email Address</th>
									<th>Phone number</th>
									<th>User type</th>
								</tr>
							</thead>
							<tbody>
								<?php 
									while($user=mysql_fetch_object($users))
									{
										?>
											<tr class="odd gradeX">
												<td><a onclick="return confirm('Are you sure?');" href="<?php echo SITE_URL.'pages/delete.php?redirect=user&table=user&field=userid&id='.$user->userid; ?>">Delete</a> | <a href="<?php echo SITE_URL.'pages/changeuserpassword.php?&id='.$user->userid; ?>">change password</a> |
												<a href="<?php echo SITE_URL.'pages/editprofile.php?&id='.$user->userid; ?>">Edit Profile</a></td>
												<td><?php echo $user->username; ?></td>
												<td><?php echo $user->email; ?></td>
												<td><?php echo $user->phone; ?></td>
												<td><?php echo $user->usertype; ?></td>
											</tr>	
										<?php
									} 
								?>								
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
    </div>    
</div>
<?php
include_once('../footer.php');
?>
<script>
jQuery(document).ready(function() {
	jQuery('#dataTables-example').DataTable({
			responsive: true
	});
});
</script>
