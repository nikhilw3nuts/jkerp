<?php
include_once('../header.php');

include_once('../dbFunction.php');
$funObj = new dbFunction();
if($_POST['addsubItemgroup'])
{
	$subitem_group = $_POST['subitem_group'];
	$isExist = $funObj->checkeAllreadyExist("item_sub_group","Sub_group_value",$subitem_group);
	if($isExist)
	{
		echo "<script>alert('sub Group Item aleady exist!')</script>";
	}
	else
	{
		$city = $funObj->addsubItemgroup($subitem_group);
	}
}
$subitem_groups = $funObj->getTableData('item_sub_group');
?>
<div id="page-wrapper">
    <div class="container">
	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">Sub Item Group </h1>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-7">
			<div class="panel panel-default">
				<div class="panel-heading clearfix">
				  Sub Item 
				</div>				
				<div class="panel-body">
					<div class="dataTable_wrapper">
						<table class="table table-striped table-bordered table-hover" id="dataTables-example">
							<thead>
								<tr>
									<th>Action</th>
									<th>SubItem Group</th>
								</tr>
							</thead>
							<tbody>
								<?php 
									while($subitem_group=mysql_fetch_object($subitem_groups))
									{
										?>
											<tr class="odd gradeX">
												<td><a onclick="return confirm('Are you sure?');" href="<?php echo SITE_URL.'pages/delete.php?redirect=sub_group&table=item_sub_group&field=sub_group_id&id='.$subitem_group->sub_group_id; ?>">Delete</a></td>
												<td><?php echo $subitem_group->Sub_group_value; ?></td>
											</tr>	
										<?php
									} 
								?>								
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
		<div class="col-lg-5">
			<div class="panel panel-default">
				<div class="panel-heading clearfix">
					Add Sub Item 
				</div>
				<div style="margin:20px">	
				<form role="form" name="addsubItemgroup" method="POST">
					<div class="form-group">
						<label>Sub Item</label>
						<input type="text" name="subitem_group" required class="form-control">
					</div>
					<input class="btn btn-success btn-block" type="submit" name="addsubItemgroup" value="Add SubItem" />
				</form>
				</div>
			</div>
		</div>
	</div>
    </div>    
</div>
<?php
include_once('../footer.php');
?>
