<?php
include_once('../header.php');

include_once('../dbFunction.php');
$funObj = new dbFunction();

if($_POST['addagent'])
{
	$isExist = $funObj->checkeAllreadyExist("agent","agent",$_POST['agentname']);
	if($isExist)
	{
		echo "<script>alert('Agent aleady exist with same email')</script>";
	}
	else
	{
		$funObj->addAgent($_POST['commission'],$_POST['agentname']);
	}
}
$agents = $funObj->getTableData('agent');
?>


<div id="page-wrapper">
    <div class="container">
	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">Add Agent</h1>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-default">
				<div class="panel-body">
					<div class="col-lg-3">
					</div>
					<div class="col-lg-6">
						<form role="form" name="addagent" method="POST">
							<div class="form-group">
								<label>Agent Name</label>
								<input type="text" name="agentname" required class="form-control">
							</div>
							<div class="form-group">
								<label>Commission</label>
								<input type="text" name="commission" required class="form-control">
							</div>
							<input class="btn btn-success btn-block" type="submit" name="addagent" value="Add Agent" />
						</form>	
					</div>
					<div class="col-lg-3">
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">Agents</h1>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-default">
				<div class="panel-heading clearfix">
					Agent Detail
				</div>				
				<div class="panel-body">
					<div class="dataTable_wrapper">
						<table class="table table-striped table-bordered table-hover" id="dataTables-example">
							<thead>
								<tr>
									<th>Action</th>
									<th>Agent Name</th>
									<th>Commission</th>
									<th>Created By</th>
								</tr>
							</thead>
							<tbody>
								<?php 
									while($agent=mysql_fetch_object($agents))
									{
										$agent_user = mysql_fetch_array($funObj->getDataById("user","userid='".$agent->created_by."'"));
										?>
										<tr class="odd gradeX">
											<td><a onclick="return confirm('Are you sure?');" href="<?php echo SITE_URL.'pages/delete.php?redirect=agent&table=agent&field=agent_id&id='.$agent->agent_id; ?>">Delete</a></td>
											<td><?php echo $agent->agent; ?></td>
											<td><?php echo $agent->agent_commission; ?></td>
											<td><?php echo $agent_user['username']; ?></td>
										</tr>
										<?php
									} 
								?>									
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
    </div>    
</div>


<?php
include_once('../footer.php');
?>
<script>
jQuery(document).ready(function() {
	jQuery('#dataTables-example').DataTable({
			responsive: true
	});
});
</script>
