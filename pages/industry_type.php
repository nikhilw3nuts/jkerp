<?php
include_once('../header.php');

include_once('../dbFunction.php');
$funObj = new dbFunction();
if($_POST['addIndustryType'])
{
	$industry_type = $_POST['industry_type'];
	$isExist = $funObj->checkeAllreadyExist("industry_type","industry_type",$industry_type);
	if($isExist)
	{
		echo "<script>alert('Industry type aleady exist!')</script>";
	}
	else
	{
		$city = $funObj->addIndustryType($industry_type);
	}
}
$industry_types = $funObj->getTableData('industry_type');
?>
<div id="page-wrapper">
    <div class="container">
	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">Industry Type </h1>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-7">
			<div class="panel panel-default">
				<div class="panel-heading clearfix">
					Industry Types
				</div>				
				<div class="panel-body">
					<div class="dataTable_wrapper">
						<table class="table table-striped table-bordered table-hover" id="dataTables-example">
							<thead>
								<tr>
									<th>Action</th>
									<th>Industry Type</th>
								</tr>
							</thead>
							<tbody>
								<?php 
									while($industry_type=mysql_fetch_object($industry_types))
									{
										?>
											<tr class="odd gradeX">
												<td><a onclick="return confirm('Are you sure?');" href="<?php echo SITE_URL.'pages/delete.php?redirect=industry_type&table=industry_type&field=industry_type_id&id='.$industry_type->industry_type_id; ?>">Delete</a></td>
												<td><?php echo $industry_type->industry_type; ?></td>
											</tr>	
										<?php
									} 
								?>								
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
		<div class="col-lg-5">
			<div class="panel panel-default">
				<div class="panel-heading clearfix">
					Add Industry Type 
				</div>
				<div style="margin:20px">	
				<form role="form" name="addIndustryType" method="POST">
					<div class="form-group">
						<label>Industry Type</label>
						<input type="text" name="industry_type" required class="form-control">
					</div>
					<input class="btn btn-success btn-block" type="submit" name="addIndustryType" value="Add Industry Type" />
				</form>
				</div>
			</div>
		</div>
	</div>
    </div>    
</div>
<?php
include_once('../footer.php');
?>
