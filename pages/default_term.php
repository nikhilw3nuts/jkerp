<?php
include_once('../header.php');

include_once('../dbFunction.php');
$funObj = new dbFunction();
if($_POST['tcgroup'])
{		
		$fieldvalue['template_for'] = $_POST['dtc_for'];
		$fieldvalue['group']  = $_POST['dtc_code'];
		$fieldvalue['terms_codition']  = $_POST['dtc_desc'];
		$funObj->addfunction('default_terms_templat',$fieldvalue);
}
$default_terms_templats = $funObj->getTableData('default_terms_templat');
$term_group = $funObj->getTableData('term_group');
?>
<div id="page-wrapper">
    <div class="container">
	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">Default Terms & Condition</h1>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-7">
			<div class="panel panel-default">
				<div class="panel-heading clearfix">
					Default Terms & Condition
				</div>				
				<div class="panel-body">
					<div class="dataTable_wrapper">
						<table class="table table-striped table-bordered table-hover" id="dataTables-example">
							<thead>
								<tr>
									<th>Action</th>
									<th>Tempalte Name</th>
                                    <th>Group Name</th>
                                    <th>Terms & Condition</th>
								</tr>
							</thead>
							<tbody>
								<?php 
									while($default_terms_templat=mysql_fetch_object($default_terms_templats))
									{
										?>
											<tr class="odd gradeX">
												<td><a onclick="return confirm('Are you sure?');" href="<?php echo SITE_URL.'pages/delete.php?redirect=default_term&table=default_terms_templat&field=default_terms_templat_id&id='.$default_terms_templat->default_terms_templat_id; ?>">Delete</a></td>
												<td><?php echo $default_terms_templat->template_for; ?></td>
                                                <td><?php echo $default_terms_templat->group; ?></td>
												<td><?php echo $default_terms_templat->terms_codition; ?></td>
											</tr>	
										<?php
									} 
								?>								
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
		<div class="col-lg-5">
			<div class="panel panel-default">
				<div class="panel-heading clearfix">
					Add Default Terms & Condition
				</div>
				<div style="margin:20px">	
				<form role="form" name="tcgroup" method="POST">
					<div class="form-group">
						<label>Default Terms & Condition For</label>
                        <select class="form-control" name="dtc_for">
                            <option>Select One</option>
                            <option  value="Inquiry">Inquiry</option>
                            <option  value="Quotation">Quotation</option>
                            <option  value="Sales">Sales</option>
                        </select>
					</div>
                    <div class="form-group">
						<label>Group </label>
						<input type="text" name="dtc_code" required readonly class="form-control termsdesc" style="min-width:0px; width:40%; !important;">
                        <a class="openpopup" style="background: #dfdfdf none repeat scroll 0 0; padding: 10px;"><i class="fa fa-binoculars"></i></a>
					</div>
                    <div class="form-group">
						<label>Terms & Condition</label>
						<textarea name="dtc_desc" required class="form-control"></textarea>
					</div>
					<input class="btn btn-success btn-block" type="submit" name="tcgroup" value="Add Template" />
				</form>
				</div>
			</div>
		</div>
	</div>
    </div>
</div>
<div class="modal fade in" id="mytermsmodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
				<h4 class="modal-title" id="myModalLabel">Terms & Condition Group</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-lg-12">
						<div class="panel panel-default">
							<div style="margin: 20px 10px">	

								<form role="form" id="addalert" name="addalert" method="POST">
									<div class="row">
										<div class="col-lg-12">
											<table class="table table-striped" id="dataTables-example">
												<thead>
													<tr>
														<th>Code</th>
														<th>Description</th>
													</tr>
												</thead>
												<tbody>
													<?php
														while($term_groups=mysql_fetch_object($term_group))
														{
															?>
																<tr class="selectTermsGroup" data-code="<?php echo $term_groups->code; ?>" data-desc="<?php echo $term_groups->term_group; ?>">																
																	<td><?php echo $term_groups->code; ?></td>
																	<td><?php echo $term_groups->term_group; ?></td>
																</tr>	
															<?php
														}
													?>
												</tbody>
											</table>
										</div>
									</div>
									
								</form>    
							</div>
						</div>
					</div>
				</div>
				
			</div>
		</div>
	</div>
</div>
<?php
include_once('../footer.php');
?>
<script>
	jQuery("document").ready(function(e) {
        jQuery('.openpopup').click(function(){
			$('#mytermsmodal').modal("show");
		});
		
		jQuery('.selectTermsGroup').click(function(){
			jQuery('.termscode').val(jQuery(this).attr('data-code'));
			jQuery('.termsdesc').val(jQuery(this).attr('data-desc'));
			$('#mytermsmodal').modal("hide");
		})
		
		jQuery('#dataTables-example').DataTable({
			responsive: true
		});
    });
</script>