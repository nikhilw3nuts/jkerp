<?php
include_once('../header.php');

include_once('../dbFunction.php');
$funObj = new dbFunction();
if($_POST['addItem'])
{
	$item_group = $_POST['item_group'];
	$checkePartyType = $funObj->checkePartyType($item_group_code);
	$tablename = "item_group_code";
	$fieldname = "item_group";
	//checkeAllreadyExist($tablename,$fieldname,$value)
	$checkeInTable = $funObj->checkeAllreadyExist($tablename,$fieldname,$item_group);
	if($checkeInTable)
	{
		echo "<script>alert('Item Group Code is aleady exist!')</script>";
	}
	else
	{
		$addParty = $funObj->addItemGroupCode($item_group);
	}
}
$item_group_codes = $funObj->getTableData('item_group_code');
?>
<div id="page-wrapper">
    <div class="container">
	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">Item Group Code</h1>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-7">
			<div class="panel panel-default">
				<div class="panel-heading clearfix">
					Item Group Code
				</div>				
				<div class="panel-body">
					<div class="dataTable_wrapper">
						<table class="table table-striped table-bordered table-hover" id="dataTables-example">
							<thead>
								<tr>
									<th>Action</th>
                                    <th>Item Group</th>
								</tr>
							</thead>
							<tbody>
								<?php 
									while($item_group_code=mysql_fetch_object($item_group_codes))
									{
										?>
											<tr class="odd gradeX">
												<td><a onclick="return confirm('Are you sure?');" href="<?php echo SITE_URL.'pages/delete.php?redirect=item_group_code&table=item_group_code&field=item_group_code_id&id='.$item_group_code->item_group_code_id; ?>">Delete</a></td>
												<td><?php echo $item_group_code->item_group; ?></td>
											</tr>	
										<?php
									} 
								?>								
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
		<div class="col-lg-5">
			<div class="panel panel-default">
				<div class="panel-heading clearfix">
					Add Item Group Code
				</div>
				<div style="margin:20px">	
				<form role="form" name="addItem" method="POST">
					<div class="form-group">
						<label>Item Group</label>
						<input type="text" name="item_group" required class="form-control">
					</div>
					<input class="btn btn-success btn-block" type="submit" name="addItem" value="Add Item Group" />
				</form>
				</div>
			</div>
		</div>
	</div>
    </div>    
</div>
<?php
include_once('../footer.php');
?>
<script>
jQuery(document).ready(function() {
	jQuery('#dataTables-example').DataTable({
			responsive: true
	});
});
</script>
