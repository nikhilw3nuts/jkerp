<?php
include_once('../header.php');

include_once('../dbFunction.php');
$funObj = new dbFunction();
if($_POST['addalerttype'])
{
	$alerttype = $_POST['alerttype'];
	$isExist = $funObj->checkeAllreadyExist("alert_type_master","alert_type",$alerttype);
	if($isExist)
	{
		echo "<script>alert('Alert Type aleady exist!')</script>";
	}
	else
	{
		$funObj->addAlertType($alerttype);
	}
}
$alert_type = $funObj->getTableData('alert_type_master');
?>
<div id="page-wrapper">
    <div class="container">
	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">Alert Type</h1>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-7">
			<div class="panel panel-default">
				<div class="panel-heading clearfix">
					Alert Type
				</div>				
				<div class="panel-body">
					<div class="dataTable_wrapper">
						<table class="table table-striped table-bordered table-hover" id="dataTables-example">
							<thead>
								<tr>
									<th>Action</th>
									<th>Alert Type</th>
								</tr>
							</thead>
							<tbody>
								<?php 
									while($alert_types=mysql_fetch_object($alert_type))
									{
										?>
											<tr class="odd gradeX">
												<td><a onclick="return confirm('Are you sure?');" href="<?php echo SITE_URL.'pages/delete.php?redirect=alerttypemaster&table=alert_type_master&field=alert_type_id&id='.$alert_types->alert_type_id; ?>">Delete</a></td>
												<td><?php echo $alert_types->alert_type; ?></td>
											</tr>	
										<?php
									} 
								?>								
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
		<div class="col-lg-5">
			<div class="panel panel-default">
				<div class="panel-heading clearfix">
					Add Alert Type
				</div>
				<div style="margin:20px">	
				<form role="form" name="addcountrcountryy" method="POST">
					<div class="form-group">
						<label>Alert type</label>
						<input type="text" name="alerttype" required class="form-control">
					</div>
					<input class="btn btn-success btn-block" type="submit" name="addalerttype" value="Add Alert Type" />
				</form>
				</div>
			</div>
		</div>
	</div>
    </div>    
</div>
<?php
include_once('../footer.php');
?>
<script>
jQuery(document).ready(function() {
	jQuery('#dataTables-example').DataTable({
			responsive: true
	});
});
</script>
