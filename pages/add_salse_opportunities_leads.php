<?php 
include_once('../header.php');
include_once('../dbFunction.php');
$funObj = new dbFunction();

if($_POST['addopportunity'])
{
	$tablename="salse_opportunities_leads";
	$filed_value['lead_title']=$_POST['lead_title'];
	$filed_value['lead_start_date']=$_POST['lead_start_date'];
	$filed_value['lead_end_date']=$_POST['lead_end_date'];
	$filed_value['industry_type']=$_POST['industry_type'];
	$filed_value['lead_source']=$_POST['lead_source'];
	$filed_value['lead_owner']=$_POST['lead_owner'];
	$filed_value['assigned_to']=$_POST['assigned_to'];
	$filed_value['inquiry_priority']=$_POST['inquiry_priority'];
	$filed_value['inquiry_reference']=$_POST['inquiry_reference'];
	$filed_value['inquiry_stage']=$_POST['inquiry_stage'];
	$filed_value['leadprovide']=$_POST['leadprovide'];
	$filed_value['inquiry_status']=$_POST['inquiry_status'];
	$filed_value['review_date']=$_POST['review_date'];
	$filed_value['lead_detail']=$_POST['lead_detail'];
	$iast_id=$funObj->addfunction($tablename,$filed_value);
	echo $iast_id;
	if($iast_id!='')
	{
		$redirect = SITE_URL."pages/salse_opportunities_lead_detail.php?id=".$iast_id."";
		?>
			<script>
                window.location.assign("<?php echo $redirect; ?>")
            </script>
		<?php
	}
}
$industry_types = $funObj->getTableData('industry_type');
$users = $funObj->getTableData('user');
$users1 = $funObj->getTableData('user');
$industry_types = $funObj->getTableData('industry_type');
$industry_stage = $funObj->getTableData('industry_stage');
$industry_status = $funObj->getTableData('industry_status');
$reference_inquirys = $funObj->getTableData('reference');
$priority = $funObj->getTableData('priority');
$lead_providers = $funObj->getTableData('lead_provider'); ?>
<div id="page-wrapper">
    <div class="container">
	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">Add Salse Opportunities Leads.</h1>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-default">
				<div class="panel-body">
					<form role="form" name="addInquiry" method="POST">
						<div class="row">
							<div class="col-lg-4">
								<div class="form-group">
									<label>Lead No.</label>
                                    <input type="text" name="lead_no"  class="form-control">
                                </div>
                            </div>
                            <div class="col-lg-4">
								<div class="form-group">
									<label>Lead Title</label>
                                    <input type="text" name="lead_title"  class="form-control">
                                </div>
                            </div>
                            <div class="col-lg-4">
								<div class="form-group">
									<label>Lead Start Date</label>
                                    <input type="text" name="lead_start_date" value="<?php echo date('d-m-Y'); ?>"  class="form-control datepickers">
                                </div>
                            </div>
						</div>	
                        <div class="row">
							<div class="col-lg-6">
								<div class="form-group">
									<label>Lead End Date</label>
									<input type="text" name="lead_end_date"  class="form-control datepickers">
								</div>
							</div>
							
						</div>                        
                        <div class="row">
							<div class="col-lg-6">
								<div class="form-group">
									<div class="form-group">
										<label>Inquery Type</label>
										 <select class="form-control" name="industry_type">
                                        <?php
                                            while($industry_type=mysql_fetch_object($industry_types))
                                            {
                                                ?>
                                                <option value="<?php echo $industry_type->industry_type_id; ?>"><?php echo $industry_type->industry_type; ?></option>
                                                <?php
                                            }
                                        ?>	
                                    </select>
									</div>
								</div>
							</div>
                            <div class="col-lg-6">
								<div class="form-group">
									<div class="form-group">
										<label>Lead Owner</label>
										<select class="form-control" name="lead_owner">
											<?php
												while($user=mysql_fetch_object($users))
												{
													?>
													<option value="<?php echo $user->userid; ?>"><?php echo $user->username; ?></option>
													<?php
												}
											?>	
										</select>
									</div>
								</div>
							</div>
						</div>
                        <div class="row">
							<div class="col-lg-6">
								<div class="form-group">
									<label>Assigned To</label>
                                    <select class="form-control" name="assigned_to">
                                        <?php
                                            while($user=mysql_fetch_object($users1))
                                            {
                                                ?>
                                                <option value="<?php echo $user->userid; ?>"><?php echo $user->username; ?></option>
                                                <?php
                                            }
                                        ?>	
                                    </select>
								</div>
							</div>
                            <div class="col-lg-6">
								<div class="form-group">
                                    <label>Priority</label>
                                    <select class="form-control" name="inquiry_priority">
                                        <?php
                                            while($prioritys=mysql_fetch_object($priority))
                                            {
                                                ?>
                                                <option value="<?php echo $prioritys->priority_id; ?>"><?php echo $prioritys->priority; ?></option>
                                                <?php
                                            }
                                        ?>	
                                    </select>
								</div>
							</div>
						</div>
                        <div class="row">
							<div class="col-lg-6">
								<div class="form-group">
									 <label>Lead Source</label>
                                    <select class="form-control" name="inquiry_reference">
                                        <?php
                                            while($reference_inquiry=mysql_fetch_object($reference_inquirys))
                                            {
                                                ?>
                                                <option value="<?php echo $reference_inquiry->referenceid; ?>"><?php echo $reference_inquiry->referenceby; ?></option>
                                                <?php
                                            }
                                        ?>	
                                    </select>
							</div>
                            <div class="col-lg-6">
								<div class="form-group">
                                     <label>Lead Stage</label>
                                    <select class="form-control" name="inquiry_stage">
                                        <?php
                                            while($industry_stages=mysql_fetch_object($industry_stage))
                                            {
                                                ?>
                                                <option value="<?php echo $industry_stages->industry_stage_id; ?>"><?php echo $industry_stages->industry_stage; ?></option>
                                                <?php
                                            }
                                        ?>	
                                    </select>
								</div>
							</div>
						</div>
                        <div class="row">
							<div class="col-lg-6">
								<div class="form-group">
									<label>Lead Provider</label>
                                    <select class="form-control" name="leadprovide">
                                        <?php
										
                                            while($lead_provider=mysql_fetch_object($lead_providers))
                                            {
                                                ?>
                                                <option value="<?php echo $lead_provider->lead_provider_id; ?>"><?php echo $lead_provider->lead_provider; ?></option>
                                                <?php
                                            }
                                        ?>	
                                    </select>
								</div>
							</div>
                            <div class="col-lg-6">
								<div class="form-group">
                                    <label>Status</label>
                                    <select class="form-control" name="inquiry_status">
                                        <?php
                                            while($industry_statuss=mysql_fetch_object($industry_status))
                                            {
                                                ?>
                                                <option value="<?php echo $industry_statuss->industry_status_id; ?>"><?php echo $industry_statuss->industry_status; ?></option>
                                                <?php
                                            }
                                        ?>	
                                    </select>
								</div>
							</div>
						</div>
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label>Review Date</label>
                                    <input type="text" name="review_date"  class="form-control datepickers">
                                </div>
                            </div>
                            
                        </div>
							
                        <div class="row">
							<div class="col-lg-12">
								<div class="form-group">
									<label>Lead Detail</label>
									<textarea name="lead_detail"></textarea>
								</div>
							</div>
						</div>
						<div class="row" style="padding-top:30px">
							<div class="col-lg-12">
								<input class="btn btn-success" type="submit" name="addopportunity" value="Add Opportunity Lead" />
							</div>
						</div>						
					</form>
				</div>
			</div>
		</div>
	</div>
    </div>    
</div>
<?php
include_once('../footer.php');
?>