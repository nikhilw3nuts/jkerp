<?php
include_once('../header.php');

include_once('../dbFunction.php');
$funObj = new dbFunction();
if($_POST['addstate'])
{
	$country = $_POST['country'];
	$state = $_POST['state'];
	$checkestate = $funObj->checkestate($state);
	if($checkestate)
	{
		echo "<script>alert('State aleady exist!')</script>";
	}
	else
	{
		$state = $funObj->addstate($country,$state);
	}
}
$states = $funObj->getTableData('state');
$countrys = $funObj->getTableData('country','ORDER BY country ASC');
?>
<div id="page-wrapper">
    <div class="container">
	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">State</h1>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-7">
			<div class="panel panel-default">
				<div class="panel-heading clearfix">
					State Detail
				</div>				
				<div class="panel-body">
					<div class="dataTable_wrapper">
						<table class="table table-striped table-bordered table-hover" id="dataTables-example">
							<thead>
								<tr>
									<th>Action</th>
									<th>State</th>
									<th>Country</th>
								</tr>
							</thead>
							<tbody>
								<?php 
									while($state=mysql_fetch_object($states))
									{
										 $countryid = mysql_fetch_array($funObj->getDataById('country',"countryid='".$state->countryid."'"));
										?>
											<tr class="odd gradeX">
												<td><a onclick="return confirm('Are you sure?');" href="<?php echo SITE_URL.'pages/delete.php?redirect=state&table=state&field=stateid&id='.$state->stateid; ?>">Delete</a></td>
												<td><?php echo $state->state; ?></td>
												<td><?php echo $countryid['country']; ?></td>
											</tr>	
										<?php
									} 
								?>								
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
		<div class="col-lg-5">
			<div class="panel panel-default">
				<div class="panel-heading clearfix">
					Add State
				</div>
				<div style="margin:20px">	
				<form role="form" name="addstate" method="POST">
					<div class="form-group">
						<label>Select Country</label>
						<select class="form-control" name="country">
							<?php
								while($country=mysql_fetch_object($countrys))
								{
									?>
									<option value="<?php echo $country->countryid; ?>"><?php echo $country->country; ?></option>
									<?php
								}
							?>	
						</select>
					</div>
					<div class="form-group">
						<label>State</label>
						<input type="text" name="state" required class="form-control">
					</div>
					<input class="btn btn-success btn-block" type="submit" name="addstate" value="Add State" />
				</form>
				</div>
			</div>
		</div>
	</div>
    </div>    
</div>
<?php
include_once('../footer.php');
?>
<script>
jQuery(document).ready(function() {
	jQuery('#dataTables-example').DataTable({
			responsive: true
	});
});
</script>
