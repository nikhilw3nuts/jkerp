<?php
include_once('../header.php');

include_once('../dbFunction.php');
$funObj = new dbFunction();
if($_POST['addClass'])
{
	$class = $_POST['class'];
	$isExist = $funObj->checkeAllreadyExist("item_class","class",$class);
	if($isExist)
	{
		echo "<script>alert('Class aleady exist!')</script>";
	}
	else
	{
		$addDepartment = $funObj->addClass($class);
	}
}
$item_class = $funObj->getTableData('item_class');
?>
<div id="page-wrapper">
    <div class="container">
	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">Class</h1>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-7">
			<div class="panel panel-default">
				<div class="panel-heading clearfix">
					Class Detail
				</div>				
				<div class="panel-body">
					<div class="dataTable_wrapper">
						<table class="table table-striped table-bordered table-hover" id="dataTables-example">
							<thead>
								<tr>
									<th>Action</th>
									<th>Class</th>
								</tr>
							</thead>
							<tbody>
								<?php 
									while($item_classs=mysql_fetch_object($item_class))
									{
										?>
											<tr class="odd gradeX">
												<td><a onclick="return confirm('Are you sure?');" href="<?php echo SITE_URL.'pages/delete.php?redirect=item_class&table=item_class&field=class_id&id='.$item_classs->class_id; ?>">Delete</a></td>
												<td><?php echo $item_classs->class; ?></td>
											</tr>	
										<?php
									} 
								?>								
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
		<div class="col-lg-5">
			<div class="panel panel-default">
				<div class="panel-heading clearfix">
					Add Class
				</div>
				<div style="margin:20px">	
				<form role="form" name="addDepartment" method="POST">
					<div class="form-group">
						<label>Class</label>
						<input type="text" name="class" required class="form-control">
					</div>
					<input class="btn btn-success btn-block" type="submit" name="addClass" value="Add Class" />
				</form>
				</div>
			</div>
		</div>
	</div>
    </div>    
</div>
<?php
include_once('../footer.php');
?>
<script>
jQuery(document).ready(function() {
	jQuery('#dataTables-example').DataTable({
			responsive: true
	});
});
</script>
