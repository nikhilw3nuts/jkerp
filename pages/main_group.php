<?php
include_once('../header.php');

include_once('../dbFunction.php');
$funObj = new dbFunction();
if($_POST['addItemgroup'])
{
	$item_group_code = $_POST['item_group_code'];
	$item_group = $_POST['item_group'];
	$item_group_desc = $_POST['item_group_desc'];
	$isExist = $funObj->checkeAllreadyExist("item_main_group","main_group_value",$item_group);
	if($isExist)
	{
		echo "<script>alert('Item group aleady exist!')</script>";
	}
	else
	{
		$city = $funObj->addItemgroup($item_group,$item_group_code,$item_group_desc);
	}
}
$item_groups = $funObj->getTableData('item_main_group');
?>
<div id="page-wrapper">
    <div class="container">
	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">Item Group category </h1>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-7">
			<div class="panel panel-default">
				<div class="panel-heading clearfix">
					Item Group
				</div>				
				<div class="panel-body">
					<div class="dataTable_wrapper">
						<table class="table table-striped table-bordered table-hover" id="dataTables-example">
							<thead>
								<tr>
									<th>Action</th>
									<th>Item Group</th>
									<th>Item Group Code</th>
									<th>Item Group Description</th>
								</tr>
							</thead>
							<tbody>
								<?php 
									while($item_group=mysql_fetch_object($item_groups))
									{
										?>
											<tr class="odd gradeX">
												<td><a onclick="return confirm('Are you sure?');" href="<?php echo SITE_URL.'pages/delete.php?redirect=main_group&table=item_main_group&field=main_group_id&id='.$item_group->main_group_id; ?>">Delete</a></td>
												<td><?php echo $item_group->main_group_value; ?></td>
												<td><?php echo $item_group->group_code; ?></td>
												<td><?php echo $item_group->group_description; ?></td>
											</tr>	
										<?php
									} 
								?>								
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
		<div class="col-lg-5">
			<div class="panel panel-default">
				<div class="panel-heading clearfix">
					Add Item Group
				</div>
				<div style="margin:20px">	
				<form role="form" name="addItemgroup" method="POST">
					<div class="form-group">
						<label>Item Group Code</label>
						<input type="text" name="item_group_code" required class="form-control">
					</div>
					<div class="form-group">
						<label>Item Group</label>
						<input type="text" name="item_group" required class="form-control">
					</div>
					<div class="form-group">
						<label>Item Group Description</label>
						<input type="text" name="item_group_desc" required class="form-control">
					</div>					
					<input class="btn btn-success btn-block" type="submit" name="addItemgroup" value="Add Item Group" />
				</form>
				</div>
			</div>
		</div>
	</div>
    </div>    
</div>
<?php
include_once('../footer.php');
?>
<script>
jQuery(document).ready(function() {
	jQuery('#dataTables-example').DataTable({
			responsive: true
	});
});
</script>