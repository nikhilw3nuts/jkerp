<?php
include_once('../header.php');

include_once('../dbFunction.php');
$funObj = new dbFunction();
if($_POST['addSales'])
{
	$sales = $_POST['sales'];
	$isExist = $funObj->checkeAllreadyExist("sales","sales",$sales);
	if($isExist)
	{
		echo "<script>alert('Sales aleady exist!')</script>";
	}
	else
	{
		$sales = $funObj->addSales($sales);
	}
}
$sales = $funObj->getTableData('sales');
?>
<div id="page-wrapper">
    <div class="container">
	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">Sales</h1>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-7">
			<div class="panel panel-default">
				<div class="panel-heading clearfix">
					Sales Detail
				</div>				
				<div class="panel-body">
					<div class="dataTable_wrapper">
						<table class="table table-striped table-bordered table-hover" id="dataTables-example">
							<thead>
								<tr>
									<th>Action</th>
									<th>Sales</th>
								</tr>
							</thead>
							<tbody>
								<?php 
									while($sale=mysql_fetch_object($sales))
									{
										?>
											<tr class="odd gradeX">
												<td><a onclick="return confirm('Are you sure?');" href="<?php echo SITE_URL.'pages/delete.php?redirect=sales&table=sales&field=sales_id&id='.$sale->sales_id; ?>">Delete</a></td>
												<td><?php echo $sale->sales; ?></td>
											</tr>	
										<?php
									} 
								?>								
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
		<div class="col-lg-5">
			<div class="panel panel-default">
				<div class="panel-heading clearfix">
					Add Sales
				</div>
				<div style="margin:20px">	
				<form role="form" name="addSales" method="POST">
					<div class="form-group">
						<label>Sales</label>
						<input type="text" name="sales" required class="form-control">
					</div>
					<input class="btn btn-success btn-block" type="submit" name="addSales" value="Add Sales" />
				</form>
				</div>
			</div>
		</div>
	</div>
    </div>    
</div>
<?php
include_once('../footer.php');
?>
<script>
jQuery(document).ready(function() {
	jQuery(".state-all").hide();
	jQuery(".state-1").show();
	jQuery(".countrycng").change(function(){
		var curt = jQuery(this).val();
		 jQuery(".state-all").hide();
		 jQuery(".state-"+curt).show();
	})
	jQuery('#dataTables-example').DataTable({
			responsive: true
	});
	
});
</script>
