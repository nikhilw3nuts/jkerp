<?php
include_once('../header.php');

include_once('../dbFunction.php');
$funObj = new dbFunction();
$quotations = $funObj->getTableData('quotation_master');

?>
<div id="page-wrapper">
    <div class="container">
	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">Quotation</h1>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-default">
				<div class="panel-heading clearfix">
					Quotation Detail
					<a class="btn btn-primary" style="float:right" href="<?php echo SITE_URL ?>pages/addquotation.php">Add Quotation</a>
				</div>				
				<div class="panel-body">
					<div class="dataTable_wrapper">
						<div class="filter-main">
                        	<h4>Filter Record</h4>
                            <div class="filter-col">
	                            <input type="checkbox" value="all" name="filterallqty" checked> View all qtys
                            </div>
                            <div class="filter-col">
                            <span>Status : </span>
                                <select name="status">
                                    <option value="1">Hot</option>
                                    <option value="2">Cold</option>
                                </select>
                            </div>
                            <div class="filter-col">
                                <span>Stage : </span>
                                <select name="status">
                                    <option value="1">Panding</option>
                                    <option value="2">In Pogress</option>
                                    <option value="3">On Hold</option>
                                    <option value="4">Deleverd</option>
                                </select>
                            </div>
                        </div>
                        <table class="table table-striped table-bordered table-hover" id="dataTables-example">
							<thead>
								<tr>
									<th>Action</th>
									<th>Quotation id</th>
									<th>Quotation date</th>
									<th>Inquiry id</th>
									<th>Customer</th>
								</tr>
							</thead>
							<tbody>
								<?php 
									while($quotation=mysql_fetch_object($quotations))
									{
										$party_master = mysql_fetch_array($funObj->getDataById("party_master","party_code='".$quotation->customer_code."'"));
										?>
										<tr class="odd gradeX">
											<td><a onclick="return confirm('Are you sure?');" href="<?php echo SITE_URL.'pages/delete.php?redirect=quotation&table=quotation_master&field=quotation_id&id='.$quotation->quotation_id; ?>">Delete</a> |  <a href="<?php echo SITE_URL.'pages/quotaionDetail.php?id='.$quotation->quotation_id; ?>">Edit</a></td>
											<td><?php echo $quotation->quotation_id; ?></td>
											<td><?php echo $quotation->quotation_date; ?></td>
											<td><?php echo $quotation->inquiry_id ?></td>
											<td><?php echo $party_master['party_name']; ?></td>
										</tr>
										<?php
									} 
								?>									
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
    </div>    
</div>
<?php
include_once('../footer.php');
?>
<script>
jQuery(document).ready(function() {
	jQuery('#dataTables-example').DataTable({
			responsive: true
	});
});
</script>
