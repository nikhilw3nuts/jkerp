<?php
$pages = "country.php";
include_once('../header.php');
include_once('../dbFunction.php');
$funObj = new dbFunction();

if($_POST['addcountry'])
{
	$country = $_POST['country'];
	$checkcountry = $funObj->checkecountry($country);
	if($checkcountry)
	{
		echo "<script>alert('Country aleady exist!')</script>";
	}
	else
	{
		//$adduer = $funObj->addCountry($country);
		unset($_POST['addcountry']);
		$_POST['user_id'] = $_SESSION['uid'];
		$lstinsid = $funObj->addfunction('country',$_POST);
		//create array for log
		$array_field_name['country']=$_REQUEST['country'];
		$array_field_name['date']=date('d-m-y h:i:s a');
		$array_field_name['countryid']=$lstinsid;
		$array_field_name['user_id']=$_SESSION['uid'];
		$action="1"; // $action=  add=1 edit=2 delete=3
		 
		$table_neme='z_country';
		maintain_log($array_field_name,$table_neme,$action); //call maintain_log function
	}
}
$country = $funObj->getTableData('country');
?>
<div id="page-wrapper">
    <div class="container">
	<div class="row">
		<div class="col-lg-12">
        	<div class="panel-heading clearfix">
				<h1 class="page-header">Country</h1>
            	<a class="btn btn-primary" target="_blank" href="<?php echo SITE_URL ?>log/master_log.php" style="float:right">Country Log</a>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-7">
			<div class="panel panel-default">
				<div class="panel-heading clearfix">
					Country Detail
				</div>				
				<div class="panel-body">
					<div class="dataTable_wrapper">
						<table class="table table-striped table-bordered table-hover" id="dataTables-example">
							<thead>
								<tr>
									<th>Action</th>
									<th>Country</th>
								</tr>
							</thead>
							<tbody>
								<?php 
									while($countrys=mysql_fetch_object($country))
									{
										?>
											<tr class="odd gradeX">
												<td><a href="<?php echo SITE_URL.'pages/edit_country.php?id='.$countrys->countryid; ?>">Edit</a> / <a onclick="return confirm('Are you sure?');" href="<?php echo SITE_URL.'pages/delete.php?redirect=country&table=country&field=countryid&id='.$countrys->countryid; ?>">Delete</a></td>
												<td><?php echo $countrys->country; ?></td>
												<td><a class="log_content" data-id="<?php echo $countrys->countryid; ?>" data-table="z_country" data-field-name="countryid" href="javascript:void(0)">Log</a></td>
											</tr>	
										<?php
									} 
								?>								
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
		<div class="col-lg-5">
			<div class="panel panel-default">
				<div class="panel-heading clearfix">
					Add Country
				</div>
				<div style="margin:20px">	
				<form role="form" name="addcountrcountryy" method="POST">
					<div class="form-group">
						<label>Country</label>
						<input type="text" name="country" required class="form-control">
					</div>
					<input class="btn btn-success btn-block" type="submit" name="addcountry" value="Add Country" />
				</form>
				</div>
			</div>
		</div>
	</div>
    </div>
</div>
<?php
include_once('../footer.php');
?>

