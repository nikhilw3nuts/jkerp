<?php
include_once('../header.php');

include_once('../dbFunction.php');
$funObj = new dbFunction();
if($_POST['addworkdetail'])
{
	$work_date = $_POST['work_date'];
	$work_detail = $_POST['work_detail'];
	$funObj->addDailyWork($work_date,$work_detail);
}
$dailywork = $funObj->getDataById("daily_work_entry","user_id='".$_SESSION['uid']."'");
?>
<div id="page-wrapper">
    <div class="container">
	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">Work Detail</h1>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-7">
			<div class="panel panel-default">
				<div class="panel-heading clearfix">
					Work Detail
				</div>				
				<div class="panel-body">
					<div class="dataTable_wrapper">
						<table class="table table-striped table-bordered table-hover" id="dataTables-example">
							<thead>
								<tr>
									<th>Action</th>
									<th>Date</th>
									<th>Work Detail</th>
								</tr>
							</thead>
							<tbody>
								<?php 
									while($dailyworks=mysql_fetch_object($dailywork))
									{
										?>
											<tr class="odd gradeX">
												<td><a onclick="return confirm('Are you sure?');" href="<?php echo SITE_URL.'pages/delete.php?redirect=dailyworkentry&table=daily_work_entry&field=daily_work_entry_id &id='.$dailyworks->daily_work_entry_id ; ?>">Delete</a></td>
												<td><?php echo $dailyworks->date ?></td>
												<td><?php echo $dailyworks->work_detail; ?></td>
											</tr>	
										<?php
									} 
								?>								
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
		<div class="col-lg-5">
			<div class="panel panel-default">
				<div class="panel-heading clearfix">
					Add Work Detail
				</div>
				<div style="margin:20px">	
				<form role="form" name="addcurrency" method="POST">
					<div class="form-group">
						<label>User : </label>
						<input type="text" name="work_user" required class="form-control"  value="<?php echo $_SESSION['username'] ?>">
					</div>
					<div class="form-group">
						<label>Date</label>
						<input type="text" name="work_date" required class="form-control datepickers" placeholder="dd-mm-yyyy" value="<?php echo date("d-m-Y") ?>">
					</div>
					<div class="form-group">
						<label>Work Detail</label>
						<textarea name="work_detail" required class="form-control"></textarea>
					</div>
					<input class="btn btn-success btn-block" type="submit" name="addworkdetail" value="Add Work Detail" />
				</form>
				</div>
			</div>
		</div>
	</div>
    </div>    
</div>
<?php
include_once('../footer.php');
?>
<script>
jQuery(document).ready(function() {
	
	$( ".datepickers" ).datepicker({ dateFormat: 'dd-mm-yy' });
		
	jQuery('#dataTables-example').DataTable({
			responsive: true
	});
	
});
</script>
