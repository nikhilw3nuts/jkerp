<?php
include_once('../header.php');

include_once('../dbFunction.php');
$funObj = new dbFunction();
if($_POST['addmakeitem'])
{
	$make_item = $_POST['subitem_group'];
	$isExist = $funObj->checkeAllreadyExist("item_make","item_make_value",$make_item);
	if($isExist)
	{
		echo "<script>alert(' make Item  aleady exist!')</script>";
	}
	else
	{
		$city = $funObj->addmakeitem($make_item);
	}
}
$make_items = $funObj->getTableData('item_make');
?>
<div id="page-wrapper">
    <div class="container">
	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">Item Make </h1>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-7">
			<div class="panel panel-default">
				<div class="panel-heading clearfix">
				  Item Make category 
				</div>				
				<div class="panel-body">
					<div class="dataTable_wrapper">
						<table class="table table-striped table-bordered table-hover" id="dataTables-example">
							<thead>
								<tr>
									<th>Action</th>
									<th>Make Items</th>
								</tr>
							</thead>
							<tbody>
								<?php 
									while($make_item=mysql_fetch_object($make_items))
									{
										?>
											<tr class="odd gradeX">
												<td><a onclick="return confirm('Are you sure?');" href="<?php echo SITE_URL.'pages/delete.php?redirect=item_make&table=	item_make&field=item_make_id&id='.$make_item->item_make_id; ?>">Delete</a></td>
												<td><?php echo $make_item->item_make_value; ?></td>
											</tr>	
										<?php
									} 
								?>								
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
		<div class="col-lg-5">
			<div class="panel panel-default">
				<div class="panel-heading clearfix">
					Add MakeItem
				</div>
				<div style="margin:20px">	
				<form role="form" name="addmakeitem" method="POST">
					<div class="form-group">
						<label>MAke Item</label>
						<input type="text" name="subitem_group" required class="form-control">
					</div>
					<input class="btn btn-success btn-block" type="submit" name="addmakeitem" value="Add make item" />
				</form>
				</div>
			</div>
		</div>
	</div>
    </div>    
</div>
<?php
include_once('../footer.php');
?>
