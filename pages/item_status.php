<?php
include_once('../header.php');

include_once('../dbFunction.php');
$funObj = new dbFunction();
if($_POST['addItemStatus'])
{
	$item_status = $_POST['status'];
	$isExist = $funObj->checkeAllreadyExist("item_stauts","item_status",$item_status);
	if($isExist)
	{
		echo "<script>alert('Status aleady exist!')</script>";
	}
	else
	{
		$item_status= $funObj->addItemStatus($item_status);
	}
}
$item_statuss= $funObj->getTableData('item_stauts');
?>
<div id="page-wrapper">
    <div class="container">
	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">Item Status </h1>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-7">
			<div class="panel panel-default">
				<div class="panel-heading clearfix">
					Item Status
				</div>				
				<div class="panel-body">
					<div class="dataTable_wrapper">
						<table class="table table-striped table-bordered table-hover" id="dataTables-example">
							<thead>
								<tr>
									<th>Action</th>
									<th>Item Status</th>
								</tr>
							</thead>
							<tbody>
								<?php 
									while($item_status=mysql_fetch_object($item_statuss))
									{
										?>
											<tr class="odd gradeX">
												<td><a onclick="return confirm('Are you sure?');" href="<?php echo SITE_URL.'pages/delete.php?redirect=item_status&table=item_stauts&field=itemstatus_id&id='.$item_status->itemstatus_id; ?>">Delete</a></td>
												<td><?php echo $item_status->item_status; ?></td>
											</tr>	
										<?php
									} 
								?>								
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
		<div class="col-lg-5">
			<div class="panel panel-default">
				<div class="panel-heading clearfix">
					Add Item Status 
				</div>
				<div style="margin:20px">	
				<form role="form" name="addItemStatus" method="POST">
					<div class="form-group">
						<label>Item Status</label>
						<input type="text" name="status" required class="form-control">
					</div>
					<input class="btn btn-success btn-block" type="submit" name="addItemStatus" value="Add Item Type" />
				</form>
				</div>
			</div>
		</div>
	</div>
    </div>    
</div>
<?php
include_once('../footer.php');
?>
