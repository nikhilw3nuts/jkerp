<?php
include_once('../header.php');

include_once('../dbFunction.php');
$funObj = new dbFunction();
if($_POST['addExcise'])
{
	$ecdesc = $_POST['ecdesc'];
	$subheading = $_POST['subheading'];
	$addexcise = $funObj->addExcise($subheading,$ecdesc);
}
$excise_temp = $funObj->getTableData('excise_temp');
?>
<div id="page-wrapper">
    <div class="container">
	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">Class</h1>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-7">
			<div class="panel panel-default">
				<div class="panel-heading clearfix">
					Excise Temp
				</div>				
				<div class="panel-body">
					<div class="dataTable_wrapper">
						<table class="table table-striped table-bordered table-hover" id="dataTables-example">
							<thead>
								<tr>
									<th>Action</th>
									<th>Excise code</th>
									<th>Excise Description</th>
									<th>Sub Heading</th>
								</tr>
							</thead>
							<tbody>
								<?php 
									while($excise_temps=mysql_fetch_object($excise_temp))
									{
										?>
											<tr class="odd gradeX">
												<td><a onclick="return confirm('Are you sure?');" href="<?php echo SITE_URL.'pages/delete.php?redirect=excise&table=excise_temp&field=excise_code&id='.$excise_temps->excise_code; ?>">Delete</a></td>
												<td><?php echo $excise_temps->excise_code; ?></td>
												<td><?php echo $excise_temps->ecdesc; ?></td>
												<td><?php echo $excise_temps->subheading; ?></td>
											</tr>	
										<?php
									} 
								?>								
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
		<div class="col-lg-5">
			<div class="panel panel-default">
				<div class="panel-heading clearfix">
					Add Excise Temp
				</div>
				<div style="margin:20px">	
				<form role="form" name="addExcise" method="POST">
					<div class="form-group">
						<label>Excise Description</label>
						<input type="text" name="ecdesc" required class="form-control">
					</div>
					<div class="form-group">
						<label>Sub Heading</label>
						<input type="text" name="subheading" required class="form-control">
					</div>
					<input class="btn btn-success btn-block" type="submit" name="addExcise" value="Add Class" />
				</form>
				</div>
			</div>
		</div>
	</div>
    </div>    
</div>
<?php
include_once('../footer.php');
?>
<script>
jQuery(document).ready(function() {
	jQuery('#dataTables-example').DataTable({
			responsive: true
	});
});
</script>
