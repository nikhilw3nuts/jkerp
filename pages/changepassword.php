<?php
include_once('../header.php');
include_once('../dbFunction.php');
$funObj = new dbFunction();
if($_POST['changepass']){
	$oldpass = $_POST['old-pass'];
	$newpass = $_POST['new-pass'];
	$repass = $_POST['re-pass'];
	if($newpass != $repass)
	{
		echo "<script>alert('Password Not match')</script>"; 
	}
	else
	{
		$chkpass = $funObj->checkpassword($_SESSION['email'],$oldpass);
		if($chkpass)
		{
			$changePass = $funObj->changepass($_SESSION['email'],$newpass);
			echo "<script>alert('Password Change successfully')</script>"; 
		}
		else
		{
			echo "<script>alert('Worng old password')</script>"; 
		}
	}
}
?>
<div id="page-wrapper">
    <div class="container">
	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">Change Password</h1>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-default">
				<div class="panel-body">
					<div class="col-lg-3">
					</div>
					<div class="col-lg-6">
						<form role="form" name="changepass" method="POST">
							<div class="form-group">
								<label>Old 	Password</label>
								<input type="password" name="old-pass" required class="form-control">
							</div>
							<div class="form-group">
								<label>New Password</label>
								<input type="password" name="new-pass" required class="form-control">
							</div>
							<div class="form-group">
								<label>Confirm Password</label>
								<input type="password" name="re-pass" required class="form-control">
							</div>
							<input class="btn btn-success btn-block" type="submit" name="changepass" value="Change Password" />
						</form>	
					</div>
					<div class="col-lg-3">
					</div>
				</div>
			</div>
		</div>
	</div>
    </div>    
</div>
<?php
include_once('../footer.php');
?>
<script>
jQuery(document).ready(function() {
	jQuery('#dataTables-example').DataTable({
			responsive: true
	});
});
</script>
	
