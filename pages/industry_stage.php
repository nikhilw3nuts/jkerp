<?php
include_once('../header.php');

include_once('../dbFunction.php');
$funObj = new dbFunction();
if($_POST['addIndustryStage'])
{
	$industry_stage = $_POST['industry_stage'];
	$isExist = $funObj->checkeAllreadyExist("industry_stage","industry_stage",$industry_stage);
	if($isExist)
	{
		echo "<script>alert('Industry Stage aleady exist!')</script>";
	}
	else
	{
		$city = $funObj->addIndustryStage($industry_stage);
	}
}
$industry_stages = $funObj->getTableData('industry_stage');
?>
<div id="page-wrapper">
    <div class="container">
	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">Industry Stage </h1>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-7">
			<div class="panel panel-default">
				<div class="panel-heading clearfix">
					Industry Stages
				</div>				
				<div class="panel-body">
					<div class="dataTable_wrapper">
						<table class="table table-striped table-bordered table-hover" id="dataTables-example">
							<thead>
								<tr>
									<th>Action</th>
									<th>Industry Stage</th>
								</tr>
							</thead>
							<tbody>
								<?php 
									while($industry_stage=mysql_fetch_object($industry_stages))
									{
										?>
											<tr class="odd gradeX">
												<td><?php echo $industry_stage->industry_stage; ?></td>
												<td><a onclick="return confirm('Are you sure?');" href="<?php echo SITE_URL.'pages/delete.php?redirect=industry_stage&table=industry_stage&field=industry_stage_id&id='.$industry_stage->industry_stage_id; ?>">Delete</a></td>
											</tr>	
										<?php
									} 
								?>								
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
		<div class="col-lg-5">
			<div class="panel panel-default">
				<div class="panel-heading clearfix">
					Add Industry Stage 
				</div>
				<div style="margin:20px">	
				<form role="form" name="addIndustryStage" method="POST">
					<div class="form-group">
						<label>Industry Stage</label>
						<input type="text" name="industry_stage" required class="form-control">
					</div>
					<input class="btn btn-success btn-block" type="submit" name="addIndustryStage" value="Add Tndustry Type" />
				</form>
				</div>
			</div>
		</div>
	</div>
    </div>    
</div>
<?php
include_once('../footer.php');
?>
