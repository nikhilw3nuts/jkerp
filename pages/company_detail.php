<?php
include_once('../header.php');
session_start();

include_once('../dbFunction.php');
$funObj = new dbFunction();
$employee_leave = $funObj->getTableData('user');
$tablename='company_detail';
$get_data = $funObj->getTableData($tablename);
$total_row=mysql_num_rows($get_data);
	
$filed_value['company_name']=$_POST['company_name'];
$filed_value['address']=$_POST['address'];
$filed_value['contact_no']=$_POST['contact_no'];
$filed_value['pan_no']=$_POST['pan_no'];
$filed_value['tan_no']=$_POST['tan_no'];
$filed_value['pf_no']=$_POST['pf_no'];
$filed_value['esic_no']=$_POST['esic_no'];
$filed_value['pt_no']=$_POST['pt_no'];
$filed_value['service_tax_no']=$_POST['service_tax_no'];
$filed_value['vat_no']=$_POST['vat_no'];
$filed_value['cst_no']=$_POST['cst_no'];
if(isset($_POST['submit']))
{
	if($total_row!=1){
	
	$funObj->addfunction($tablename,$filed_value);
	}else{
		
		$funObj->updatefunction($tablename,$filed_value,'id',1);
	}
}
$get_data = $funObj->getTableData($tablename);
$val=mysql_fetch_array($get_data);
 
?>
<div id="page-wrapper">
    <div class="container">
	<div class="row"> 
		<div class="col-lg-12">
			<h1 class="page-header">Company's Information</h1>
		</div>
		
	</div>
	<div class="row">
	
		<div class="col-lg-12">
			<div class="panel panel-default">
				<div class="panel-heading clearfix">
					Add Data
				</div>				
				<div class="panel-body">
					<div class="dataTable_wrapper">
					<form method="POST">
						<div class="form-group">
							<label>Company Name</label>
							<input type="text" class="form-control" value="<?php echo $val['company_name']; ?>" name="company_name">
						</div>
						<div class="form-group">
							<label>Company Name</label>
							<textarea class="form-control" name="address"><?php echo $val['address']; ?></textarea>
						</div>
						<div class="form-group">
							<label>Contact No.</label>
							<input type="text" class="form-control" value="<?php echo $val['contact_no']; ?>" name="contact_no">
						</div>
						<div class="form-group">
							<label>Pan. No.</label>
							<input type="text" class="form-control" value="<?php echo $val['pan_no']; ?>" name="pan_no">
						</div>
						<div class="form-group">
							<label>TAN. No.</label>
							<input type="text" class="form-control" value="<?php echo $val['tan_no']; ?>" name="tan_no">
						</div>
						<div class="form-group">
							<label>PF No.</label>
							<input type="text" class="form-control" value="<?php echo $val['pf_no']; ?>" name="pf_no">
						</div>
						<div class="form-group">
							<label>ESIC No.</label>
							<input type="text" class="form-control" value="<?php echo $val['esic_no']; ?>" name="esic_no">
						</div>
						<div class="form-group">
							<label>PT No.</label>
							<input type="text" class="form-control" value="<?php echo $val['pt_no']; ?>" name="pt_no">
						</div>
						<div class="form-group">
							<label>Service Tax No.</label>
							<input type="text" class="form-control" value="<?php echo $val['service_tax_no']; ?>" name="service_tax_no">
						</div>
						<div class="form-group">
							<label>Vat No.</label>
							<input type="text" class="form-control" value="<?php echo $val['vat_no']; ?>" name="vat_no">
						</div>
						<div class="form-group">
							<label>CST No.</label>
							<input type="text" class="form-control" value="<?php echo $val['cst_no']; ?>" name="cst_no">
						</div>
						<div class="form-group">
						<input type="submit" class="btn btn-success" name="submit" value="Submit">
						</div>
						
					</form>
					</div>
				</div>
			</div>
		</div>
	 
	</div>
    </div>    
</div>
<?php
include_once('../footer.php');
?>