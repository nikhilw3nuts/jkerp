<?php
include_once('../header.php');
include_once('../dbFunction.php');
$funObj = new dbFunction();
if($_POST['addUOM'])
{
	$uom = $_POST['uom'];
	$checkeUom = $funObj->checkeUom($uom);
	if($checkeUom)
	{
		echo "<script>alert('UOM aleady exist!')</script>";
	}
	else
	{
		$addParty = $funObj->addUom($uom);
	}
}
$uom = $funObj->getTableData('uom');
?>
<div id="page-wrapper">
    <div class="container">
	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">UOM</h1>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-7">
			<div class="panel panel-default">
				<div class="panel-heading clearfix">
					UOM Detail
				</div>				
				<div class="panel-body">
					<div class="dataTable_wrapper">
						<table class="table table-striped table-bordered table-hover" id="dataTables-example">
							<thead>
								<tr>
									<th>Action</th>
									<th>UOM</th>
								</tr>
							</thead>
							<tbody>
								<?php 
									while($uoms=mysql_fetch_object($uom))
									{
										?>
											<tr class="odd gradeX">
												<td><a onclick="return confirm('Are you sure?');" href="<?php echo SITE_URL.'pages/delete.php?redirect=uom&table=uom&field=uom_id&id='.$uoms->uom_id; ?>">Delete</a></td>
												<td><?php echo $uoms->uom; ?></td>
											</tr>	
										<?php
									} 
								?>								
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
		<div class="col-lg-5">
			<div class="panel panel-default">
				<div class="panel-heading clearfix">
					UOM
				</div>
				<div style="margin:20px">	
				<form role="form" name="addUOM" method="POST">
					<div class="form-group">
						<label>UOM</label>
						<input type="text" name="uom" required class="form-control">
					</div>
					<input class="btn btn-success btn-block" type="submit" name="addUOM" value="Add UOM" />
				</form>
				</div>
			</div>
		</div>
	</div>
    </div>    
</div>
<?php
include_once('../footer.php');
?>
<script>
jQuery(document).ready(function() {
	jQuery('#dataTables-example').DataTable({
			responsive: true
	});
});
</script>
