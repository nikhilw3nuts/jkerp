<?php
include_once('../header.php');

include_once('../dbFunction.php');
$funObj = new dbFunction();
if($_POST['tcgroup'])
{
	$check1 = $funObj->checkeAllreadyExist('terms_details','terms_group_code',$_POST['code']);
	if($check || $check1)
	{
		echo "<script>alert('Terms & condition detail aleady exist!')</script>";
	}
	else
	{
		
		$fieldvalue['terms_group_code'] = $_POST['code'];
		$fieldvalue['terms_group']  = $_POST['d_code'];
		$fieldvalue['terms_details_code'] = $_POST['d_desc'];
		$fieldvalue['terms_details']  = $_POST['description'];
		$funObj->addfunction('terms_details',$fieldvalue);
	}
}
$terms_details = $funObj->getTableData('terms_details');
$term_group = $funObj->getTableData('term_group');
?>
<div id="page-wrapper">
    <div class="container">
	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">Terms & Condition Detail</h1>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-7">
			<div class="panel panel-default">
				<div class="panel-heading clearfix">
					Terms & Condition Detail
				</div>				
				<div class="panel-body">
					<div class="dataTable_wrapper">
						<table class="table table-striped table-bordered table-hover" id="dataTables-example">
							<thead>
								<tr>
									<th>Action</th>
									<th>Code</th>
                                    <th>Group</th>
                                    <th>Detail</th>
								</tr>
							</thead>
							<tbody>
								<?php 
									while($terms_detail=mysql_fetch_object($terms_details))
									{
										?>
											<tr class="odd gradeX">
												<td><a onclick="return confirm('Are you sure?');" href="<?php echo SITE_URL.'pages/delete.php?redirect=terms_condition_detail&table=terms_details&field=terms_details_id&id='.$terms_detail->terms_details_id; ?>">Delete</a></td>
												<td><?php echo $terms_detail->terms_group_code; ?></td>
                                                <td><?php echo $terms_detail->terms_details_code; ?></td>
												<td><?php echo $terms_detail->terms_details; ?></td>
											</tr>	
										<?php
									} 
								?>								
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
		<div class="col-lg-5">
			<div class="panel panel-default">
				<div class="panel-heading clearfix">
					Add Terms & Condition Detail
				</div>
				<div style="margin:20px">	
				<form role="form" name="tcgroup" method="POST">
					<div class="form-group">
						<label>Code</label>
						<input type="text" name="code" required class="form-control">
					</div>
                    <div class="form-group">
						<label>Group Code/Name</label>
						<input type="text" name="d_code" required readonly class="form-control termscode" style="min-width:0px; width:13%; !important;">
                        <input type="text" name="d_desc" required readonly class="form-control termsdesc" style="min-width:0px; width:50%; !important;">
                        <a class="openpopup" style="background: #dfdfdf none repeat scroll 0 0; padding: 10px;"><i class="fa fa-binoculars"></i></a>
					</div>
                    <div class="form-group">
						<label>Description</label>
						<textarea name="description" required class="form-control"></textarea>
					</div>
					<input class="btn btn-success btn-block" type="submit" name="tcgroup" value="Add Terms & Condition Group" />
				</form>
				</div>
			</div>
		</div>
	</div>
    </div>
</div>
<div class="modal fade in" id="mytermsmodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
				<h4 class="modal-title" id="myModalLabel">Terms & Condition Group</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-lg-12">
						<div class="panel panel-default">
							<div style="margin: 20px 10px">	
								<form role="form" id="addalert" name="addalert" method="POST">
									<div class="row">
										<div class="col-lg-12">
											<table class="table table-striped" id="dataTables-example">
												<thead>
													<tr>
														<th>Code</th>
														<th>Description</th>
													</tr>
												</thead>
												<tbody>
													<?php
														while($term_groups=mysql_fetch_object($term_group))
														{
															?>
																<tr class="selectTermsGroup" data-code="<?php echo $term_groups->code; ?>" data-desc="<?php echo $term_groups->term_group; ?>">																
																	<td><?php echo $term_groups->code; ?></td>
																	<td><?php echo $term_groups->term_group; ?></td>
																</tr>	
															<?php
														}
													?>
												</tbody>
											</table>
										</div>
									</div>
									
								</form>    
							</div>
						</div>
					</div>
				</div>
				
			</div>
		</div>
	</div>
</div>
<?php
include_once('../footer.php');
?>
<script>
	jQuery("document").ready(function(e) {
        jQuery('.openpopup').click(function(){
			$('#mytermsmodal').modal("show");
		});
		
		jQuery('.selectTermsGroup').click(function(){
			jQuery('.termscode').val(jQuery(this).attr('data-code'));
			jQuery('.termsdesc').val(jQuery(this).attr('data-desc'));
			$('#mytermsmodal').modal("hide");
		})
		
		jQuery('#dataTables-example').DataTable({
			responsive: true
		});
    });
</script>