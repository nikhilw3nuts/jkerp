<?php
include_once('../header.php');

include_once('../dbFunction.php');
$funObj = new dbFunction();
if($_POST['addmaterial'])
{
	$material = $_POST['itematerial'];
	$isExist = $funObj->checkeAllreadyExist("item_material","material_value",$material);
	if($isExist)
	{
		echo "<script>alert(' Material Specification already exist!')</script>";
	}
	else
	{
		$city = $funObj->addmaterial($material);
	}
}
$materials = $funObj->getTableData('item_material');
?>
<div id="page-wrapper">
    <div class="container">
	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">Item Material Specification </h1>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-7">
			<div class="panel panel-default">
				<div class="panel-heading clearfix">
				  Item Material  Specification
				</div>				
				<div class="panel-body">
					<div class="dataTable_wrapper">
						<table class="table table-striped table-bordered table-hover" id="dataTables-example">
							<thead>
								<tr>
									<th>Action</th>
									<th>Materials</th>
								</tr>
							</thead>
							<tbody>
								<?php 
									while($material=mysql_fetch_object($materials))
									{
										?>
											<tr class="odd gradeX">
												<td><a onclick="return confirm('Are you sure?');" href="<?php echo SITE_URL.'pages/delete.php?redirect=item_material&table=item_material&field=material_id&id='.$material->material_id; ?>">Delete</a></td>
												<td><?php echo $material->material_value; ?></td>
											</tr>	
										<?php
									} 
								?>								
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
		<div class="col-lg-5">
			<div class="panel panel-default">
				<div class="panel-heading clearfix">
					Add Material
				</div>
				<div style="margin:20px">	
				<form role="form" name="addmaterial" method="POST">
					<div class="form-group">
						<label>Material</label>
						<input type="text" name="itematerial" required class="form-control">
					</div>
					<input class="btn btn-success btn-block" type="submit" name="addmaterial" value="Add material" />
				</form>
				</div>
			</div>
		</div>
	</div>
    </div>    
</div>
<?php
include_once('../footer.php');
?>
