<?php
include_once('../header.php');
include_once('../dbFunction.php');
$funObj = new dbFunction();

if($_POST['addInquiry'])
{
	$lead_owner = $_POST['lead_owner'];
	$assigned_to = $_POST['assigned_to'];
	$industry_type = $_POST['industry_type'];
	$inquiry_no = $_POST['inquiry_no'];
	$inquiry_date = $_POST['inquiry_date'];
	$customer_code1 = $_POST['customer_code1'];
	$customer_code2 = $_POST['customer_code2'];
	$reference_date = $_POST['reference_date'];
	$kind_attn = $_POST['kind_attn'];
	$inquiry_contact_no = $_POST['inquiry_contact_no'];
	$inquiry_status = $_POST['inquiry_status'];
	$email = $_POST['email'];
	$inquiry_stage = $_POST['inquiry_stage'];
	$inquiry_reference = $_POST['inquiry_reference'];
	$inquiry_priority = $_POST['inquiry_priority'];
	$leadprovide = $_POST['leadprovide'];
	$lead_date = $_POST['lead_date'];
	$lead_detail = $_POST['lead_detail'];
	$review_date = $_POST['review_date'];
	$addInquiry = $funObj->addInquiry($lead_owner,$assigned_to,$industry_type,$inquiry_no,$inquiry_date,$customer_code1,$customer_code2,$reference_date,$kind_attn,$inquiry_contact_no,$inquiry_status,$email,$inquiry_stage,$inquiry_reference,$inquiry_priority,$leadprovide,$lead_date,$lead_detail,$review_date);	
	if($addInquiry!= "")
	{
		$redirect = SITE_URL."pages/inquiryDetail.php?id=".$addInquiry."";
		?>
			<script>
                window.location.assign("<?php echo $redirect; ?>")
            </script>
		<?php
	}
}

$users = $funObj->getTableData('user');
$users1 = $funObj->getTableData('user');
$industry_types = $funObj->getTableData('industry_type');
$industry_stage = $funObj->getTableData('industry_stage');
$industry_status = $funObj->getTableData('industry_status');
$reference_inquirys = $funObj->getTableData('reference');
$priority = $funObj->getTableData('priority');
$lead_providers = $funObj->getTableData('lead_provider');	
?>
<div id="page-wrapper">
    <div class="container">
	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">Add Inquiry</h1>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-default">
				<div class="panel-body">
					<form role="form" name="addInquiry" method="POST">
						<div class="row">
							<div class="col-lg-4">
								<div class="form-group">
									<label>Lead Owner</label>
                                    <select class="form-control" name="lead_owner">
                                        <?php
                                            while($user=mysql_fetch_object($users))
                                            {
                                                ?>
                                                <option value="<?php echo $user->userid; ?>"><?php echo $user->username; ?></option>
                                                <?php
                                            }
                                        ?>	
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-4">
								<div class="form-group">
									<label>Assigned To</label>
                                    <select class="form-control" name="assigned_to">
                                        <?php
                                            while($user=mysql_fetch_object($users1))
                                            {
                                                ?>
                                                <option value="<?php echo $user->userid; ?>"><?php echo $user->username; ?></option>
                                                <?php
                                            }
                                        ?>	
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-4">
								<div class="form-group">
									<label>Industry Type</label>
                                    <select class="form-control" name="industry_type">
                                        <?php
                                            while($industry_type=mysql_fetch_object($industry_types))
                                            {
                                                ?>
                                                <option value="<?php echo $industry_type->industry_type_id; ?>"><?php echo $industry_type->industry_type; ?></option>
                                                <?php
                                            }
                                        ?>	
                                    </select>
                                </div>
                            </div>
						</div>	
                        <div class="row">
							<div class="col-lg-6">
								<div class="form-group">
									<label>Inquiry no</label>
									<input type="text" name="inquiry_no" disabled class="form-control">
								</div>
							</div>
							<div class="col-lg-6">
								<div class="form-group">
									<label>Inquiry Date</label>
									<input type="text" name="inquiry_date" required class="form-control datepickers" value="<?php echo date('d-m-Y'); ?>" placeholder="dd-mm-yyyy">
								</div>
							</div>
						</div>                        
                        <div class="row">
							<div class="col-lg-6">
								<div class="form-group">
									<div class="form-group">
										<label>Customer Code (note. Do you val customer code and customre name vice versa call?)</label>
										<div style="display:flex">
											<input type="text" name="customer_code1" readonly required class="form-control cusromerCode">
											<div class="customer_block">
												<input type="text" name="customer_code2" required class="form-control getCustomer">
												<ul class="displayCustomer search-dropdown">
													
												</ul>
											</div>
										</div>
									</div>
								</div>
							</div>
                            <div class="col-lg-6">
								<div class="form-group">
									<div class="form-group">
										<label>Reference Date</label>
										<input type="text" name="reference_date" required class="form-control datepickers" value="<?php echo date('d-m-Y'); ?>">
									</div>
								</div>
							</div>
						</div>
                        <div class="row">
							<div class="col-lg-6">
								<div class="form-group">
									<label> Kind Attn.</label>
                                    <select class="form-control kind_attn" name="kind_attn">                                                                        	
                                    </select>
								</div>
							</div>
                            <div class="col-lg-6">
								<div class="form-group">
                                    <label>Review Date</label>
                                    <input type="text" name="review_date" required class="form-control datepickers" value="<?php echo date('d-m-Y'); ?>">
								</div>
							</div>
						</div>
                        <div class="row">
							<div class="col-lg-6">
								<div class="form-group">
									<label>Contact No.</label>
                                    <input type="text" name="inquiry_contact_no" required class="form-control">
								</div>
							</div>
                            <div class="col-lg-6">
								<div class="form-group">
                                    <label>Status</label>
                                    <select class="form-control" name="inquiry_status">
                                        <?php
                                            while($industry_statuss=mysql_fetch_object($industry_status))
                                            {
                                                ?>
                                                <option value="<?php echo $industry_statuss->industry_status_id; ?>"><?php echo $industry_statuss->industry_status; ?></option>
                                                <?php
                                            }
                                        ?>	
                                    </select>
								</div>
							</div>
						</div>
                        <div class="row">
							<div class="col-lg-6">
								<div class="form-group">
									<label>Email ID</label>
                                    <input type="text" name="email" required class="form-control">
								</div>
							</div>
                            <div class="col-lg-6">
								<div class="form-group">
                                    <label>Stage</label>
                                    <select class="form-control" name="inquiry_stage">
                                        <?php
                                            while($industry_stages=mysql_fetch_object($industry_stage))
                                            {
                                                ?>
                                                <option value="<?php echo $industry_stages->industry_stage_id; ?>"><?php echo $industry_stages->industry_stage; ?></option>
                                                <?php
                                            }
                                        ?>	
                                    </select>
								</div>
							</div>
						</div>
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label>Reference</label>
                                    <select class="form-control" name="inquiry_reference">
                                        <?php
                                            while($reference_inquiry=mysql_fetch_object($reference_inquirys))
                                            {
                                                ?>
                                                <option value="<?php echo $reference_inquiry->referenceid; ?>"><?php echo $reference_inquiry->referenceby; ?></option>
                                                <?php
                                            }
                                        ?>	
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label>Priority</label>
                                    <select class="form-control" name="inquiry_priority">
                                        <?php
                                            while($prioritys=mysql_fetch_object($priority))
                                            {
                                                ?>
                                                <option value="<?php echo $prioritys->priority_id; ?>"><?php echo $prioritys->priority; ?></option>
                                                <?php
                                            }
                                        ?>	
                                    </select>
                                </div>
                            </div>
                        </div>
						<div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label>Lead Provider</label>
                                    <select class="form-control" name="leadprovide">
                                        <?php
                                            while($lead_provider=mysql_fetch_object($lead_providers))
                                            {
                                                ?>
                                                <option value="<?php echo $lead_provider->lead_provider_id; ?>"><?php echo $lead_provider->lead_provider; ?></option>
                                                <?php
                                            }
                                        ?>	
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label>Lead End Date</label>
                                    <input type="text" name="lead_date" required class="form-control datepickers" value="<?php echo date('d-m-Y'); ?>">
                                </div>
                            </div>
                        </div>	
                        <div class="row">
							<div class="col-lg-12">
								<div class="form-group">
									<div class="form-group">
										<label>Lead Details</label>
										<input  type="text" name="lead_detail" class="form-control">
										<!--<textarea rows="3" name="lead_detail" class="form-control"></textarea>-->
									</div>
								</div>
							</div>
						</div>
						<div class="row" style="padding-top:30px">
							<div class="col-lg-12">
								<input class="btn btn-success" type="submit" name="addInquiry" value="Add Inquiry" />
							</div>
						</div>						
					</form>
				</div>
			</div>
		</div>
	</div>
    </div>    
</div>
<?php
include_once('../footer.php');
?>
<script>
jQuery(document).ready(function() {
	
	$(".displayCustomer").hide();
	
	$( ".getCustomer" ).focus(function(){
		var serchby = jQuery(this).val();
		$.ajax({
			type: "POST",
			url: "<?php echo SITE_URL.'ajaxcall.php' ?>",
			data: {serchby:serchby,ajax:'getParty'},
			cache: false,
				success: function(html) {
					$('.displayCustomer').html(html);
					$('.displayCustomer').show();
				}	
		});		
	});
	
	
	$( ".getCustomer" ).keyup(function(){
		var serchby = jQuery(this).val();
		$.ajax({
			type: "POST",
			url: "<?php echo SITE_URL.'ajaxcall.php' ?>",
			data: {serchby:serchby,ajax:'getParty'},
			cache: false,
				success: function(html) {
					$('.displayCustomer').html(html);
					$('.displayCustomer').show();
				}	
		});		
	});
	
	$( ".displayCustomer" ).delegate( ".cusomervalue", "click", function() {
		var partycode = $(this).attr('data-code');
		$('.getCustomer').val($(this).html());
		$('.cusromerCode').val($(this).attr('data-code'));
		$('.displayCustomer').hide();
		$.ajax({
			type: "POST",
			url: "<?php echo SITE_URL.'ajaxcall.php' ?>",
			data: {partycode:partycode,ajax:'getContactPersonByParty'},
			cache: false,
				success: function(html) {
					$('.kind_attn').html(html);
				}	
		});	
	});
	
	$( ".datepickers" ).datepicker({ dateFormat: 'dd-mm-yy' });
	
	jQuery('#dataTables-example').DataTable({
			responsive: true
	});
	
});

</script>
	
