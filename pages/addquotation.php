<?php
include_once('../header.php');
include_once('../dbFunction.php');
$funObj = new dbFunction();

if($_POST['addQuotation'])
{
	$inquiry_no = $_REQUEST['inquiry_no'];
	$inquiry_date = $_REQUEST['inquiry_date'];
	$quotation_date = $_REQUEST['quotation_date'];
	$revision = $_REQUEST['revision'];
	$revision_date = $_REQUEST['revision_date'];
	$review_date = $_REQUEST['review_date'];
	$due_date = $_REQUEST['due_date'];
	$customer_code1 = $_REQUEST['customer_code1'];
	$reason = $_REQUEST['reason'];
	$reason_remarks = $_REQUEST['reason_remarks'];
	$currency = $_REQUEST['currency'];
	$conversion_rate = $_REQUEST['conversion_rate'];
	$sales = $_REQUEST['sales'];
	$quotation_type = $_REQUEST['quotation_type'];
	$action = $_REQUEST['action'];
	$status = $_REQUEST['status'];
	$sales_type = $_REQUEST['sales_type'];
	$quotation_stage = $_REQUEST['quotation_stage'];
	$rounding = $_REQUEST['rounding'];
	
	$addQuotation = $funObj->addQuotation($inquiry_no,$inquiry_date,$quotation_date,$revision,$revision_date,$review_date,$due_date,$customer_code1,$reason,$reason_remarks,$currency,$conversion_rate,$sales,$quotation_type,$action,$status,$sales_type,$quotation_stage,$rounding);	
	
	$getItemtoInquiry = $funObj->getItemtoInquiry($inquiry_no,$addQuotation);
	
	if($addQuotation!= "")
	{
		$redirect = SITE_URL."pages/quotaionDetail.php?id=".$addQuotation."";
		?>
			<script>
                window.location.assign("<?php echo $redirect; ?>")
            </script>
		<?php
	}
	
}

$quotation_reason = $funObj->getTableData('quotation_reason');
$sales = $funObj->getTableData('sales');
$quotation_type = $funObj->getTableData('quotation_type');
$quotation_action = $funObj->getTableData('quotation_action');
$quotation_status = $funObj->getTableData('quotation_status');
$sales_type = $funObj->getTableData('sales_type');
$quotation_stage = $funObj->getTableData('quotation_stage');
$inquiries = $funObj->getTableData('inquiry');
$currency =  $funObj->getTableData('currency');
?>
<div id="page-wrapper">
    <div class="container">
	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">Add Quotation</h1>
			<div class="panel-body">
				<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="myModal" class="modal fade" style="display: none;">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<h4 id="myModalLabel" class="modal-title">list of inquiry</h4>
							</div>
							<div class="modal-body">
								<ul style="display:block">
									<li>Inquiry No. &nbsp;&nbsp;&nbsp; Inquiry data &nbsp;&nbsp;&nbsp; inquiry By</li>
								<?php 
									while($inquirys=mysql_fetch_object($inquiries))
									{
											$getParty = mysql_fetch_array($funObj->getDataById("party_master","party_code='".$inquirys->customer_code."'"));
											?>
												<li><a href="javascript:void(0)" class="setInqueryQua" data-id="<?php echo $inquirys->inquiry_id; ?>"><?php echo $inquirys->inquiry_id ?> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  <?php echo $inquirys->inquiry_date ?></a>  &nbsp;&nbsp;&nbsp;&nbsp;  <?php echo $getParty['party_name']; ?></li>
											<?php
									}									
								?>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<div aria-hidden="true" aria-labelledby="myModalLabel" role="dialog" tabindex="-1" id="modal_quotation" class="modal fade" style="display: none;">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<h4 id="myModalLabel" class="modal-title">list of Quotation</h4>
							</div>
							<div class="modal-body">
								<a class="btn btn-primary newQuotation" style="float:right">Add New</a>
								<ul class="getQuotationList" style="display:block">
								
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-default">
				<div class="panel-body">
					<form role="form" name="addQuotation" method="POST">
						<div class="row">
							<div class="col-lg-6">
								<div class="form-group">
									<label>Inquiry No</label>
									<input type="text" name="inquiry_no" required class="form-control inquiry_no" >
								</div>
							</div>
							<div class="col-lg-6">
								<div class="form-group">
									<label>Inquiry Date</label>
									<input type="text" name="inquiry_date" required class="form-control datepickers inquiry_date" value="<?php echo date('d-m-Y') ?>" placeholder="dd-mm-yyyy"> 
								</div>
							</div>
						</div>	
						<div class="row">
							<div class="col-lg-6">
								<div class="form-group">
									<label>Quotation No</label>
									<input type="text" name="quotation_no" disabled class="form-control">
								</div>
							</div>
							<div class="col-lg-6">
								<div class="form-group">
									<label>Quotation Date</label>
									<input type="text" name="quotation_date" required class="form-control datepickers Inquiry Date" value="<?php echo date('d-m-Y') ?>" placeholder="dd-mm-yyyy" > 
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-lg-6">
								<div class="form-group">
									<label>Revision</label>
									<input type="text" name="revision" value="1" required class="form-control revisionJS">
								</div>
							</div>
							<div class="col-lg-6">
								<div class="form-group">
									<label>Revision Date</label>
									<input type="text" name="revision_date" required class="form-control datepickers" value="<?php echo date('d-m-Y') ?>" placeholder="dd-mm-yyyy"> 
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-lg-6">
								<div class="form-group">
									<label>Review Date</label>
									<input type="text" name="review_date" required class="form-control datepickers" value="<?php echo date('d-m-Y') ?>" placeholder="dd-mm-yyyy">
								</div>
							</div>
							<div class="col-lg-6">
								<div class="form-group">
									<label>Due Date</label>
									<input type="text" name="due_date" required class="form-control datepickers" value="<?php echo date('d-m-Y') ?>" placeholder="dd-mm-yyyy"> 
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-lg-12">
								<div class="form-group">
									<label>Customer Code (Do you want edit customer?)</label>
									<div style="display:flex">
										<input type="text" class="form-control cusromerCode" required readonly name="customer_code1">
										<div class="customer_block">
											<input type="text" class="form-control getCustomer" required name="customer_code2">
											<ul class="displayCustomer" style="display: none;">
												
											</ul>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-lg-6">
								<div class="form-group">
									<label>Reason</label>
									<select class="form-control" name="reason">
										<?php
											while($quotation_reasons=mysql_fetch_object($quotation_reason))
											{
												?>
												<option value="<?php echo $quotation_reasons->reason_id; ?>"><?php echo $quotation_reasons->reason; ?></option>
												<?php
											}
										?>
									</select>	
								</div>
							</div>
							<div class="col-lg-6">
								<div class="form-group">
									<label>Reason Remarks</label>
									<textarea class="form-control" name="reason_remarks"></textarea>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-lg-6">
								<div class="form-group">
									<label>Currency	</label>
									<select class="form-control" name="currency">
										<?php
											while($currencys=mysql_fetch_object($currency))
											{
												?>
												<option value="<?php echo $currencys->currency_id; ?>"><?php echo $currencys->Currency; ?></option>
												<?php
											}
										?>
									</select>	
								</div>
							</div>
							<div class="col-lg-6">
								<div class="form-group">
									<label>Conversion Rate</label>
									<input type="text" class="form-control" required name="conversion_rate">
								</div>
							</div>
						</div>
						
						<div class="row">
							<div class="col-lg-6">
								<div class="form-group">
									<label>Sales</label>
									<select class="form-control" name="sales">
										<?php
											while($saless=mysql_fetch_object($sales))
											{
												?>
												<option value="<?php echo $saless->sales_id; ?>"><?php echo $saless->sales; ?></option>
												<?php
											}
										?>	
									</select>	
								</div>
							</div>
							<div class="col-lg-6">
								<div class="form-group">
									<label>Quotation Type</label>
									<select class="form-control" name="quotation_type">
										<?php
											while($quotation_types=mysql_fetch_object($quotation_type))
											{
												?>
												<option value="<?php echo $quotation_types->type_id; ?>"><?php echo $quotation_types->type; ?></option>
												<?php
											}
										?>	
									</select>	
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-lg-6">
								<div class="form-group">
									<label>Action</label>
									<select class="form-control" name="action">
										<?php
											while($quotation_actions=mysql_fetch_object($quotation_action))
											{
												?>
												<option value="<?php echo $quotation_actions->action_id; ?>"><?php echo $quotation_actions->action; ?></option>
												<?php
											}
										?>
									</select>	
								</div>
							</div>
							<div class="col-lg-6">
								<div class="form-group">
									<label>Status</label>
									<select class="form-control" name="status">
										<?php
											while($quotation_statuss=mysql_fetch_object($quotation_status))
											{
												?>
												<option value="<?php echo $quotation_statuss->status_id; ?>"><?php echo $quotation_statuss->status; ?></option>
												<?php
											}
										?>
									</select>	
								</div>
							</div>
						</div>			
						<div class="row">
							<div class="col-lg-6">
								<div class="form-group">
									<label>Sales Type</label>
									<select class="form-control" name="sales_type">
										<?php
											while($sales_types=mysql_fetch_object($sales_type))
											{
												?>
												<option value="<?php echo $sales_types->sales_type_id; ?>"><?php echo $sales_types->sales_type; ?></option>
												<?php
											}
										?>
									</select>	
								</div>
							</div>
							<div class="col-lg-6">
								<div class="form-group">
									<label>Quotation Stage</label>
									<select class="form-control" name="quotation_stage">
										<?php
											while($quotation_stages=mysql_fetch_object($quotation_stage))
											{
												?>
												<option value="<?php echo $quotation_stages->stage_id; ?>"><?php echo $quotation_stages->stage; ?></option>
												<?php
											}
										?>
									</select>	
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-lg-12">
								<div class="form-group">
									<label class="checkbox-inline">
										<input type="checkbox" value="1" name="rounding">Rounding
									</label>	
								</div>
							</div>
						</div>
						<div class="row" style="padding-top:30px">
							<div class="col-lg-12">
								<input class="btn btn-success" type="submit" name="addQuotation" value="Add Item" />
							</div>
						</div>		
						
					</form>
				</div>
			</div>
		</div>
	</div>
    </div>    
</div>
<?php
include_once('../footer.php');
?>

<script>
	$(document).ready(function(){
		$( ".datepickers" ).datepicker({ dateFormat: 'dd-mm-yy' });
		$("#myModal").modal('show');
		$(".setInqueryQua").click(function(){
			var inq_id = $(this).attr('data-id');
			$.ajax({
				type: "POST",
				url: "<?php echo SITE_URL.'ajaxcall.php' ?>",
				data: {inq_id:inq_id,ajax:'setInquiryToQuotation'},
				cache: false,
				success: function(html) {
					var res = html.split("_||_");
					if(res[4] == "")
					{		
						$('.inquiry_no').val(res[0]);
						$('.inquiry_date').val(res[1]);
						$('.cusromerCode').val(res[2]);
						$('.getCustomer').val(res[3]);
					}
					else
					{
						$('.inquiry_no').val(res[0]);
						$('.inquiry_date').val(res[1]);
						$('.cusromerCode').val(res[2]);
						$('.getCustomer').val(res[3]);
						$('.getQuotationList').html(res[4]);
						$('.revisionJS').val(res[5]);
						$("#modal_quotation").modal('show');
					}
					$("#myModal").modal('hide');
					
				}	
			});
		})
		$( "body" ).delegate( ".setQuptaionOfInq", "click", function(){
			var qua_id = $(this).attr('data-id');
			window.location.href = "<?php echo SITE_URL ?>pages/quotaionDetail.php?id="+qua_id;
		})
		$( "body" ).delegate( ".newQuotation", "click", function(){
			$("#modal_quotation").modal('hide');
		});
		
	});
</script>
