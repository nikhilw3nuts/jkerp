<?php
include_once('../header.php');

include_once('../dbFunction.php');
$funObj = new dbFunction();
$partys = $funObj->getTableData('party_master');
?>
<div id="page-wrapper">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Party</h1>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading clearfix">
                        Party Detail
                        <a class="btn btn-primary" style="float:right" href="<?php echo SITE_URL ?>pages/addParty.php">Add Party</a>
                    </div>				
                    <div class="panel-body">
                        <div class="dataTable_wrapper">
                            <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                <thead>
                                    <tr>
                                        <th>Action</th>
                                        <th>Party Name</th>
                                        <th>Email Address</th>
                                        <th>Phone number</th>
                                        <th>Created by</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php 
                                        while($party=mysql_fetch_object($partys))
                                        {
                                            ?>
                                                <tr class="odd gradeX">
                                                    <td><a onclick="return confirm('Are you sure?');" href="<?php echo SITE_URL.'pages/delete.php?redirect=party&table=party_master&field=party_code&id='.$party->party_code; ?>">Delete</a></td>
                                                    <td><a href="<?php echo SITE_URL.'pages/partyDetail.php?id='.$party->party_code; ?>"><?php echo $party->party_name; ?></a></td>
                                                    <td><?php echo $party->email; ?></td>
                                                    <td><?php echo $party->phones; ?></td>
                                                    <td><?php echo $party->created_by; ?></td>
                                                </tr>	
                                            <?php
                                        } 
                                    ?>								
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
include_once('../footer.php');
?>
<script>
jQuery(document).ready(function() {
	jQuery('#dataTables-example').DataTable({
			responsive: true
	});
});
</script>
