<?php
include_once('../header.php');

include_once('../dbFunction.php');
$funObj = new dbFunction();
if($_POST['addquotationType'])
{
	$quotation_type = $_POST['quotation_type'];
	$isExist = $funObj->checkeAllreadyExist("quotation_type","type",$quotation_type);
	if($isExist)
	{
		echo "<script>alert('Quotation Type aleady exist!')</script>";
	}
	else
	{
		$quotation_type = $funObj->addQuotationType($quotation_type);
	}
}
$quotation_types = $funObj->getTableData('quotation_type');
?>
<div id="page-wrapper">
    <div class="container">
	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">Quotation Type</h1>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-7">
			<div class="panel panel-default">
				<div class="panel-heading clearfix">
					Quotation Type Detail
				</div>				
				<div class="panel-body">
					<div class="dataTable_wrapper">
						<table class="table table-striped table-bordered table-hover" id="dataTables-example">
							<thead>
								<tr>
									<th>Action</th>
									<th>Quotation Type</th>
								</tr>
							</thead>
							<tbody>
								<?php 
									while($quotation_type=mysql_fetch_object($quotation_types))
									{
										?>
											<tr class="odd gradeX">
												<td><a onclick="return confirm('Are you sure?');" href="<?php echo SITE_URL.'pages/delete.php?redirect=quotation_type&table=quotation_type&field=type_id&id='.$quotation_type->type_id; ?>">Delete</a></td>
												<td><?php echo $quotation_type->type; ?></td>
											</tr>	
										<?php
									} 
								?>								
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
		<div class="col-lg-5">
			<div class="panel panel-default">
				<div class="panel-heading clearfix">
					Add Quotation Type
				</div>
				<div style="margin:20px">	
				<form role="form" name="addquotationType" method="POST">
					<div class="form-group">
						<label>Quotation Type</label>
						<input type="text" name="quotation_type" required class="form-control">
					</div>
					<input class="btn btn-success btn-block" type="submit" name="addquotationType" value="Add Quotation Type" />
				</form>
				</div>
			</div>
		</div>
	</div>
    </div>    
</div>
<?php
include_once('../footer.php');
?>
<script>
jQuery(document).ready(function() {
	jQuery(".state-all").hide();
	jQuery(".state-1").show();
	jQuery(".countrycng").change(function(){
		var curt = jQuery(this).val();
		 jQuery(".state-all").hide();
		 jQuery(".state-"+curt).show();
	})
	jQuery('#dataTables-example').DataTable({
			responsive: true
	});
	
});
</script>
