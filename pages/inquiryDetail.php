<?php
include_once('../header.php');
include_once('../dbFunction.php');
$funObj = new dbFunction();
$inquiryid = $_GET['id'];
if($_POST['addInquiry'])
{
	$lead_owner = $_POST['lead_owner'];
	$assigned_to = $_POST['assigned_to'];
	$industry_type = $_POST['industry_type'];
	$inquiry_no = $_POST['inquiry_no'];
	$inquiry_date = $_POST['inquiry_date'];
	$customer_code1 = $_POST['customer_code1'];
	$customer_code2 = $_POST['customer_code2'];
	$reference_date = $_POST['reference_date'];
	$kind_attn = $_POST['kind_attn'];
	$inquiry_contact_no = $_POST['inquiry_contact_no'];
	$inquiry_status = $_POST['inquiry_status'];
	$email = $_POST['email'];
	$inquiry_stage = $_POST['inquiry_stage'];
	$inquiry_reference = $_POST['inquiry_reference'];
	$inquiry_priority = $_POST['inquiry_priority'];
	$leadprovide = $_POST['leadprovide'];
	$lead_date = $_POST['lead_date'];
	$lead_detail = $_POST['lead_detail'];
	$review_date = $_POST['review_date'];
	$addInquiry = $funObj->editInquiry($lead_owner,$assigned_to,$industry_type,$inquiry_no,$inquiry_date,$customer_code1,$customer_code2,$reference_date,$kind_attn,$inquiry_contact_no,$inquiry_status,$email,$inquiry_stage,$inquiry_reference,$inquiry_priority,$leadprovide,$lead_date,$lead_detail,$review_date,$inquiryid);	
}

$users = $funObj->getTableData('user');
$users1 = $funObj->getTableData('user');
$industry_types = $funObj->getTableData('industry_type');
$industry_stage = $funObj->getTableData('industry_stage');
$industry_status = $funObj->getTableData('industry_status');
$reference_inquirys = $funObj->getTableData('reference');
$priority = $funObj->getTableData('priority');
$lead_providers = $funObj->getTableData('lead_provider');
$getInquiry = mysql_fetch_array($funObj->getDataById("inquiry","inquiry_id='".$inquiryid."'"));	
$getParty = mysql_fetch_array($funObj->getDataById("party_master","party_code='".$getInquiry['customer_code']."'"));	
$getPartyCP = $funObj->getDataById("contact_person","party_code='".$getInquiry['customer_code']."'");
$uoms = $funObj->getTableData('uom');
$ii_status = $funObj->getTableData('industry_status');
$ii_status = $funObj->getTableData('industry_status');
$ii_status = $funObj->getTableData('industry_status');
$getII = $funObj->getDataById("inquiry_item","inquiry_id='".$inquiryid."'");
$inquiry_conformations = $funObj->getTableData('inquiry_conformation');
$inquiry_conformations1 = $funObj->getTableData('inquiry_conformation');
$countrys = $funObj->getTableData('country','ORDER BY country ASC');
$states = $funObj->getTableData('state','ORDER BY state ASC');
$citys = $funObj->getTableData('city','ORDER BY city ASC');
?>
<div id="page-wrapper">
    <div class="container">
	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">Edit Inquiry</h1>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-default">
				<div class="panel-body">
					<form role="form" name="addInquiry" method="POST">
						<div class="row">
							<div class="col-lg-4">
								<div class="form-group">
									<label>Lead Owner</label>
                                    <select class="form-control" name="lead_owner">
                                        <?php
                                            while($user=mysql_fetch_object($users))
                                            {
                                                ?>
                                                <option <?php echo ($getInquiry['lead_owner'] == $user->userid) ? 'selected':''; ?> value="<?php echo $user->userid; ?>"><?php echo $user->username; ?></option>
                                                <?php
                                            }
                                        ?>	
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-4">
								<div class="form-group">
									<label>Assigned To</label>
                                    <select class="form-control" name="assigned_to">
                                        <?php
                                            while($user=mysql_fetch_object($users1))
                                            {
                                                ?>
                                                <option <?php echo ($getInquiry['assigned_to'] == $user->userid) ? 'selected':''; ?> value="<?php echo $user->userid; ?>"><?php echo $user->username; ?></option>
                                                <?php
                                            }
                                        ?>	
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-4">
								<div class="form-group">
									<label>Industry Type</label>
                                    <select class="form-control" name="industry_type">
                                        <?php
                                            while($industry_type=mysql_fetch_object($industry_types))
                                            {
                                                ?>
                                                <option <?php echo ($getInquiry['industry_type'] == $industry_type->industry_type_id) ? 'selected':''; ?> value="<?php echo $industry_type->industry_type_id; ?>"><?php echo $industry_type->industry_type; ?></option>
                                                <?php
                                            }
                                        ?>	
                                    </select>
                                </div>
                            </div>
						</div>	
                        <div class="row">
							<div class="col-lg-6">
								<div class="form-group">
									<label>Inquiry no</label>
									<input type="text" name="inquiry_no" value="<?php echo $inquiryid; ?>" disabled class="form-control">
								</div>
							</div>
							<div class="col-lg-6">
								<div class="form-group">
									<label>Inquiry Date</label>
									<input type="text" name="inquiry_date" value="<?php echo $getInquiry['inquiry_date']; ?>" required class="form-control datepickers" placeholder="dd-mm-yyyy">
								</div>
							</div>
						</div>                        
                        <div class="row">
							<div class="col-lg-6">
								<div class="form-group">
									<div class="form-group">
										<label>Customer Code (note. Do you val customer code and customre name vice versa call?)</label>
										<div style="display:flex">
											<input type="text" name="customer_code1" value="<?php echo $getInquiry['customer_code']; ?>" readonly class="form-control cusromerCode">
											<div class="customer_block">
												<input type="text" name="customer_code2" value="<?php echo $getParty['party_name'] ?>" required class="form-control getCustomer">
												<ul class="displayCustomer">
													
												</ul>
											</div>
										</div>
									</div>
								</div>
							</div>
                            <div class="col-lg-6">
								<div class="form-group">
									<div class="form-group">
										<label>Reference Date</label>
										<input type="text" name="reference_date" value="<?php echo $getInquiry['reference_date'] ?>" required class="form-control datepickers" placeholder="dd-mm-yyyy">
									</div>
								</div>
							</div>
						</div>
                        <div class="row">
							<div class="col-lg-6">
								<div class="form-group">
									<label> Kind Attn.</label>
                                    <select class="form-control kind_attn" name="kind_attn">    
										<?php 
											while($getPartyCPs=mysql_fetch_object($getPartyCP))
											{
												$selected = "";
												if($getPartyCPs->contact_person_id == $getInquiry['kind_attn'])
													$selected = "selected";
												echo "<option ".$selected." value='".$getPartyCPs->contact_person_id."'>".$getPartyCPs->contact_person."</option>";
											}
										?>                                                                    	
                                    </select>
								</div>
							</div>
                            <div class="col-lg-6">
								<div class="form-group">
                                    <label>Review Date</label>
                                    <input type="text" name="review_date" value="<?php echo $getInquiry['review_date'] ?>" required class="form-control datepickers" placeholder="dd-mm-yyyy">
								</div>
							</div>
						</div>
                        <div class="row">
							<div class="col-lg-6">
								<div class="form-group">
									<label>Contact No.</label>
                                    <input type="text" name="inquiry_contact_no" value="<?php echo $getInquiry['contact_no'] ?>" required class="form-control">
								</div>
							</div>
                            <div class="col-lg-6">
								<div class="form-group">
                                    <label>Status</label>
                                    <select class="form-control" name="inquiry_status">
                                        <?php
                                            while($industry_statuss=mysql_fetch_object($industry_status))
                                            {
                                                ?>
                                                <option <?php echo ($getInquiry['status'] == $industry_statuss->industry_status_id) ? 'selected':''; ?> value="<?php echo $industry_statuss->industry_status_id; ?>"><?php echo $industry_statuss->industry_status; ?></option>
                                                <?php
                                            }
                                        ?>	
                                    </select>
								</div>
							</div>
						</div>
                        <div class="row">
							<div class="col-lg-6">
								<div class="form-group">
									<label>Email ID</label>
                                    <input type="text" name="email" value="<?php echo $getInquiry['emial'] ?>" required class="form-control">
								</div>
							</div>
                            <div class="col-lg-6">
								<div class="form-group">
                                    <label>Stage</label>
                                    <select class="form-control" name="inquiry_stage">
                                        <?php
                                            while($industry_stages=mysql_fetch_object($industry_stage))
                                            {
                                                ?>
                                                <option <?php echo ($getInquiry['stage'] == $industry_stages->industry_stage_id) ? 'selected':''; ?> value="<?php echo $industry_stages->industry_stage_id; ?>"><?php echo $industry_stages->industry_stage; ?></option>
                                                <?php
                                            }
                                        ?>	
                                    </select>
								</div>
							</div>
						</div>
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label>Reference</label>
                                    <select class="form-control" name="inquiry_reference">
                                        <?php
                                            while($reference_inquiry=mysql_fetch_object($reference_inquirys))
                                            {
                                                ?>
                                                <option <?php echo ($getInquiry['reference'] == $reference_inquiry->referenceid) ? 'selected':''; ?> value="<?php echo $reference_inquiry->referenceid; ?>"><?php echo $reference_inquiry->referenceby; ?></option>
                                                <?php
                                            }
                                        ?>	
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label>Priority</label>
                                    <select class="form-control" name="inquiry_priority">
                                        <?php
                                            while($prioritys=mysql_fetch_object($priority))
                                            {
                                                ?>
                                                <option <?php echo ($getInquiry['priority'] == $prioritys->priority_id) ? 'selected':''; ?> value="<?php echo $prioritys->priority_id; ?>"><?php echo $prioritys->priority; ?></option>
                                                <?php
                                            }
                                        ?>	
                                    </select>
                                </div>
                            </div>
                        </div>
						<div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label>Lead Provider</label>
                                    <select class="form-control" name="leadprovide">
                                        <?php
                                            while($lead_provider=mysql_fetch_object($lead_providers))
                                            {
                                                ?>
                                                <option <?php echo ($getInquiry['lead_provider'] == $lead_provider->lead_provider_id) ? 'selected':''; ?> value="<?php echo $lead_provider->lead_provider_id; ?>"><?php echo $lead_provider->lead_provider; ?></option>
                                                <?php
                                            }
                                        ?>	
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label>Lead End Date</label>
                                    <input type="text" name="lead_date" value="<?php echo $getInquiry['lead_end_date'] ?>" required class="form-control datepickers" placeholder="dd-mm-yyyy">
                                </div>
                            </div>
                        </div>	
                        <div class="row">
							<div class="col-lg-12">
								<div class="form-group">
									<div class="form-group">
										<label>Lead Details</label>
										<input type="type" name="lead_detail" class="form-control" value="<?php echo $getInquiry['lead_details']; ?>" >
										<!--<textarea rows="3" name="lead_detail" class="form-control"><?php echo $getInquiry['lead_details']; ?></textarea>-->
									</div>
								</div>
							</div>
						</div>
						<div class="row" style="padding-top:30px">
							<div class="col-lg-12">
								<input class="btn btn-success" type="submit" name="addInquiry" value="Edit Inquiry" />
							</div>
						</div>						
					</form>
				</div>
				<div class="panel-body">
					<div class="row" style="padding-top:20px">
						<div class="col-lg-12">
							<ul class="nav nav-tabs">
								<li><a data-toggle="tab" href="#inquiryitem" aria-expanded="false">Inquiry Item</a></li>
								<li><a data-toggle="tab" href="#customerdetails" aria-expanded="true">Customer Details</a></li>
								<li><a data-toggle="tab" href="#confirmation" aria-expanded="false">Confirmation</a></li>
								<li><a data-toggle="tab" href="#login">Login</a></li>
							</ul>
							<div class="tab-content">
								<div id="inquiryitem" class="tab-pane fade">
									<h4>Inquiry Item</h4>
									<div class="panel-body">
										<div class="dataTable_wrapper">
											<table class="table table-striped table-bordered table-hover" id="dataTables-example">
												<thead>
													<tr>
														<th>Action</th>
														<th>No</th>
														<th>Item Code</th>
														<th>Sample text</th>
														<th>UOM</th>
														<th>Quantity</th>
														<th>Drawing No.</th>
														<th>Rev</th>
														<th>Status</th>
													</tr>
												</thead>
												<tbody class="ajaxii">
													<?php
														$i=1;
														while($getIIs=mysql_fetch_object($getII))
														{
															$getUomById = mysql_fetch_array($funObj->getDataById("uom","uom_id='".$getIIs->uom."'"));
															$getStatusById = mysql_fetch_array($funObj->getDataById("industry_status","industry_status_id='".$getIIs->status."'"));
															?>
																<tr class="odd gradeX iidelete<?php echo $getIIs->inquiry_item_id ?>">
																	<td><a class="iidelete" href="javascript:void(0)" data-id="<?php echo $getIIs->inquiry_item_id ?>">Delete</a></td>
																	<td><?php echo $i; ?></td>
																	<td><?php echo $getIIs->item_code; ?></td>
																	<td><?php echo $getIIs->note; ?></td>
																	<td><?php echo $getUomById['uom']; ?></td>
																	<td><?php echo $getIIs->required_qty ?></td>
																	<td><?php echo $getIIs->drawing_no; ?></td>
																	<td><?php echo $getIIs->rev; ?></td>
																	<td><?php echo $getStatusById['industry_status']; ?></td>
																</tr>
															<?php
															$i++;
														}
													?>
													
																					
												</tbody>
											</table>
										</div>
									</div>	
									<form role="form" method="post" id="saveii" name="savecp">
										<input type="hidden" class="ajax" name="ajax" value="ii" />
										<input type="hidden" name="ii_inquiryid" value="<?php echo $inquiryid ?>" />
										<div class="row">
											<div class="col-lg-4">
												<div class="form-group">
													<div class="form-group">
														<label>Item Code</label>
														<div class="customer_block">
															<input type="text" name="ii_code" required class="form-control ii_code">
															<ul class="getItems search-dropdown">
															
															</ul>
														</div>
													</div>
												</div>
											</div>
											<div class="col-lg-4">
												<div class="form-group">
													<div class="form-group">
														<label>Drawing No.</label>
														<input type="text" name="ii_drawing_no" required class="form-control">
													</div>
												</div>
											</div>
											<div class="col-lg-4">
												<div class="form-group">
													<div class="form-group">
														<label>Rev</label>
														<input type="text" name="ii_rev" required class="form-control">
													</div>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-lg-6">
												<div class="form-group">
													<div class="form-group">
														<label>Note</label>
														<input type="text" name="ii_note" required class="form-control">
													</div>
												</div>
											</div>
											<div class="col-lg-6">
												<div class="form-group">
													<div class="form-group">
														<label>UOM</label>
														<select class="form-control" name="ii_uom">
															<?php
																while($uom=mysql_fetch_object($uoms))
																{
																	?>
																	<option value="<?php echo $uom->uom_id; ?>"><?php echo $uom->uom; ?></option>
																	<?php
																}
															?>
														</select>
													</div>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-lg-6">
												<div class="form-group">
													<div class="form-group">
														<label>Required Qty</label>
														<input type="text" name="ii_qty" required class="form-control">
													</div>
												</div>
											</div>
											<div class="col-lg-6">
												<div class="form-group">
													<div class="form-group">
														<label>Price</label>
														<input type="text" name="ii_price" required class="form-control">
													</div>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-lg-6">
												<div class="form-group">
													<div class="form-group">
														<label>Item Description</label>
														<input type="text" id="p_phones" class="form-control" name="li-description">
														<!--<textarea id="p_phones" class="form-control" name="ii_description" rows="5"></textarea>-->
													</div>
												</div>
											</div>
											<div class="col-lg-6">
												<div class="form-group">
													<div class="form-group">
														<label>Status</label>
														<select class="form-control" name="ii_status">
															
															<?php
																while($ii_statuss=mysql_fetch_object($ii_status))
																{
																	?>
																	<option value="<?php echo $ii_statuss->industry_status_id; ?>"><?php echo $ii_statuss->industry_status; ?></option>
																	<?php
																}
															?>
														</select>
													</div>
												</div>
											</div>
										</div>
										<div class="row viewimagebtndisp">
											<div class="col-lg-12 viewimagebtn">
												
											</div>
										</div>
										<div class="row" style="padding-top:30px">
											<div class="col-lg-12">
												<input class="btn btn-success" type="submit" name="saveii" value="Add Item" />
											</div>
										</div>
									</form>
								</div>
								<div id="customerdetails" class="tab-pane fade">
									<h4>Customer Details</h4>
									<form role="form" method="post" id="savecustdetail" name="savecustdetail">
										<input type="hidden" class="ajax" name="ajax" value="custdetail" />
										<div class="row">
											<div class="col-lg-6">
												<div class="form-group">
													<div class="form-group">
														<label>Customer Code</label>
														<input type="text" name="customercode" readonly value="<?php echo $getParty['party_code']; ?>" class="form-control">
													</div>
												</div>
											</div>
											<div class="col-lg-6">
												<div class="form-group">
													<div class="form-group">
														<label>Customer Name</label>
														<input type="text" readonly name="customername" value="<?php echo $getParty['party_name']; ?>" class="form-control">
													</div>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-lg-4">
												<div class="form-group">
													<div class="form-group">
														<label>Address</label>
														<textarea name="customeraddress" readonly class="form-control"><?php echo $getParty['address']; ?></textarea>
													</div>
												</div>
											</div>
											<div class="col-lg-4">
												<div class="form-group">
													<div class="form-group">
														<label>Country</label>
														<select class="form-control selcountry" readonly name="country">
															<option>Select Country</option>
															<?php
																while($country=mysql_fetch_object($countrys))
																{
																	?>
																	<option <?php echo ($getParty['country'] == $country->countryid) ? 'selected' : ''; ?> class="countrydisp country-<?php echo $country->countryid; ?>" value="<?php echo $country->countryid; ?>"><?php echo $country->country; ?></option>
																	<?php
																}
															?>
														</select>
													</div>
												</div>		
												<div class="form-group">
													<div class="form-group">
														<select class="form-control selstates" readonly name="state">
															<option>State State</option>
															<?php
																while($state=mysql_fetch_object($states))
																{
																	?>
																	<option <?php echo ($getParty['state'] == $state->stateid) ? 'selected' : ''; ?> class="statedisp sCountry-<?php echo $state->countryid; ?> state-<?php echo $state->stateid; ?>" data-country="<?php echo $state->countryid; ?>" value="<?php echo $state->stateid; ?>"><?php echo $state->state; ?></option>
																	<?php
																}
															?>
														</select>
													</div>
												</div>
											</div>
											<div class="col-lg-4">
												<div class="form-group">
													<div class="form-group">
														<label>City</label>
														<select class="form-control selcity" readonly name="city">
															<option>Select City</option>
															<?php
																while($city=mysql_fetch_object($citys))
																{
																	$countryid = mysql_fetch_array($funObj->getDataById('country',"countryid='".$city->stateid."'"));
																	?>
																	<option <?php echo ($getParty['city'] == $city->cityid) ? 'selected' : ''; ?> class="citydisp cCountry-<?php echo $countryid['countryid']; ?> cState-<?php echo $city->stateid; ?> city-<?php echo $city->cityid; ?>" data-state="<?php echo $city->stateid; ?>" data-country="<?php echo $countryid['countryid']; ?>"  value="<?php echo $city->cityid; ?>"><?php echo $city->city; ?></option>
																	<?php
																}
															?>
														</select>
													</div>
												</div>
												<div class="form-group">
													<div class="form-group">
														<label>Pincode</label>
														<input type="text" name="customername" readonly value="<?php echo $getParty['pincode']; ?>" class="form-control">
													</div>
												</div>
											</div>
										</div>		
										<div class="row">
											<div class="col-lg-4">
												<div class="form-group">
													<div class="form-group">
														<label>Phone No</label>
														<textarea name="customeraddress" readonly class="form-control"><?php echo $getParty['phones']; ?></textarea>
													</div>
												</div>
											</div>
											<div class="col-lg-4">
												<div class="form-group">
													<div class="form-group">
														<label>Fax No.</label>
														<input type="text" name="customername" readonly value="<?php echo $getParty['fax']; ?>" class="form-control">
													</div>
												</div>		
												<div class="form-group">
													<div class="form-group">
														<label>Email</label> 
														<input type="text" name="customername" readonly value="<?php echo $getParty['email']; ?>" class="form-control">
													</div>
												</div>
											</div>
											<div class="col-lg-4">
												<div class="form-group">
													<div class="form-group">
														<label>Website</label>
														<input type="text" name="customername" readonly value="<?php echo $getParty['website']; ?>" class="form-control">
													</div>
												</div>
											</div>
										</div>		
									</form>
								</div>
								<div id="confirmation" class="tab-pane fade">
									<h4>Inquiry Confirmation</h4>
									<form role="form" method="post" id="saveic" name="savecp">
										<input type="hidden" class="ajax" name="ajax" value="iconform" />
										<input type="hidden" name="ii_inquiryid" value="<?php echo $inquiryid ?>" />
										<div class="row">
											<div class="col-lg-6">
												<h4>Enquiry Received By	</h4>
												<div class="row">
													<?php 
														while($inquiry_conformation=mysql_fetch_object($inquiry_conformations))
														{
															$inqu_con = array(); 
															$strc = str_replace(' ', '_',strtolower($inquiry_conformation->inquiry_conformation));
															$getiReceived = $funObj->getDataById("inquiry_received","inquiry_id='".$inquiryid."'");
															while($getiReceiveds=mysql_fetch_object($getiReceived))
															{
																if($getiReceiveds->inquiry_received_by == $inquiry_conformation->inquiry_conformation)
																{
																	$inqu_con['chb'.$inquiry_conformation->inquiry_conformation] = "checked";
																	$inqu_con['txt'.$inquiry_conformation->inquiry_conformation] = $getiReceiveds->inquiry_received_value; 
																}
																
															}
															?>
																<div class="col-lg-12">
																	<div class="form-group">
																		<div class="form-group">
																			<label><?php echo $inquiry_conformation->inquiry_conformation; ?></label>
																			<input type="checkbox" <?php echo $inqu_con['chb'.$inquiry_conformation->inquiry_conformation] ?> name="<?php echo 'rchbox'.$strc; ?>" class="form-control" value="<?php echo $inquiry_conformation->inquiry_conformation; ?>">
																			<input type="text" value="<?php echo $inqu_con['txt'.$inquiry_conformation->inquiry_conformation] ?>" name="<?php echo 'rtext'.$strc; ?>" class="form-control">
																		</div>
																	</div>
																</div>
															<?php
														}
													?>
												</div>	
											</div>
											<div class="col-lg-6">
												<h4>Send Confirmation By</h4>
												<div class="row">
													<?php 
														while($inquiry_conformation=mysql_fetch_object($inquiry_conformations1))
														{
															$inqu_con = array(); 
															$strc = str_replace(' ', '_',strtolower($inquiry_conformation->inquiry_conformation));
															$getiConformation = $funObj->getDataById("inquiry_conform_by","inquiry_id='".$inquiryid."'");
															while($getiConformations=mysql_fetch_object($getiConformation))
															{
																if($getiConformations->conform_by == $inquiry_conformation->inquiry_conformation)
																{
																	$inqu_con['chb'.$inquiry_conformation->inquiry_conformation] = "checked";
																	$inqu_con['txt'.$inquiry_conformation->inquiry_conformation] = $getiConformations->conform_by_value; 
																}
																
															}
															?>
																<div class="col-lg-12">
																	<div class="form-group">
																		<div class="form-group">
																			<label><?php echo $inquiry_conformation->inquiry_conformation; ?></label>
																			<input type="checkbox" name="<?php echo 'schbox'.$strc; ?>" class="form-control" <?php echo $inqu_con['chb'.$inquiry_conformation->inquiry_conformation] ?> value="<?php echo $inquiry_conformation->inquiry_conformation; ?>">
																			<input value="<?php echo $inqu_con['txt'.$inquiry_conformation->inquiry_conformation] ?>" type="text" name="<?php echo 'stext'.$strc; ?>" class="form-control">
																		</div>
																	</div>
																</div>
															<?php
														}
													?>
												</div>	
											</div>
										</div>
										<div class="row" style="padding-top:30px">
											<div class="col-lg-12">
												<input class="btn btn-success" type="submit" name="saveic" value="Add Conformation" />
											</div>
										</div>
									</form>
								</div>
								<div id="login" class="tab-pane fade">
									<h4>Login</h4>
									<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
								</div>
							</div>
						</div>	
					</div>
				</div>
			</div>
		</div>
	</div>
    </div>    
</div>
<?php
include_once('../footer.php');
?>
<script>
jQuery(document).ready(function() {
	
	$(".displayCustomer").hide();
	$(".getItems").hide();
	$(".viewimagebtndisp").hide();
	
	$( ".getCustomer" ).focus(function(){
		var serchby = jQuery(this).val();
		$.ajax({
			type: "POST",
			url: "<?php echo SITE_URL.'ajaxcall.php' ?>",
			data: {serchby:serchby,ajax:'getParty'},
			cache: false,
				success: function(html) {
					$('.displayCustomer').html(html);
					$('.displayCustomer').show();
				}	
		});		
	});
		
	$( ".getCustomer" ).keyup(function(){
		var serchby = jQuery(this).val();
		$.ajax({
			type: "POST",
			url: "<?php echo SITE_URL.'ajaxcall.php' ?>",
			data: {serchby:serchby,ajax:'getParty'},
			cache: false,
				success: function(html) {
					$('.displayCustomer').html(html);
					$('.displayCustomer').show();
				}	
		});		
	});
	
	jQuery("#saveii").submit(function(e) {
		var cpval = jQuery( this ).serialize();
		$.ajax({
			type: "POST",
			url: "<?php echo SITE_URL.'ajaxcall.php' ?>",
			data: cpval,
			cache: false,
				success: function(html) {
					$('.ajaxii').append(html);
				}	
		});
		return false;       
    });
    
    jQuery("#saveic").submit(function(e) {
		var cpval = jQuery( this ).serialize();
		$.ajax({
			type: "POST",
			url: "<?php echo SITE_URL.'ajaxcall.php' ?>",
			data: cpval,
			cache: false,
				success: function(html) {
					//alert(html);
				}	
		});
		return false;       
    });
    
    jQuery(".dataTable_wrapper").delegate(".iidelete","click",function(){
		var co = confirm('Are you sure?');
		if (co == true) {
			var tcid = jQuery(this).attr('data-id');
			$.ajax({
				type: "POST",
				url: "<?php echo SITE_URL.'ajaxcall.php' ?>",
				data: {tcid:tcid,ajax:'iidelete'},
				cache: false,
					success: function(html) {
						jQuery('.iidelete'+tcid).remove();
					}	
			});
		}
		else
		{
			return false;
		}
	})
	
	$( ".displayCustomer" ).delegate( ".cusomervalue", "click", function() {
		var partycode = $(this).attr('data-code');
		$('.getCustomer').val($(this).html());
		$('.cusromerCode').val($(this).attr('data-code'));
		$('.displayCustomer').hide();
		$.ajax({
			type: "POST",
			url: "<?php echo SITE_URL.'ajaxcall.php' ?>",
			data: {partycode:partycode,ajax:'getContactPersonByParty'},
			cache: false,
				success: function(html) {
					$('.kind_attn').html(html);
				}	
		});	
	});
	
	$( ".ii_code").keyup(function(){
		var iivalue = $(this).val();
		$.ajax({
			type: "POST",
			url: "<?php echo SITE_URL.'ajaxcall.php' ?>",
			data: {iivalue:iivalue,ajax:'getItemCode'},
			cache: false,
				success: function(html) {
					$(".getItems").html(html);
					$(".getItems").show();
				}	
		})
	})
	
	$('body').delegate( ".selItemCode", "click", function() {
		$('.ii_code').val($(this).attr('data-code'));
		var itemid = $(this).attr('data-imgid');
		$(".getItems").hide();
		$.ajax({
			type: "POST",
			url: "<?php echo SITE_URL.'ajaxcall.php' ?>",
			data: {itemid:itemid,ajax:'getItemImage'},
			cache: false,
				success: function(html) {
					$('.viewimagebtn').html(html);
					$('.viewimagebtndisp').show();
				}	
		})
	})
	
	$( ".datepickers" ).datepicker({ dateFormat: 'dd-mm-yy' });
	
	jQuery('#dataTables-example').DataTable({
			responsive: true
	});
	
	jQuery(".selcity").change(function(){
		var id = jQuery(this).val();
		if(id != "Select City"){
			var stateid = jQuery('option:selected', this).attr('data-state');
			var countryid = jQuery('option:selected', this).attr('data-country');
			jQuery('.state-'+stateid).prop("selected",true);
			jQuery('.country-'+countryid).prop("selected",true);
			jQuery('.citydisp').hide();
			jQuery('.cState-'+stateid).show();
			jQuery('.statedisp').hide(); 
			jQuery('.sCountry-'+countryid).show();
		}
		else
		{
			jQuery('.selstates option:first').prop('selected','selected');
			jQuery('.selcountry option:first').prop('selected','selected');
			jQuery('.citydisp').show();
			jQuery('.statedisp').show(); 
		}
	})
	jQuery(".selstates").change(function(){
		var id = jQuery(this).val();
		var countryid = jQuery('option:selected', this).attr('data-country');
		if(id != "State State"){
			jQuery('.country-'+countryid).prop("selected",true);
			jQuery('.selcity option:first').prop('selected','selected');
			jQuery('.citydisp').hide();
			jQuery('.cState-'+id).show();
		}
		else
		{
			jQuery('.selcountry option:first').prop('selected','selected');
			jQuery('.selcity option:first').prop('selected','selected');
			jQuery('.citydisp').show();
			jQuery('.statedisp').show();
		}
	})
	
	jQuery(".selcountry").change(function(){
		var id = jQuery(this).val();
		if(id != "Select Country"){
			jQuery('.selcity option:first').prop('selected','selected');
			jQuery('.selstates option:first').prop('selected','selected');
			jQuery('.citydisp').hide();
			jQuery('.cCountry-'+id).show();
			jQuery('.statedisp').hide(); 
			jQuery('.sCountry-'+id).show();
		}
		else
		{
			jQuery('.selcity option:first').prop('selected','selected');
			jQuery('.selstates option:first').prop('selected','selected');
			jQuery('.citydisp').show();
			jQuery('.statedisp').show();
		}
	})
	
});

</script>
	
