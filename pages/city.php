<?php
include_once('../header.php');

include_once('../dbFunction.php');
$funObj = new dbFunction();
if($_POST['addcity'])
{
	$country = $_POST['country'];
	$state = $_POST['state'];
	$city = $_POST['city'];
	$checkecity = $funObj->checkcity($city);
	if($checkecity)
	{
		echo "<script>alert('City aleady exist!')</script>";
	}
	else
	{
		$city = $funObj->addcity($state,$city);
	}
}
$states = $funObj->getTableData('state','ORDER BY state ASC');
$countrys = $funObj->getTableData('country','ORDER BY country ASC');
$citys = $funObj->getTableData('city');
?>
<div id="page-wrapper">
    <div class="container">
	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">City</h1>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-7">
			<div class="panel panel-default">
				<div class="panel-heading clearfix">
					City Detail
				</div>				
				<div class="panel-body">
					<div class="dataTable_wrapper">
						<table class="table table-striped table-bordered table-hover" id="dataTables-example">
							<thead>
								<tr>
									<th>Action</th>
									<th>City</th>
									<th>State</th>
									<th>Country</th>
								</tr>
							</thead>
							<tbody>
								<?php 
									while($city=mysql_fetch_object($citys))
									{
										$stateid = mysql_fetch_array($funObj->getDataById('state',"stateid='".$city->stateid."'"));
										$countryid = mysql_fetch_array($funObj->getDataById('country',"countryid='".$stateid['countryid']."'"));
										?>
											<tr class="odd gradeX">
												<td><a onclick="return confirm('Are you sure?');" href="<?php echo SITE_URL.'pages/delete.php?redirect=city&table=city&field=cityid&id='.$city->cityid; ?>">Delete</a></td>
												<td><?php echo $city->city; ?></td>
												<td><?php echo $stateid['state']; ?></td>
												<td><?php echo $countryid['country']; ?></td>
											</tr>	
										<?php
									} 
								?>								
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
		<div class="col-lg-5">
			<div class="panel panel-default">
				<div class="panel-heading clearfix">
					Add City
				</div>
				<div style="margin:20px">	
				<form role="form" name="addcity" method="POST">
					<div class="form-group">
						<label>Select Country</label>
						<select class="form-control countrycng" name="country">
							<?php
								while($country=mysql_fetch_object($countrys))
								{
									?>
									<option value="<?php echo $country->countryid; ?>"><?php echo $country->country; ?></option>
									<?php
								}
							?>	
						</select>
					</div>
					<div class="form-group">
						<label>Select State</label>
						<select class="form-control state" name="state">
							<option>Select State</option>
							<?php
								while($state=mysql_fetch_object($states))
								{
									?>
									<option class="state-all state-<?php echo $state->countryid ?>" value="<?php echo $state->stateid; ?>"><?php echo $state->state; ?></option>
									<?php
									
								}
							?>	
						</select>
					</div>
					<div class="form-group">
						<label>City</label>
						<input type="text" name="city" required class="form-control">
					</div>
					<input class="btn btn-success btn-block" type="submit" name="addcity" value="Add City" />
				</form>
				</div>
			</div>
		</div>
	</div>
    </div>    
</div>
<?php
include_once('../footer.php');
?>
<script>
jQuery(document).ready(function() {
	jQuery(".state-all").hide();
	jQuery(".state-1").show();
	jQuery(".countrycng").change(function(){
		var curt = jQuery(this).val();
		 jQuery(".state-all").hide();
		 jQuery(".state-"+curt).show();
	})
	jQuery('#dataTables-example').DataTable({
			responsive: true
	});
	
});
</script>
