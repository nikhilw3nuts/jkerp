<?php
include_once('../header.php');
include_once('../dbFunction.php');
$funObj = new dbFunction();

$salesorderid = $_GET['editid'];

if($_GET['editid'] !="")
{
	if($_POST['addSalesOrder'])
	{
		unset($_POST['addSalesOrder']);
		$_POST['last_update_by']=$_SESSION['uid'];
		$funObj->updatefunction('sales_oder',$_POST,'sales_oder_id',$_GET['editid']);		
	}
	$get_sorder_data = mysql_fetch_array($funObj->getDataById("sales_oder","sales_oder_id='".$_GET['editid']."'"));
	$customerDetail = mysql_fetch_array($funObj->getDataById("party_master","party_code='".$get_sorder_data['sell_party_id']."'"));
	$getPartyCP = mysql_fetch_array($funObj->getDataById("contact_person","party_code='".$getQuotation['customer_code']."' ORDER BY priority ASC LIMIT 1"));
	$states = $funObj->getTableData('state','ORDER BY state ASC');
	$citys = $funObj->getTableData('city','ORDER BY city ASC');
	$countrys = $funObj->getTableData('country','ORDER BY country ASC');
	$customerDetail1 = mysql_fetch_array($funObj->getDataById("party_master","party_code='".$get_sorder_data['sell_party_id']."'"));
	$getPartyCP1 = mysql_fetch_array($funObj->getDataById("contact_person","party_code='".$getQuotation['customer_code']."' ORDER BY priority ASC LIMIT 1"));
	$states1 = $funObj->getTableData('state','ORDER BY state ASC');
	$citys1 = $funObj->getTableData('city','ORDER BY city ASC');
	$countrys1 = $funObj->getTableData('country','ORDER BY country ASC');
	$term_group = $funObj->getTableData('term_group');	
	$getTcs = $funObj->getDataById("sales_order_condition","sales_order_id='".$_GET['editid']."'");
	$getsoNote = mysql_fetch_array($funObj->getDataById("sales_order_note","sales_order_id='".$_GET['editid']."'"));
	$sales = $funObj->getTableData('sales');
	$currency =  $funObj->getTableData('currency');
	$status = $funObj->getTableData('status');
	$qitem = $funObj->getDataById('quotation_item','quotation_id="'.$get_sorder_data['quotation_id'].'"');
	$quaotationDetail = mysql_fetch_array($funObj->getDataById('quotation_master','quotation_id="'.$get_sorder_data['quotation_id'].'"'));
	$projects = $funObj->getTableData('project');
	$billing_temps = $funObj->getDataById('selas_billing_temp','sales_order_id="'.$_GET['editid'].'"');
}
else
{
	if($_POST['addSalesOrder'])
	{
		unset($_POST['addSalesOrder']);
		$_POST['created_on']=date('Y-m-d H:i:s');
		$_POST['created_by']=$_SESSION['uid'];
		$_POST['last_update_by']=$_SESSION['uid'];
		$funObj->addfunction('sales_oder',$_POST);	

	}
}


$branchs = $funObj->getTableData('branch');
$selQuotation = mysql_query("SELECT q.quotation_id,q.quotation_date,c.party_code,c.party_name FROM quotation_master as q LEFT JOIN party_master AS c ON q.customer_code=c.party_code");
$btmemplates = $funObj->getTableData('billing_template');

?>
<div id="page-wrapper">
    <div class="container">
	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">Sales Order</h1>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-default">
				<div class="panel-body">
					<form role="form" name="addSalesOrder" method="POST" enctype="multipart/form-data">
                    	<input type="hidden" name="quotation_id" id="quotation_id" value="" />						
                        <div class="row">
							<div class="col-lg-6">
								<div class="form-group">
									<div class="form-group">
		                                <label>Sell to Party</label>
    		                            <div>
											<input type="text" name="sell_party_id" value="<?php echo $get_sorder_data['sell_party_id']; ?>" id="sell_party_id" required class="form-control input-text-sm">
											<input type="text" name="sell_party_name" value="<?php echo $get_sorder_data['sell_party_name']; ?>" id="sell_party_name" required class="form-control input-text-sm">
										</div>
                                        
									</div>
                                </div>
							</div>
                            <div class="col-lg-6">
                            	<a style="background: #dfdfdf none repeat scroll 0 0; padding: 10px;" class="openpopup"><i class="fa fa-binoculars"></i></a>
                            </div>
						</div>	
						<div class="row">
							<div class="col-lg-6">
								<div class="form-group">
									<div class="form-group">
										<label>Ship to Party</label>
										<div>
											<input type="text" name="ship_party_id" value="<?php echo $get_sorder_data['ship_party_id']; ?>" id="ship_party_id" required class="form-control input-text-sm">
											<input type="text" name="ship_party_name" id="ship_party_name" required value="<?php echo $get_sorder_data['ship_party_name']; ?>" class="form-control input-text-sm">
										</div>
									</div>
								</div>
							</div>
                        </div>
						<div class="row">
							<div class="col-lg-6">
								<div class="form-group">
									<label>Sales Order No.</label>
									<input type="text" readonly value="<?php echo $get_sorder_data['sales_oder_id']; ?>" class="form-control">
                            	</div>
							</div>
							<div class="col-lg-6">
								<div class="form-group">
									<label>S. O. Date</label>
									<input type="text" name="sales_oder_date" required value="<?php echo $get_sorder_data['sales_oder_date']; ?>" class="form-control datepickers">
								</div>
							</div>
						</div>
                        <div class="row">
                        	<div class="col-lg-6">
								<div class="form-group">
									<label>Customer P. O. No.</label>
									<input type="text" name="purchase_order_no" value="<?php echo $get_sorder_data['purchase_order_no']; ?>" required class="form-control ">
								</div>
							</div>
							<div class="col-lg-6">
								<div class="form-group">
									<label>P. O. Date</label>
									<input type="text" name="purchase_order_date" value="<?php echo $get_sorder_data['purchase_order_date']; ?>" required class="form-control datepickers">
								</div>
							</div>
						</div>
                        <div class="row">
                        	<div class="col-lg-6">
								<div class="form-group">
									<label>Branch</label>
									<select class="form-control" name="branch">
										<option>Select Branch</option>
										<?php
											while($branch=mysql_fetch_object($branchs))
											{
												?>
												<option <?php echo ($get_sorder_data['branch'] == $branch->branch_id) ? 'selected' : ''; ?> value="<?php echo $branch->branch_id; ?>"><?php echo $branch->branch; ?></option>
												<?php
											}
										?>
									</select>
								</div>
							</div>
							<div class="col-lg-6">
								<div class="form-group">
									<label>Rounding</label>
                                    <div class="checkbox">
                                        <label>
                                            <input type="checkbox" <?php echo ($get_sorder_data['rounding'] == 1) ? 'checked' : ''; ?> name="rounding" value="1">
	                                    </label>
									</div>
    							</div>
							</div>
						</div>
                        
						<div class="row" style="padding-top:30px">
							<div class="col-lg-12">
								<input class="btn btn-success" type="submit" name="addSalesOrder" value="Sales Order" />
							</div>
						</div>		
						
					</form>
				</div>
                <?php if($_GET['editid'] !="") { ?>
                        <div class="panel-body">
                            <ul class="nav nav-tabs">
                                <li><a href="#salesorder" data-toggle="tab">Sales Order</a></li>
                                <li><a href="#items" data-toggle="tab">Items</a></li>
                                <li><a href="#billterns" data-toggle="tab">Billing terms</a></li>
                                <li><a href="#tc" data-toggle="tab">terms & conditions</a></li>
                                <li><a href="#note" data-toggle="tab">Note</a></li>
                                <li><a href="#party" data-toggle="tab">Party</a></li>
                                <li><a href="#party" data-toggle="tab">Buyer</a></li>
                                <li><a href="#party" data-toggle="tab">Consignee</a></li>
                                <li><a href="#report" data-toggle="tab">Report</a></li>
                                <li><a href="#login" data-toggle="tab">Login</a></li>
                            </ul>    		
                            <div class="tab-content">
                                <div class="tab-pane fade" id="salesorder">
                                    <h4>Sales Order Detail</h4>
                                    <form role="form" method="post" id="saveOrder" name="saveOrder">
										<input type="hidden" class="ajax" name="ajax" value="saveOrder" />
										<input type="hidden" name="sales_order_id" value="<?php echo $salesorderid ?>" /> 
                                        <div class="row">
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <div class="form-group">
                                                       <label>Sales Order No.</label>
                                                       <input type="text" class="form-control" name="sales_order_no" value="<?php echo $get_sorder_data['sales_oder_id']; ?>"/> 
                                                    </div>	
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <div class="form-group">
                                                        <label>Cust P.O.  No.</label>
                                                        <input type="text" class="form-control" name="purchase_order_no" value="<?php echo $get_sorder_data['purchase_order_no']; ?>"/>
                                                    </div>	
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-4">
                                                <div class="form-group">
                                                    <div class="form-group">
                                                       <label>S.O. Date</label>
                                                       <input type="text" class="datepickers form-control" name="sales_oder_date" value="<?php echo $get_sorder_data['sales_oder_date']; ?>"/> 
                                                    </div>	
                                                </div>
                                            </div>
                                            <div class="col-lg-4">
                                                <div class="form-group">
                                                    <div class="form-group">
                                                        <label>P.O. Date.</label>
                                                        <input type="text" class="form-control datepickers" name="purchase_order_date" value="<?php echo $get_sorder_data['purchase_order_date']; ?>"/>
                                                    </div>	
                                                </div>
                                            </div>
                                            <div class="col-lg-4">
                                                <div class="form-group">
                                                    <div class="form-group">
                                                        <label>Sales</label>
                                                        <select class="form-control" name="sales">
                                                            <option>Select Sales</option>	
                                                            <?php
                                                                while($saless=mysql_fetch_object($sales))
                                                                {
                                                                    ?>
                                                                    <option <?php echo ($getQuotation['sales'] == $saless->sales_id)?'selected':''; ?> value="<?php echo $saless->sales_id; ?>"><?php echo $saless->sales; ?></option>
                                                                    <?php
                                                                }
                                                            ?>	
                                                        </select>
                                                    </div>	
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <div class="form-group">
                                                    <div class="form-group">
                                                        <label>Committed Date</label>
                                                        <input type="text" class="form-control datepickers" name="committed_date" value="<?php echo date('d-m-Y'); ?>"/> 
                                                        <a class="btn btn-default" href="#">Generate Delivery Date</a>
                                                        <font color="red">how it's works?</font>
                                                    </div>	
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-4">
                                                <div class="form-group">
                                                    <div class="form-group">
                                                       <label>Currency</label>
                                                       <select class="form-control" name="currency">
                                                            <?php
                                                                while($currencys=mysql_fetch_object($currency))
                                                                {
                                                                    ?>
                                                                    <option <?php echo ($quaotationDetail['currency'] == $currencys->currency_id)?'selected':''; ?> value="<?php echo $currencys->currency_id; ?>"><?php echo $currencys->Currency; ?></option>
                                                                    <?php
                                                                }
                                                            ?>
                                                        </select>
                                                    </div>	
                                                </div>
                                            </div>
                                            <div class="col-lg-4">
                                                <div class="form-group">
                                                    <div class="form-group">
                                                        <label>Conversion Rate</label>
                                                        <input type="text" class="form-control" name="conversion_rate" value="<?php echo $quaotationDetail['conversion_rate'] ?>"/>
                                                    </div>	
                                                </div>
                                            </div>
                                            <div class="col-lg-4">
                                                <div class="form-group">
                                                    <div class="form-group">
                                                        <label>Status</label>
                                                        <?php  ?>
                                                        <select class="form-control" name="status">
                                                            <?php
                                                                while($statu=mysql_fetch_object($status))
                                                                {
                                                                    ?>
                                                                    <option <?php echo ($getQuotation['status'] == $statu->status_id)?'selected':''; ?> value="<?php echo $statu->status_id; ?>"><?php echo $statu->status; ?></option>
                                                                    <?php
                                                                }
                                                            ?>
                                                        </select>
                                                    </div>	
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <div class="form-group">
                                                       <label>Project</label>
                                                       <select class="form-control" name="project">
                                                            <?php
                                                                while($project=mysql_fetch_object($projects))
                                                                {
                                                                    ?>
                                                                    <option <?php echo ($get_sorder_data['project'] == $project->project_id) ? 'selected' : ''; ?> value="<?php echo $project->project_id; ?>"><?php echo $project->project; ?></option>
                                                                    <?php
                                                                }
                                                            ?>
                                                       </select>
                                                    </div>	
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <div class="form-group">
                                                        <label>Order Type</label>
                                                        <input type="text" class="form-control" name="order_type" value="<?php echo $get_sorder_data['order_type'] ?>"/>
                                                    </div>	
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-4">
                                                <div class="form-group">
                                                    <div class="form-group">
                                                       <label>Kind Attn</label>
                                                       <input type="text" class="form-control" name="kindattn" value="<?php echo $getPartyCP['contact_person']; ?>"/> 
                                                    </div>	
                                                </div>
                                            </div>
                                            <div class="col-lg-4">
                                                <div class="form-group">
                                                    <div class="form-group">
                                                        <label>Contact No.</label>
                                                        <input type="text" class="form-control" name="contactno" value="<?php echo $getPartyCP['mobile']; ?>"/>
                                                    </div>	
                                                </div>
                                            </div>
                                            <div class="col-lg-4">
                                                <div class="form-group">
                                                    <div class="form-group">
                                                        <label>Email Id.</label>
                                                        <input type="text" class="form-control" name="email" value="<?php echo $getPartyCP['email']; ?>"/>
                                                    </div>	
                                                </div>
                                            </div>
                                    	</div>     
                                        <div class="row" style="padding-top:30px">
											<div class="col-lg-12">
												<input class="btn btn-success" type="submit" name="saveOrder" value="Save" />
											</div>
										</div>                           	
                                	</form>
                                </div>
                                <div class="tab-pane fade" id="items">
                                    <div class="panel-body">
										<div class="dataTable_wrapper">
											<table class="table table-striped table-bordered table-hover" id="dataTab-example">
												<thead>
													<tr>
														<th>No</th>
														<th>Item Code</th>
                                                        <th>Description</th>
                                                        <th>UOM</th>
                                                        <th>Quantity</th>
                                                        <th>Rate</th>
                                                        <th>Amount</th>
                                                        <th>Status</th>
													</tr>
												</thead>
												<tbody class="ajaxmake">
													<?php
														$i=1; 
														while($qitemsx=mysql_fetch_object($qitem))
														{
															$getUomById = mysql_fetch_array($funObj->getDataById("uom","uom_id='".$qitemsx->uom."'"));
															$getStatusById = mysql_fetch_array($funObj->getDataById("industry_status","industry_status_id='".$qitemsx->status."'"));
															?>
																<tr class="getdetail odd gradeX" data-code="<?php echo $qitemsx->item_code; ?>" data-description="<?php echo $qitemsx->item_description ?>" data-qty="<?php echo $qitemsx->required_qty; ?>" data-rate="<?php echo $qitemsx->price; ?>" data-amount="<?php echo ($qitemsx->price)*($qitemsx->required_qty); ?>">
																	<td><?php echo $i; ?></td>
                                                                    <td><?php echo $qitemsx->item_code; ?></td>
                                                                    <td><?php echo $qitemsx->item_description ?></td>
                                                                    <td><?php echo $getUomById['uom']; ?></td>
                                                                    <td><?php echo $qitemsx->required_qty; ?></td>
                                                                    <td><?php echo $qitemsx->price; ?></td>
                                                                    <td><?php echo ($qitemsx->price)*($qitemsx->required_qty); ?></td>
                                                                    <td><?php echo $getStatusById['industry_status']; ?></td>
																</tr>
															<?php
															$i++;
														}
													?>								
												</tbody>
											</table>
										</div>
										<h4>Item Detail</h4>
                                    	<div class="row">
                                            <div class="col-lg-4">
                                                <div class="form-group">
                                                    <div class="form-group">
                                                       	<label>Item Code</label>
                                                        <input type="text" name="itemcode" required class="form-control disitemcode">
                                                    </div>	
                                                </div>
                                                <div class="form-group">
                                                    <div class="form-group">
                                                       	<label>Item Description</label>
                                                        <input type="text" name="header" required class="form-control disdescri">
                                                    </div>	
                                                </div>
                                                <div class="form-group">
                                                    <div class="form-group">
                                                       	<label>Item Desc(Cust)</label>
                                                        <input type="text" name="header" required class="form-control">
                                                    </div>	
                                                </div>
                                                <div class="form-group">
                                                    <div class="form-group">
                                                       	<label>Add. Descrition</label>
                                                        <input type="text" name="header" required class="form-control">
                                                    </div>	
                                                </div>
                                                <div class="form-group">
                                                    <div class="form-group">
                                                       	<label>Detail Descrition</label>
                                                        <input type="text" name="header" required class="form-control">
                                                    </div>	
                                                </div>
                                                <div class="form-group">
                                                    <div class="form-group">
                                                       	<label>Drawing number</label>
                                                        <input type="text" name="header" required class="form-control">
                                                    </div>	
                                                </div>
                                                <div class="form-group">
                                                    <div class="form-group">
                                                       	<label>Drawing Revision</label>
                                                        <input type="text" name="header" required class="form-control">
                                                    </div>	
                                                </div>	
                                                <div class="form-group">
                                                    <div class="form-group">
                                                       	<label>Project</label>
                                                        <input type="text" name="header" required class="form-control">
                                                    </div>	
                                                </div>
                                                <div class="form-group">
                                                    <div class="form-group">
                                                       	<label>Forecast no.</label>
                                                        <input type="text" name="header" required class="form-control">
                                                    </div>	
                                                </div>
                                            </div>
                                            <div class="col-lg-8">
                                            	<div class="form-group">
                                                    <div class="form-group">
                                                        <label>Sales Item</label>
                                                        <label class="radio-inline">
                                                            <input type="radio" name="optionsRadiosInline" id="optionsRadiosInline1" value="option1" checked>Spcific to party
                                                        </label>
                                                        <label class="radio-inline">
                                                            <input type="radio" name="optionsRadiosInline" id="optionsRadiosInline2" value="option2">Standard
                                                        </label>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                	<a class="btn btn-default">Rate</a>
                                                    <a class="btn btn-default">Stock</a>
                                                    <a class="btn btn-default">Work Order details</a>
                                                    <a class="btn btn-default">Desdatch Detail</a>
                                                    <a class="btn btn-default">Delivery Schedule</a>
                                                </div>	
                                                <div class="row">
                                                	<div class="col-lg-6">
                                                		<div class="form-group">
                                                            <div class="form-group">
                                                                <label>Quantity</label>
                                                                <input type="text" name="header" required class="form-control disp-qty">
                                                            </div>	
                                                        </div>
                                                        <div class="form-group">
                                                            <div class="form-group">
                                                                <label>Rate</label>
                                                                <input type="text" name="header" required class="form-control disp-rate">
                                                            </div>	
                                                        </div>
                                                        <div class="form-group">
                                                            <div class="form-group">
                                                                <label>Amount</label>
                                                                <input type="text" name="header" required class="form-control disp-amount">
                                                            </div>	
                                                        </div>    
                                                    </div>
                                                    <div class="col-lg-6">
                                                		<div class="form-group">
                                                            <div class="form-group">
                                                                <label>Disc(%)</label>
                                                                <input type="text" name="header" required class="form-control">
                                                            </div>	
                                                        </div> 
                                                        <div class="form-group">
                                                            <div class="form-group">
                                                                <label>Disc(Value)</label>
                                                                <input type="text" name="header" required class="form-control">
                                                            </div>	
                                                        </div>
                                                        <div class="form-group">
                                                            <div class="form-group">
                                                                <label>Net Amount</label>
                                                                <input type="text" name="header" required class="form-control">
                                                            </div>	
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                            		<div class="col-lg-6">
                                                    	<div class="form-group">
                                                            <div class="form-group">
                                                                <label>Status</label>
                                                                <input type="text" name="header" required class="form-control">
                                                            </div>	
                                                        </div>
                                                    </div>    
                                                   	<div class="col-lg-6">
                                                        <div class="form-group">
                                                            <div class="form-group">
                                                                <label>Item type</label>
                                                                <input type="text" name="header" required class="form-control">
                                                            </div>	
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                            		<div class="col-lg-6">
                                                    	<div class="form-group">
                                                            <div class="form-group">
                                                                <label>Qty Ref.</label>
                                                                <input type="text" name="header" required class="form-control">
                                                            </div>	
                                                        </div>
                                                    </div>    
                                                   	<div class="col-lg-6">
                                                        <div class="form-group">
                                                            <div class="form-group">
                                                                <label>Qtn SrNo.</label>
                                                                <input type="text" name="header" required class="form-control">
                                                            </div>	
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                            		<div class="col-lg-6">
                                                    	<div class="form-group">
                                                            <div class="form-group">
                                                                <label>Grade</label>
                                                                <input type="text" name="header" required class="form-control">
                                                            </div>	
                                                        </div>
                                                    </div>    
                                                   	<div class="col-lg-6">
                                                        <div class="form-group">
                                                            <div class="form-group">
                                                                <label>Cust Part No.</label>
                                                                <input type="text" name="header" required class="form-control">
                                                            </div>	
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                	<a class="btn btn-default">Stock Status</a>
                                                    <a class="btn btn-default">Item Image</a>
                                                    <a class="btn btn-default">Stock Allocation</a>
                                                    <a class="btn btn-default">Hold/Un Hold Item</a>
                                                    <a class="btn btn-default">Delete Item</a>
                                                </div>
                                            </div>
                                        </div>    
                                    </div>
									
                                </div>
                                <div class="tab-pane fade" id="billterns">
                                	<div class="billterns">
                                        <h4>Billing Terms</h4>
                                        <a class="btn btn-default openPopTemplate">Change Template</a>
                                        <table id="dataTables-example" class="table table-striped">
                                            <thead>
                                                <tr>
                                                    <th>No.</th>
                                                    <th>Code</th>
                                                    <th>CalDesc</th>
                                                    <th>CalDefination</th>
                                                    <th>CalDefination</th>
                                                    <th>Narration</th>
                                                    <th>Per(%)</th>
                                                    <th>Value</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php 
                                                    $i = 1;
                                                    $basvale = "";
													while($billing_temp=mysql_fetch_object($billing_temps))
                                                    {
														if($basvale == "")
															$basvale = $billing_temp->value
														?>
                                                            <tr class="chageSalesTemp" data-id="<?php echo $billing_temp->selas_terms_id; ?>" data-calcode="<?php echo $billing_temp->cal_code; ?>" data-description="<?php echo $billing_temp->description; ?>" data-percentage="<?php echo $billing_temp->percentage; ?>" data-narration="<?php echo $billing_temp->narration; ?>" data-value="<?php echo $billing_temp->value; ?>" data-amt="<?php echo $basvale; ?>">
                                                                <td><?php echo $i; ?></td>
                                                                <td><?php echo $billing_temp->cal_code; ?></td>
                                                                <td><?php echo $billing_temp->description; ?></td>
                                                                <td><?php echo $billing_temp->cal_defination; ?></td>
                                                                <td><?php echo $billing_temp->cal_defination1; ?></td>
                                                                <td><?php echo $billing_temp->narration; ?></td>
                                                                <td><?php echo $billing_temp->percentage; ?></td>
                                                                <td><?php echo $billing_temp->value; ?></td>
                                                            </tr>
                                                        <?php
                                                        $i++;
                                                    }
                                                ?>															
                                            </tbody>
                                        </table>
                                    </div>
                                    <form role="form" method="post" id="saveorderTemp" name="saveorderTemp">
										<input type="hidden" class="ajax" name="ajax" value="saveorderTemp" />
										<input type="hidden" name="sales_order_id" value="<?php echo $salesorderid ?>" />
                                        <input type="hidden" name="templateid" id="id-templateid" value="" />
                                        <input type="hidden" name="basicamt" id="id-basicamt" value="" />    
                                        <div class="row">
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <div class="form-group">
                                                        <label>Cal Code</label>
                                                        <input type="text" name="cal_code" readonly id="id-calcode" class="form-control termscode" style="min-width:0px; width:13%; !important;">
                                                        <input type="text" name="description" readonly id="id-description" class="form-control termsdesc" style="min-width:0px; width:50%; !important;">
                                                    </div>
                                                </div>
                                            </div>             	
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <div class="form-group">
                                                       <label>Narration</label>
                                                       <input type="text" name="narration" id="id-narration" class="form-control termsdesc">
                                                    </div>	
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <div class="form-group">
                                                        <label>Percentage</label>
                                                        <input type="text" name="percentage" id="id-percentage" class="form-control termscode" style="min-width:0px; width:13%; !important;">
                                                        <label>Value</label>	
                                                        <input type="text" name="value" id="id-value" class="form-control termsdesc" style="min-width:0px; width:50%; !important;">
                                                    </div>
                                                </div>
                                            </div>             	
                                        </div>
                                        <div class="row" style="padding-top:30px">
                                            <div class="col-lg-12">
                                                <input class="btn btn-success" type="submit" name="saveorderTemp" value="Save" />
                                            </div>
                                        </div>
									</form>
                                </div>
                                <div class="tab-pane fade" id="tc">
                                    <h4>Terms & Conditions</h4>
                                    <div class="panel-body">
										<div class="dataTable_wrapper">
											<table class="table table-striped table-bordered table-hover" id="dataTab-example">
												<thead>
													<tr>
														<th>Action</th>
														<th>Group Code</th>
														<th>Group Name</th>
                                                        <th>Terms & Condition</th>
													</tr>
												</thead>
												<tbody class="ajaxtc">
													<?php 

														while($getTc=mysql_fetch_object($getTcs))
														{
															?>
																<tr class="odd gradeX tcdelete<?php echo $getTc->sales_order_condition ?>">
																	<td><a class="tcdelete" href="javascript:void(0)" data-id="<?php echo $getTc->sales_order_condition ?>">Delete</a></td>
																	<td><?php echo $getTc->terms_group_code ?></td>
                                                                    <td><?php echo $getTc->terms_group ?></td>
                                                                    <td><?php echo $getTc->description ?></td>
																</tr>
															<?php
														}
													?>								
												</tbody>
											</table>
										</div>
									</div>
                                    <form role="form" method="post" id="savecp" name="savecp">
										<input type="hidden" class="ajax" name="ajax" value="socp" />
										<input type="hidden" name="sales_order_id" value="<?php echo $salesorderid ?>" />    
                                        <div class="row">
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <div class="form-group">
                                                        <label>Group Code/Name</label>
                                                        <input type="text" name="terms_group_code" required readonly class="form-control termscode" style="min-width:0px; width:13%; !important;">
                                                        <input type="text" name="terms_group" required readonly class="form-control termsdesc" style="min-width:0px; width:50%; !important;">
                                                        <a class="openpopup1" style="background: #dfdfdf none repeat scroll 0 0; padding: 10px;"><i class="fa fa-binoculars"></i></a>
                                                    </div>
                                                </div>
                                            </div>             	
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <div class="form-group">
                                                       <label>Description</label>
                                                        <textarea name="description" required class="form-control"></textarea>
                                                    </div>	
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row" style="padding-top:30px">
                                            <div class="col-lg-12">
                                                <input class="btn btn-success" type="submit" name="savecp" value="Save Contact Person" />
                                            </div>
                                        </div>
									</form>
                                </div>
                                <div class="tab-pane fade" id="note">
                                    <h4>Note</h4>
                                    <form role="form" method="post" id="sonote" name="sonote">
										<input type="hidden" class="ajax" name="ajax" value="sonote" />
										<input type="hidden" name="sales_order_id" value="<?php echo $salesorderid ?>" />    
                                        <div class="row">
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <div class="form-group">
                                                       <label>Header</label>
                                                        <textarea name="header" required class="form-control"><?php echo $getsoNote['header'] ?></textarea>
                                                    </div>	
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <div class="form-group">
                                                       <label>Footer</label>
                                                        <textarea name="footer" required class="form-control"><?php echo $getsoNote['footer'] ?></textarea>
                                                    </div>	
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <div class="form-group">
                                                       <label>Note</label>
                                                        <textarea name="note" required class="form-control"><?php echo $getsoNote['note'] ?></textarea>
                                                    </div>	
                                                </div>
                                            </div>
                                            <div class="col-lg-6">
                                                <div class="form-group">
                                                    <div class="form-group">
                                                       <label>Note 2</label>
                                                        <textarea name="note2" required class="form-control"><?php echo $getsoNote['note2'] ?></textarea>
                                                    </div>	
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <div class="form-group">
                                                    <div class="form-group">
                                                       <label>Internal Note</label>
                                                        <textarea name="internalnote" required class="form-control"><?php echo $getsoNote['internalnote'] ?></textarea>
                                                    </div>	
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row" style="padding-top:30px">
                                            <div class="col-lg-12">
                                                <input class="btn btn-success" type="submit" name="sonote" value="Save Note" />
                                            </div>
                                        </div>
									</form>
                                </div>
                                <div class="tab-pane fade" id="party">
                                    <h4>Party Detail</h4>
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <div class="form-group">
                                                    <label>Customer Code</label>
                                                    <input type="text" name="customercode" readonly value="<?php echo $customerDetail['party_code']; ?>" class="form-control">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <div class="form-group">
                                                    <label>Customer Name</label>
                                                    <input type="text" readonly name="customername" value="<?php echo $customerDetail['party_name']; ?>" class="form-control">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="form-group">
                                                <div class="form-group">
                                                    <label>Address</label>
                                                    <textarea name="customeraddress" readonly class="form-control"><?php echo $customerDetail['address']; ?></textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-4">
                                            <div class="form-group">
                                                <div class="form-group">
                                                    <label>Country</label>
                                                    <select class="form-control selcountry" readonly name="country">
                                                        <option>Select Country</option>
                                                        <?php
                                                            while($country=mysql_fetch_object($countrys))
                                                            {
                                                                ?>
                                                                <option <?php echo ($customerDetail['country'] == $country->countryid) ? 'selected' : ''; ?> class="countrydisp country-<?php echo $country->countryid; ?>" value="<?php echo $country->countryid; ?>"><?php echo $country->country; ?></option>
                                                                <?php
                                                            }
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>		
                                            <div class="form-group">
                                                <div class="form-group">
                                                    <select class="form-control selstates" readonly name="state">
                                                        <option>State State</option>
                                                        <?php
                                                            while($state=mysql_fetch_object($states))
                                                            {
                                                                ?>
                                                                <option <?php echo ($customerDetail['state'] == $state->stateid) ? 'selected' : ''; ?> class="statedisp sCountry-<?php echo $state->countryid; ?> state-<?php echo $state->stateid; ?>" data-country="<?php echo $state->countryid; ?>" value="<?php echo $state->stateid; ?>"><?php echo $state->state; ?></option>
                                                                <?php
                                                            }
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-4">
                                            <div class="form-group">
                                                <div class="form-group">
                                                    <label>City</label>
                                                    <select class="form-control selcity" readonly name="city">
                                                        <option>Select City</option>
                                                        <?php
                                                            while($city=mysql_fetch_object($citys))
                                                            {
                                                                $countryid = mysql_fetch_array($funObj->getDataById('country',"countryid='".$city->stateid."'"));
                                                                ?>
                                                                <option <?php echo ($customerDetail['city'] == $city->cityid) ? 'selected' : ''; ?> class="citydisp cCountry-<?php echo $countryid['countryid']; ?> cState-<?php echo $city->stateid; ?> city-<?php echo $city->cityid; ?>" data-state="<?php echo $city->stateid; ?>" data-country="<?php echo $countryid['countryid']; ?>"  value="<?php echo $city->cityid; ?>"><?php echo $city->city; ?></option>
                                                                <?php
                                                            }
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="form-group">
                                                    <label>Pincode</label>
                                                    <input type="text" name="customername" readonly value="<?php echo $customerDetail['pincode']; ?>" class="form-control">
                                                </div>
                                            </div>
                                        </div>
                                    </div>		
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="form-group">
                                                <div class="form-group">
                                                    <label>Phone No</label>
                                                    <textarea name="customeraddress" readonly class="form-control"><?php echo $customerDetail['phones']; ?></textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-4">
                                            <div class="form-group">
                                                <div class="form-group">
                                                    <label>Fax No.</label>
                                                    <input type="text" name="customername" readonly value="<?php echo $customerDetail['fax']; ?>" class="form-control">
                                                </div>
                                            </div>		
                                            <div class="form-group">
                                                <div class="form-group">
                                                    <label>Email</label> 
                                                    <input type="text" name="customername" readonly value="<?php echo $customerDetail['email']; ?>" class="form-control">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-4">
                                            <div class="form-group">
                                                <div class="form-group">
                                                    <label>Website</label>
                                                    <input type="text" name="customername" readonly value="<?php echo $customerDetail['website']; ?>" class="form-control">
                                                </div>
                                            </div>
                                        </div>
                                    </div>	
                                    <div class="row">
                                        <div class="col-lg-12"><h4>Kind Attn. Detail</h4></div>
                                        <div class="col-lg-4">
                                            <div class="form-group">
                                                <div class="form-group">
                                                    <label>Person Name</label>
                                                    <input type="text" name="customername" readonly value="<?php echo $getPartyCP['contact_person']; ?>" class="form-control">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-4">
                                            <div class="form-group">
                                                <div class="form-group">
                                                    <label> Email.</label>
                                                    <input type="text" name="customername" readonly value="<?php echo $getPartyCP['email']; ?>" class="form-control">
                                                </div>
                                            </div>		
                                        </div>
                                        <div class="col-lg-4">
                                            <div class="form-group">
                                                <div class="form-group">
                                                    <label>Phone</label>
                                                    <input type="text" name="customername" readonly value="<?php echo $getPartyCP['mobile']; ?>" class="form-control">
                                                </div>
                                            </div>
                                        </div>
                                    </div>	
                                </div>
                                <div class="tab-pane fade" id="buyer">
                                    <h4>Party Detail</h4>
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <div class="form-group">
                                                    <label>Customer Code</label>
                                                    <input type="text" name="customercode" readonly value="<?php echo $customerDetail1['party_code']; ?>" class="form-control">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <div class="form-group">
                                                    <label>Customer Name</label>
                                                    <input type="text" readonly name="customername" value="<?php echo $customerDetail1['party_name']; ?>" class="form-control">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="form-group">
                                                <div class="form-group">
                                                    <label>Address</label>
                                                    <textarea name="customeraddress" readonly class="form-control"><?php echo $customerDetail1['address']; ?></textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-4">
                                            <div class="form-group">
                                                <div class="form-group">
                                                    <label>Country</label>
                                                    <select class="form-control selcountry" readonly name="country">
                                                        <option>Select Country</option>
                                                        <?php
                                                            while($country=mysql_fetch_object($countrys1))
                                                            {
                                                                ?>
                                                                <option <?php echo ($customerDetail['country'] == $country->countryid) ? 'selected' : ''; ?> class="countrydisp country-<?php echo $country->countryid; ?>" value="<?php echo $country->countryid; ?>"><?php echo $country->country; ?></option>
                                                                <?php
                                                            }
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>		
                                            <div class="form-group">
                                                <div class="form-group">
                                                    <select class="form-control selstates" readonly name="state">
                                                        <option>State State</option>
                                                        <?php
                                                            while($state=mysql_fetch_object($states1))
                                                            {
                                                                ?>
                                                                <option <?php echo ($customerDetail['state'] == $state->stateid) ? 'selected' : ''; ?> class="statedisp sCountry-<?php echo $state->countryid; ?> state-<?php echo $state->stateid; ?>" data-country="<?php echo $state->countryid; ?>" value="<?php echo $state->stateid; ?>"><?php echo $state->state; ?></option>
                                                                <?php
                                                            }
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-4">
                                            <div class="form-group">
                                                <div class="form-group">
                                                    <label>City</label>
                                                    <select class="form-control selcity" readonly name="city">
                                                        <option>Select City</option>
                                                        <?php
                                                            while($city=mysql_fetch_object($citys1))
                                                            {
                                                                $countryid = mysql_fetch_array($funObj->getDataById('country',"countryid='".$city->stateid."'"));
                                                                ?>
                                                                <option <?php echo ($customerDetail['city'] == $city->cityid) ? 'selected' : ''; ?> class="citydisp cCountry-<?php echo $countryid['countryid']; ?> cState-<?php echo $city->stateid; ?> city-<?php echo $city->cityid; ?>" data-state="<?php echo $city->stateid; ?>" data-country="<?php echo $countryid['countryid']; ?>"  value="<?php echo $city->cityid; ?>"><?php echo $city->city; ?></option>
                                                                <?php
                                                            }
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="form-group">
                                                    <label>Pincode</label>
                                                    <input type="text" name="customername" readonly value="<?php echo $customerDetail1['pincode']; ?>" class="form-control">
                                                </div>
                                            </div>
                                        </div>
                                    </div>		
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="form-group">
                                                <div class="form-group">
                                                    <label>Phone No</label>
                                                    <textarea name="customeraddress" readonly class="form-control"><?php echo $customerDetail1['phones']; ?></textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-4">
                                            <div class="form-group">
                                                <div class="form-group">
                                                    <label>Fax No.</label>
                                                    <input type="text" name="customername" readonly value="<?php echo $customerDetail1['fax']; ?>" class="form-control">
                                                </div>
                                            </div>		
                                            <div class="form-group">
                                                <div class="form-group">
                                                    <label>Email</label> 
                                                    <input type="text" name="customername" readonly value="<?php echo $customerDetail['email']; ?>" class="form-control">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-4">
                                            <div class="form-group">
                                                <div class="form-group">
                                                    <label>Website</label>
                                                    <input type="text" name="customername" readonly value="<?php echo $customerDetail1['website']; ?>" class="form-control">
                                                </div>
                                            </div>
                                        </div>
                                    </div>	
                                    <div class="row">
                                        <div class="col-lg-12"><h4>Kind Attn. Detail</h4></div>
                                        <div class="col-lg-4">
                                            <div class="form-group">
                                                <div class="form-group">
                                                    <label>Person Name</label>
                                                    <input type="text" name="customername" readonly value="<?php echo $getPartyCP1['contact_person']; ?>" class="form-control">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-4">
                                            <div class="form-group">
                                                <div class="form-group">
                                                    <label> Email.</label>
                                                    <input type="text" name="customername" readonly value="<?php echo $getPartyCP1['email']; ?>" class="form-control">
                                                </div>
                                            </div>		
                                        </div>
                                        <div class="col-lg-4">
                                            <div class="form-group">
                                                <div class="form-group">
                                                    <label>Phone</label>
                                                    <input type="text" name="customername" readonly value="<?php echo $getPartyCP1['mobile']; ?>" class="form-control">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="report">
                                    <h4>Settings Tab</h4>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                                </div>
                                <div class="tab-pane fade" id="login">
                                    <h4>Settings Tab</h4>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                                </div>
                            </div>
                        </div>
                <?php } ?>
			</div>
		</div>
	</div>
    </div>    
</div>
<div class="modal fade in" id="mytermsmodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
				<h4 class="modal-title" id="myModalLabel">Terms & Condition Group</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-lg-12">
						<div class="panel panel-default">
							<div style="margin: 20px 10px">	
								<form role="form" id="addalert" name="addalert" method="POST">
									<div class="row">
										<div class="col-lg-12">
											<table class="table table-striped" id="dataTables-example">
												<thead>
													<tr>
														<th>Code</th>
														<th>Description</th>
													</tr>
												</thead>
												<tbody>
													<?php
														while($term_groups=mysql_fetch_object($term_group))
														{
															?>
																<tr class="selectTermsGroup" data-code="<?php echo $term_groups->code; ?>" data-desc="<?php echo $term_groups->term_group; ?>">																
																	<td><?php echo $term_groups->code; ?></td>
																	<td><?php echo $term_groups->term_group; ?></td>
																</tr>	
															<?php
														}
													?>
												</tbody>
											</table>
										</div>
									</div>
									
								</form>    
							</div>
						</div>
					</div>
				</div>
				
			</div>
		</div>
	</div>
</div>
<div class="modal fade in" id="mytermsmodal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
				<h4 class="modal-title" id="myModalLabel">Terms & Condition Group</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-lg-12">
						<div class="panel panel-default">
							<div style="margin: 20px 10px">	
								<form role="form" id="addalert" name="addalert" method="POST">
									<div class="row">
										<div class="col-lg-12">
                                        	<table class="table table-striped" id="dataTables-example">
												<thead>
													<tr>
														<th>Code</th>
														<th>Description</th>
													</tr>
												</thead>
												<tbody>
                                                	<?php
														while($selQuotations=mysql_fetch_object($selQuotation))
														{
															?>
																<tr class="selectQuotation" data-id="<?php echo $selQuotations->quotation_id; ?>" data-code="<?php echo $selQuotations->party_code; ?>" data-desc="<?php echo $selQuotations->party_name; ?>">																
																	<td><?php echo $selQuotations->party_code; ?></td>
																	<td><?php echo $selQuotations->party_name; ?></td>
                                                                    <td><?php echo $selQuotations->quotation_date; ?></td>
																</tr>	
															<?php
														}
													?>
												</tbody>
											</table>
										</div>
									</div>
									
								</form>    
							</div>
                        </div>
                        
					</div>
				</div>
				
			</div>
		</div>
	</div>
</div>

<div class="modal fade in" id="mytemplateModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
				<h4 class="modal-title" id="myModalLabel">Select Template</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-lg-12">
						<div class="panel panel-default">
							<div style="margin: 20px 10px">	
                            	<input type="hidden" id="sales_order_id" name="sales_order_id" value="<?php echo $salesorderid ?>" />
								<form role="form" id="addalert" name="addalert" method="POST">
									<div class="row">
										<div class="col-lg-12">
											<table class="table table-striped" id="dataTables-example">
												<thead>
													<tr>
														<th>Template No.</th>
														<th>Template Name</th>
													</tr>
												</thead>
												<tbody>
													<?php
														while($btmemplatess=mysql_fetch_object($btmemplates))
														{
															?>
																<tr class="selecttemplate" data-id="<?php echo $btmemplatess->template_id; ?>" data-code="<?php echo $selQuotations->party_code; ?>" data-desc="<?php echo $selQuotations->party_name; ?>">																
																	<td><?php echo $btmemplatess->template_id; ?></td>
																	<td><?php echo $btmemplatess->template; ?></td>
																</tr>	
															<?php
														}
													?>
												</tbody>
											</table>
                                            <div class="loadTemplateDetail">
                                            	
                                                
                                            </div>
										</div>
									</div>									
								</form>    
							</div>
                        </div>
                        
					</div>
				</div>
				
			</div>
		</div>
	</div>
</div>

<?php
include_once('../footer.php');
?>
<script>
jQuery(document).ready(function() {
	jQuery('body').delegate('.chageSalesTemp','click',function(){
		jQuery("#id-calcode").val(jQuery(this).attr('data-calcode'));
		jQuery("#id-description").val(jQuery(this).attr('data-description'));
		jQuery("#id-narration").val(jQuery(this).attr('data-value'));
		jQuery("#id-templateid").val(jQuery(this).attr('data-id'));
		jQuery("#id-basicamt").val(jQuery(this).attr('data-amt'));
	});
	
	jQuery('body').delegate('#id-percentage','change',function(){
		var basicamt = parseInt(jQuery("#id-basicamt").val());
		var n = parseInt(jQuery("#id-narration").val());
		var percentage = parseInt(jQuery(this).val());
		var value = n+(basicamt*percentage)/100;
		jQuery("#id-value").val(value);
	})
	jQuery('body').delegate('.applyTemp','click',function(){
		var billtempID = jQuery(this).attr("data-id");
		var ordID = jQuery("#sales_order_id").val();
		$.ajax({
				type: "POST",
				url: "<?php echo SITE_URL.'ajaxcall.php' ?>",
				data: {billtempID:billtempID,ordID:ordID,ajax:'calTempData'},
				cache: false,
					success: function(html) {
						jQuery(".billterns").html(html);
						jQuery('#mytemplateModal').modal("hide");
					}	
			});
	});
	
	jQuery('body').delegate('.selecttemplate','click',function(){
		var temp_id = jQuery(this).attr('data-id');
		$.ajax({
				type: "POST",
				url: "<?php echo SITE_URL.'ajaxcall.php' ?>",
				data: {temp_id:temp_id,ajax:'getTempData'},
				cache: false,
					success: function(html) {
						jQuery(".loadTemplateDetail").html(html);
					}	
			});
	});
	
	jQuery('body').delegate('.tcdelete','click',function(){
		var co = confirm('Are you sure?');
		if (co == true) {
			var delete_id = jQuery(this).attr('data-id');
			$.ajax({
				type: "POST",
				url: "<?php echo SITE_URL.'ajaxcall.php' ?>",
				data: {delete_id:delete_id,ajax:'seltcdelete'},
				cache: false,
					success: function(html) {
						jQuery('.tcdelete'+delete_id).remove();
					}	
			});
		}
		else
		{
			return false;
		}	
	})
	
	jQuery("#saveorderTemp").submit(function(e) {
		var cpval = jQuery( this ).serialize();
		$.ajax({
			type: "POST",
			url: "<?php echo SITE_URL.'ajaxcall.php' ?>",
			data: cpval,
			cache: false,
				success: function(html) {
					jQuery(".billterns").html(html);
				}	
		});
		return false;
	});
	
	jQuery('#saveOrder').submit(function(e) {
		var cpval = jQuery( this ).serialize();
		$.ajax({
			type: "POST",
			url: "<?php echo SITE_URL.'ajaxcall.php' ?>",
			data: cpval,
			cache: false,
				success: function(html) {
					//alert(html);
				}	
		});
		return false;
	});
	
	jQuery('#savecp').submit(function(e) {
		var cpval = jQuery( this ).serialize();
		$.ajax({
			type: "POST",
			url: "<?php echo SITE_URL.'ajaxcall.php' ?>",
			data: cpval,
			cache: false,
				success: function(html) {
					
					jQuery('.ajaxtc').append(html);
					//alert(html);
				}	
		});
		return false;
	});
	
	jQuery('#sonote').submit(function(e) {
		var cpval = jQuery( this ).serialize();
		$.ajax({
			type: "POST",
			url: "<?php echo SITE_URL.'ajaxcall.php' ?>",
			data: cpval,
			cache: false,
				success: function(html) {
						
				}	
		});
		return false;
	});
	
	jQuery('.openpopup1').click(function(){
		$('#mytermsmodal').modal("show");
	});
	
	jQuery('body').delegate('.openPopTemplate','click',function(){
		$('#mytemplateModal').modal("show");	
	})
		
	jQuery('.selectTermsGroup').click(function(){
		jQuery('.termscode').val(jQuery(this).attr('data-code'));
		jQuery('.termsdesc').val(jQuery(this).attr('data-desc'));
		$('#mytermsmodal').modal("hide");
	})
	
	jQuery('body').delegate('.openpopup','click',function(){
		jQuery("#mytermsmodal1").modal("show");	
	});
	
	jQuery(".selectQuotation").click(function(){
		var qid = jQuery(this).attr("data-id");
		var code = jQuery(this).attr("data-code");
		var desc = jQuery(this).attr("data-desc");
		jQuery("#quotation_id").val(qid);
		jQuery("#sell_party_id").val(code);
		jQuery("#sell_party_name").val(desc);
		jQuery("#ship_party_id").val(code);
		jQuery("#ship_party_name").val(desc);
		jQuery("#mytermsmodal1").modal("hide");	
	})
		
	$( ".datepickers" ).datepicker({ dateFormat: 'dd-mm-yy' });
	jQuery(".selcity").change(function(){
		var id = jQuery(this).val();
		if(id != "Select City"){
			var stateid = jQuery('option:selected', this).attr('data-state');
			var countryid = jQuery('option:selected', this).attr('data-country');
			jQuery('.state-'+stateid).prop("selected",true);
			jQuery('.country-'+countryid).prop("selected",true);
			jQuery('.citydisp').hide();
			jQuery('.cState-'+stateid).show();
			jQuery('.statedisp').hide(); 
			jQuery('.sCountry-'+countryid).show();
		}
		else
		{
			jQuery('.selstates option:first').prop('selected','selected');
			jQuery('.selcountry option:first').prop('selected','selected');
			jQuery('.citydisp').show();
			jQuery('.statedisp').show(); 
		}
	})
	jQuery(".selstates").change(function(){
		var id = jQuery(this).val();
		var countryid = jQuery('option:selected', this).attr('data-country');
		if(id != "State State"){
			jQuery('.country-'+countryid).prop("selected",true);
			jQuery('.selcity option:first').prop('selected','selected');
			jQuery('.citydisp').hide();
			jQuery('.cState-'+id).show();
		}
		else
		{
			jQuery('.selcountry option:first').prop('selected','selected');
			jQuery('.selcity option:first').prop('selected','selected');
			jQuery('.citydisp').show();
			jQuery('.statedisp').show();
		}
	})
	
	jQuery(".selcountry").change(function(){
		var id = jQuery(this).val();
		if(id != "Select Country"){
			jQuery('.selcity option:first').prop('selected','selected');
			jQuery('.selstates option:first').prop('selected','selected');
			jQuery('.citydisp').hide();
			jQuery('.cCountry-'+id).show();
			jQuery('.statedisp').hide(); 
			jQuery('.sCountry-'+id).show();
		}
		else
		{
			jQuery('.selcity option:first').prop('selected','selected');
			jQuery('.selstates option:first').prop('selected','selected');
			jQuery('.citydisp').show();
			jQuery('.statedisp').show();
		}
	})
	
	jQuery(".getdetail").click(function(){
		jQuery(".disitemcode").val(jQuery(this).attr("data-code"));
		jQuery(".disdescri").val(jQuery(this).attr('data-description'));
		jQuery(".disp-qty").val(jQuery(this).attr("data-qty"));
		jQuery(".disp-rate").val(jQuery(this).attr("data-rate"));
		jQuery(".disp-amount").val(jQuery(this).attr("data-amount"));
	})
	
});

</script>