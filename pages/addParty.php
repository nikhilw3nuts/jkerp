<?php
include_once('../header.php');
include_once('../dbFunction.php');
$funObj = new dbFunction();

if($_POST['addParty'])
{
	$partyname = $_POST['partyname'];
	$partytype1 = $_POST['partytype1'];
	$partytype2 = $_POST['partytype2'];
	$openingblance = $_POST['openingblance'];
	$debitcredit = $_POST['debitcredit'];
	$creditlimit = $_POST['creditlimit'];	
	$reference = $_POST['references'];
	$referenceNote = $_POST['referenceNote'];
	$companyprofile = $_POST['companyprofile'];
	$country = $_POST['country'];
	$state = $_POST['state'];
	$city = $_POST['city'];
	$pincode = $_POST['pincode'];
	$area = $_POST['area'];
	$phones = $_POST['phones'];
	$fax = $_POST['fax'];
	$email = $_POST['email'];
	$website = $_POST['website'];
	$note = $_POST['note'];
	$address = $_POST['address'];
	$addparty = $funObj->addParty($partyname,$partytype1,$partytype2,$openingblance,$debitcredit,$creditlimit,$reference,$referenceNote,$companyprofile,$country,$state,$city,$pincode,$area,$phones,$fax,$email,$website,$note,$address,$isInquiryActive);
	
	
	
	//add log
	
		$array_field_name['party_code']=$lstinsid;
		$array_field_name['party_name']=$partyname;
		$array_field_name['party_type1_id']=$partytype1;
		$array_field_name['party_type2_id']=$partytype2;
		$array_field_name['reference_id']=$openingblance;
		$array_field_name['reference_text']=$debitcredit;
		$array_field_name['opening_balance']=$creditlimit;
		$array_field_name['credit/debit']=$reference;
		$array_field_name['credit_limit']=$referenceNote;
		$array_field_name['company_profile']=$companyprofile;
		$array_field_name['address']=$address;
		$array_field_name['city']=$city;
		$array_field_name['state']=$_REQUEST['country'];
		$array_field_name['country']=$country;
		$array_field_name['pincode']=$pincode;
		$array_field_name['area']=$area;
		$array_field_name['fax']=$fax;
		$array_field_name['phones']=$phones;
		$array_field_name['email']=$email;
		$array_field_name['website']=$website;
		$array_field_name['note']=$note;
		$array_field_name['status']=$_POST['isInquiryActive'];
		$array_field_name['created_by']=$_SESSION['username'];
		$array_field_name['updated_by']=$_SESSION['username'];
		$array_field_name['created_on']=$creadtedts; 
		$array_field_name['log_date']=date('d-m-y h:i:s a'); 
		$array_field_name['user_id']=$_SESSION['uid'];
		$action="1"; // $action=  add=1 edit=2 delete=3 
		$table_neme='z_party_master'; 
		maintain_log($array_field_name,$table_neme,$action); //call maintain_log function
	//add log end
		
		if($addparty!= "")
		{
			$redirect = SITE_URL."pages/partyDetail.php?id=".$addparty."";
			?>
				<script>
					window.location.assign("<?php echo $redirect; ?>")
				</script>
			<?php
		}
	
	header("location:http://192.168.1.51/nikhil/projects/jaykhodiyar/pages/partyDetail.php?id=3");
}
$partytype1 = $funObj->getTableData('partytype1');
$partytype2 = $funObj->getTableData('partytype2');
$reference = $funObj->getTableData('reference');
$countrys = $funObj->getTableData('country','ORDER BY country ASC');
$states = $funObj->getTableData('state','ORDER BY state ASC');
$citys = $funObj->getTableData('city','ORDER BY city ASC');
$countrys1 = $funObj->getTableData('country','ORDER BY country ASC');
$states1 = $funObj->getTableData('state','ORDER BY state ASC');
$citys1 = $funObj->getTableData('city','ORDER BY city ASC');
?>
<div id="page-wrapper">
    <div class="container">
	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">Add Party</h1>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-default">
				<div class="panel-body">
					<form role="form" name="addParty" method="POST">
						<div class="row">
							<div class="col-lg-6">
								<div class="form-group">
									<label>Party Name</label>
									<input type="text" name="partyname" required class="form-control input-lg-1">
								</div>
							</div>
							<div class="col-lg-6">
								<div class="form-group">
									<label>Active?</label>
									<input type="checkbox" name="isInquiryActive" checked value="1">
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-lg-12">
								<div class="form-group">
                                    <label>Party Type</label>
                                    <select class="form-control" name="partytype1">
                                        <?php
                                            while($partyt1=mysql_fetch_object($partytype1))
                                            {
                                                ?>
                                                <option value="<?php echo $partyt1->partytype1_id; ?>"><?php echo $partyt1->partytype1; ?></option>
                                                <?php
                                            }
                                        ?>	
                                    </select>
                                    <select class="form-control" name="partytype2">
                                        <?php
                                            while($partyty2=mysql_fetch_object($partytype2))
                                            {
                                                ?>
                                                <option value="<?php echo $partyty2->partytype2_id; ?>"><?php echo $partyty2->partytype2; ?></option>
                                                <?php
                                            }
                                        ?>	
                                    </select>
								</div>
							</div>
						</div>	
						<div class="row">
							<div class="col-lg-6">
								<div class="form-group">
									<div class="form-group">
										<label>Opening Balance</label>
										<div>
											<input type="text" name="openingblance" required class="form-control input-text-sm">
											<select class="form-control input-text-sm" name="debitcredit">
												<option value="0">Debit</option>
												<option value="1">Credit</option>
											</select>
										</div>
									</div>
								</div>
							</div>
							<div class="col-lg-6">
								<div class="form-group">
									<div class="form-group">
										<label>Credit Limit</label>
										<input type="text" name="creditlimit" required class="form-control">
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-lg-6">
								<div class="form-group">
									<label>Reference</label>
									<select class="form-control input-text-sm" name="references">
										<option>Select Reference</option>
										<?php
											while($references=mysql_fetch_object($reference))
											{
												?>
												<option value="<?php echo $references->referenceid; ?>"><?php echo $references->referenceby; ?></option>
												<?php
											}
										?>
									</select>
                                    <input type="text" name="referenceNote" required class="form-control input-text-sm">
								</div>
							</div>
							<div class="col-lg-6">
								<div class="form-group">
                                	<label>Company Profile</label>
                                	<input type="text" name="companyprofile" required class="form-control">
								</div>
							</div>
						</div>
						<div class="row" style="padding-top:20px">
							<div class="col-lg-6">
								<div class="form-group">
									<label>Address</label>
									<textarea rows="3" name="address" class="form-control"></textarea>
								</div>
							</div>
							<div class="col-lg-6">
								<div class="form-group">
									<label>Country</label>
									<select class="form-control selcountry" name="country">
										<option>Select Country</option>
										<?php
											while($country=mysql_fetch_object($countrys))
											{
												?>
												<option class="countrydisp country-<?php echo $country->countryid; ?>" value="<?php echo $country->countryid; ?>"><?php echo $country->country; ?></option>
												<?php
											}
										?>
									</select>
                                    <label>State</label>
									<select class="form-control selstates" name="state">
										<option>State State</option>
										<?php
											while($state=mysql_fetch_object($states))
											{
												?>
												<option class="statedisp sCountry-<?php echo $state->countryid; ?> state-<?php echo $state->stateid; ?>" data-country="<?php echo $state->countryid; ?>" value="<?php echo $state->stateid; ?>"><?php echo $state->state; ?></option>
												<?php
											}
										?>
									</select>
								</div>
								<div class="form-group">
									<label>City</label>
									<select class="form-control selcity" name="city">
										<option>Select City</option>
										<?php
											while($city=mysql_fetch_object($citys))
											{
												$countryid = mysql_fetch_array($funObj->getDataById('country',"countryid='".$city->stateid."'"));
												?>
												<option class="citydisp cCountry-<?php echo $countryid['countryid']; ?> cState-<?php echo $city->stateid; ?> city-<?php echo $city->cityid; ?>" data-state="<?php echo $city->stateid; ?>" data-country="<?php echo $countryid['countryid']; ?>"  value="<?php echo $city->cityid; ?>"><?php echo $city->city; ?></option>
												<?php
											}
										?>
									</select>
                                    <label>Pincode</label>
									<input type="text" name="pincode" required class="form-control">
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-lg-6">
								<div class="form-group">
									<label>Phones</label>
									<textarea rows="3" name="phones" class="form-control"></textarea>
								</div>
							</div>
							<div class="col-lg-6">
								<div class="form-group">
									<label>Fax</label>
									<input type="text" name="fax" required class="form-control">
                                    <label>Email</label>
									<input type="email" name="email" required class="form-control">
								</div>
								<div class="form-group">
									<label>Website</label>
									<input type="url" name="website" required class="form-control">
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-lg-12">
								<label>Note</label>
								<textarea rows="3" name="note" class="form-control"></textarea>
							</div>	
						</div>
						<div class="row" style="padding-top:30px">
							<div class="col-lg-12">
								<input class="btn btn-success" type="submit" name="addParty" value="Add Party" />
							</div>
						</div>		
						
					</form>
				</div>
			</div>
		</div>
	</div>
    </div>
</div>
<?php
include_once('../footer.php');
?>
<script>
jQuery(document).ready(function() {
	jQuery(".selcity").change(function(){
		var id = jQuery(this).val();
		if(id != "Select City"){
			var stateid = jQuery('option:selected', this).attr('data-state');
			var countryid = jQuery('option:selected', this).attr('data-country');
			jQuery('.state-'+stateid).prop("selected",true);
			jQuery('.country-'+countryid).prop("selected",true);
			jQuery('.citydisp').hide();
			jQuery('.cState-'+stateid).show();
			jQuery('.statedisp').hide(); 
			jQuery('.sCountry-'+countryid).show();
		}
		else
		{
			jQuery('.selstates option:first').prop('selected','selected');
			jQuery('.selcountry option:first').prop('selected','selected');
			jQuery('.citydisp').show();
			jQuery('.statedisp').show(); 
		}
	})
	jQuery(".selstates").change(function(){
		var id = jQuery(this).val();
		var countryid = jQuery('option:selected', this).attr('data-country');
		if(id != "State State"){
			jQuery('.country-'+countryid).prop("selected",true);
			jQuery('.selcity option:first').prop('selected','selected');
			jQuery('.citydisp').hide();
			jQuery('.cState-'+id).show();
		}
		else
		{
			jQuery('.selcountry option:first').prop('selected','selected');
			jQuery('.selcity option:first').prop('selected','selected');
			jQuery('.citydisp').show();
			jQuery('.statedisp').show();
		}
	})
	
	jQuery(".selcountry").change(function(){
		var id = jQuery(this).val();
		if(id != "Select Country"){
			jQuery('.selcity option:first').prop('selected','selected');
			jQuery('.selstates option:first').prop('selected','selected');
			jQuery('.citydisp').hide();
			jQuery('.cCountry-'+id).show();
			jQuery('.statedisp').hide(); 
			jQuery('.sCountry-'+id).show();
		}
		else
		{
			jQuery('.selcity option:first').prop('selected','selected');
			jQuery('.selstates option:first').prop('selected','selected');
			jQuery('.citydisp').show();
			jQuery('.statedisp').show();
		}
	})
	
	
	jQuery('#dataTables-example').DataTable({
			responsive: true
	});
	
});

</script>
	
