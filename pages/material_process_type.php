<?php
include_once('../header.php');

include_once('../dbFunction.php');
$funObj = new dbFunction();
if($_POST['addMaterialProc'])
{
	$material_process_type = $_POST['material_process_type'];
	$item_group = $_POST['item_group'];
	$checkePartyType = $funObj->checkePartyType($material_process_type);
	$tablename = "material_process_type";
	$fieldname = "material_process_type";
	//checkeAllreadyExist($tablename,$fieldname,$value)
	$checkeInTable = $funObj->checkeAllreadyExist($tablename,$fieldname,$material_process_type);
	if($checkeInTable)
	{
		echo "<script>alert('Material Process Type is aleady exist!')</script>";
	}
	else
	{
		$addParty = $funObj->addMaterialProcessType($material_process_type);
	}
}
$material_process_type = $funObj->getTableData('material_process_type');
?>
<div id="page-wrapper">
    <div class="container">
	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">Material Process Type</h1>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-7">
			<div class="panel panel-default">
				<div class="panel-heading clearfix">
					Material Process Type
				</div>				
				<div class="panel-body">
					<div class="dataTable_wrapper">
						<table class="table table-striped table-bordered table-hover" id="dataTables-example">
							<thead>
								<tr>
									<th>Action</th>
									<th>Material Process Type</th>
								</tr>
							</thead>
							<tbody>
								<?php 
									while($material_process_types=mysql_fetch_object($material_process_type))
									{
										?>
											<tr class="odd gradeX">
												<td><a onclick="return confirm('Are you sure?');" href="<?php echo SITE_URL.'pages/delete.php?redirect=material_process_type&table=material_process_type&field=material_process_type_id&id='.$material_process_types->material_process_type_id; ?>">Delete</a></td>
												<td><?php echo $material_process_types->material_process_type; ?></td>
											</tr>	
										<?php
									} 
								?>								
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
		<div class="col-lg-5">
			<div class="panel panel-default">
				<div class="panel-heading clearfix">
					Add Material Process Type
				</div>
				<div style="margin:20px">	
				<form role="form" name="addMaterialProc" method="POST">
					<div class="form-group">
						<label>Material Process Type</label>
						<input type="text" name="material_process_type" required class="form-control">
					</div>
					<input class="btn btn-success btn-block" type="submit" name="addMaterialProc" value="Add Material Process Type" />
				</form>
				</div>
			</div>
		</div>
	</div>
    </div>    
</div>
<?php
include_once('../footer.php');
?>
<script>
jQuery(document).ready(function() {
	jQuery('#dataTables-example').DataTable({
			responsive: true
	});
});
</script>
