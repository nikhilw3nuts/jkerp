<?php
include_once('../header.php');

include_once('../dbFunction.php');
$funObj = new dbFunction();
if($_POST['addParttype'])
{
	$partytype = $_POST['partytype'];
	$checkePartyType = $funObj->checkePartyType($partytype);
	if($checkePartyType)
	{
		echo "<script>alert('Party Type aleady exist!')</script>";
	}
	else
	{
		$addParty = $funObj->addPartyType($partytype);
	}
}
$partytype = $funObj->getTableData('partytype1');
?>
<div id="page-wrapper">
    <div class="container">
	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">Party Type</h1>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-7">
			<div class="panel panel-default">
				<div class="panel-heading clearfix">
					Party Type Detail
				</div>				
				<div class="panel-body">
					<div class="dataTable_wrapper">
						<table class="table table-striped table-bordered table-hover" id="dataTables-example">
							<thead>
								<tr>
									<th>Action</th>
									<th>Party Type</th>
								</tr>
							</thead>
							<tbody>
								<?php 
									while($partytypes=mysql_fetch_object($partytype))
									{
										?>
											<tr class="odd gradeX">
												<td><a href="<?php echo SITE_URL.'pages/delete.php?redirect=partytype1&table=partytype1&field=partytype1_id&id='.$partytypes->partytype1_id; ?>">Delete</a></td>
												<td><?php echo $partytypes->partytype1; ?></td>
											</tr>	
										<?php
									} 
								?>								
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
		<div class="col-lg-5">
			<div class="panel panel-default">
				<div class="panel-heading clearfix">
					Add Party Type
				</div>
				<div style="margin:20px">	
				<form role="form" name="addParttype" method="POST">
					<div class="form-group">
						<label>Party Type</label>
						<input type="text" name="partytype" required class="form-control">
					</div>
					<input class="btn btn-success btn-block" type="submit" name="addParttype" value="Add Party Type" />
				</form>
				</div>
			</div>
		</div>
	</div>
    </div>    
</div>
<?php
include_once('../footer.php');
?>
<script>
jQuery(document).ready(function() {
	jQuery('#dataTables-example').DataTable({
			responsive: true
	});
});
</script>
