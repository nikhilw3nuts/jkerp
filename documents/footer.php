<?php
$user= $funObj->getTableData('user');
$alert_type = $funObj->getTableData('alert_type_master');
?>
<div class="modal fade in" id="myModalalert" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
				<h4 class="modal-title" id="myModalLabel">Add Alert</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-lg-12">
						<div class="panel panel-default">
							<div style="margin: 20px 10px">	
								<form role="form" id="addalert" name="addalert" method="POST">
									<input type="hidden" name="pagename" class="thispage" value="" />
									<input type="hidden" name="thisid" class="thisid" value="" />
									<input type="hidden" name="ajax" value="alert" />
									<div class="row">
										<div class="col-lg-12">
											<div class="form-group">
												<label>Reminder Time</label>
												<input type="text" name="reminder" required class="form-control datepickers" placeholder="dd-mm-yyyy"> 
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-lg-6"> 
											<div class="form-control">
												<select name="time[]">
												  <option value="01">01</option>
												  <option value="02">02</option>
												  <option value="03">03</option>
												  <option value="04">04</option>
												  <option value="05">05</option>
												  <option value="06">06</option>
												  <option value="07">07</option>
												  <option value="08">08</option>
												  <option value="09">09</option>
												  <option value="10">10</option>
												  <option value="11">11</option>
												  <option value="12">12</option>
												</select> 
												<select name="time[]">
												  <option value="01">01</option>
												  <option value="02">02</option>
												  <option value="03">03</option>
												  <option value="04">04</option>
												  <option value="05">05</option>
												  <option value="06">06</option>
												  <option value="07">07</option>
												  <option value="08">08</option>
												  <option value="09">09</option>
												  <option value="10">10</option>
												  <option value="11">11</option>
												  <option value="12">12</option>
												  <option value="13">13</option>
												  <option value="14">14</option>
												  <option value="15">15</option>
												  <option value="16">16</option>
												  <option value="17">17</option>
												  <option value="18">18</option>
												  <option value="19">19</option>
												  <option value="20">20</option>
												  <option value="21">21</option>
												  <option value="22">22</option>
												  <option value="23">23</option>
												  <option value="24">24</option>
												  <option value="25">25</option>
												  <option value="26">26</option>
												  <option value="27">27</option>
												  <option value="28">28</option>
												  <option value="29">29</option>
												  <option value="30">30</option>
												  <option value="31">31</option>
												  <option value="32">32</option>
												  <option value="33">33</option>
												  <option value="34">34</option>
												  <option value="35">35</option>
												  <option value="36">36</option>
												  <option value="37">37</option>
												  <option value="38">38</option>
												  <option value="39">39</option>
												  <option value="40">40</option>
												  <option value="41">41</option>
												  <option value="42">42</option>
												  <option value="43">43</option>
												  <option value="44">44</option>
												  <option value="45">45</option>
												  <option value="46">46</option>
												  <option value="47">47</option>
												  <option value="48">48</option>
												  <option value="49">49</option>
												  <option value="50">50</option>
												  <option value="51">51</option>
												  <option value="52">52</option>
												  <option value="53">53</option>
												  <option value="54">54</option>
												  <option value="55">55</option>
												  <option value="56">56</option>
												  <option value="57">57</option>
												  <option value="58">58</option>
												  <option value="59">59</option>
												  <option value="60">60</option>
												</select> 
												<select name="time[]">
													<option value="AM">AM</option>
													<option value="PM">PM</option>
												</select>
											 </div>
										 </div>
										 <div class="col-lg-6">
											<div class="form-group">
												<input type="Checkbox" name="allevent" value="1" >
												<label>All Day Event</label><br />
												<input type="submit" value="Dismiss" name="dismiss" class="btn btn-success"/>
											</div>
										</div>
									 </div>
									<div class="row">
										<div class="col-lg-6">
											<div class="form-group">
												<label>Activity</label>
												<input type="text" name="activity" value="" required class="form-control inquiry_no">
											</div>
										</div>
										<div class="col-lg-6">
											<div class="form-group">
												<label>Category</label>
												<select name="cate" class="form-control">
													<?php
														while($alert_types=mysql_fetch_object($alert_type))
														{
															?>
																 <option value="<?php echo $alert_types->alert_type_id; ?>"><?php echo $alert_types->alert_type; ?></option>
															<?php
														}
													?>
												</select>
											</div>
										</div>
									 </div>		
									<div class="row">
										<div class="col-lg-6">
											<div class="form-group">
												<label>Doc. No.</label>
												<input type="text" name="doc" required class="form-control inquiry_no" value="">
											</div>
											<div class="form-group">
												<label>Type</label>
												<input type="text" name="altp" required class="form-control inquiry_no">
											</div>
										</div>
										
										<div class="col-lg-6">
											<div class="form-group">
												<label>Description</label>
												<textarea name="desc" rows="4" cols="30" class="form-control"></textarea>
											</div>
										</div>
								   </div>
									<div class="row">
										<div class="col-lg-6">
											<div class="form-group">
												<label>Activity By</label>
												<input type="text" name="actby" value="<?php echo $_SESSION['username']; ?>" required class="form-control inquiry_no">
												<br>
												<label>Activity To</label>
												 <select name="actto[]" multiple="multiple" class="form-control">
													 <?php
														while($users=mysql_fetch_object($user))
														{
															?>
																 <option value="<?php echo $users->userid; ?>"><?php echo $users->username; ?></option>
															<?php
														}
													?>
												</select> 
											</div>
										</div>
										<div class="col-lg-6">
											<div class="form-group">
												<label>Reply</label>
												<textarea name="rep" rows="4" cols="30" class="form-control"></textarea>
											</div>
										</div>
								   </div>
									<div class="row" style="padding-top:30px">
										<div class="col-lg-12">
											<input class="btn btn-success" type="submit" name="addalert" value="Save" />
											<input class="btn btn-success" type="clear" name="Cancel" value="Cancel" />	
										</div>
									</div>
								</form>    
							</div>
						</div>
					</div>
				</div>
				
			</div>
		</div>
	</div>
</div>



<button type="button" style="display:none" class="ifrm-opn btn btn-primary btn-lg" data-toggle="modal" data-target="#myModal">
  ADD/EDIT/DELETE LOGS
</button>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h4 class="modal-title" id="myModalLabel">Modal title</h4>
      </div>
      <div class="modal-body replace_content">
		<iframe id="iframe" src="<?php echo SITE_URL.'pages/log_iframe.php'; ?>" height="500px" width="570px"></iframe>
		
      </div>
      <!--div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div-->
    </div>
  </div>
</div>

</div>
    <script src="<?php echo SITE_URL; ?>bower_components/jquery/dist/jquery.min.js"></script>
	<script src="<?php echo SITE_URL; ?>bower_components/lightbox/lightbox-plus-jquery.min.js"></script>
    <script src="<?php echo SITE_URL; ?>bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="<?php echo SITE_URL; ?>bower_components/metisMenu/dist/metisMenu.min.js"></script>
    <script src="<?php echo SITE_URL; ?>bower_components/raphael/raphael-min.js"></script>
    <script src="<?php echo SITE_URL; ?>bower_components/jquery-ui/jquery-ui.js"></script>
	<script src="<?php echo SITE_URL; ?>bower_components/datatables/media/js/jquery.dataTables.min.js"></script>
    <script src="<?php echo SITE_URL; ?>bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js"></script>
    <script src="<?php echo SITE_URL; ?>dist/js/sb-admin-2.js"></script>
	<script>
	$(document).ready(function(){
		  $( ".datepickers" ).datepicker({ dateFormat: 'dd-mm-yy' }); 
		$(".alertmodel").click(function(){
			$(".thisid").val($(this).attr("data-id"));
			$(".thispage").val($(this).attr("data-page"));
			$("#myModalalert").modal("show")
			});
		jQuery("#addalert").submit(function(e) {	
			var cpval = jQuery( this ).serialize();
			$.ajax({
				type: "POST",
				url: "<?php echo SITE_URL.'ajaxcall.php' ?>",
				data: cpval,
				cache: false,
					success: function(html) {
						alert(html);
					}	
			});
			return false;       
		});	
	})
 


jQuery(document).ready(function(){
	//aa badhai ma avashe
	 var iframe_url="";
	jQuery(".log_content").click(function(){
		
		 var field=jQuery(this).attr("data-field-name");
		 var field_table=jQuery(this).attr("data-table");
		 var id=jQuery(this).attr("data-id");
		 
		 var iframe_url = "<?php echo SITE_URL."pages/log_iframe.php"; ?>?fieldname="+field+"&tablenme="+field_table+"&id="+id;
		/*  alert(iframe_url); */
		 $('#iframe').attr('src', iframe_url);
		 jQuery(".ifrm-opn").click();
		 
	});
	});
</script>

<script src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.1/js/select2.min.js"></script>

<script>
jQuery(document).ready(function(){
  function setCurrency (currency) {
      if (!currency.id) { return currency.text; }
        var $currency = $('<span class="glyphicon glyphicon-' + currency.element.value + '">' + currency.text + '</span>');
        return $currency;
    };
    jQuery(".templatingSelect2").select2({
        placeholder: "Select Option", //placeholder
        templateResult: setCurrency,
        templateSelection: setCurrency
    });
})
</script>
</body>
</html>
