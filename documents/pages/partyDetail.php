<?php
include_once('../header.php');
include_once('../dbFunction.php');
$funObj = new dbFunction();
$partyid = $_GET['id'];

if($_POST['editParty'])
{
	$partyname = $_POST['partyname'];
	$partytype1 = $_POST['partytype1'];
	$partytype2 = $_POST['partytype2'];
	$openingblance = $_POST['openingblance'];
	$debitcredit = $_POST['debitcredit'];
	$creditlimit = $_POST['creditlimit'];	
	$reference = $_POST['references'];
	$referenceNote = $_POST['referenceNote'];
	$companyprofile = $_POST['companyprofile'];
	$country = $_POST['country'];
	$state = $_POST['state'];
	$city = $_POST['city'];
	$pincode = $_POST['pincode'];
	$area = $_POST['area'];
	$phones = $_POST['phones'];
	$fax = $_POST['fax'];
	$email = $_POST['email'];
	$website = $_POST['website'];
	$note = $_POST['note'];
	$address = $_POST['address'];
	$partyState = $_POST['partyState'];
	$addparty = $funObj->editParty($partyid,$partyname,$partytype1,$partytype2,$openingblance,$debitcredit,$creditlimit,$reference,$referenceNote,$companyprofile,$country,$state,$city,$pincode,$area,$phones,$fax,$email,$website,$note,$address,$partyState);	
	$isInquiryActive = ($_POST['isInquiryActive'] != "") ? $_POST['isInquiryActive'] : 0;
	
	
	
	//edit log
	
		$array_field_name['party_code']=$_REQUEST['id'];
		$array_field_name['party_name']=$partyname;
		$array_field_name['party_type1_id']=$partytype1;
		$array_field_name['party_type2_id']=$partytype2;
		$array_field_name['reference_id']=$openingblance;
		$array_field_name['reference_text']=$debitcredit;
		$array_field_name['opening_balance']=$creditlimit;
		$array_field_name['credit/debit']=$reference;
		$array_field_name['credit_limit']=$referenceNote;
		$array_field_name['company_profile']=$companyprofile;
		$array_field_name['address']=$address;
		$array_field_name['city']=$city;
		$array_field_name['state']=$_REQUEST['country'];
		$array_field_name['country']=$country;
		$array_field_name['pincode']=$pincode;
		$array_field_name['area']=$area;
		$array_field_name['fax']=$fax;
		$array_field_name['phones']=$phones;
		$array_field_name['email']=$email;
		$array_field_name['website']=$website;
		$array_field_name['note']=$note;
		$array_field_name['status']=$_POST['isInquiryActive'];
		$array_field_name['created_by']=$_SESSION['username'];
		$array_field_name['updated_by']=$_SESSION['username'];
		$array_field_name['created_on']=$creadtedts; 
		$array_field_name['log_date']=date('d-m-y h:i:s a'); 
		$array_field_name1['user_id']=$_SESSION['uid'];
		$action="2"; // $action=  add=1 edit=2 delete=3 
		$table_neme='z_party_master'; 
		maintain_log($array_field_name,$table_neme,$action); //call maintain_log function
	
	//edit log end
	
}

$getParty = mysql_fetch_array($funObj->getDataById("party_master","party_code='".$partyid."'"));
$partytype1 = $funObj->getTableData('partytype1');
$partytype2 = $funObj->getTableData('partytype2');
$reference = $funObj->getTableData('reference');
$countrys = $funObj->getTableData('country','ORDER BY country ASC');
$states = $funObj->getTableData('state','ORDER BY state ASC');
$citys = $funObj->getTableData('city','ORDER BY city ASC');
$countrys1 = $funObj->getTableData('country','ORDER BY country ASC');
$states1 = $funObj->getTableData('state','ORDER BY state ASC');
$citys1 = $funObj->getTableData('city','ORDER BY city ASC');
$countrys2 = $funObj->getTableData('country','ORDER BY country ASC');
$states2 = $funObj->getTableData('state','ORDER BY state ASC');
$citys2 = $funObj->getTableData('city','ORDER BY city ASC');
$designation = $funObj->getTableData('city');
$department = $funObj->getTableData('department');
$designation = $funObj->getTableData('designation');
$term_group = $funObj->getTableData('term_group');
$cp_row = $funObj->getDataById("contact_person","party_code='".$partyid."'");
$wp_row = mysql_fetch_array($funObj->getDataById("workaddress","partycode='".$partyid."'"));
$get_tc = $funObj->getDataById("party_terms_condition","partycode='".$partyid."'");
$get_te = mysql_fetch_array($funObj->getDataById("tax_excise","partycode='".$partyid."'"));
$get_bt = mysql_fetch_array($funObj->getDataById("billing_terms","partycode='".$partyid."'"));
$get_party_cp = mysql_fetch_array($funObj->getDataById("party_company_profile","party_id='".$partyid."'"));
$get_party_da = mysql_fetch_array($funObj->getDataById("party_delivery_address","party_id='".$partyid."'"));
?>
<div id="page-wrapper">
    <div class="container">
	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">Add Party</h1>
			<h4><p><a class="log_content" data-id="<?php echo $_GET['id']; ?>" data-table="z_party_master" data-field-name="party_code" href="javascript:void(0)">Log</a></p></h4>
			<button class="btn btn-primary alertmodel" data-page="party detail" data-id="<?php echo $partyid; ?>">Add Alert</button>
		</div>		
	</div>
	<div class="row">
		<div class="col-lg-12">
        	<div class="panel panel-default">
				<div class="panel-body">
					<form role="form" name="editParty" method="POST" class="party-form">
						<div class="row">
							<div class="col-lg-6">
								<div class="form-group">
									<label>Party Code</label>
									<input type="text" disabled name="partycode" value="<?php echo $getParty['party_code']; ?>" required class="form-control">
								</div>
							</div>
							<div class="col-lg-6">
								<div class="form-group">
									<label>Party Name</label>
									<input type="text" name="partyname" value="<?php echo $getParty['party_name']; ?>" required class="form-control">
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-lg-6">
								<div class="form-group">
									<label>Party Type</label>
										<select class="form-control templatingSelect2" name="partytype1">
											<?php
												while($partyt1=mysql_fetch_object($partytype1))
												{
													?>
													<option <?php echo ($getParty['party_type1_id'] == $partyt1->partytype1_id) ? 'selected' : '' ; ?> value="<?php echo $partyt1->partytype1_id; ?>"><?php echo $partyt1->partytype1; ?></option>
													<?php
												}
											?>	
										</select>
									</div>
								</div>
							<div class="col-lg-6">
								<div class="form-group">
									<label>Party Type</label>
									<div>
										<select class="form-control templatingSelect2" name="partytype2">
											<?php
												while($partyty2=mysql_fetch_object($partytype2))
												{
													?>
													<option <?php echo ($getParty['party_type2_id'] == $partyty2->partytype2_id) ? 'selected' : '' ; ?>  value="<?php echo $partyty2->partytype2_id; ?>"><?php echo $partyty2->partytype2; ?></option>
													<?php
												}
											?>	
										</select>
									</div>
								</div>
							</div>
						</div>	
						<div class="row">
							<div class="col-lg-6">
								<div class="form-group">
                                    <label>Opening Balance</label>
                                    <div class="custome-div">
                                        <input type="text" value="<?php echo $getParty['opening_balance'] ?>" name="openingblance" required class="form-control input-text-sm">
                                        <select class="form-control input-text-sm templatingSelect2" name="debitcredit">
                                            <option <?php echo ($getParty['credit/debit'] == 0) ? 'selected' : ''; ?> value="0">Debit</option>
                                            <option <?php echo ($getParty['credit/debit'] == 1) ? 'selected' : ''; ?> value="1">Credit</option>
                                        </select>
                                    </div>
								</div>
							</div>
							<div class="col-lg-6">
								<div class="form-group">
                                    <label>Credit Limit</label>
                                    <input type="text" value="<?php echo $getParty['credit_limit'] ?>" name="creditlimit" required class="form-control">
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-lg-6">
								<div class="form-group">
									<label>Reference</label>
                                    <div class="custome-div">
									<select class="form-control templatingSelect2" name="references">
										<option>Select Reference</option>
										<?php
											while($references=mysql_fetch_object($reference))
											{
												?>
												<option <?php echo ($getParty['reference_id'] == $references->referenceid) ? 'selected' : ''; ?> value="<?php echo $references->referenceid; ?>"><?php echo $references->referenceby; ?></option>
												<?php
											}
										?>
									</select>
                                    <input type="text" name="referenceNote" required class="form-control input-text-sm" value="<?php echo $getParty['reference_text']; ?>">
                                    </div>
								</div>
							</div>
							<div class="col-lg-6">
								<div class="form-group">
									<label>Company Profile</label>
                                    <input type="text" name="companyprofile" required class="form-control" value="<?php echo $getParty['company_profile']; ?>">
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-lg-6">
								<div class="form-group">
									<label>Address</label>
									<textarea rows="3" name="address" id="p_address" class="form-control"><?php echo $getParty['address']; ?></textarea>
								</div>
							</div>
							<div class="col-lg-6">
								<div class="form-group">
									<label>Country</label>
									<select class="form-control selcountry templatingSelect2" name="country">
										<option>Select Country</option>
										<?php
											while($country=mysql_fetch_object($countrys))
											{
												?>
												<option <?php echo ($getParty['country'] == $country->countryid) ? 'selected' : ''; ?> class="countrydisp country-<?php echo $country->countryid; ?>" value="<?php echo $country->countryid; ?>"><?php echo $country->country; ?></option>
												<?php
											}
										?>
									</select>
                                    </div>
                                    <div class="form-group">
                                    <label>State</label>
									<select class="form-control selstates templatingSelect2" name="state">
										<option>State State</option>
										<?php
											while($state=mysql_fetch_object($states))
											{
												?>
												<option <?php echo ($getParty['state'] == $state->stateid) ? 'selected' : ''; ?> class="statedisp sCountry-<?php echo $state->countryid; ?> state-<?php echo $state->stateid; ?>" data-country="<?php echo $state->countryid; ?>" value="<?php echo $state->stateid; ?>"><?php echo $state->state; ?></option>
												<?php
											}
										?>
									</select>
								</div>
								<div class="form-group">
                                    <label>City</label>
									<select class="form-control selcity templatingSelect2" name="city">
										<option>Select City</option>
										<?php
											while($city=mysql_fetch_object($citys))
											{
												$countryid = mysql_fetch_array($funObj->getDataById('country',"countryid='".$city->stateid."'"));
												?>
												<option <?php echo ($getParty['city'] == $city->cityid) ? 'selected' : ''; ?> class="citydisp cCountry-<?php echo $countryid['countryid']; ?> cState-<?php echo $city->stateid; ?> city-<?php echo $city->cityid; ?>" data-state="<?php echo $city->stateid; ?>" data-country="<?php echo $countryid['countryid']; ?>"  value="<?php echo $city->cityid; ?>"><?php echo $city->city; ?></option>
												<?php
											}
										?>
									</select>
                                    </div>
                                    <div class="form-group">
                                    <label>Pincode</label>
									<input type="text" value="<?php echo $getParty['pincode'] ?>" name="pincode" required class="form-control">
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-lg-6">
								<div class="form-group">
									<label>Phones</label>
									<textarea rows="3" name="phones" id="p_phones" class="form-control"><?php echo $getParty['phones'] ?></textarea>
								</div>
							</div>
							<div class="col-lg-6">
								<div class="form-group">
									<label>Fax</label>
									<input type="text" id="p_fax" value="<?php echo $getParty['fax'] ?>" name="fax" required class="form-control">
                                    <label>Email</label>
									<input type="email" value="<?php echo $getParty['email'] ?>" name="email" id="p_email" required class="form-control">
								</div>
								<div class="form-group">
									<label>Website</label>
									<input type="url" value="<?php echo $getParty['website'] ?>" name="website" id="p_website" required class="form-control">
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-lg-6">
								<label>Note</label>
								<textarea rows="3" id="p_note" name="note" class="form-control"><?php echo $getParty['note']; ?></textarea>
							</div>	
							<div class="col-lg-6">
								<div class="form-group">
                                    <label>Active?</label>
                                    <input type="checkbox" name="partyState" <?php echo ($getParty['status'] == 1) ? 'checked' : '' ; ?> value="1">
								</div>
							</div>
						</div>
						<div class="row" style="padding-top:30px">
							<div class="col-lg-12">
								<input class="btn btn-success" type="submit" name="editParty" value="Edit Party" />
							</div>
						</div>		
						
					</form>
				</div>
				<div class="panel-body">
					<div class="row">
						<div class="col-lg-12">
							<ul class="nav nav-tabs">
								<li><a data-toggle="tab" href="#tabcontactperason" aria-expanded="false">Contact Person</a></li>
								<li><a data-toggle="tab" href="#tabaddresswork" aria-expanded="true">Address Work</a></li>
								<li><a data-toggle="tab" href="#tabterms" aria-expanded="false">Terms & conditions</a></li>
								<li><a data-toggle="tab" href="#tabtax">Tax & Excise</a></li>
								<li><a data-toggle="tab" href="#tabgeneral">General</a></li>
								<li><a data-toggle="tab" href="#tabcompanyprofile">Company Profile</a></li>
								<li><a data-toggle="tab" href="#tabbillterms">Billing Terms</a></li>
								<li><a data-toggle="tab" href="#tabdeliveryaddress">Delivery Address</a></li>
							</ul>
							<div class="tab-content">
								<div id="tabcontactperason" class="tab-pane fade">
									<h4>Contact Person</h4>
									<h4><p><a class="log_content1" target="_blank" href="<?php echo SITE_URL ?>log/party_contact_person.php">Log</a></p></h4>
									<div class="panel-body">
										<div class="dataTable_wrapper">
											<table class="table table-striped table-bordered table-hover" id="dataTables-example">
												<thead>
													<tr>
														<th>Action</th>
														<th>Priority</th>
														<th>Name</th>
														<th>Designation</th>
														<th>Department</th>
														<th>Phone No.</th>
														<th>Status</th>
													</tr>
												</thead>
												<tbody class="ajaxcp">
													<?php 
														while($cp_res=mysql_fetch_object($cp_row))
														{
															$designationid = mysql_fetch_array($funObj->getDataById('designation',"designation_id='".$cp_res->designation."'"));
															$departmentid = mysql_fetch_array($funObj->getDataById('department',"department_id='".$cp_res->department."'"));
															?>
																<tr class="odd gradeX cpdelete<?php echo $cp_res->contact_person_id ?>">
																	<td><a class="cpdelete" href="javascript:void(0)" data-id="<?php echo $cp_res->contact_person_id ?>">Delete</a></td>
																	<td><?php echo $cp_res->priority; ?></td>
																	<td><?php echo $cp_res->contact_person; ?></td>
																	<td><?php echo $designationid['designation']; ?></td>
																	<td><?php echo $departmentid['department']; ?></td>
																	<td><?php echo $cp_res->phone; ?></td>
																	<td><?php echo ($cp_res->status == 1) ? 'Active' : 'Deactive'; ?></td>
																</tr>
															<?php
														}
													?>								
												</tbody>
											</table>
										</div>
									</div>	
									<form role="form" method="post" id="savecp" name="savecp">
										<input type="hidden" class="ajax" name="ajax" value="cp" />
										<input type="hidden" name="cp_partycode" value="<?php echo $partyid ?>" />
										
										<div class="row">
											<div class="col-lg-6">
												<div class="form-group">
													<div class="form-group">
														<label>Name</label>
														<input type="text" name="cp_name" required class="form-control">
													</div>
												</div>
											</div>
											<div class="col-lg-6">
												<div class="form-group">
													<div class="form-group">
														<label>Priority</label>
														<input type="text" name="cp_priority" required class="form-control">
													</div>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-lg-6">
												<div class="form-group">
													<div class="form-group">
														<label>Designation</label>
														<select class="form-control templatingSelect2" name="cp_designation">
															<?php 
																while($designations=mysql_fetch_object($designation))
																{
																	?>
																		<option value="<?php echo $designations->designation_id ?>"><?php echo $designations->designation; ?></option>
																	<?php
																}
															?>
														</select>
													</div>
												</div>
											</div>
											<div class="col-lg-6">
												<div class="form-group">
													<div class="form-group">
														<label>Department</label>
														<select class="form-control templatingSelect2" name="cp_department">
															<?php 
																while($departments=mysql_fetch_object($department))
																{
																	?>
																		<option value="<?php echo $departments->department_id ?>"><?php echo $departments->department; ?></option>
																	<?php
																}
															?>
														</select>
													</div>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-lg-6">
												<div class="form-group">
													<div class="form-group">
														<label>Phone</label>
														<input type="text" name="cp_phone" required class="form-control">
													</div>
												</div>
											</div>
											<div class="col-lg-6">
												<div class="form-group">
													<div class="form-group">
														<label>Mobile</label>
														<input type="text" name="cp_mobile" required class="form-control">
													</div>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-lg-6">
												<div class="form-group">
													<div class="form-group">
														<label>Fax</label>
														<input type="text" name="cp_fax" required class="form-control">
													</div>
												</div>
											</div>
											<div class="col-lg-6">
												<div class="form-group">
													<div class="form-group">
														<label>Email</label>
														<input type="email" name="cp_email" required class="form-control">
													</div>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-lg-6">
												<div class="form-group">
													<div class="form-group">
														<label>Note</label>
														<input type="text" name="cp_note" required class="form-control">
													</div>
												</div>
											</div>
											<div class="col-lg-6">
												<div class="form-group">
													<div class="form-group">
														<label>Active?</label><br/>
														<input type="checkbox" name="cp_state" checked value="1">
													</div>
												</div>
											</div>
										</div>
										<div class="row" style="padding-top:30px">
											<div class="col-lg-12">
												<input class="btn btn-success" type="submit" name="savecp" value="Save Contact Person" />
											</div>
										</div>
									</form>
								</div>
								<div id="tabaddresswork" class="tab-pane fade">
									<h4>Address Work</h4>
									<div class="panel-body">
									<div class="row">
										<div class="col-lg-6">
											<a class="btn btn-success sm_party_add" href="javascript:void(0)">Make Same As Party Details.</a>
										</div>
									</div>
									</div>
									<form role="form" method="post" id="saveaw" name="saveaw">
										<input type="hidden" name="ajax" value="aw" />
										<input type="hidden" name="cp_partycode" value="<?php echo $partyid ?>" />
										<div class="row">
											<div class="col-lg-6">
												<div class="form-group">
													<label>Address</label>
													<textarea name="aw_address" id="aw_address" required class="form-control"><?php echo $wp_row['workaddress'] ?></textarea>
												</div>
											</div>	
											<div class="col-lg-6">
												<div class="form-group">
													<label>Country</label>
													<select class="form-control aw_country templatingSelect2" required name="aw_country">
														<option>Select Country</option>
														<?php
															while($country=mysql_fetch_object($countrys1))
															{
																?>
																<option <?php echo ( $wp_row['country'] == $country->countryid ) ? 'selected' : '' ; ?> class="awcountrydisp awcountry-<?php echo $country->countryid; ?>" value="<?php echo $country->countryid; ?>"><?php echo $country->country; ?></option>
																<?php
															}
														?>
													</select>
												</div>
												<div class="form-group">
													<label>State</label>
													<select class="form-control aw_state templatingSelect2" required name="aw_state">
														<option>State State</option>
														<?php
															while($state=mysql_fetch_object($states1))
															{
																?>
																<option <?php echo ( $wp_row['state'] == $state->stateid ) ? 'selected' : '' ; ?> class="awstatedisp awsCountry-<?php echo $state->countryid; ?> awstate-<?php echo $state->stateid; ?>" data-country="<?php echo $state->countryid; ?>" value="<?php echo $state->stateid; ?>"><?php echo $state->state; ?></option>
																<?php
															}
														?>
													</select>
												</div>
												<div class="form-group">
													<label>City</label>
													<select class="form-control aw_city templatingSelect2" required name="aw_city">
														<option>Select City</option>
														<?php
															while($city=mysql_fetch_object($citys1))
															{
																$countryid = mysql_fetch_array($funObj->getDataById('country',"countryid='".$city->stateid."'"));
																?>
																<option <?php echo ( $wp_row['city'] == $city->cityid ) ? 'selected' : '' ; ?> class="awcitydisp awcCountry-<?php echo $countryid['countryid']; ?> awcState-<?php echo $city->stateid; ?> awcity-<?php echo $city->cityid; ?>" data-state="<?php echo $city->stateid; ?>" data-country="<?php echo $countryid['countryid']; ?>"  value="<?php echo $city->cityid; ?>"><?php echo $city->city; ?></option>
																<?php
															}
														?>
													</select>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-lg-6">
												<div class="form-group">
													<label>Phones</label>
													<textarea name="aw_phones" id="aw_phones" required class="form-control"><?php echo $wp_row['phone'] ?></textarea>
												</div>
											</div>
											<div class="col-lg-6">
												<div class="form-group">
													<div class="form-group">
														<label>Email</label>
														<input type="email" name="aw_email" id="aw_email" value="<?php echo $wp_row['email'] ?>" required class="form-control">
													</div>
												</div>
												<div class="form-group">
													<div class="form-group">
														<label>Web</label>
														<input type="url" name="aw_web" id="aw_web" value="<?php echo $wp_row['web'] ?>" required class="form-control">
													</div>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-lg-12">
												<div class="form-group">
													<div class="form-group">
														<label>Note</label>
														<textarea name="aw_note" id="aw_note" class="form-control"><?php echo $wp_row['note'] ?></textarea>
													</div>
												</div>
											</div>
										</div>
										<div class="row" style="padding-top:30px">
											<div class="col-lg-12">
												<input class="btn btn-success" type="submit" name="saveaw" value="Save Work Address" />
											</div>
										</div>
									</form>
								</div>
								<div id="tabterms" class="tab-pane fade">
									<h4>Add Terms & Condition</h4>
									<div class="panel-body">
										<div class="dataTable_wrapper">
											<table class="table table-striped table-bordered table-hover" id="dataTables-example">
												<thead>
													<tr>
														<th>Action</th>
														<th>No.</th>
														<th>Terms Group</th>
														<th>Terms</th>
														<th>Created On</th>
														<th>Created By</th>
													</tr>
												</thead>
												<tbody class="ajaxtc">
													<?php 
														$i=1;
														while($get_tcs=mysql_fetch_object($get_tc))
														{
															$termsgroupid = mysql_fetch_array($funObj->getDataById('term_group',"term_group_id='".$get_tcs->termsgroup."'"));
															?>
																<tr class="odd gradeX tcdelete<?php echo $get_tcs->termsid ?>">
																	<td><a class="tcdelete" href="javascript:void(0)" data-id="<?php echo $get_tcs->termsid ?>">Delete</a></td>
																	<td><?php echo $i; ?></td>
																	<td><?php echo $termsgroupid['termsgroup']; ?></td>
																	<td><?php echo $get_tcs->terms; ?></td>
																	<td><?php echo $get_tcs->createdon; ?></td>
																	<td><?php echo $get_tcs->createdby; ?></td>
																</tr>
															<?php
															$i++;
														}
													?>								
												</tbody>
											</table>
										</div>
									</div>	
									<form role="form" method="post" id="addterms" name="addterms">
										<input type="hidden" name="ajax" value="tc" />
										<input type="hidden" name="tc_partycode" value="<?php echo $partyid ?>" />
										<div class="row">
											<div class="col-lg-6">
												<div class="form-group">
													<label>Terms Group</label>
													<select class="form-control templatingSelect2" name="tc_termsGroup">
														<?php
															while($term_groups=mysql_fetch_object($term_group))
															{
																?>
																	<option value="<?php echo $term_groups->term_group_id ?>"><?php echo $term_groups->term_group; ?></option>	
																<?php
															}
														?>
													</select>	
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-lg-12">
												<div class="form-group">
													<label>Terms</label>
													<textarea name="tc_terms" class="form-control"></textarea>
												</div>
											</div>
										</div>
										<div class="row" style="padding-top:30px">
											<div class="col-lg-12">
												<input class="btn btn-success" type="submit" name="addterms" value="Add Terms" />
											</div>
										</div>
									</form>
								</div>
								<div id="tabtax" class="tab-pane fade">
									<h4>Tax & Excise</h4>
									<form role="form" method="post" id="addtax" name="addtax">
										<input type="hidden" name="ajax" value="te" />
										<input type="hidden" name="te_partycode" value="<?php echo $partyid ?>" />
										<div class="row">
											<div class="col-lg-6">
												<div class="form-group">
													<label>TIN VAT No.</label>
													<input type="text" name="te_tin_vat_no" value="<?php echo $get_te['tin_vat_no']; ?>" required class="form-control">
												</div>
											</div>
											<div class="col-lg-6">
												<div class="form-group">
													<label>TIN CST No.</label>
													<input type="text" name="te_tin_cst_no" value="<?php echo $get_te['tin_cst_no']; ?>" required class="form-control">
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-lg-6">
												<div class="form-group">
													<label>ECC No.</label>
													<input type="text" name="te_ecc_no" value="<?php echo $get_te['ecc_no']; ?>" required class="form-control">
												</div>
											</div>
											<div class="col-lg-6">
												<div class="form-group">
													<label>PAN No.</label>
													<input type="text" name="te_pan_no" value="<?php echo $get_te['pan_no']; ?>" required class="form-control">
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-lg-6">
												<div class="form-group">
													<label>Range</label>
													<input type="text" name="te_rang" value="<?php echo $get_te['range']; ?>" required class="form-control">
												</div>
											</div>
											<div class="col-lg-6">
												<div class="form-group">
													<label>Division</label>
													<input type="text" name="te_division" value="<?php echo $get_te['division']; ?>" required class="form-control">
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-lg-6">
												<div class="form-group">
													<label>Service Tax No.</label>
													<input type="text" name="te_service_tax_no" value="<?php echo $get_te['service_tax_no']; ?>" required class="form-control">
												</div>
											</div>
										</div>
										<div class="row" style="padding-top:30px">
											<div class="col-lg-12">
												<input class="btn btn-success" type="submit" name="addtax" value="Save Tax & Excise" />
											</div>
										</div>
									</form>
								</div>
								<div id="tabgeneral" class="tab-pane fade">
									
									<div class="row">
										<div class="col-lg-6">
											<h4>General Detail</h4>
											<p>We not done this.</p>
											<img src="<?php echo SITE_URL ?>img/gdetail.png" alt="general details"/>
										</div>
										<div class="col-lg-6">
											<h4>Login Detail</h4>
											<p><B>Created By</B> <?php echo $getParty['created_on']; ?></p>
											<p><B>Date & Time :</B>  <?php echo $getParty['created_by']; ?></p>
											<p><B>Last Modified By :</B> <?php echo $getParty['updated_on']; ?></p>
											<p><B>Date & Time :</B> <?php echo $getParty['updated_by']; ?></p>
										</div>
									</div>
								</div>
								<div id="tabcompanyprofile" class="tab-pane fade">
									<h4>Company Profile</h4>
									<form role="form" method="post" id="addpartycp" name="addpartycp">
										<input type="hidden" name="ajax" value="partycp" />
										<input type="hidden" name="tc_partycode" value="<?php echo $partyid ?>" />
										<div class="row">
											<div class="col-lg-12">
												<div class="form-group">
													<label>Company Profile</label>
													<textarea name="partycp" class="form-control"><?php echo $get_party_cp['company_profile'] ?></textarea>
												</div>
											</div>
										</div>
										<div class="row" style="padding-top:30px">
											<div class="col-lg-12">
												<input class="btn btn-success" type="submit" name="addterms" value="Add Terms" />
											</div>
										</div>
									</form>
								</div>
								<div id="tabbillterms" class="tab-pane fade">
									<h4>Billing Terms</h4>
										<form role="form" method="post" name="addBT" id="addBT">
										<input type="hidden" name="ajax" value="bt" />
										<input type="hidden" name="te_partycode" value="<?php echo $partyid ?>" />
										<div class="row">
											<div class="col-lg-3">
												<div class="form-group">
													<label>Billing Term</label>
													<input type="text" name="bt_code" value="<?php echo $get_bt['billing_terms_code'] ?>" placeholder="Code" required class="form-control">
												</div>
											</div>
											<div class="col-lg-9">
												<div class="form-group">
													<label>.</label>
													<input type="text" name="bt_term" value="<?php echo $get_bt['billing_terms'] ?>" placeholder="Billing Term" required class="form-control">
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-lg-12">
												<div class="form-group">
													<label>Narration</label>
													<input type="text" name="bt_narration" value="<?php echo $get_bt['billing_terms_narration'] ?>" required class="form-control">
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-lg-6">
												<div class="form-group">
													<label>Percentage</label>
													<input type="text" name="bt_percentage" value="<?php echo $get_bt['billing_terms_percentage'] ?>" required class="form-control">
												</div>
											</div>
											<div class="col-lg-6">
												<div class="form-group">
													<label>Value</label>
													<input type="text" name="bt_value" value="<?php echo $get_bt['billing_terms_value'] ?>" required class="form-control">
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-lg-12">
												<div class="form-group">
													<label>GL A/c </label>
													<div style="width:100%;display:inline-flex">
														<input type="text" name="bt_blac1" value="<?php echo $get_bt['billing_terms_glac1'] ?>" style="width:23%; margin-right:10px" required class="form-control">
														<input type="text" name="bt_blac2" value="<?php echo $get_bt['billing_terms_glac2'] ?>" style="width:76%" required class="form-control">
													</div>
												</div>
											</div>
										</div>
										
										<div class="row" style="padding-top:30px">
											<div class="col-lg-12">
												<input class="btn btn-success" type="submit" name="addBT" value="Add Billing Terms" />
											</div>
										</div>
									</form>
								</div>
								<div id="tabdeliveryaddress" class="tab-pane fade">
									<h4>Delivery Address</h4>
									<form role="form" method="post" id="savedw" name="savedw">
										<input type="hidden" name="ajax" value="wd" />
										<input type="hidden" name="cp_partycode" value="<?php echo $partyid ?>" />
										<div class="row">
											<div class="col-lg-6">
												<div class="form-group">
													<label>Address</label>
													<textarea name="dw_address" id="aw_address" required class="form-control"><?php echo $get_party_da['delivery_address'] ?></textarea>
												</div>
											</div>	
											<div class="col-lg-6">
												<div class="form-group">
													<label>Country</label>
													<select class="form-control dw_country templatingSelect2" required name="dw_country">
														<option>Select Country</option>
														<?php
															while($country=mysql_fetch_object($countrys2))
															{
																?>
																<option <?php echo ( $get_party_da['country'] == $country->countryid ) ? 'selected' : '' ; ?> class="dwcountrydisp awcountry-<?php echo $country->countryid; ?>" value="<?php echo $country->countryid; ?>"><?php echo $country->country; ?></option>
																<?php
															}
														?>
													</select>
												</div>
												<div class="form-group">
													<label>State</label>
													<select class="form-control dw_state templatingSelect2" required name="dw_state">
														<option>State State</option>
														<?php
															while($state=mysql_fetch_object($states2))
															{
																?>
																<option <?php echo ( $get_party_da['state'] == $state->stateid ) ? 'selected' : '' ; ?> class="dwstatedisp dwsCountry-<?php echo $state->countryid; ?> dwstate-<?php echo $state->stateid; ?>" data-country="<?php echo $state->countryid; ?>" value="<?php echo $state->stateid; ?>"><?php echo $state->state; ?></option>
																<?php
															}
														?>
													</select>
												</div>
												<div class="form-group">
													<label>City</label>
													<select class="form-control dw_city" required name="dw_city">
														<option>Select City</option>
														<?php
															while($city=mysql_fetch_object($citys2))
															{
																$countryid = mysql_fetch_array($funObj->getDataById('country',"countryid='".$city->stateid."'"));
																?>
																<option <?php echo ( $get_party_da['city'] == $city->cityid ) ? 'selected' : '' ; ?> class="dwcitydisp dwcCountry-<?php echo $countryid['countryid']; ?> dwcState-<?php echo $city->stateid; ?> dwcity-<?php echo $city->cityid; ?>" data-state="<?php echo $city->stateid; ?>" data-country="<?php echo $countryid['countryid']; ?>"  value="<?php echo $city->cityid; ?>"><?php echo $city->city; ?></option>
																<?php
															}
														?>
													</select>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-lg-6">
												<div class="form-group">
													<label>Phones</label>
													<textarea name="dw_phones" id="dw_phones" required class="form-control"><?php echo $get_party_da['phones'] ?></textarea>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-lg-12">
												<div class="form-group">
													<div class="form-group">
														<label>Note</label>
														<textarea name="dw_note" id="dw_note" class="form-control"><?php echo $get_party_da['note'] ?></textarea>
													</div>
												</div>
											</div>
										</div>
										<div class="row" style="padding-top:30px">
											<div class="col-lg-12">
												<input class="btn btn-success" type="submit" name="savedw" value="Save Delivery Address" />
											</div>
										</div>
									</form>
								</div>
							</div>
						</div>	
					</div>
				</div>
			</div>
		</div>
	</div>
    </div>    
</div>
<?php
include_once('../footer.php');
?>

<script>
 jQuery(function() {
    jQuery('.tcdelete').click(function(){
		var co = confirm('Are you sure?');
		if (co == true) {
			var tcid = jQuery(this).attr('data-id');
			$.ajax({
				type: "POST",
				url: "<?php echo SITE_URL.'ajaxcall.php' ?>",
				data: {tcid:tcid,ajax:'tcdelete'},
				cache: false,
					success: function(html) {
						jQuery('.tcdelete'+tcid).remove();
					}	
			});
		}
		else
		{
			return false;
		}
	})
	
    })
	
jQuery(document).ready(function(){
	
	jQuery('.sm_party_add').click(function(){
		jQuery('#aw_address').val(jQuery('#p_address').val());
		jQuery('.awcountry-'+jQuery('.selcountry').val()).attr('selected',true);
		jQuery('.awstate-'+jQuery('.selstates').val()).attr('selected',true);
		jQuery('.awcity-'+jQuery('.selcity').val()).attr('selected',true);
		jQuery('#aw_phones').val(jQuery('#p_phones').val());
		jQuery('#aw_email').val(jQuery('#p_email').val());
		jQuery('#aw_web').val(jQuery('#p_website').val());
		jQuery('#aw_note').val(jQuery('#p_note').val());
		
	});
	
	jQuery('#savedw').submit(function(e) {
		var cpval = jQuery( this ).serialize();
		$.ajax({
			type: "POST",
			url: "<?php echo SITE_URL.'ajaxcall.php' ?>",
			data: cpval,
			cache: false,
				success: function(html) {
					alert(html);
				}	
		});
		return false;
	});
	
	jQuery("#addpartycp").submit(function(e) {
		var cpval = jQuery( this ).serialize();
		$.ajax({
			type: "POST",
			url: "<?php echo SITE_URL.'ajaxcall.php' ?>",
			data: cpval,
			cache: false,
				success: function(html) {
					//alert(html);
				}	
		});
		return false;
	})
	
	jQuery("#addBT").submit(function(e) {
		var cpval = jQuery( this ).serialize();
		$.ajax({
			type: "POST",
			url: "<?php echo SITE_URL.'ajaxcall.php' ?>",
			data: cpval,
			cache: false,
				success: function(html) {
					alert('Billing Terms save successfully.')
				}	
		});
		return false;       
	});	
	jQuery("#addtax").submit(function(e) {	
		var cpval = jQuery( this ).serialize();
			$.ajax({
				type: "POST",
				url: "<?php echo SITE_URL.'ajaxcall.php' ?>",
				data: cpval,
				cache: false,
					success: function(html) {
						alert('Tax & Excise detail save successfully.')
						//jQuery('.ajaxtc').append(html);
					}	
			});
			return false;       
		});	

	jQuery("#addterms").submit(function(e) {
		var cpval = jQuery( this ).serialize();
		$.ajax({
			type: "POST",
			url: "<?php echo SITE_URL.'ajaxcall.php' ?>",
			data: cpval,
			cache: false,
				success: function(html) {
					jQuery('.ajaxtc').append(html);
				}	
		});
		return false;       
    });
	
	jQuery("#saveaw").submit(function(e) {
		var cpval = jQuery( this ).serialize();
		$.ajax({
			type: "POST",
			url: "<?php echo SITE_URL.'ajaxcall.php' ?>",
			data: cpval,
			cache: false,
				success: function(html) {
					alert('Work Address detail save successfully.')
				}	
		});
		return false;       
    });
	
    jQuery("#savecp").submit(function(e) {
		//if(jQuery(this).children().val() == "cp")
		var cpval = jQuery( this ).serialize();
		$.ajax({
			type: "POST",
			url: "<?php echo SITE_URL.'ajaxcall.php' ?>",
			data: cpval,
			cache: false,
				success: function(html) {
					jQuery('.ajaxcp').append(html);
				}	
		});
		return false;       
    });
   
    jQuery('.cpdelete').click(function(){
		var co = confirm('Are you sure?');
		if (co == true) {
			var cpid = jQuery(this).attr('data-id');
			$.ajax({
				type: "POST",
				url: "<?php echo SITE_URL.'ajaxcall.php' ?>",
				data: {cpid:cpid,ajax:'cpdelete'},
				cache: false,
					success: function(html) {
						jQuery('.cpdelete'+cpid).remove();
					}	
			});
		}
		else
		{
			return false;
		}
	})
});

	
jQuery(document).ready(function() {
	jQuery(".selcity").change(function(){
		var id = jQuery(this).val();
		if(id != "Select City"){
			var stateid = jQuery('option:selected', this).attr('data-state');
			var countryid = jQuery('option:selected', this).attr('data-country');
			jQuery('.state-'+stateid).prop("selected",true);
			jQuery('.country-'+countryid).prop("selected",true);
			jQuery('.citydisp').hide();
			jQuery('.cState-'+stateid).show();
			jQuery('.statedisp').hide(); 
			jQuery('.sCountry-'+countryid).show();
		}
		else
		{
			jQuery('.selstates option:first').prop('selected','selected');
			jQuery('.selcountry option:first').prop('selected','selected');
			jQuery('.citydisp').show();
			jQuery('.statedisp').show(); 
		}
	})
	jQuery(".selstates").change(function(){
		var id = jQuery(this).val();
		var countryid = jQuery('option:selected', this).attr('data-country');
		if(id != "State State"){
			jQuery('.country-'+countryid).prop("selected",true);
			jQuery('.selcity option:first').prop('selected','selected');
			jQuery('.citydisp').hide();
			jQuery('.cState-'+id).show();
		}
		else
		{
			jQuery('.selcountry option:first').prop('selected','selected');
			jQuery('.selcity option:first').prop('selected','selected');
			jQuery('.citydisp').show();
			jQuery('.statedisp').show();
		}
	})
	
	jQuery(".selcountry").change(function(){
		var id = jQuery(this).val();
		if(id != "Select Country"){
			jQuery('.selcity option:first').prop('selected','selected');
			jQuery('.selstates option:first').prop('selected','selected');
			jQuery('.citydisp').hide();
			jQuery('.cCountry-'+id).show();
			jQuery('.statedisp').hide(); 
			jQuery('.sCountry-'+id).show();
		}
		else
		{
			jQuery('.selcity option:first').prop('selected','selected');
			jQuery('.selstates option:first').prop('selected','selected');
			jQuery('.citydisp').show();
			jQuery('.statedisp').show();
		}
	})
	
	jQuery(".aw_city").change(function(){
		var id = jQuery(this).val();
		if(id != "Select City"){
			var stateid = jQuery('option:selected', this).attr('data-state');
			var countryid = jQuery('option:selected', this).attr('data-country');
			jQuery('.awstate-'+stateid).prop("selected",true);
			jQuery('.awcountry-'+countryid).prop("selected",true);
			jQuery('.awcitydisp').hide();
			jQuery('.awcState-'+stateid).show();
			jQuery('.awstatedisp').hide(); 
			jQuery('.awsCountry-'+countryid).show();
		}
		else
		{
			jQuery('.aw_state option:first').prop('selected','selected');
			jQuery('.aw_country option:first').prop('selected','selected');
			jQuery('.awcitydisp').show();
			jQuery('.awstatedisp').show(); 
		}
	})
	jQuery(".aw_state").change(function(){
		var id = jQuery(this).val();
		var countryid = jQuery('option:selected', this).attr('data-country');
		if(id != "State State"){
			jQuery('.awcountry-'+countryid).prop("selected",true);
			jQuery('.aw_city option:first').prop('selected','selected');
			jQuery('.awcitydisp').hide();
			jQuery('.awcState-'+id).show();
		}
		else
		{
			jQuery('.aw_country option:first').prop('selected','selected');
			jQuery('.aw_city option:first').prop('selected','selected');
			jQuery('.awcitydisp').show();
			jQuery('.awstatedisp').show();
		}
	})
	
	jQuery(".aw_country").change(function(){
		var id = jQuery(this).val();
		if(id != "Select Country"){
			jQuery('.aw_city option:first').prop('selected','selected');
			jQuery('.aw_state option:first').prop('selected','selected');
			jQuery('.awcitydisp').hide();
			jQuery('.awcCountry-'+id).show();
			jQuery('.awstatedisp').hide(); 
			jQuery('.awsCountry-'+id).show();
		}
		else
		{
			jQuery('.aw_city option:first').prop('selected','selected');
			jQuery('.aw_state option:first').prop('selected','selected');
			jQuery('.awcitydisp').show();
			jQuery('.awstatedisp').show();
		}
	})
	
	jQuery(".dw_city").change(function(){
		var id = jQuery(this).val();
		if(id != "Select City"){
			var stateid = jQuery('option:selected', this).attr('data-state');
			var countryid = jQuery('option:selected', this).attr('data-country');
			jQuery('.dwstate-'+stateid).prop("selected",true);
			jQuery('.dwcountry-'+countryid).prop("selected",true);
			jQuery('.dwcitydisp').hide();
			jQuery('.dwcState-'+stateid).show();
			jQuery('.dwstatedisp').hide(); 
			jQuery('.dwsCountry-'+countryid).show();
		}
		else
		{
			jQuery('.aw_state option:first').prop('selected','selected');
			jQuery('.aw_country option:first').prop('selected','selected');
			jQuery('.awcitydisp').show();
			jQuery('.awstatedisp').show(); 
		}
	})
	jQuery(".dw_state").change(function(){
		var id = jQuery(this).val();
		var countryid = jQuery('option:selected', this).attr('data-country');
		if(id != "State State"){
			jQuery('.dwcountry-'+countryid).prop("selected",true);
			jQuery('.dw_city option:first').prop('selected','selected');
			jQuery('.dwcitydisp').hide();
			jQuery('.dwcState-'+id).show();
		}
		else
		{
			jQuery('.dw_country option:first').prop('selected','selected');
			jQuery('.dw_city option:first').prop('selected','selected');
			jQuery('.dwcitydisp').show();
			jQuery('.dwstatedisp').show();
		}
	})
	
	jQuery(".dw_country").change(function(){
		var id = jQuery(this).val();
		if(id != "Select Country"){
			jQuery('.dw_city option:first').prop('selected','selected');
			jQuery('.dw_state option:first').prop('selected','selected');
			jQuery('.dwcitydisp').hide();
			jQuery('.dwcCountry-'+id).show();
			jQuery('.dwstatedisp').hide(); 
			jQuery('.dwsCountry-'+id).show();
		}
		else
		{
			jQuery('.dw_city option:first').prop('selected','selected');
			jQuery('.dw_state option:first').prop('selected','selected');
			jQuery('.dwcitydisp').show();
			jQuery('.dwstatedisp').show();
		}
	})
	
	
	jQuery('#dataTables-example').DataTable({
			responsive: true
	});
	
});

</script>
	
