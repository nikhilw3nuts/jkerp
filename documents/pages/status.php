<?php
include_once('../header.php');

include_once('../dbFunction.php');
$funObj = new dbFunction();
if($_POST['addstatus'])
{
	$status = $_POST['action'];
	$isExist = $funObj->checkeAllreadyExist("quotation_status","status",$status);
	if($isExist)
	{
		echo "<script>alert('Status aleady exist!')</script>";
	}
	else
	{
		$status= $funObj->addstatus($status);
	}
}
$status= $funObj->getTableData('quotation_status');
?>
<div id="page-wrapper">
    <div class="container">
	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">Sales Status</h1>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-7">
			<div class="panel panel-default">
				<div class="panel-heading clearfix">
					Sales Status Detail
				</div>				
				<div class="panel-body">
					<div class="dataTable_wrapper">
						<table class="table table-striped table-bordered table-hover" id="dataTables-example">
							<thead>
								<tr>
									<th>Action</th>
									<th>Sales Status</th>
								</tr>
							</thead>
							<tbody>
								<?php 
									while($statuss=mysql_fetch_object($status))
									{
										?>
											<tr class="odd gradeX">
												<td><a onclick="return confirm('Are you sure?');" href="<?php echo SITE_URL.'pages/delete.php?redirect=status&table=quotation_status&field=status_id&id='.$statuss->status_id; ?>">Delete</a></td>
												<td><?php echo $statuss->status; ?></td>
											</tr>	
										<?php
									} 
								?>								
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
		<div class="col-lg-5">
			<div class="panel panel-default">
				<div class="panel-heading clearfix">
					Add Sales Status
				</div>
				<div style="margin:20px">	
				<form role="form" name="addstatus" method="POST">
					<div class="form-group">
						<label>Sales Status</label>
						<input type="text" name="action" required class="form-control">
					</div>
					<input class="btn btn-success btn-block" type="submit" name="addstatus" value="Add Status" />
				</form>
				</div>
			</div>
		</div>
	</div>
    </div>    
</div>
<?php
include_once('../footer.php');
?>
<script>
jQuery(document).ready(function() {
	jQuery(".state-all").hide();
	jQuery(".state-1").show();
	jQuery(".countrycng").change(function(){
		var curt = jQuery(this).val();
		 jQuery(".state-all").hide();
		 jQuery(".state-"+curt).show();
	})
	jQuery('#dataTables-example').DataTable({
			responsive: true
	});
	
});
</script>
