<?php
include_once('../header.php');

include_once('../dbFunction.php');
$funObj = new dbFunction();
if($_POST['addinquerycon'])
{
	$inquery_con = $_POST['inquery_conf'];
	$isExist = $funObj->checkeAllreadyExist("inquiry_conformation","inquiry_conformation",$inquery_con);
	if($isExist)
	{
		echo "<script>alert('Inquery Confirmation alrady exist!')</script>";
	}
	else
	{
		$city = $funObj->addinquerycon($inquery_con);
	}
}
$inquery_cons = $funObj->getTableData('inquiry_conformation');
?>
<div id="page-wrapper">
    <div class="container">
	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">Inquiry Confirmation</h1>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-7">
			<div class="panel panel-default">
				<div class="panel-heading clearfix">
					Inquiry Confirmation
				</div>				
				<div class="panel-body">
					<div class="dataTable_wrapper">
						<table class="table table-striped table-bordered table-hover" id="dataTables-example">
							<thead>
								<tr>
									<th>Action</th>
									<th>Inquiry Confirmation</th>
								</tr>
							</thead>
							<tbody>
								<?php 
									while($inquery_con=mysql_fetch_object($inquery_cons))
									{
										?>
											<tr class="odd gradeX">
												<td><a onclick="return confirm('Are you sure?');" href="<?php echo SITE_URL.'pages/delete.php?redirect=inquery_conform&table=inquiry_conformation&field=inquery_conform_id&id='.$inquery_con->inquery_conform_id; ?>">Delete</a></td>
												<td><?php echo $inquery_con->inquiry_conformation; ?></td>
											</tr>	
										<?php
									} 
								?>								
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
		<div class="col-lg-5">
			<div class="panel panel-default">
				<div class="panel-heading clearfix">
					Add Inquiry Confirmation
				</div>
				<div style="margin:20px">	
				<form role="form" name="addinquerycon" method="POST">
					<div class="form-group">
						<label>Inquiry Confirmation</label>
						<input type="text" name="inquery_conf" required class="form-control">
					</div>
					<input class="btn btn-success btn-block" type="submit" name="addinquerycon" value="Add Inquiry Confirmation" />
				</form>
				</div>
			</div>
		</div>
	</div>
    </div>    
</div>
<?php
include_once('../footer.php');
?>
