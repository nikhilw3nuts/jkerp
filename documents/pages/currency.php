<?php
include_once('../header.php');

include_once('../dbFunction.php');
$funObj = new dbFunction();
if($_POST['addcurrency'])
{
	$currency = $_POST['currency'];
	$isExist = $funObj->checkeAllreadyExist("currency","Currency",$currency);
	if($isExist)
	{
		echo "<script>alert('Currency aleady exist!')</script>";
	}
	else
	{
		$currency = $funObj->addCurrency($currency);
	}
}
$currency = $funObj->getTableData('currency');
?>
<div id="page-wrapper">
    <div class="container">
	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">Currency</h1>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-7">
			<div class="panel panel-default">
				<div class="panel-heading clearfix">
					Currency Detail
				</div>				
				<div class="panel-body">
					<div class="dataTable_wrapper">
						<table class="table table-striped table-bordered table-hover" id="dataTables-example">
							<thead>
								<tr>
									<th>Action</th>
									<th>Currency</th>
								</tr>
							</thead>
							<tbody>
								<?php 
									while($currencys=mysql_fetch_object($currency))
									{
										?>
											<tr class="odd gradeX">
												<td><a onclick="return confirm('Are you sure?');" href="<?php echo SITE_URL.'pages/delete.php?redirect=currency&table=currency&field=currency_id &id='.$currencys->currency_id ; ?>">Delete</a></td>
												<td><?php echo $currencys->Currency; ?></td>
											</tr>	
										<?php
									} 
								?>								
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
		<div class="col-lg-5">
			<div class="panel panel-default">
				<div class="panel-heading clearfix">
					Add Currency
				</div>
				<div style="margin:20px">	
				<form role="form" name="addcurrency" method="POST">
					<div class="form-group">
						<label>Currency</label>
						<input type="text" name="currency" required class="form-control">
					</div>
					<input class="btn btn-success btn-block" type="submit" name="addcurrency" value="Add Currency" />
				</form>
				</div>
			</div>
		</div>
	</div>
    </div>    
</div>
<?php
include_once('../footer.php');
?>
<script>
jQuery(document).ready(function() {
	jQuery(".state-all").hide();
	jQuery(".state-1").show();
	jQuery(".countrycng").change(function(){
		var curt = jQuery(this).val();
		 jQuery(".state-all").hide();
		 jQuery(".state-"+curt).show();
	})
	jQuery('#dataTables-example').DataTable({
			responsive: true
	});
	
});
</script>
