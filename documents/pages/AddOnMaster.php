<?php
include_once('../header.php');
include_once('../dbFunction.php');
$funObj = new dbFunction();
$quotations = $funObj->getTableData('quotation_master');
?>
<div id="page-wrapper">
    <div class="container">
		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header">AddOn Master</h1>
			</div>
		</div>
		<div class="row">
		<div class="col-lg-6">
			<div class="panel panel-default">
				<div class="panel-heading clearfix">
				  AddOn Listion
				</div>				
				<div class="panel-body">
					<div class="dataTable_wrapper">
						<table class="table table-striped table-bordered table-hover" id="dataTables-example">
							<thead>
								<tr>
									<th>Action</th>
									<th>Materials Grade</th>
									<th>Materials Spacification</th>
								</tr>
								<tr>
									<td>Edit/Delete</td>
									<td>Dummy AddOn 1</td>
									<td>Dummy AddOn Description</td>
								</tr>
								<tr>
									<td>Edit/Delete</td>
									<td>Dummy AddOn 2</td>
									<td>Dummy AddOn 2 Description</td>
								</tr>
							</thead>
						</table>
					</div>
				</div>
				</div>
		</div>
	
	<div class="col-lg-6">
		<div class="panel panel-default">
			<div class="panel-heading clearfix">
				<div class="panel panel-default">
					<div class="panel-heading clearfix">
						<div class="form-group">
							<label>Title</label>
							<input type="text" name="AddOnTitle" class="form-control">
						</div>
						<div class="form-group">
							<label>Description</label>
							<input type="text" name="AddOnDescription" class="form-control">
						</div>
						<input class="btn btn-success btn-block" type="submit" name="AddAddOn" value="Add AddOn"/>
					</div>
				</div>
			</div>
		</div>
	</div>
	</div>
</div>

<?php
	include_once('../footer.php');
?>




