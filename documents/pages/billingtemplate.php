<?php
include_once('../header.php');

include_once('../dbFunction.php');
$funObj = new dbFunction();

if($_GET['action'] != "addTemplateDetail")
{
	if($_POST['addTemplate'])
	{
		$template = $_POST['template'];
		$isExist = $funObj->checkeAllreadyExist("billing_template","template",$template);
		if($isExist)
		{
			echo "<script>alert('Template aleady exist!')</script>";
		}
		else
		{
			unset($_POST['addTemplate']);
			$funObj->addfunction('billing_template',$_POST);
		}
	}
	$templates = $funObj->getTableData('billing_template');
	?>
	<div id="page-wrapper">
		<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header">Templates</h1>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-7">
				<div class="panel panel-default">
					<div class="panel-heading clearfix">
						Template Detail
					</div>				
					<div class="panel-body">
						<div class="dataTable_wrapper">
							<table class="table table-striped table-bordered table-hover" id="dataTables-example">
								<thead>
									<tr>
										<th>Action</th>
										<th>Templates</th>
									</tr>
								</thead>
								<tbody>
									<?php 
										while($template=mysql_fetch_object($templates))
										{
											?>
												<tr class="odd gradeX">
													<td>
														<a onclick="return confirm('Are you sure?');" href="<?php echo SITE_URL.'pages/delete.php?redirect=billingtemplate&table=billing_template&field=template_id&id='.$template->template_id; ?>">Delete</a> |
														<a href="?&action=addTemplateDetail&id=<?php echo $template->template_id; ?>">Add Template Detail</a>
													 </td>
													<td><?php echo $template->template; ?></td>
												</tr>	
											<?php
										} 
									?>								
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
			<div class="col-lg-5">
				<div class="panel panel-default">
					<div class="panel-heading clearfix">
						Add Template
					</div>
					<div style="margin:20px">	
					<form role="form" name="addTemplate" method="POST">
						<div class="form-group">
							<label>Template</label>
							<input type="text" name="template" required class="form-control">
						</div>
						<input class="btn btn-success btn-block" type="submit" name="addTemplate" value="Add Template" />
					</form>
					</div>
				</div>
			</div>
		</div>
		</div>    
	</div>
<?php 
}
else
{
	if($_POST['addTemplate'])
	{
		unset($_POST['addTemplate']);
		$funObj->addfunction('billing_template_detail',$_POST);
	}
	$templateDetails = $funObj->getDataById('billing_template_detail','billing_template_id="'. $_GET['id'].'"');
	?>
    	<div id="page-wrapper">
		<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<h1 class="page-header">Template Detail</h1>
				<a href="<?php echo SITE_URL ?>pages/billingtemplate.php">Back to template</a>
            </div>
        </div>
		<div class="row">
			<div class="col-lg-7">
				<div class="panel panel-default">
					<div class="panel-heading clearfix">
						Template Detail
					</div>				
					<div class="panel-body">
						<div class="dataTable_wrapper">
							<table class="table table-striped table-bordered table-hover" id="dataTables-example">
								<thead>
									<tr>
										<th>Action</th>
										<th>Cal Code</th>
                                        <th>Description</th>
                                        <th>Percentage</th>
                                        <th>Cal Defination</th>
									</tr>
								</thead>
								<tbody>
									<?php 
										while($templateDetail=mysql_fetch_object($templateDetails))
										{
											?>
												<tr class="odd gradeX">
													<td>
														<a onclick="return confirm('Are you sure?');" >Delete</a>
													 </td>
													<td><?php echo $templateDetail->cal_code; ?></td>
                                                    <td><?php echo $templateDetail->description; ?></td>
                                                    <td><?php echo $templateDetail->percentage; ?></td>
                                                    <td><?php echo $templateDetail->cal_defination; ?></td>
												</tr>	
											<?php
										} 
									?>								
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
			<div class="col-lg-5">
				<div class="panel panel-default">
					<div class="panel-heading clearfix">
						Add Template
					</div>
					<div style="margin:20px">	
					<form role="form" name="addTemplate" method="POST">
                    	<input type="hidden" name="billing_template_id" required class="form-control" value="<?php echo $_GET['id']; ?>">
						<div class="form-group">
							<label>Cal Code </label>
							<input type="text" name="cal_code" required class="form-control">
						</div>
                        <div class="form-group">
							<label>Description</label>
							<input type="text" name="description" required class="form-control">
						</div>
						<div class="form-group">
							<label>Percentage</label>
							<input type="text" name="percentage" required class="form-control">
						</div>
                        <div class="form-group">
							<label>Cal Defination</label>
							<input type="text" name="cal_defination" required class="form-control">
						</div>
						<input class="btn btn-success btn-block" type="submit" name="addTemplate" value="Add" />
					</form>
					</div>
				</div>
			</div>
		</div>
		</div>    
	</div>
    <?php
}
?>
<?php
include_once('../footer.php');
?>
<script>
jQuery(document).ready(function() {
	jQuery(".state-all").hide();
	jQuery(".state-1").show();
	jQuery(".countrycng").change(function(){
		var curt = jQuery(this).val();
		 jQuery(".state-all").hide();
		 jQuery(".state-"+curt).show();
	})
	jQuery('#dataTables-example').DataTable({
			responsive: true
	});
	
});
</script>
