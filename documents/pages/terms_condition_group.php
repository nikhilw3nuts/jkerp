<?php
include_once('../header.php');

include_once('../dbFunction.php');
$funObj = new dbFunction();
if($_POST['tcgroup'])
{
	$check = $funObj->checkeAllreadyExist('term_group','term_group',$_POST['description']);
	$check1 = $funObj->checkeAllreadyExist('term_group','code',$_POST['code']);
	if($check || $check1)
	{
		echo "<script>alert('Terms & condition group aleady exist!')</script>";
	}
	else
	{
		$fieldvalue['code']= $_POST['code'];
		$fieldvalue['term_group'] = $_POST['description'];
		$funObj->addfunction('term_group',$fieldvalue);
	}
}
$term_group = $funObj->getTableData('term_group');

?>
<div id="page-wrapper">
    <div class="container">
	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">Terms & Condition Group</h1>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-7">
			<div class="panel panel-default">
				<div class="panel-heading clearfix">
					Terms & Condition Group
				</div>				
				<div class="panel-body">
					<div class="dataTable_wrapper">
						<table class="table table-striped table-bordered table-hover" id="dataTables-example">
							<thead>
								<tr>
									<th>Action</th>
									<th>Code</th>
                                    <th>Desctiption</th>
								</tr>
							</thead>
							<tbody>
								<?php 
									while($term_groups=mysql_fetch_object($term_group))
									{
										?>
											<tr class="odd gradeX">
												<td><a onclick="return confirm('Are you sure?');" href="<?php echo SITE_URL.'pages/delete.php?redirect=terms_condition_group&table=term_group&field=term_group_id&id='.$term_groups->term_group_id; ?>">Delete</a></td>
												<td><?php echo $term_groups->code; ?></td>
												<td><?php echo $term_groups->term_group; ?></td>
											</tr>	
										<?php
									} 
								?>								
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
		<div class="col-lg-5">
			<div class="panel panel-default">
				<div class="panel-heading clearfix">
					Add Terms & Condition Group
				</div>
				<div style="margin:20px">	
				<form role="form" name="tcgroup" method="POST">
					<div class="form-group">
						<label>Code</label>
						<input type="text" name="code" required class="form-control">
					</div>
                    <div class="form-group">
						<label>Description</label>
						<input type="text" name="description" required class="form-control">
					</div>
					<input class="btn btn-success btn-block" type="submit" name="tcgroup" value="Add Terms & Condition Group" />
				</form>
				</div>
			</div>
		</div>
	</div>
    </div>
</div>
<?php
include_once('../footer.php');
?>