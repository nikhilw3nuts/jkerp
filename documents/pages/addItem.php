<?php
include_once('../header.php');
include_once('../dbFunction.php');
$funObj = new dbFunction();

if($_POST['addItem'])
{
	$item_cate = $_POST['item_cate'];
	$isItemActive = $_POST['isItemActive'];
	$item_code1 = $_POST['item_code1'];
	$item_code2 = $_POST['item_code2'];
	$item_name = $_POST['item_name'];
	$item_uom = $_POST['item_uom'];	
	$item_cfactor  = $_POST['item_cfactor'];	
	$item_conv_qty = $_POST['item_conv_qty'];
	$item_conv_uom = $_POST['item_conv_uom'];
	$item_type = $_POST['item_type'];
	$item_mpt = $_POST['item_mpt'];
	$item_bws = $_POST['item_bws'];
	$item_group = $_POST['item_group'];
	$item_status = $_POST['item_status'];
	$main_dir = '../uploads/';
	if (!file_exists($main_dir)) {
		mkdir($main_dir, 0777, true);
	}
	$target_dir = $main_dir.'item/';
	if (!file_exists($target_dir)) {
		mkdir($target_dir, 0777, true);
	}

	$uploadOk = 1;
	$err ="";
	
	$valid_formats = array("doc", "docx", "txt", "xl");
	$max_file_size = 1024*300;
	$count = 0;
	
	foreach ($_FILES['uploaddoc']['name'] as $f => $name) {     
	    if ($_FILES['uploaddoc']['error'][$f] == 4) {
	        continue; // Skip file if any error found
			$uploadOk =1;
		}	       
	    if ($_FILES['uploaddoc']['error'][$f] == 0) {	           
	        if ($_FILES['uploaddoc']['size'][$f] > $max_file_size) {
	            $message[] = "$name is too large!.";
	            $uploadOk =0;
				continue; // Skip large files
	        }
			elseif( ! in_array(pathinfo($name, PATHINFO_EXTENSION), $valid_formats) ){
				$message[] = "$name is not a valid format";
				$uploadOk =0;
				continue; // Skip invalid file formats
			}
	        else{ // No error found! Move uploaded files 
	            
	            $count++; // Number of successfully uploaded file
				$uploadOk =1;
			}
	    }
	}
	
	$valid_formats1 = array("jpg", "png", "gif", "zip", "bmp", "jpeg");
	$max_file_size1 = 1024*300;
	$count = 0;
	
	foreach ($_FILES['itemimage']['name'] as $f => $name) {

	    if ($_FILES['itemimage']['error'][$f] == 4) {
	        continue; // Skip file if any error found
			$uploadOk =1;
		}	       
	    if ($_FILES['itemimage']['error'][$f] == 0) {	           
	        if ($_FILES['itemimage']['size'][$f] > $max_file_size1) {
	            $message[] = "$name is too large!.";
	            $uploadOk =0;
				continue; // Skip large files
	        }
			elseif( ! in_array(pathinfo($name, PATHINFO_EXTENSION), $valid_formats1) ){
				$message[] = "$name is not a valid format";
				$uploadOk =0;
				continue; // Skip invalid file formats
			}
	        else{ // No error found! Move uploaded files 
	            
	            $count++; // Number of successfully uploaded file
				$uploadOk =1;
			}
	    }
	}

	if($uploadOk == 1)
	{
		$addparty = $funObj->addItem($item_cate,$isItemActive,$item_code1,$item_code2,$item_name,$item_uom,$item_cfactor,$item_conv_qty,$item_conv_uom,$item_type,$item_mpt,$item_bws,$item_status,$item_group);	
		$addImagestore = array();
		$i=1;
		foreach ($_FILES['uploaddoc']['name'] as $f => $name) 
		{
			$imageFileType = pathinfo($name,PATHINFO_EXTENSION);
			$filename = "item_".$addparty."_".time().".".$imageFileType;
			
			if(move_uploaded_file($_FILES["uploaddoc"]["tmp_name"][$f], $target_dir.$filename))
			{
				$addImage = $funObj->addImage('uploads/item/'.$filename,'item');
				$addImagestore[] = $addImage;
			}
			$i++;
		}	
		$updateitem = $funObj->updateField('item_master','upload_doc',serialize($addImagestore),'item_id='.$addparty);
		$addImagestore1 = array();
		$i=1;
		foreach ($_FILES['itemimage']['name'] as $f => $name) 
		{
			$imageFileType = pathinfo($name,PATHINFO_EXTENSION);
			$filename = "item_".$addparty."_".time().".".$imageFileType;
			if(move_uploaded_file($_FILES["itemimage"]["tmp_name"][$f], $target_dir.$filename))
			{
				$addImage = $funObj->addImage('uploads/item/'.$filename,'item');
				$addImagestore1[] = $addImage;
			}		
			$i++;
		}
		$updateitem = $funObj->updateField('item_master','item_image',serialize($addImagestore1),'item_id='.$addparty);
		
		$redirect = SITE_URL."pages/itemDetail.php?id=".$addparty."";
		?>
			<script>
                window.location.assign("<?php echo $redirect; ?>")
            </script>
		<?php
	
	}
	else
	{
		
		$mesgee = "";
		foreach($message as $messagee)
		{
			$mesgee .= $messagee.'\n\r';
		}
		?><script>
			alert("<?php echo $mesgee; ?>");
		</script>
		<?php
	}
}

$item_category = $funObj->getTableData('item_category');
$uom = $funObj->getTableData('uom');
$uom1 = $funObj->getTableData('uom');
$mpt = $funObj->getTableData('material_process_type');
$industry_status = $funObj->getTableData('industry_status');
$item_type = $funObj->getTableData('item_type');
$item_statuss= $funObj->getTableData('item_stauts');
$item_group = $funObj->getTableData('item_group_code');
?>
<div id="page-wrapper">
    <div class="container">
	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">Add Item</h1>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-default">
				<div class="panel-body">
					<form role="form" name="addItem" method="POST" enctype="multipart/form-data">
						<div class="row">
							<div class="col-lg-6">
								<div class="form-group">
									<label>Item Category</label>
										<select class="form-control" name="item_cate">
											<?php
												while($item_categorys=mysql_fetch_object($item_category))
												{
													?>
													<option value="<?php echo $item_categorys->item_category_id; ?>"><?php echo $item_categorys->item_category; ?></option>
													<?php
												}
											?>	
										</select>
									</div>
								</div>
							<div class="col-lg-6">
								<div class="form-group">
									<label>Item Group</label>
									<select class="form-control" name="item_group">
										<?php
											while($item_groups=mysql_fetch_object($item_group))
											{
												?>
												<option value="<?php echo $item_groups->item_group_code_id; ?>"><?php echo $item_groups->item_group; ?></option>
												<?php
											}
										?>	
									</select>
									<label>Active?</label>
									<input type="checkbox" name="isItemActive" checked value="1">
								</div>
							</div>
						</div>	
						<div class="row">
							<div class="col-lg-6">
								<div class="form-group">
									<div class="form-group">
										<label>Item Code</label>
										<div>
											<input type="text" name="item_code1" required class="form-control input-text-sm">
											<input type="text" name="item_code2" required class="form-control input-text-sm">
										</div>
									</div>
								</div>
							</div>
                            <div class="col-lg-6">
								<div class="form-group">
									<div class="form-group">
										<label>Item Name</label>
										<input type="text" name="item_name" required class="form-control">
										<a class="btn btn-default" href="#">Repeat Item (How this work?)</a>
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-lg-6">
								<div class="form-group">
									<label>UOM</label>
									<select class="form-control" name="item_uom">
										<option>Select UOM</option>
										<?php
											while($uoms=mysql_fetch_object($uom))
											{
												?>
												<option value="<?php echo $uoms->uom_id	; ?>"><?php echo $uoms->uom; ?></option>
												<?php
											}
										?>
									</select>
								</div>
							</div>
							<div class="col-lg-6">
								<div class="form-group">
									<label>CFactor</label>
									<input type="text" name="item_cfactor" required class="form-control">
								</div>
							</div>
						</div>
                        <div class="row">
                        	<div class="col-lg-6">
								<div class="form-group">
									<label>Conv Qty</label>
									<input type="text" name="item_conv_qty" required class="form-control">
								</div>
							</div>
							<div class="col-lg-6">
								<div class="form-group">
									<label>Conv. UOM</label>
									<select class="form-control" name="item_conv_uom">
										<option>Select Conv. UOM</option>
										<?php
											while($uoms=mysql_fetch_object($uom1))
											{
												?>
												<option value="<?php echo $uoms->uom_id; ?>"><?php echo $uoms->uom; ?></option>
												<?php
											}
										?>
									</select>
								</div>
							</div>
						</div>
                        <div class="row">
                        	<div class="col-lg-6">
								<div class="form-group">
									<label>Item Type</label>
									<select class="form-control" name="item_type">
										<option>Select Item Type</option>
										<?php
											while($item_types=mysql_fetch_object($item_type))
											{
												?>
												<option value="<?php echo $item_types->item_type_id; ?>"><?php echo $item_types->item_type; ?></option>
												<?php
											}
										?>
									</select>
								</div>
							</div>
							<div class="col-lg-6">
								<div class="form-group">
									<label>Material Process Type</label>
									<select class="form-control" name="item_mpt">
										<option>Select Material Process Type</option>
										<?php
											while($mpts=mysql_fetch_object($mpt))
											{
												?>
												<option value="<?php echo $mpts->material_process_type_id; ?>"><?php echo $mpts->material_process_type; ?></option>
												<?php
											}
										?>
									</select>
								</div>
							</div>
						</div>
                        <div class="row">
                        	<div class="col-lg-6">
								<div class="form-group">
									<label>Batch Wise Stock ?</label>
									<select class="form-control" name="item_bws">
										<option value="0">No</option>
										<option value="1">Yes</option>
									</select>
								</div>
							</div>
							<div class="col-lg-6">
								<div class="form-group">
									<label>Status </label>
									<select class="form-control" name="item_status">
										<option>Select Status</option>
										<?php
											while($item_status=mysql_fetch_object($item_statuss))
											{
												?>
												<option value="<?php echo $item_status->itemstatus_id; ?>"><?php echo $item_status->item_status; ?></option>
												<?php
											}
										?>
									</select>
								</div>
							</div>
						</div>
						
						<div class="row">
                        	<div class="col-lg-6">
								<div class="form-group">
									<label>Upload Document </label>
									<input type="file" name="uploaddoc[]" multiple="multiple" class="form-control" />
								</div>
							</div>
							<div class="col-lg-6">
								<div class="form-group">
									<label>Item image </label>
									<input type="file" name="itemimage[]" multiple="multiple" class="form-control" />
								</div>
							</div>
						</div>
						
						<div class="row" style="padding-top:30px">
							<div class="col-lg-12">
								<input class="btn btn-success" type="submit" name="addItem" value="Add Item" />
							</div>
						</div>		
						
					</form>
				</div>
			</div>
		</div>
	</div>
    </div>    
</div>
<?php
include_once('../footer.php');
?>
<script>
jQuery(document).ready(function() {
	jQuery(".selcity").change(function(){
		var id = jQuery(this).val();
		if(id != "Select City"){
			var stateid = jQuery('option:selected', this).attr('data-state');
			var countryid = jQuery('option:selected', this).attr('data-country');
			jQuery('.state-'+stateid).prop("selected",true);
			jQuery('.country-'+countryid).prop("selected",true);
			jQuery('.citydisp').hide();
			jQuery('.cState-'+stateid).show();
			jQuery('.statedisp').hide(); 
			jQuery('.sCountry-'+countryid).show();
		}
		else
		{
			jQuery('.selstates option:first').prop('selected','selected');
			jQuery('.selcountry option:first').prop('selected','selected');
			jQuery('.citydisp').show();
			jQuery('.statedisp').show(); 
		}
	})
	jQuery(".selstates").change(function(){
		var id = jQuery(this).val();
		var countryid = jQuery('option:selected', this).attr('data-country');
		if(id != "State State"){
			jQuery('.country-'+countryid).prop("selected",true);
			jQuery('.selcity option:first').prop('selected','selected');
			jQuery('.citydisp').hide();
			jQuery('.cState-'+id).show();
		}
		else
		{
			jQuery('.selcountry option:first').prop('selected','selected');
			jQuery('.selcity option:first').prop('selected','selected');
			jQuery('.citydisp').show();
			jQuery('.statedisp').show();
		}
	})
	
	jQuery(".selcountry").change(function(){
		var id = jQuery(this).val();
		if(id != "Select Country"){
			jQuery('.selcity option:first').prop('selected','selected');
			jQuery('.selstates option:first').prop('selected','selected');
			jQuery('.citydisp').hide();
			jQuery('.cCountry-'+id).show();
			jQuery('.statedisp').hide(); 
			jQuery('.sCountry-'+id).show();
		}
		else
		{
			jQuery('.selcity option:first').prop('selected','selected');
			jQuery('.selstates option:first').prop('selected','selected');
			jQuery('.citydisp').show();
			jQuery('.statedisp').show();
		}
	})
	
	
	jQuery('#dataTables-example').DataTable({
			responsive: true
	});
	
});

</script>
	
