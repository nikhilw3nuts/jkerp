<?php
include_once('../header.php');

include_once('../dbFunction.php');
$funObj = new dbFunction();
$quotations = $funObj->getTableData('quotation_master');

?>
<div id="page-wrapper">
    <div class="container">
	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">Quotation</h1>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-default">
				<div class="panel-heading clearfix">
					Quotation Detail
					<a class="btn btn-primary" style="float:right" href="<?php echo SITE_URL ?>pages/addquotation.php">Add Quotation</a>&nbsp;&nbsp;
					<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#AdvanceSearch">Advance Filtering</button>
					
				</div>				
				<div class="panel-body">
					<div class="dataTable_wrapper">
						<div class="filter-main">
                        	<h4>Filter Record</h4>
                            <div class="filter-col">
	                            <input type="checkbox" value="all" name="filterallqty" checked> View all qtys
                            </div>
                            <div class="filter-col">
                            <span>Status : </span>
                                <select name="status">
                                    <option value="1">Hot</option>
                                    <option value="2">Cold</option>
                                </select>
                            </div>
                            <div class="filter-col">
                                <span>Stage : </span>
                                <select name="status">
                                    <option value="1">Panding</option>
                                    <option value="2">In Pogress</option>
                                    <option value="3">On Hold</option>
                                    <option value="4">Deleverd</option>
                                </select>
                            </div>
                        </div>
                        <table class="table table-striped table-bordered table-hover" id="dataTables-example">
							<thead>
								<tr>
									<th>Action</th>
									<th>Quotation id</th>
									<th>Quotation date</th>
									<th>Inquiry id</th>
									<th>Customer</th>
								</tr>
							</thead>
							<tbody>
								<?php 
									while($quotation=mysql_fetch_object($quotations))
									{
										$party_master = mysql_fetch_array($funObj->getDataById("party_master","party_code='".$quotation->customer_code."'"));
										?>
										<tr class="odd gradeX">
											<td><a onclick="return confirm('Are you sure?');" href="<?php echo SITE_URL.'pages/delete.php?redirect=quotation&table=quotation_master&field=quotation_id&id='.$quotation->quotation_id; ?>">Delete</a> |  <a href="<?php echo SITE_URL.'pages/quotaionDetail.php?id='.$quotation->quotation_id; ?>">Edit</a></td>
											<td><?php echo $quotation->quotation_id; ?></td>
											<td><?php echo $quotation->quotation_date; ?></td>
											<td><?php echo $quotation->inquiry_id ?></td>
											<td><?php echo $party_master['party_name']; ?></td>
										</tr>
										<?php
									} 
								?>									
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
    </div>    
</div>

<!-- Modal -->
<div id="AdvanceSearch" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Modal Header</h4>
      </div>
      <div class="modal-body">
        <div class="row">
			<div class="col-lg-6">
			<h3>Report Criteria</h3>
			
				<div class="form-group">
					<input type="radio">Quatation No. wise<br>
				</div>
			
			
				<div class="form-group">
					<input type="radio">Party wise<br>
				</div>
			
			
				<div class="form-group">
					<input type="radio">Item wise<br>
				</div>
			
			
				<div class="form-group">
					<input type="radio">Qty. Type wise<br>
				</div>
			
			
				<div class="form-group">
					<input type="radio">User<br>
				</div>
			
			
				<div class="form-group">
					<input type="radio">Branch Wise<br>
				</div>
			
			
				<div class="form-group">
					<input type="radio">Cat. / Group Wise<br>
				</div>
			
			
				<div class="form-group">
					<input type="radio">Qtn Stage Wise<br>
				</div>
			
			
				<div class="form-group">
					<input type="radio">Reason Wise<br>
				</div>
			
			
				<div class="form-group">
					<input type="radio">Follow up Hist. wise<br>
				</div>
			
			
				<div class="form-group">
					<input type="radio">Follow up User. wise
				</div>
			
			
				<div class="form-group">
					<input type="radio">Salse type wise
				</div>
			
			
			</div>
			<div class="col-lg-6">
				<h1 class="page-header">Advance Criteria</h1>
				<div class="form-group">
					<label>Quatation Status</label>
					<select name="">
						<option>All</option>
						<option>Hot</option>
						<option>Cold</option>
					</select>
				</div>
				<div class="form-group">
					<label>Item Status</label>
					<select name="">
						<option>All</option>
						<option>Hot</option>
						<option>Cold</option>
					</select>
				</div>
				<div class="form-group">
					<label>Stage</label>
					<select name="">
						<option>All</option>
						<option>Prepared</option>
						<option>Cold</option>
					</select>
				</div>
				<div class="form-group">
					<label>From Date</label>
					<input type="text" name="" placeholder="12/10/2016">
				</div>
				<div class="form-group">
					<label>TO Date</label>
					<input type="text" name="" placeholder="10/10/2016">
				</div>
				<div class="form-group">
					<label>Qtyamount</label>
				</div>
				<div class="form-group">
					
					<select name="">
						<option>All</option>
						<option><</option>
						<option><=</option>
						<option>></option>
						<option>>=</option>
						<option>=</option>
						<option>!=</option>
					</select>
					<input type="text" name="" placeholder="amount">
				</div>
				
				<div class="form-group">
					
					<select name="">
						<option>All</option>
						<option><</option>
						<option><=</option>
						<option>></option>
						<option>>=</option>
						<option>=</option>
						<option>!=</option>
					</select>
					<input type="text" name="" placeholder="amount">
				</div>
				
				<div class="form-group">
					<label>Qtn. Type</label>
					<select name="">
						<option>All</option>
						<option>Product Quatation</option>
					</select>
				</div>
				
				<div class="form-group">
					<label>User</label>
					<select name="">
						<option>All</option>
						<option>Salse1</option>
						<option>Salse2</option>
					</select>
				</div>
				<div class="form-group">
					<label>Qty. Stage</label>
					<select name="">
						<option>All</option>
						<option>In Process</option>
					</select>
				</div>
				
				<div class="form-group">
					<label>Salse Type</label>
					<select name="">
						<option>All</option>
						<option>Domestic</option>
					</select>
				</div>
				
				<div class="form-group">
					<label>Branch</label>
				</div>
				
				<div class="form-group">
					<input type="text"><input type="text">
				</div>
			</div>
			
      </div>
	  <div class="row">
				<div class="col-lg-12">
					<h3>Selection Critearea</h3>
					
					<div class="form-group">
						<label>CUstomer</label>
						<input type="text"><input type="text">
					</div>
					<div class="form-group">
						<label>Item</label>
						<input type="text"><input type="text">
					</div>
					<div class="form-group">
						<label>City</label>
						<input type="text"><input type="text">
					</div>
					<div class="form-group">
						<label>Country</label>
						<input type="text"><input type="text">
					</div>
					<div class="form-group">
						<label>Cotegory</label>
						<input type="text"><input type="text">
					</div>
					<div class="form-group">
						<label>Reason</label>
						<input type="text"><input type="text">
					</div>
					<div class="form-group">
						<label>Enq. ref</label>
						<input type="text">
					</div>
				</div>
		</div>
			<div class="row">
				<div class="col-lg-12">
					<table width="100%" border="1" align="center">
					<tr>
						<th>&nbsp;</th>
						<th>Party Name</th>
						<th>City</th>
					</tr>
					<tr>
						<td><input type="checkbox"></td>
						<td>Mr. sudarshan</td>
						<td>Rajkot</td>
					</tr>
					<tr>
						<td><input type="checkbox"></td>
						<td>Mr. Amar</td>
						<td>Baroda</td>
					</tr>
					<tr>
						<td><input type="checkbox"></td>
						<td>Mr. Dhananjay</td>
						<td>Amareli</td>
					</tr>
					<tr>
						<td><input type="checkbox"></td>
						<td>Mr. Test</td>
						<td>Test CIty</td>
					</tr>
					</table>
					
				</div>
			</div>
			
      <div class="modal-footer">
		<div class="form-group" style="float:left;">
			<input  type="checkbox">&nbsp;
			<label >Select All</label>
		</div>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
<?php
include_once('../footer.php');
?>
<script>
jQuery(document).ready(function() {
	jQuery('#dataTables-example').DataTable({
			responsive: true
	});
});
</script>
