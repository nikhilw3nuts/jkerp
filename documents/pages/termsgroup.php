<?php
include_once('../header.php');

include_once('../dbFunction.php');
$funObj = new dbFunction();
if($_POST['addTermsGroup'])
{
	$termsgroup = $_POST['termsgroup'];
	$checktermsgroup = $funObj->checkTermsGroup($termsgroup);
	if($checktermsgroup)
	{
		echo "<script>alert('Terms group already exist!')</script>";
	}
	else
	{
		$addDepartment = $funObj->addTermsGroup($termsgroup);
	}
}
$term_group = $funObj->getTableData('term_group');
?>
<div id="page-wrapper">
    <div class="container">
	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">Terms Group</h1>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-7">
			<div class="panel panel-default">
				<div class="panel-heading clearfix">
					Terms Group
				</div>				
				<div class="panel-body">
					<div class="dataTable_wrapper">
						<table class="table table-striped table-bordered table-hover" id="dataTables-example">
							<thead>
								<tr>
									<th>Action</th>
									<th>Terms Group</th>
								</tr>
							</thead>
							<tbody>
								<?php 
									while($term_groups=mysql_fetch_object($term_group))
									{
										?>
											<tr class="odd gradeX">
												<td><a onclick="return confirm('Are you sure?');" href="<?php echo SITE_URL.'pages/delete.php?redirect=termsgroup&table=term_group&field=term_group_id&id='.$term_groups->term_group_id; ?>">Delete</a></td>
												<td><?php echo $term_groups->term_group; ?></td>
											</tr>	
										<?php
									} 
								?>								
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
		<div class="col-lg-5">
			<div class="panel panel-default">
				<div class="panel-heading clearfix">
					Add Terms Group
				</div>
				<div style="margin:20px">	
				<form role="form" name="addTermsGroup" method="POST">
					<div class="form-group">
						<label>Terms Group</label>
						<input type="text" name="termsgroup" required class="form-control">
					</div>
					<input class="btn btn-success btn-block" type="submit" name="addTermsGroup" value="Add Department" />
				</form>
				</div>
			</div>
		</div>
	</div>
    </div>    
</div>
<?php
include_once('../footer.php');
?>
<script>
jQuery(document).ready(function() {
	jQuery('#dataTables-example').DataTable({
			responsive: true
	});
});
</script>
