<?php
include_once('../header.php');

include_once('../dbFunction.php');
$funObj = new dbFunction();
if($_POST['adddesignation'])
{
	$designation = $_POST['designation'];
	$checkdesignation = $funObj->checkedesignation($designation);
	if($checkdesignation)
	{
		echo "<script>alert('Designation aleady exist!')</script>";
	}
	else
	{
		$adddesignation = $funObj->addDesignation($designation);
	}
}
$designation = $funObj->getTableData('designation');
?>
<div id="page-wrapper">
    <div class="container">
	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">Designation</h1>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-7">
			<div class="panel panel-default">
				<div class="panel-heading clearfix">
					Designation Detail
				</div>				
				<div class="panel-body">
					<div class="dataTable_wrapper">
						<table class="table table-striped table-bordered table-hover" id="dataTables-example">
							<thead>
								<tr>
									<th>Action</th>
									<th>Designation</th>
								</tr>
							</thead>
							<tbody>
								<?php 
									while($designations=mysql_fetch_object($designation))
									{
										?>
											<tr class="odd gradeX">
												<td><a onclick="return confirm('Are you sure?');" href="<?php echo SITE_URL.'pages/delete.php?redirect=designation&table=designation&field=designation_id&id='.$designations->designation_id; ?>">Delete</a></td>
												<td><?php echo $designations->designation; ?></td>
											</tr>	
										<?php
									} 
								?>								
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
		<div class="col-lg-5">
			<div class="panel panel-default">
				<div class="panel-heading clearfix">
					Add Designation
				</div>
				<div style="margin:20px">	
				<form role="form" name="adddesignation" method="POST">
					<div class="form-group">
						<label>Designation</label>
						<input type="text" name="designation" required class="form-control">
					</div>
					<input class="btn btn-success btn-block" type="submit" name="adddesignation" value="Add Designation" />
				</form>
				</div>
			</div>
		</div>
	</div>
    </div>    
</div>
<?php
include_once('../footer.php');
?>
<script>
jQuery(document).ready(function() {
	jQuery('#dataTables-example').DataTable({
			responsive: true
	});
});
</script>
