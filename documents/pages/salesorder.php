<?php
include_once('../header.php');

include_once('../dbFunction.php');
$funObj = new dbFunction();
$salesorders = $funObj->getTableData('sales_oder');


?>
<div id="page-wrapper">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Sales Order</h1>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading clearfix">
                        Sales Order
                        <a class="btn btn-primary" style="float:right" href="<?php echo SITE_URL ?>pages/addSalesOrder.php">Add Sales Order</a>
                    </div>				
                    <div class="panel-body">
                        <div class="dataTable_wrapper">
                            <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                <thead>
                                    <tr>
                                        <th>Action</th>
                                        <th>S.O. No</th>
                                        <th>S.O. Date</th>
                                        <th>Purchase Order No.</th>
                                        <th>P. O. Date</th>
                                        <th>Status</th>
                                        <th>Project</th>
                                        <th>Created by</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php 
                                        while($salesorder=mysql_fetch_object($salesorders))
                                        {
											?>
                                                <tr class="odd gradeX">
                                                    <td><a href="addSalesOrder.php?editid=<?php echo $salesorder->sales_oder_id; ?>" >Edit</a> | <a onclick="return confirm('Are you sure?');" href="<?php echo SITE_URL.'pages/delete.php?redirect=salesorder&table=sales_oder&field=sales_oder_id&id='.$salesorder->sales_oder_id; ?>">Delete</a></td>
                                                    <td><?php echo $salesorder->sales_oder_id; ?></td>
                                                    <td><?php echo $salesorder->sales_oder_date; ?></td>
                                                    <td><?php echo $salesorder->sell_party_name; ?></td>
                                                    <td><?php echo $salesorder->purchase_order_date; ?></td>
                                                    <td><?php echo $salesorder->status; ?></td>
                                                    <td></td>
                                                    <td><?php echo $salesorder->created_by; ?></td>
                                                </tr>	
                                            <?php
                                        } 
                                    ?>								
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
include_once('../footer.php');
?>
<script>
jQuery(document).ready(function() {
	jQuery('#dataTables-example').DataTable({
			responsive: true
	});
});
</script>
