<?php
include_once('../header.php');

include_once('../dbFunction.php');
$funObj = new dbFunction();
if($_POST['addquotationReason'])
{
	$quotation_reason = $_POST['quotation_reason'];
	$isExist = $funObj->checkeAllreadyExist("quotation_reason","reason",$quotation_reason);
	if($isExist)
	{
		echo "<script>alert('Quotation Reason aleady exist!')</script>";
	}
	else
	{
		$quotation_reasona = $funObj->addQuotationReason($quotation_reason);
	}
}
$quotation_reason = $funObj->getTableData('quotation_reason');
?>
<div id="page-wrapper">
    <div class="container">
	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">Quotation Reason</h1>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-7">
			<div class="panel panel-default">
				<div class="panel-heading clearfix">
					Quotation Reason Detail
				</div>				
				<div class="panel-body">
					<div class="dataTable_wrapper">
						<table class="table table-striped table-bordered table-hover" id="dataTables-example">
							<thead>
								<tr>
									<th>Action</th>
									<th>Quotation Reason</th>
								</tr>
							</thead>
							<tbody>
								<?php 
									while($quotation_reasons=mysql_fetch_object($quotation_reason))
									{
										?>
											<tr class="odd gradeX">
												<td><a onclick="return confirm('Are you sure?');" href="<?php echo SITE_URL.'pages/delete.php?redirect=quotation_reason&table=quotation_reason&field=reason_id&id='.$quotation_reasons->reason_id; ?>">Delete</a></td>
												<td><?php echo $quotation_reasons->reason; ?></td>
											</tr>	
										<?php
									} 
								?>								
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
		<div class="col-lg-5">
			<div class="panel panel-default">
				<div class="panel-heading clearfix">
					Quotation Reason Type
				</div>
				<div style="margin:20px">	
				<form role="form" name="addquotationReason" method="POST">
					<div class="form-group">
						<label>Quotation Reason</label>
						<input type="text" name="quotation_reason" required class="form-control">
					</div>
					<input class="btn btn-success btn-block" type="submit" name="addquotationReason" value="Add Quotation Reason" />
				</form>
				</div>
			</div>
		</div>
	</div>
    </div>    
</div>
<?php
include_once('../footer.php');
?>
