<?php
include_once('../header.php');

include_once('../dbFunction.php');
$funObj = new dbFunction();
if($_POST['addIndustryStatus'])
{
	$industry_status = $_POST['industry_status'];
	$isExist = $funObj->checkeAllreadyExist("industry_status","	industry_status",$industry_status);
	if($isExist)
	{
		echo "<script>alert('Industry Status aleady exist!')</script>";
	}
	else
	{
		$city = $funObj->addIndustryStatus($industry_status);
	}
}
$industry_statuss = $funObj->getTableData('industry_status');
?>
<div id="page-wrapper">
    <div class="container">
	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">Industry Status </h1>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-7">
			<div class="panel panel-default">
				<div class="panel-heading clearfix">
					Industry Status
				</div>				
				<div class="panel-body">
					<div class="dataTable_wrapper">
						<table class="table table-striped table-bordered table-hover" id="dataTables-example">
							<thead>
								<tr>
									<th>Action</th>
									<th>Industry Status</th>
								</tr>
							</thead>
							<tbody>
								<?php 
									while($industry_status=mysql_fetch_object($industry_statuss))
									{
										?>
											<tr class="odd gradeX">
												<td><a onclick="return confirm('Are you sure?');" href="<?php echo SITE_URL.'pages/delete.php?redirect=industry_status&table=industry_status&field=industry_status_id&id='.$industry_status->industry_status_id; ?>">Delete</a></td>
												<td><?php echo $industry_status->industry_status; ?></td>
											</tr>	
										<?php
									} 
								?>								
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
		<div class="col-lg-5">
			<div class="panel panel-default">
				<div class="panel-heading clearfix">
					Add Industry Status 
				</div>
				<div style="margin:20px">	
				<form role="form" name="addIndustryStatus" method="POST">
					<div class="form-group">
						<label>Industry Status</label>
						<input type="text" name="industry_status" required class="form-control">
					</div>
					<input class="btn btn-success btn-block" type="submit" name="addIndustryStatus" value="Add Tndustry Type" />
				</form>
				</div>
			</div>
		</div>
	</div>
    </div>    
</div>
<?php
include_once('../footer.php');
?>
