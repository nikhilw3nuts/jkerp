<?php
include_once('../header.php');

include_once('../dbFunction.php');
$funObj = new dbFunction();

$inquirys = $funObj->getTableData('inquiry');
?>
<div id="page-wrapper">
    <div class="container">
	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">Inquiry</h1>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-default">
				<div class="panel-heading clearfix">
					Inquiry Detail
                    <a class="btn btn-primary" style="float:right" href="<?php echo SITE_URL ?>pages/addInquiry.php">Add Inquiry</a>
				</div>				
				<div class="panel-body">
					<div class="dataTable_wrapper">
						<table class="table table-striped table-bordered table-hover" id="dataTables-example">
							<thead>
								<tr>
									<th>Action</th>
									<th>Inquiry By</th>
									<th>Lead Owner</th>
									<th>Assigned To</th>
									<th>Industry Type</th>
									<th>Inquiry Date</th>
								</tr>
							</thead>
							<tbody>
								<?php 
									while($inquiry=mysql_fetch_object($inquirys))
									{
										$getParty = mysql_fetch_array($funObj->getDataById('party_master',"party_code='".$inquiry->customer_code."'"));
										$lead_owner = mysql_fetch_array($funObj->getDataById('user',"userid='".$inquiry->lead_owner."'"));
										$assigned_to = mysql_fetch_array($funObj->getDataById('user',"userid='".$inquiry->assigned_to."'"));
										$industry_type = mysql_fetch_array($funObj->getDataById('industry_type',"industry_type_id='".$inquiry->industry_type."'"));
										?>
											<tr class="odd gradeX">
												<td><a onclick="return confirm('Are you sure?');" href="<?php echo SITE_URL.'pages/delete.php?redirect=inquiry&table=inquiry&field=inquiry_id&id='.$inquiry->inquiry_id; ?>">Delete</a> | <a href="<?php echo SITE_URL.'pages/inquiryDetail.php?id='.$inquiry->inquiry_id; ?>">Edit</a> </td>
												<td><?php echo $getParty['party_name']; ?></td>
												<td><?php echo $lead_owner['username']; ?></td>
												<td><?php echo $assigned_to['username']; ?></td>
												<td><?php echo $industry_type['industry_type']; ?></td>
												<td><?php echo $inquiry->inquiry_date; ?></td>
											</tr>	
										<?php
									} 
								?>								
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
    </div>
</div>
<?php
include_once('../footer.php');
?>
<script>
jQuery(document).ready(function() {
	jQuery('#dataTables-example').DataTable({
			responsive: true
	});
});
</script>