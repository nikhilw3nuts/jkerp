<?php
include_once('../header.php');

include_once('../dbFunction.php');
$funObj = new dbFunction();

	//define table name
	$tablename="user";

	//create array with Keys
	
	$filed_value['username']=$_POST['name'];
	$filed_value['phone']=$_POST['contact_no'];
	$filed_value['usertype']=$_POST['usertype'];
	//$filed_value['email']=date("Y-m-d",strtotime($_POST['date']));
	$filed_value['email']=$_POST['email'];
	$filed_value['name']=$_POST['name'];
	if($_FILES["fileToUpload"]["name"]!=''){
		$filed_value['user_image']=time().$_FILES["fileToUpload"]["name"];
	}
	$filed_value['birth_date']=date('Y-m-d',strtotime($_POST['birth_date']));
	$filed_value['qualification']=$_POST['qualification'];
	$filed_value['gender']=$_POST['gender'];
	$filed_value['father_name']=$_POST['father_name'];
	$filed_value['mother_name']=$_POST['mother_name'];
	$filed_value['married_status']=$_POST['married_status'];
	$filed_value['husbent/wife_name']=$_POST['husbent/wife_name'];
	$filed_value['blod_grup']=$_POST['blod_grup'];
	$filed_value['address']=$_POST['address'];
	$filed_value['permenent_address']=$_POST['permenent_address'];
	$filed_value['designation']=$_POST['designation'];
	$filed_value['grade']=$_POST['grade'];
	$filed_value['date_of_joining']=date('Y-m-d',strtotime($_POST['date_of_joining']));
	$filed_value['date_of_leaving']=date('Y-m-d',strtotime($_POST['date_of_leaving']));
	$filed_value['salary']=$_POST['salary'];
	$filed_value['basic_pay']=$_POST['basic_pay'];
	$filed_value['house_rent']=$_POST['house_rent'];
	$filed_value['traveling_allowance']=$_POST['traveling_allowance'];
	$filed_value['edication_allowance']=$_POST['edication_allowance'];
	$filed_value['pf_amount']=$_POST['pf_amount'];
	$filed_value['bonus_amount']=$_POST['bonus_amount'];
	$filed_value['medical_reimbursement']=$_POST['medical_reimbursement'];
	$filed_value['other_allousment']=$_POST['other_allousment'];
	$filed_value['monthly_gross']=$_POST['monthly_gross'];
	$filed_value['pf_employeer']=$_POST['pf_employeer'];
	$filed_value['professional_tax']=$_POST['professional_tax'];
	$filed_value['cost_to_company']=$_POST['cost_to_company'];
	$filed_value['bank_name']=$_POST['bank_name'];
	$filed_value['bank_acc_no']=$_POST['bank_acc_no'];
	$filed_value['pan_no']=$_POST['pan_no'];
	$filed_value['allow_pf']=$_POST['allow_pf'];
	$filed_value['pf_no']=$_POST['pf_no'];
	$filed_value['esic_no']=$_POST['esic_no'];
	$filed_value['emp_no']=$_POST['emp_no'];
	$filed_value['emp_id']=$_POST['emp_id'];
	$filed_value['uan_id']=$_POST['uan_id'];
	$filed_value['esic_id']=$_POST['esic_id'];
	
//for add record to user table



if( isset($_POST['submit']))
{
	
	/*user image uploading code*/
	$target_dir = "../uploads/user_image/";
	$target_file = $target_dir . basename(time().$_FILES["fileToUpload"]["name"]);
	$uploadOk = 1;
	$imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
	// Check if image file is a actual image or fake image
	if(isset($_POST["submit"])) {
		$check = getimagesize($_FILES["fileToUpload"]["tmp_name"]);
		if($check !== false) {
			echo "File is an image - " . $check["mime"] . ".";
			$uploadOk = 1;
		} else {
			//echo "File is not an image.";
			$uploadOk = 0;
		}
	}
	// Check if file already exists
	if (file_exists($target_file)) {
		echo "Sorry, file already exists.";
		$uploadOk = 0;
	}
	// Check file size
	if ($_FILES["fileToUpload"]["size"] > 500000) {
		echo "Sorry, your file is too large.";
		$uploadOk = 0;
	}
	// Allow certain file formats
	if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
	&& $imageFileType != "gif" ) {
		//echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
		$uploadOk = 0;
	}
	// Check if $uploadOk is set to 0 by an error
	if ($uploadOk == 0) {
		//echo "Sorry, your file was not uploaded.";
	// if everything is ok, try to upload file
	} else {
		if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
			echo "The file ". basename( $_FILES["fileToUpload"]["name"]). " has been uploaded.";
		} else {
			echo "Sorry, there was an error uploading your file.";
		}
	}
	
	//call function
	//call function
	if($_GET['action']=="add"){
		$funObj->addfunction($tablename,$filed_value);
		 
	}
	if($_GET['action']=="edit" && $_GET['id']!=''){
		$funObj->updatefunction($tablename,$filed_value,'userid',$_GET['id']);
		 
	}
}
?>
<div id="page-wrapper">
    <div class="container">
	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">Employee Master</h1>
		</div>
	</div>
	<a class="btn btn-success" href="employee_master.php?action=add">ADD NEW</a> 
	<?php if(isset($_GET['action'])){ ?>
	<a class="btn btn-success" href="employee_master.php?action=viewall">VIEW ALL</a>
	<?php } ?>
	<?php if($_GET['action']=="viewall" || $_GET['action']=="" ){ ?>
	
	<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-default">
				<div class="panel-heading clearfix">
					Interviewer Detail
				</div>				
				<div class="panel-body">
					<div class="dataTable_wrapper">
						<table class="table table-striped table-bordered table-hover" id="dataTables-example">
							<thead>
								<tr>
									<th>Action</th>
									<th>Name</th>
									<th>Phone</th>
									<th>Email</th>
									<th>Salary</th>									
									<th>Image</th>
									<th>Birth Date</th>
									<th>Qualification</th>
									<th>Date Of Joining</th> 
								</tr>
							</thead>
							<tbody>
								<?php 
									 $get_data = $funObj->getTableData('user');
									 while($fetch_val=mysql_fetch_array($get_data)):
										?>
											<tr class="odd gradeX">
												<td><a  href="employee_master.php?action=edit&id=<?php echo $fetch_val['userid'] ?>">EDIT</a>/ 
												<a onclick="return confirm('Are you sure?');" href="<?php echo SITE_URL.'pages/delete.php?redirect=employee_master&table=user&field=userid&id='.$fetch_val['userid']; ?>">Delete</a></td>
												<td><?php echo $fetch_val['name']; ?></td>
												<td><?php echo $fetch_val['phone']; ?></td>
												<td><?php echo $fetch_val['email']; ?></td>
												<td><?php echo $fetch_val['salary']; ?></td>
												<td align="center"><img height="80px" width="80px" src="../uploads/user_image/<?php echo $fetch_val['user_image']; ?>"></td>
												<td><?php echo date('d-m-Y',strtotime($fetch_val['birth_date'])); ?></td>
												<td><?php echo $fetch_val['qualification']; ?></td>
												<td><?php echo date('d-m-Y',strtotime($fetch_val['date_of_joining'])); ?></td>
											</tr>
																							
									<?php endwhile; ?>								
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
    </div>
	 
	
	<?php }else{ ?>
	
	<?php if($_GET['action']=='edit' && $_GET['id']!=''):
	$select_user=mysql_fetch_array($funObj->getDataById("user","userid='".$_GET['id']."'"));
	endif;
	?>
<div class="row">
		<div class="col-lg-12 employee-master">
			<div class="panel panel-default">
				<div class="panel-heading clearfix">
					Add Employee Master Record
				</div><br>
				<div class="row">
							<div class="col-lg-3 col-md-3">
								<div class="form-group">
									<a href="#Experiance_Latter" class="btn btn-success btn-block">Experiance Latter</a>
								</div>
							</div>
						
							<div class="col-lg-3 col-md-3">
								<div class="form-group">
									<a href="#NOC_Latter" class="btn btn-success btn-block">NOC Latter</a>
								</div>
							</div>
						</div>
				<div style="margin:20px">
			
				<form role="form" name="addcountrcountryy" method="POST" enctype="multipart/form-data">
				<div class="row">
				<h2>Personal Information</h2>
					<div class="col-lg-4 col-md-6">
						<div class="form-group">
							<label>Name</label>
							<input type="text" name="name" value="<?php echo $select_user['name']; ?>" class="form-control">
						</div>
					</div>
					<div class="col-lg-4 col-md-6">
						<div class="form-group">
							<label>Email</label>
							<input type="text" name="email" value="<?php echo $select_user['email']; ?>" class="form-control">
						</div>
					</div>
					<div class="col-lg-4 col-md-6">
						<div class="form-group">
							<label>Image</label>
							<input type="file" name="fileToUpload"  class="form-control">
							<?php if($_GET['action']=='edit' && $_GET['id']!=''){ ?>
							<img height="50px" width="50px" src="../uploads/user_image/<?php echo $select_user['user_image']; ?>">
							<?php } ?>
						</div>
					</div>					
					<div class="col-lg-4 col-md-6">
						<div class="form-group">
							<label>Birth Date</label>
							<input type="text" name="birth_date" value="<?php echo date('d-m-Y',strtotime($select_user['birth_date'])); ?>" class="form-control datepickers">
						</div>
					</div>
					<div class="col-lg-4 col-md-6">
						<div class="form-group">
							<label>Qualification</label>
							<input class="form-control" value="<?php echo $select_user['qualification']; ?>" type="text" name="qualification">
						</div>
					</div>
					<div class="col-lg-4 col-md-6">
						<div class="form-group">
							<label>Gender</label>
							<input type="radio" <?php if($select_user['gender']=='m'){ echo 'checked'; } ?>  value="m"  name="gender">
							: male
							
							<input type="radio" <?php if($select_user['gender']=='f'){ echo 'checked'; } ?>  value="f" name="gender">
							: Femail
						</div>
					</div>
					
					<div class="col-lg-4 col-md-6">
						<div class="form-group">
							<label>Father Name</label>
							<input type="text"  value="<?php echo $select_user['father_name']; ?>" name="father_name" class="form-control">
						</div>
					</div>
					<div class="col-lg-4 col-md-6">
						<div class="form-group">
							<label>Mother Name</label>
							<input type="text" name="mother_name" value="<?php echo $select_user['mother_name']; ?>" class="form-control">
						</div>
					</div>
					<div class="col-lg-4 col-md-6">
						<div class="form-group">
							<label>Marriage Status</label>
							<input type="radio" <?php if($select_user['married_status']=='1'){ echo 'checked'; } ?> value="1" name="married_status">
							: Marrid
							
							<input type="radio" <?php if($select_user['married_status']=='0'){ echo 'checked'; } ?> value="0" name="married_status">
							: Unmarrid
						</div>
					</div>
					<div class="col-lg-4 col-md-6">
						<div class="form-group">
							<label>Husbent/wife Name</label>
							<input type="text" name="husbent/wife_name" value="<?php echo $select_user['husbent/wife_name']; ?>" class="form-control">
						</div>
					</div>
					<div class="col-lg-4 col-md-6">
						<div class="form-group">
							<label>Blod Group</label>
							<input type="text" name="blod_grup" value="<?php echo $select_user['blod_grup']; ?>" class="form-control">
						</div>
					</div>
					<div class="col-lg-4 col-md-6">
						<div class="form-group">
							<label>Contact No.</label>
							<input type="text" name="contact_no" value="<?php echo $select_user['phone']; ?>"  class="form-control">
						</div>
					</div>
					<div class="col-lg-4 col-md-6">
						<div class="form-group">
							<label>Address</label>
							<textarea class="form-control"   name="address"><?php echo $select_user['address']; ?></textarea>
						</div>
					</div>
					<div class="col-lg-4 col-md-6">
						<div class="form-group">
							<label>Permenent Address</label>
							<textarea class="form-control"  name="permenent_address"> <?php echo $select_user['permenent_address']; ?></textarea>
						</div>
					</div>
					</div>
					<div class="row">
					<h2>Current Position</h2>
					<div class="col-lg-4 col-md-6">
						<?php $user_type = $funObj->getTableData('designation');  ?>
						<div class="form-group">
							<label>Designation</label>
							<select name="designation" class="form-control templatingSelect2">
							<option value="">Select designation</option>
							<?php while($fetch=mysql_fetch_array($user_type)): ?>
							<option <?php if($fetch['designation']==$select_user['designation']) echo "selected"; ?> value="<?php echo $fetch['designation']; ?>"><?php echo $fetch['designation']; ?></option>
							<?php endwhile; ?>
							</select>
						</div>
					</div>
					<div class="col-lg-4 col-md-6">
						<?php $user_type = $funObj->getTableData('usertype');  ?>
						<div class="form-group">
							<label>Department</label>
							<td><select name="usertype" class="form-control templatingSelect2">
							<option value="">Select Position</option>
							<?php while($fetch=mysql_fetch_array($user_type)): ?>
							<option <?php if($fetch['usertype']==$select_user['usertype']) echo "selected"; ?> value="<?php echo $fetch['usertype']; ?>"><?php echo $fetch['usertype']; ?></option>
							<?php endwhile; ?>
							</select>
						</div>
					</div>
					
					<div class="col-lg-4 col-md-6">
						<div class="form-group">
							<label>Grade</label>
							<select name="grade" class="form-control templatingSelect2">
								<option value="1">1</option>
								<option value="2">2</option>
								<option value="3">3</option>
								<option value="4">4</option>
							</select>
						</div>
					</div>
					 
					 <div class="col-lg-4 col-md-6">
						<div class="form-group">
							<label>Date of Joining</label>
							<input type="text" name="date_of_joining" value="<?php echo date('d-m-Y',strtotime($select_user['date_of_joining'])); ?>" class="form-control datepickers">
						</div>
					</div>
					
					<div class="col-lg-4 col-md-6">
						<div class="form-group">
							<label>Date of Leaving</label>
							<input type="text" name="date_of_leaving" value="<?php echo date('d-m-Y',strtotime($select_user['date_of_leaving'])); ?>" class="form-control datepickers">
						</div>
					</div>
					</div>
					
					
					<div class="row">
					<h2>Salary Detail</h2>
						<div class="col-lg-6">
							<div class="form-group">
								<label>Allow For PF.</label>
								
								<input type="radio" class="pfstatus" value="yes" <?php if($select_user['allow_pf']=='yes'){ echo 'checked '; } ?>   name="allow_pf">
								: Yes
								<input type="radio" class="pfstatus" <?php if($select_user['allow_pf']=='no'){ echo 'checked '; } ?>   value="no" name="allow_pf">
								: No
							</div>
						</div>
					</div>
					<div class="row">
					
					<div class="col-lg-6">
						<div class="form-group">
							<label>Salary</label>
							<input class="form-control calculation" value="<?php echo $select_user['salary']; ?>" id="salary" type="text" name="salary">
						</div>
					</div>
					</div>

					<div class="row">
					<div class="col-lg-6">
						<div class="form-group">
							<label>Basic Pay</label>
							<input class="form-control calculation" id="basic_pay" value="<?php echo $select_user['basic_pay']; ?>" type="text" name="basic_pay">
						</div>
					</div>
					</div>
					<div class="row">
					<div class="col-lg-6">
						<div class="form-group">
							<label>House Rent</label>
							<input class="form-control calculation" id="house_rent" value="<?php echo $select_user['house_rent']; ?>" type="text" name="house_rent">
						</div>
					</div>
					</div>
					
					<div class="row">
					
					<div class="col-lg-6">
						<div class="form-group">
							<label>Traveling Allowance</label>
							<?php 
							$traveling_allowance=800;
							if($_GET['action']=='edit') {
								$traveling_allowance=$select_user['traveling_allowance'];
							} ?>
							<input class="form-control calculation" id="traveling_allowance" value="<?php echo $traveling_allowance; ?>" type="text" name="traveling_allowance">
						</div>
					</div>
					</div>
					<div class="row">
					<div class="col-lg-6">
						<div class="form-group">
							<label>Edication Allowance</label>
							<?php 
							$edication_allowance=200;
							if($_GET['action']=='edit') {
								$edication_allowance=$select_user['edication_allowance'];
							} ?>
							<input class="form-control calculation" id="edication_allowance" value="<?php echo $edication_allowance; ?>" type="text" name="edication_allowance">
						</div>
					</div>
					</div>
					<div class="row">
					
					<div class="col-lg-6">
						<div class="form-group">
							<label>Pf Amount</label>
							<input class="form-control calculation" id="pf_amount" value="<?php echo $select_user['pf_amount']; ?>" type="text" name="pf_amount">
						</div>
					</div>
					</div>
					<div class="row">
					<div class="col-lg-6">
						<div class="form-group">
							<label>Bonus Amount</label>
							<input class="form-control calculation" id="bonus_amount" value="<?php echo $select_user['bonus_amount']; ?>" type="text" name="bonus_amount">
						</div>
					</div>
					</div>
					<div class="row">
					<div class="col-lg-6">
						<div class="form-group">
							<label>Medical Reimbursement</label>
							<?php 
							$medical_reimbursement=1250;
							if($_GET['action']=='edit') {
								$medical_reimbursement=$select_user['edication_allowance'];
							} ?>
							<input class="form-control calculation" id="medical_reimbursement" value="<?php echo $medical_reimbursement; ?>" type="text" name="medical_reimbursement">
						</div>
					</div>
					</div>
					 
					<div class="row">
					<div class="col-lg-6">
						<div class="form-group">
							<label>Other Allousment</label>
							<input class="form-control calculation" id="other_allousment" value="<?php echo $select_user['other_allousment']; ?>" type="text" name="other_allousment">
						</div>
					</div>
					</div>
					
					<div class="row">
					<div class="col-lg-6">
						<div class="form-group">
							<label>Monthly Gross</label>
							<input class="form-control calculation" id="monthly_gross" value="<?php echo $select_user['monthly_gross']; ?>" type="text" name="monthly_gross">
						</div>
					</div>
					</div>
					
					<div class="row">
					<div class="col-lg-6">
						<div class="form-group">
							<label>Pf Employeer</label>
							<input class="form-control calculation" id="pf_employeer" value="<?php echo $select_user['pf_employeer']; ?>" type="text" name="pf_employeer">
						</div>
					</div>
					</div>
					<div class="row">
					<div class="col-lg-6">
						<div class="form-group">
							<label>Professional Tax</label>
							<?php 
							$professional_tax=200;
							if($_GET['action']=='edit') {
								$professional_tax=$select_user['edication_allowance'];
							} ?>
							<input class="form-control calculation" id="professional_tax" value="<?php echo $professional_tax; ?>" type="text" name="professional_tax">
						</div>
					</div>
					</div>
					<div class="row">
					<div class="col-lg-6">
						<div class="form-group">
							<label>Cost To Company</label>
							<input class="form-control calculation" id="cost_to_company" value="<?php echo $select_user['cost_to_company']; ?>" type="text" name="cost_to_company">
						</div>
					</div>
					</div>
					
					<div class="col-lg-4 col-md-6">
						<div class="form-group">
							<label>Bank Name</label>
							<input class="form-control" value="<?php echo $select_user['bank_name']; ?>" type="text" name="bank_name">
						</div>
					</div>
					<div class="col-lg-4 col-md-6">
						<div class="form-group">
							<label>Pan No.</label>
							<input class="form-control" value="<?php echo $select_user['pan_no']; ?>" type="text" name="pan_no">
						</div>
					</div>
					
					<div class="col-lg-4 col-md-6">
						<div class="form-group">
							<label>PF No.</label>
							<input class="form-control" value="<?php echo $select_user['pf_no']; ?>" type="text" name="pf_no">
						</div>
					</div>
					
					<div class="col-lg-4 col-md-6">
						<div class="form-group">
							<label>ESIC No.</label>
							<input class="form-control" value="<?php echo $select_user['esic_no']; ?>" type="text" name="esic_no">
						</div>
					</div>
					
					<div class="col-lg-4 col-md-6">
						<div class="form-group">
							<label>Bank Acc. No.</label>
							<input class="form-control" value="<?php echo $select_user['bank_acc_no']; ?>" type="text" name="bank_acc_no">
						</div>
					</div>
					<div class="col-lg-4 col-md-6">
						<div class="form-group">
							<label>Employee No.</label>
							<input class="form-control" value="<?php echo $select_user['emp_no']; ?>" type="text" name="emp_no">
						</div>
					</div>
					<div class="col-lg-4 col-md-6">
						<div class="form-group">
							<label>Employee ID.</label>
							<input class="form-control" value="<?php echo $select_user['emp_id']; ?>" type="text" name="emp_id">
						</div>
					</div>
					<div class="col-lg-4 col-md-6">
						<div class="form-group">
							<label>UAN No.</label>
							<input class="form-control" value="<?php echo $select_user['uan_id']; ?>" type="text" name="uan_id">
						</div>
					</div>
					<div class="col-lg-4 col-md-6">
						<div class="form-group">
							<label>ESIC No.</label>
							<input class="form-control" value="<?php echo $select_user['esic_id']; ?>" type="text" name="esic_id">
						</div>
					</div>
					
					</div>
					<div class="row">
					<div class="col-lg-3 col-md-3">
						<div class="form-group">
							 
							
						</div>
					</div>
					<div class="col-lg-6 col-md-6">
						<div class="form-group">
							 
							<input class="btn btn-success btn-block" type="submit" name="submit" value="Submit" />
						</div>
					</div> 
				</div> 
				</form>
				</div>
			</div>
		</div>
	</div>
	
	<?php } ?>
</div>
<?php
include_once('../footer.php');
?>
<script>
 
jQuery(document).ready(function() {
	jQuery('#dataTables-example').DataTable({
			responsive: true
	});
});
 
jQuery(document).ready(function () {
    $('.pfstatus').change(function () {
         var pf_status=jQuery('input[name=allow_pf]:checked').val();
		  
		  
		  var salary=jQuery("#salary").val();
		  var traveling_allowance=jQuery("#traveling_allowance").val();
		  var edication_allowance=jQuery("#edication_allowance").val();
		  var medical_reimbursement=jQuery("#medical_reimbursement").val();
		  var bonus_amount=jQuery('#bonus_amount').val();
		  var professional_tax=jQuery('#professional_tax').val();
		  
		  var basic_pay=salary*40/100;
		  jQuery('#basic_pay').val(basic_pay);
		  
		  var house_rent=basic_pay*40/100;
		  jQuery('#house_rent').val(house_rent);
		  
		  if(pf_status=='yes')
		  {
			var pf_amount=basic_pay*12/100;
		  }else{
			  var pf_amount=0;
		  }
		  jQuery('#pf_amount').val(pf_amount);
		  
		  var bonus_amount=parseInt(basic_pay*8.33/100);
		  jQuery('#bonus_amount').val(bonus_amount);
		  
		  var other_allousment=parseFloat(basic_pay)+parseFloat(house_rent)
		  +parseFloat(traveling_allowance)+
		  parseFloat(edication_allowance)+parseFloat(pf_amount)+parseFloat(bonus_amount)
		  +parseFloat(medical_reimbursement);
		  
		  jQuery('#other_allousment').val(parseInt(salary)-parseInt(other_allousment));
		  jQuery('#monthly_gross').val(parseInt(salary)-(parseInt(pf_amount)+parseInt(bonus_amount)));
		 
		  jQuery('#pf_employeer').val(pf_amount);
		 
		 
		  jQuery('#cost_to_company').val(parseInt(jQuery('#monthly_gross').val())-(parseInt(pf_amount)+parseInt(professional_tax)));
    });

  
});
 
jQuery(function () {
      jQuery(".calculation").bind('input', function() {
		  var pf_status=jQuery('input[name=allow_pf]:checked').val();
		  
		  
		  var salary=jQuery("#salary").val();
		  var traveling_allowance=jQuery("#traveling_allowance").val();
		  var edication_allowance=jQuery("#edication_allowance").val();
		  var medical_reimbursement=jQuery("#medical_reimbursement").val();
		  var bonus_amount=jQuery('#bonus_amount').val();
		  var professional_tax=jQuery('#professional_tax').val();
		  
		  var basic_pay=salary*40/100;
		  jQuery('#basic_pay').val(basic_pay);
		  
		  var house_rent=basic_pay*40/100;
		  jQuery('#house_rent').val(house_rent);
		   
		  if(pf_status=='yes')
		  {
			var pf_amount=basic_pay*12/100;
		  }else{
			  var pf_amount=0;
		  }
		  jQuery('#pf_amount').val(pf_amount);
		  
		  var bonus_amount=parseInt(basic_pay*8.33/100);
		  jQuery('#bonus_amount').val(bonus_amount);
		  
		  var other_allousment=parseFloat(basic_pay)+parseFloat(house_rent)
		  +parseFloat(traveling_allowance)+
		  parseFloat(edication_allowance)+parseFloat(pf_amount)+parseFloat(bonus_amount)
		  +parseFloat(medical_reimbursement);
		  
		  jQuery('#other_allousment').val(parseInt(salary)-parseInt(other_allousment));
		  jQuery('#monthly_gross').val(parseInt(salary)-(parseInt(pf_amount)+parseInt(bonus_amount)));
		 
		  jQuery('#pf_employeer').val(pf_amount);
		 
		 
		  jQuery('#cost_to_company').val(parseInt(jQuery('#monthly_gross').val())-(parseInt(pf_amount)+parseInt(professional_tax)));
		 // alert(house_rent);
      //alert(jQuery(this).val());
	  
      });
   });
</script>