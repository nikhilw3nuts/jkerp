<?php
include_once('../header.php');

include_once('../dbFunction.php');
$funObj = new dbFunction();
if($_POST['addItemCategory'])
{
	$item_category = $_POST['item_category'];
	$isExist = $funObj->checkeAllreadyExist("item_category","item_category",$item_category);
	if($isExist)
	{
		echo "<script>alert('Item Category  aleady exist!')</script>";
	}
	else
	{
		$city = $funObj->addItemCategory($item_category);
	}
}
$item_categorys = $funObj->getTableData('item_category');
?>
<div id="page-wrapper">
    <div class="container">
	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">Item category </h1>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-7">
			<div class="panel panel-default">
				<div class="panel-heading clearfix">
					Item category
				</div>				
				<div class="panel-body">
					<div class="dataTable_wrapper">
						<table class="table table-striped table-bordered table-hover" id="dataTables-example">
							<thead>
								<tr>
									<th>Action</th>
									<th>Item category</th>
								</tr>
							</thead>
							<tbody>
								<?php 
									while($item_category=mysql_fetch_object($item_categorys))
									{
										?>
											<tr class="odd gradeX">
												<td><a onclick="return confirm('Are you sure?');" href="<?php echo SITE_URL.'pages/delete.php?redirect=item_category&table=item_category&field=item_category_id&id='.$item_category->item_category_id; ?>">Delete</a></td>
												<td><?php echo $item_category->item_category; ?></td>
											</tr>	
										<?php
									} 
								?>								
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
		<div class="col-lg-5">
			<div class="panel panel-default">
				<div class="panel-heading clearfix">
					Add Item category
				</div>
				<div style="margin:20px">	
				<form role="form" name="addItemCategory" method="POST">
					<div class="form-group">
						<label>Item category</label>
						<input type="text" name="item_category" required class="form-control">
					</div>
					<input class="btn btn-success btn-block" type="submit" name="addItemCategory" value="Add Item Category" />
				</form>
				</div>
			</div>
		</div>
	</div>
    </div>    
</div>
<?php
include_once('../footer.php');
?>
