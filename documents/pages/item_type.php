<?php
include_once('../header.php');
include_once('../dbFunction.php');
$funObj = new dbFunction();
if($_POST['addItemType'])
{
	$item_type = $_POST['item_type'];
	$tablename = "item_type";
	$fieldname = "item_type";
	$isExist = $funObj->checkeAllreadyExist("item_type","item_type",$item_type);
	if($isExist)
	{
		echo "<script>alert('Item type aleady exist!')</script>";
	}
	else
	{
		$addParty = $funObj->addItemType($item_type);
	}
}
$item_type = $funObj->getTableData('item_type');
?>
<div id="page-wrapper">
    <div class="container">
	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">Item type</h1>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-7">
			<div class="panel panel-default">
				<div class="panel-heading clearfix">
					Item type Detail
				</div>				
				<div class="panel-body">
					<div class="dataTable_wrapper">
						<table class="table table-striped table-bordered table-hover" id="dataTables-example">
							<thead>
								<tr>
									<th>Action</th>
									<th>Item type</th>
								</tr>
							</thead>
							<tbody>
								<?php 
									while($item_types=mysql_fetch_object($item_type))
									{
										?>
											<tr class="odd gradeX">
												<td><a onclick="return confirm('Are you sure?');" href="<?php echo SITE_URL.'pages/delete.php?redirect=item_type&table=item_type&field=item_type_id&id='.$item_types->item_type_id; ?>">Delete</a></td>
												<td><?php echo $item_types->item_type; ?></td>
											</tr>	
										<?php
									} 
								?>								
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
		<div class="col-lg-5">
			<div class="panel panel-default">
				<div class="panel-heading clearfix">
					Item type
				</div>
				<div style="margin:20px">	
				<form role="form" name="addItemType" method="POST">
					<div class="form-group">
						<label>Item type</label>
						<input type="text" name="item_type" required class="form-control">
					</div>
					<input class="btn btn-success btn-block" type="submit" name="addItemType" value="Item Type" />
				</form>
				</div>
			</div>
		</div>
	</div>
    </div>    
</div>
<?php
include_once('../footer.php');
?>
<script>
jQuery(document).ready(function() {
	jQuery('#dataTables-example').DataTable({
			responsive: true
	});
});
</script>
