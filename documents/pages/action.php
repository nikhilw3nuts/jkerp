<?php
include_once('../header.php');

include_once('../dbFunction.php');
$funObj = new dbFunction();
if($_POST['addaction'])
{
	$action = $_POST['action'];
	$isExist = $funObj->checkeAllreadyExist("action","action",$action);
	if($isExist)
	{
		echo "<script>alert('Sales Action aleady exist!')</script>";
	}
	else
	{
		$action= $funObj->addaction($action);
	}
}
$action = $funObj->getTableData('quotation_action');
?>
<div id="page-wrapper">
    <div class="container">
	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">Sales Action</h1>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-7">
			<div class="panel panel-default">
				<div class="panel-heading clearfix">
					Sales Action Detail
				</div>				
				<div class="panel-body">
					<div class="dataTable_wrapper">
						<table class="table table-striped table-bordered table-hover" id="dataTables-example">
							<thead>
								<tr>
									<th>Action</th>
									<th>Sales Action</th>
								</tr>
							</thead>
							<tbody>
								<?php 
									while($actions=mysql_fetch_object($action))
									{
										?>
											<tr class="odd gradeX">
												<td><a onclick="return confirm('Are you sure?');" href="<?php echo SITE_URL.'pages/delete.php?redirect=action&table=quotation_action&field=action_id&id='.$actions->action_id; ?>">Delete</a></td>
												<td><?php echo $actions->action; ?></td>
											</tr>	
										<?php
									} 
								?>								
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
		<div class="col-lg-5">
			<div class="panel panel-default">
				<div class="panel-heading clearfix">
					Add Sales Action
				</div>
				<div style="margin:20px">	
				<form role="form" name="addaction" method="POST">
					<div class="form-group">
						<label>Sales Action</label>
						<input type="text" name="action" required class="form-control">
					</div>
					<input class="btn btn-success btn-block" type="submit" name="addaction" value="Add Sales Action" />
				</form>
				</div>
			</div>
		</div>
	</div>
    </div>
</div>
<?php
include_once('../footer.php');
?>
<script>
jQuery(document).ready(function() {
	jQuery(".state-all").hide();
	jQuery(".state-1").show();
	jQuery(".countrycng").change(function(){
		var curt = jQuery(this).val();
		 jQuery(".state-all").hide();
		 jQuery(".state-"+curt).show();
	})
	jQuery('#dataTables-example').DataTable({
			responsive: true
	});
	
});
</script>
