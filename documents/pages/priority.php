<?php
include_once('../header.php');

include_once('../dbFunction.php');
$funObj = new dbFunction();
if($_POST['addPriority'])
{
	$priority = $_POST['priority'];
	$checketype = $funObj->checkpriority($priority);
	if($checketype)
	{
		echo "<script>alert('Priority aleady exist!')</script>";
	}
	else
	{
		$city = $funObj->addPriority($priority);
	}
}
$prioritys = $funObj->getTableData('priority');
?>
<div id="page-wrapper">
    <div class="container">
	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">Priority </h1>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-7">
			<div class="panel panel-default">
				<div class="panel-heading clearfix">
					Priority
				</div>				
				<div class="panel-body">
					<div class="dataTable_wrapper">
						<table class="table table-striped table-bordered table-hover" id="dataTables-example">
							<thead>
								<tr>
									<th>Action</th>
									<th>Priority</th>
								</tr>
							</thead>
							<tbody>
								<?php 
									while($priority=mysql_fetch_object($prioritys))
									{
										?>
											<tr class="odd gradeX">
												<td><a onclick="return confirm('Are you sure?');" href="<?php echo SITE_URL.'pages/delete.php?redirect=priority&table=priority&field=priority_id&id='.$priority->priority_id; ?>">Delete</a></td>
												<td><?php echo $priority->priority; ?></td>
											</tr>	
										<?php
									} 
								?>								
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
		<div class="col-lg-5">
			<div class="panel panel-default">
				<div class="panel-heading clearfix">
					Add Priority 
				</div>
				<div style="margin:20px">	
				<form role="form" name="addPriority" method="POST">
					<div class="form-group">
						<label>Priority</label>
						<input type="text" name="priority" required class="form-control">
					</div>
					<input class="btn btn-success btn-block" type="submit" name="addPriority" value="Add priority" />
				</form>
				</div>
			</div>
		</div>
	</div>
    </div>    
</div>
<?php
include_once('../footer.php');
?>