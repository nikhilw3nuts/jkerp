<?php 
include_once('../header.php');
include_once('../dbFunction.php');
$funObj = new dbFunction();
$opportu_id = $_GET['id'];
$industry_types = $funObj->getTableData('industry_type');
$users = $funObj->getTableData('user');
$users1 = $funObj->getTableData('user');
$industry_types = $funObj->getTableData('industry_type');
$industry_stage = $funObj->getTableData('industry_stage');
$industry_status = $funObj->getTableData('industry_status');
$reference_inquirys = $funObj->getTableData('reference');
$priority = $funObj->getTableData('priority');
$lead_providers = $funObj->getTableData('lead_provider');
$partytype1 = $funObj->getTableData('partytype1');
?>
<div id="page-wrapper">
    <div class="container">
	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">Add Salse Opportunities Leads.</h1>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-default">
				<div class="panel-body">
					<form role="form" name="addInquiry" method="POST">
						<div class="row">
							<div class="col-lg-4">
								<div class="form-group">
									<label>Lead No.</label>
                                    <input type="text" name="lead_no" placeholder="Lead No." class="form-control">
                                </div>
                            </div>
                            <div class="col-lg-4">
								<div class="form-group">
									<label>Lead Title</label>
                                    <input type="text" placeholder="Lead Title" name="lead_title"  class="form-control">
                                </div>
                            </div>
                            <div class="col-lg-4">
								<div class="form-group">
									<label>Lead Start Date</label>
                                    <input type="text" placeholder="Lead Start Date" name="lead_start_date" value="<?php echo date('d-m-Y'); ?>"  class="form-control datepickers">
                                </div>
                            </div>
						</div>	
                        <div class="row">
							<div class="col-lg-6">
								<div class="form-group">
									<label>Lead End Date</label>
									<input type="text" placeholder="Lead End Date" name="lead_end_date"  class="form-control datepickers">
								</div>
							</div>
							
						</div>                        
                        <div class="row">
							<div class="col-lg-6">
								<div class="form-group">
									<div class="form-group">
										<label>Inquery Type</label>
										 <select class="form-control templatingSelect2" name="industry_type">
                                        <?php
                                            while($industry_type=mysql_fetch_object($industry_types))
                                            {
                                                ?>
                                                <option value="<?php echo $industry_type->industry_type_id; ?>"><?php echo $industry_type->industry_type; ?></option>
                                                <?php
                                            }
                                        ?>	
                                    </select>
									</div>
								</div>
							</div>
                            <div class="col-lg-6">
								<div class="form-group">
									<div class="form-group">
										<label>Lead Owner</label>
										<select class="form-control templatingSelect2" name="lead_owner">
											<?php
												while($user=mysql_fetch_object($users))
												{
													?>
													<option value="<?php echo $user->userid; ?>"><?php echo $user->username; ?></option>
													<?php
												}
											?>	
										</select>
									</div>
								</div>
							</div>
						</div>
                        <div class="row">
							<div class="col-lg-6">
								<div class="form-group">
									<label>Assigned To</label>
                                    <select class="form-control templatingSelect2" name="assigned_to">
                                        <?php
                                            while($user=mysql_fetch_object($users1))
                                            {
                                                ?>
                                                <option value="<?php echo $user->userid; ?>"><?php echo $user->username; ?></option>
                                                <?php
                                            }
                                        ?>	
                                    </select>
								</div>
							</div>
                            <div class="col-lg-6">
								<div class="form-group">
                                    <label>Priority</label>
                                    <select class="form-control templatingSelect2" name="inquiry_priority">
                                        <?php
                                            while($prioritys=mysql_fetch_object($priority))
                                            {
                                                ?>
                                                <option value="<?php echo $prioritys->priority_id; ?>"><?php echo $prioritys->priority; ?></option>
                                                <?php
                                            }
                                        ?>	
                                    </select>
								</div>
							</div>
						</div>
                        <div class="row">
							<div class="col-lg-6">
								<div class="form-group">
									 <label>Lead Source</label>
                                    <select class="form-control templatingSelect2" name="inquiry_reference">
                                        <?php
                                            while($reference_inquiry=mysql_fetch_object($reference_inquirys))
                                            {
                                                ?>
                                                <option value="<?php echo $reference_inquiry->referenceid; ?>"><?php echo $reference_inquiry->referenceby; ?></option>
                                                <?php
                                            }
                                        ?>	
                                    </select>
							</div>
                            <div class="col-lg-6">
								<div class="form-group">
                                     <label>Lead Stage</label>
                                    <select class="form-control templatingSelect2" name="inquiry_stage">
                                        <?php
                                            while($industry_stages=mysql_fetch_object($industry_stage))
                                            {
                                                ?>
                                                <option value="<?php echo $industry_stages->industry_stage_id; ?>"><?php echo $industry_stages->industry_stage; ?></option>
                                                <?php
                                            }
                                        ?>	
                                    </select>
								</div>
							</div>
						</div>
                        <div class="row">
							<div class="col-lg-6">
								<div class="form-group">
									<label>Lead Provider</label>
                                    <select class="form-control templatingSelect2" name="leadprovide">
                                        <?php
										
                                            while($lead_provider=mysql_fetch_object($lead_providers))
                                            {
                                                ?>
                                                <option value="<?php echo $lead_provider->lead_provider_id; ?>"><?php echo $lead_provider->lead_provider; ?></option>
                                                <?php
                                            }
                                        ?>	
                                    </select>
								</div>
							</div>
                            <div class="col-lg-6">
								<div class="form-group">
                                    <label>Status</label>
                                    <select class="form-control templatingSelect2" name="inquiry_status">
                                        <?php
                                            while($industry_statuss=mysql_fetch_object($industry_status))
                                            {
                                                ?>
                                                <option value="<?php echo $industry_statuss->industry_status_id; ?>"><?php echo $industry_statuss->industry_status; ?></option>
                                                <?php
                                            }
                                        ?>	
                                    </select>
								</div>
							</div>
						</div>
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label>Review Date</label>
                                    <input type="text" placeholder="Review Date" name="review_date"  class="form-control datepickers">
                                </div>
                            </div>
                            
                        </div>
							
                        <div class="row">
							<div class="col-lg-12">
								<div class="form-group">
									<label>Lead Detail</label>
									<textarea placeholder="Lead Detail" name="lead_detail"></textarea>
								</div>
							</div>
						</div>
						<div class="row" style="padding-top:30px">
							<div class="col-lg-12">
								<input class="btn btn-success" type="submit" name="addopportunity" value="Add Opportunity Lead" />
							</div>
						</div>						
					</form>
				</div>
				<!--tab start-->
				<div class="panel-body">
					<div class="row" style="padding-top:20px">
						<div class="col-lg-12">
							<ul class="nav nav-tabs">
								<li><a data-toggle="tab" href="#party_detail" aria-expanded="false">Party Detail</a></li>
								<li><a data-toggle="tab" href="#customerdetails" aria-expanded="true">Contact Person</a></li>
								<li><a data-toggle="tab" href="#salseactivity" aria-expanded="false">Sales Activity</a></li>
							</ul>
							<div class="tab-content">
								<div id="party_detail" class="tab-pane fade">
									<h4>Add Party Detail</h4>
									<form role="form" method="post" id="inquery_item" name="savecp">
										
										<input type="hidden" name="opportunity_id" value="<?php echo $opportu_id; ?>" />
										<div class="row">
											<div class="col-lg-6">
												<div class="form-group">
													<div class="form-group">
														<label>Party Name	</label>
														<div class="customer_block">
															<input value="" placeholder="party Name" type="text" name="party_name" required class="form-control party_name">
														</div>
													</div>
												</div>
											</div>
											<div class="col-lg-6">
												<div class="form-group">
													<div class="form-group">
														<label>Referance</label>
														<textarea  name="referance" value="" placeholder="Referance" class="form-control"></textarea>
													</div>
												</div>
											</div>
											</div>
											<div class="row">
											<div class="col-lg-6">
												<div class="form-group">
											
													<div class="form-group">
														<label>party Type</label>
														<select class="form-control templatingSelect2" name="party_type">
														<option  value="">select</option>
														<?php while($partytype=mysql_fetch_object($partytype1)){ ?>
														<option value="<?php echo $partytype->partytype1_id; ?>"><?php echo $partytype->partytype1; ?></option>
														<?php } ?>
														</select>
													</div>
												</div>
											</div>
											<div class="col-lg-6">
												<div class="form-group">
													<div class="form-group">
														<label>Pincode</label>
														<input type="text" name="pin_code" value="" placeholder="Pincode" required class="form-control">
													</div>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-lg-6">
												<div class="form-group">
													<div class="form-group">
														<label>Address</label>
														<textarea name="address" value="" placeholder="Address" required class="form-control"></textarea>
													</div>
												</div>
											</div>
											<div class="col-lg-6">
												<div class="form-group">
													<label>Phone No</label>
													<textarea name="phone_no" value="" placeholder="Phone No" class="form-control"></textarea>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-lg-6">
												<div class="form-group">
													<div class="form-group">
														<label>City</label>
														<input type="text" value="" placeholder="City" name="city" required class="form-control">
													</div>
												</div>
											</div>
											<div class="col-lg-6">
												<div class="form-group">
													<div class="form-group">
														<label>State</label>
														<input type="text" name="state" value="" placeholder="State" required class="form-control">
													</div>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-lg-6">
												<div class="form-group">
													<div class="form-group">
														<label>Country</label>
														<input type="text" value="" placeholder="Country" class="form-control" name="country">
													</div>
												</div>
											</div>
											<div class="col-lg-6">
												<div class="form-group">
													<div class="form-group">
														<label>Email</label>
														<input type="text" value="" placeholder="Email" class="form-control" name="email_id">
													</div>
												</div>
											</div>
										</div>
										<div class="row ">
											<div class="col-lg-6">
												<div class="form-group">
													<div class="form-group">
														<label>Fax No.</label>
														<input type="text" value="" placeholder="Fax No." class="form-control" name="fax_no">
													</div>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-lg-6">
												<div class="form-group">
													<div class="form-group">
														<label>Web Site</label>
														<input type="text" value="" placeholder="Web Site"  class="form-control" name="web_site">
													</div>
												</div>
											</div>
											<div class="col-lg-6">
												<input type="submit" name="submit" class="btn btn-success" value="submit">
											</div>
										</div>
										
									</form>
								</div>
								<div id="customerdetails" class="tab-pane fade">
								<h4>Contact Person Detail</h4>
								
									<div class="panel-body">
										<div class="dataTable_wrapper">
											<table class="table table-striped table-bordered table-hover" id="dataTables-example">
												<thead>
													<tr>
														<th>Sr</th>
														<th>Person Name</th>
														<th>Department</th>
														<th>Designation</th>
														<th>Division</th>
														<th>Phone No.</th>
													</tr>
												</thead>
												<tbody>
												<tr>
													<td>1</td>	
													<td>DR.C.sudarshan</td>
													<td>Salse</td>
													<td>Salse Department</td>
													<td>9859859850</td>
													<td>dummy division</td>
												</tr>
												<tr>
													<td>2</td>
													<td>DR.Subrahmanyam</td>
													<td>Marketing</td>
													<td>Marketing Department</td>
													<td>9856478950</td>
													<td>dummy division</td>
												</tr>
												</tbody>
											</table>
										</div>
									</div>
								<div class="row">
									<div class="col-lg-6">
										<div class="form-group">
											<div class="form-group">
												<label>Contact Person</label>
												<input type="text" value="" placeholder="DR.C.sudarshan" class="form-control" name="web_site">
											</div>
										</div>
										<div class="form-group">
											<div class="form-group">
												<label>Department</label>
												<input type="text" value="" placeholder="Department" class="form-control" name="department">
											</div>
										</div>
										<div class="form-group">
											<div class="form-group">
												<label>Designation</label>
												<input type="text" value="" placeholder="Designation" class="form-control" name="designation">
											</div>
										</div>
										<div class="form-group">
											<div class="form-group">
												<label>Phone No.</label>
												<input type="text" value="" placeholder="Phone No." class="form-control" name="phone_no">
											</div>
										</div>
										<div class="form-group">
											<div class="form-group">
												<label>Mobile No.</label>
												<input type="text" value="" placeholder="Mobile No." class="form-control" name="mobile_no">
											</div>
										</div>
										<div class="form-group">
											<div class="form-group">
												<label>Fax No.</label>
												<input type="text" value="" placeholder="Fax No" class="form-control" name="fax_no">
											</div>
										</div>
										
										</div>
									<div class="col-lg-6">
									<div class="form-group">
										<div class="form-group">
											<button class="btn btn-success">Add</button> &nbsp;&nbsp; <button class="btn btn-success">Cancel</button>
										</div>
									</div>
										<div class="form-group">
											<div class="form-group">
												<label>Email Id.</label>
												<input type="text" value="" placeholder="Email" class="form-control" name="email_id">
											</div>
										</div>
										<div class="form-group">
											<div class="form-group">
												<label>Division</label>
												<input type="text" value="" placeholder="Division" class="form-control" name="division">
											</div>
										</div>
										<div class="form-group">
											<div class="form-group">
												<label>Address</label>
												<textarea value="" placeholder="Address" class="form-control" name="Address"></textarea>
											</div>
										</div>
									</div>
									<!--input type="submit" name="submit" class="btn btn-success" value="submit"-->
								 
								</div>
								
								
								
							</div>
							<div id="salseactivity" class="tab-pane fade">
							<h4>Salse Activity</h4>
							<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</p>
							</div>
						</div>	
					</div>
				</div>
				<!--tab end-->
			</div>
		</div>
	</div>
    </div>    
</div>
<?php
include_once('../footer.php');
?>
<script>
jQuery("#inquery_item").submit(function(e) {
		var cpval = jQuery( this ).serialize();
		alert(cpval);
		$.ajax({
			type: "POST",
			url: "<?php echo SITE_URL.'ajaxcall.php?le_opp=add' ?>",
			data: cpval,
			cache: false,
				success: function(html) {
					alert(html);
				}	
		});
		return false;       
    });
</script>