<?php
include_once('../header.php');

include_once('../dbFunction.php');
$funObj = new dbFunction();

$inquirys = $funObj->getTableData('inquiry');

?>
<div id="page-wrapper">
    <div class="container">
	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">Sales Opportunities/Leads</h1>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-default">
				<div class="panel-heading clearfix">
					Sales Opportunities/Leads Detail
                    <a class="btn btn-primary" style="float:right" href="<?php echo SITE_URL ?>pages/add_salse_opportunities_leads.php">Add Sales Opportunities/Leads</a>
				</div>				
				<div class="panel-body">
					<div class="dataTable_wrapper">
						<table class="table table-striped table-bordered table-hover" id="dataTables-example">
							<thead>
								<tr>
									<th>Lead No</th>
									<th>Lead Title</th>
									<th>Start Date</th>
									<th>End Date</th>
									<th>Lead Owner</th>
									<th>Assign To</th>
									<th>Stage</th>
									<th>Status</th>
								</tr>
							</thead>
							<tbody>
								<?php 
									/* while($inquiry=mysql_fetch_object($inquirys))
									{ */
										?>
											<tr class="gradeX odd" role="row">
												<td class="sorting_1"><a href="" onclick="return confirm('Are you sure?');">Delete</a> | <a href="salse_opportunities_lead_detail.php?id=2">Edit</a> </td>
												<td>1</td>
												<td>This is Title</td>
												<td>9-12-2016</td>
												<td>9-1-2017</td>
												<td>W3nuts</td>
												<td>Test</td>
												<td>Active</td>
											</tr>
											<tr class="gradeX odd" role="row">
												<td class="sorting_1"><a href="" onclick="return confirm('Are you sure?');">Delete</a> | <a href="salse_opportunities_lead_detail.php?id=2">Edit</a> </td>
												<td>2</td>
												<td>This is Title2</td>
												<td>10-10-2016</td>
												<td>10-11-2017</td>
												<td>W3nuts</td>
												<td>Test</td>
												<td>Active</td>
											</tr>
											<tr class="gradeX odd" role="row">
												<td class="sorting_1"><a href="" onclick="return confirm('Are you sure?');">Delete</a> | <a href="salse_opportunities_lead_detail.php?id=2">Edit</a> </td>
												<td>3</td>
												<td>This is Title2</td>
												<td>11-10-2016</td>
												<td>11-11-2017</td>
												<td>W3nuts</td>
												<td>Test</td>
												<td>Active</td>
											</tr>
											<tr class="gradeX odd" role="row">
												<td class="sorting_1"><a href="" onclick="return confirm('Are you sure?');">Delete</a> | <a href="salse_opportunities_lead_detail.php?id=2">Edit</a> </td>
												<td>4</td>
												<td>This is Title2</td>
												<td>12-10-2016</td>
												<td>12-11-2017</td>
												<td>W3nuts</td>
												<td>Test</td>
												<td>Active</td>
											</tr>											
										<?php
									/* } */ 
								?>								
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
    </div>
</div>
<?php
include_once('../footer.php');
?>
<script>
jQuery(document).ready(function() {
	jQuery('#dataTables-example').DataTable({
			responsive: true
	});
});
</script>