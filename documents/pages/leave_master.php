<?php
include_once('../header.php');

include_once('../dbFunction.php');
$funObj = new dbFunction();

$filed_value['total_seek_leave']=$_POST['total_seek_leave'];
$filed_value['total_casual_leave']=$_POST['total_casual_leave'];
$filed_value['total_el_pl_leave']=$_POST['total_el_pl_leave'];
$tablename="leave_master";

if( isset($_POST['submit']))
{
	//$funObj->addfunction($tablename,$filed_value);
	$funObj->updatefunction($tablename,$filed_value,'id',1);
}
$get_data = $funObj->getTableData($tablename);
$fetch_val=mysql_fetch_array($get_data);
?>
<div id="page-wrapper">
    <div class="container">
	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">Leave Master</h1>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-12 employee-master">
			<div class="panel panel-default">
				<div class="panel-heading clearfix">
					Total Free Leave For Employee
				</div>
				<div style="margin:20px">	
				<form role="form" name="addleave" method="POST">
				<div class="row">
				
					<div class="col-lg-4 col-md-6">
						<div class="form-group">
							<label>Total SL.</label>
							<input type="text" name="total_seek_leave" value="<?php echo $fetch_val['total_seek_leave']; ?>" class="form-control">
						</div>
					</div>
					<div class="col-lg-4 col-md-6">
						<div class="form-group">
							<label>Total CL.</label>
							<input type="text" name="total_casual_leave" value="<?php echo $fetch_val['total_casual_leave']; ?>" class="form-control">
						</div>
					</div>
					<div class="col-lg-4 col-md-6">
						<div class="form-group">
							<label>Total EL./PL.</label>
							<input type="text" name="total_el_pl_leave" value="<?php echo $fetch_val['total_el_pl_leave']; ?>" class="form-control">
						</div>
					</div>	 
				</div>
					
					
				<div class="row">
					<div class="col-lg-4 col-md-6">
						<div class="form-group">
							<label></label>
							<input class="btn btn-success btn-block" type="submit" name="submit" value="Submit" />
						</div>
					</div>
				</div>
				
					
					
				</form>
				</div>
			</div>
		</div>
	</div>
    </div>
</div>
<?php
include_once('../footer.php');
?>

