<?php
include_once('../header.php');

include_once('../dbFunction.php');
$funObj = new dbFunction();
if($_POST['addproject'])
{
	$project = $_POST['project'];
	$isExist = $funObj->checkeAllreadyExist("project","project",$project);
	if($isExist)
	{
		echo "<script>alert('Project aleady exist!')</script>";
	}
	else
	{
		unset($_POST['addproject']);
		$funObj->addfunction('project',$_POST);
	}
}
$projects = $funObj->getTableData('project');
?>
<div id="page-wrapper">
    <div class="container">
	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">Project</h1>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-7">
			<div class="panel panel-default">
				<div class="panel-heading clearfix">
					Project Detail
				</div>				
				<div class="panel-body">
					<div class="dataTable_wrapper">
						<table class="table table-striped table-bordered table-hover" id="dataTables-example">
							<thead>
								<tr>
									<th>Action</th>
									<th>Project</th>
								</tr>
							</thead>
							<tbody>
								<?php 
									while($project=mysql_fetch_object($projects))
									{
										?>
											<tr class="odd gradeX">
												<td><a onclick="return confirm('Are you sure?');" href="<?php echo SITE_URL.'pages/delete.php?redirect=project&table=project&field=project_id&id='.$project->project_id; ?>">Delete</a></td>
												<td><?php echo $project->project; ?></td>
											</tr>	
										<?php
									} 
								?>								
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
		<div class="col-lg-5">
			<div class="panel panel-default">
				<div class="panel-heading clearfix">
					Add Currency
				</div>
				<div style="margin:20px">	
				<form role="form" name="addcurrency" method="POST">
					<div class="form-group">
						<label>Project</label>
						<input type="text" name="project" required class="form-control">
					</div>
					<input class="btn btn-success btn-block" type="submit" name="addproject" value="Add Project" />
				</form>
				</div>
			</div>
		</div>
	</div>
    </div>    
</div>
<?php
include_once('../footer.php');
?>
<script>
jQuery(document).ready(function() {
	jQuery(".state-all").hide();
	jQuery(".state-1").show();
	jQuery(".countrycng").change(function(){
		var curt = jQuery(this).val();
		 jQuery(".state-all").hide();
		 jQuery(".state-"+curt).show();
	})
	jQuery('#dataTables-example').DataTable({
			responsive: true
	});
	
});
</script>
