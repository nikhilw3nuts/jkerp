<?php
include_once('../header.php');

include_once('../dbFunction.php');
$funObj = new dbFunction();

$users = $funObj->getTableData('user');
?>
<div id="page-wrapper">
    <div class="container">
	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">City</h1>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-default">
				<div class="panel-heading clearfix">
					City Detail
				</div>				
				<div class="panel-body">
					<div class="dataTable_wrapper">
						<table class="table table-striped table-bordered table-hover" id="dataTables-example">
							<thead>
								<tr>
									<th>Action</th>
									<th>Name</th>
									<th>Email</th>
									<th>Phone</th>
								</tr>
							</thead>
							<tbody>
								<?php 
									while($row=mysql_fetch_object($users))
									{
										
										$stateid = mysql_fetch_array($funObj->getDataById('state',"stateid='".$city->stateid."'"));
										$countryid = mysql_fetch_array($funObj->getDataById('country',"countryid='".$stateid['countryid']."'"));
										?>
											<tr class="odd gradeX">
												<td><a onclick="return confirm('Are you sure?');" href="#">Delete</a>/ <a href="#edit">Edit</a></td>
												<td><?php echo $row->username; ?></td>
												<td><?php echo $row->email; ?></td>
												<td><?php echo $row->phone; ?></td>
											</tr>	
										<?php
									} 
								?>								
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
		
	</div>
    </div>    
</div>
<?php
include_once('../footer.php');
?>
<script>
jQuery(document).ready(function() {
	jQuery(".state-all").hide();
	jQuery(".state-1").show();
	jQuery(".countrycng").change(function(){
		var curt = jQuery(this).val();
		 jQuery(".state-all").hide();
		 jQuery(".state-"+curt).show();
	})
	jQuery('#dataTables-example').DataTable({
			responsive: true
	});
	
});
</script>
