<?php
include_once('../header.php');
include_once('../dbFunction.php');
$funObj = new dbFunction();
if($_POST['addLeadProvider'])
{
	$lead_provider = $_POST['lead_provider'];
	$checkeLeadProvider = $funObj->checkeLeadProvider($lead_provider);
	if($checkeLeadProvider)
	{
		echo "<script>alert('Lead Provider aleady exist!')</script>";
	}
	else
	{
		$addParty = $funObj->addLeadProvider($lead_provider);
	}
}
$lead_provider = $funObj->getTableData('lead_provider');
?>
<div id="page-wrapper">
    <div class="container">
	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">Lead Provider</h1>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-7">
			<div class="panel panel-default">
				<div class="panel-heading clearfix">
					Lead Provider Detail
				</div>				
				<div class="panel-body">
					<div class="dataTable_wrapper">
						<table class="table table-striped table-bordered table-hover" id="dataTables-example">
							<thead>
								<tr>
									<th>Action</th>
									<th>Lead Provider</th>
								</tr>
							</thead>
							<tbody>
								<?php 
									while($lead_providers=mysql_fetch_object($lead_provider))
									{
										?>
											<tr class="odd gradeX">
												<td><a onclick="return confirm('Are you sure?');" href="<?php echo SITE_URL.'pages/delete.php?redirect=lead_provider&table=lead_provider&field=lead_provider_id&id='.$lead_providers->lead_provider_id; ?>">Delete</a></td>
												<td><?php echo $lead_providers->lead_provider; ?></td>
											</tr>	
										<?php
									} 
								?>								
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
		<div class="col-lg-5">
			<div class="panel panel-default">
				<div class="panel-heading clearfix">
					Lead Provider Type
				</div>
				<div style="margin:20px">	
				<form role="form" name="addLeadProvider" method="POST">
					<div class="form-group">
						<label>Lead Provider</label>
						<input type="text" name="lead_provider" required class="form-control">
					</div>
					<input class="btn btn-success btn-block" type="submit" name="addLeadProvider" value="Add Lead Provider" />
				</form>
				</div>
			</div>
		</div>
	</div>
    </div>    
</div>
<?php
include_once('../footer.php');
?>
<script>
jQuery(document).ready(function() {
	jQuery('#dataTables-example').DataTable({
			responsive: true
	});
});
</script>
