<?php
include_once('../header.php');
include_once('../dbFunction.php');
$funObj = new dbFunction();

?>
<div id="page-wrapper">
    <div class="container">
	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">Invoice</h1>
		</div>		
	</div>
	<div class="row">
		<div class="col-lg-12">
        	<div class="panel panel-default">
				<div class="panel-body">
					<form role="form" name="editParty" method="POST">
						<div class="row">
							<div class="col-lg-6">
								<div class="form-group">
									<label>Sell To Party</label>
                                    <div>
                                        <input type="text" class="form-control input-text-sm">
                                        <input type="text" class="form-control input-text-sm">
                                    </div>
								</div>
							</div>
							<div class="col-lg-6">
								<div class="form-group">
									<label>Ship To Party</label>
                                    <div>
                                        <input type="text" class="form-control input-text-sm">
                                        <input type="text" class="form-control input-text-sm">
                                    </div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-lg-6">
								<div class="form-group">
									<label>Challan / Invoice No.</label>
									<input type="text" class="form-control">	
								</div>
							</div>
							<div class="col-lg-6">
								<div class="form-group">
									<label>Date</label>
                                    <input type="text" class="form-control">
								</div>
							</div>
						</div>	
						<div class="row">
							<div class="col-lg-6">
								<div class="form-group">
									<div class="form-group">
										<label>Invoice</label>
										<select class="form-control templatingSelect2">
                                        	<option>General</option>
                                            <option>Other</option>
                                        </select>
									</div>
								</div>
							</div>
							<div class="col-lg-6">
								<div class="form-group">
									<div class="form-group">
										<label>Invoice Type</label>
										<select class="form-control templatingSelect2">
                                        	<option>Tax Invoice</option>
                                            <option>Retail Invoice</option>
                                            <option>Export</option>
                                        </select>
                                        <a class="btn btn-default">? Check List ?</a>
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-lg-6">
								<div class="form-group">
									<label>Custome P.O. No</label>
                                    <input type="text" class="form-control">
								</div>
							</div>
							<div class="col-lg-6">
								<div class="form-group">
									<label>Customer P.O. Date</label>
                                    <input type="text" class="form-control">
								</div>
							</div>
						</div>
						<div class="row" style="padding-top:20px">
							<div class="col-lg-6">
								<div class="form-group">
									<label>Currency</label>
									<select class="form-control templatingSelect2">
                                        <option>INR</option>
                                        <option>DOLOR</option>
                                        <option>POUND</option>
                                    </select>
								</div>
							</div>
							<div class="col-lg-6">
								<div class="form-group">
									<label>Conversion Rate</label>
									<input type="text" class="form-control">
                                    <a class="btn btn-default">? Get Rate ?</a>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-lg-6">
								<div class="form-group">
									<label>? Rounding ?</label>
									<input type="checkbox" class="form-control">
								</div>
							</div>
						</div>
                        <div class="row">
                        	<div class="col-lg-12">
                                <a class="btn btn-default">? Payment Schedules ?</a>
                            </div>
                        </div>
						<div class="row" style="padding-top:30px">
							<div class="col-lg-12">
								<input class="btn btn-success" type="submit" name="editParty" value="Save Challan" />
							</div>
						</div>		
						
					</form>
				</div>
				<div class="panel-body">
					<div class="row" style="padding-top:20px">
						<div class="col-lg-12">
							<ul class="nav nav-tabs">
								<li><a data-toggle="tab" href="#tabitem" aria-expanded="false">Items</a></li>
								<li><a data-toggle="tab" href="#tabbillterms" aria-expanded="false">Billing Terms</a></li>
                                <li><a data-toggle="tab" href="#tabotherterms" aria-expanded="false">Other Terms</a></li>
                                <li><a data-toggle="tab" href="#tabparty" aria-expanded="false">Party</a></li>
                                <li><a data-toggle="tab" href="#tabbuyer" aria-expanded="false">Buyer</a></li>
                                <li><a data-toggle="tab" href="#tabConsignee" aria-expanded="false"> ? Consignee ? </a></li>
                                <li><a data-toggle="tab" href="#tabee" aria-expanded="false">Excise / Export</a></li>
                                <li><a data-toggle="tab" href="#tabAccounting" aria-expanded="false">Accounting</a></li>
                                <li><a data-toggle="tab" href="#tabReports" aria-expanded="false">Reports</a></li>
							</ul>
							<div class="tab-content">
								<div id="tabitem" class="tab-pane fade">
									<h4>Items</h4>
									<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque eget ante hendrerit, ultrices ligula nec, egestas nunc. Quisque lorem lectus, dignissim ac elementum non, </p>
								</div>
                                <div id="tabbillterms" class="tab-pane fade">
									<h4>Items</h4>
									<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque eget ante hendrerit, ultrices ligula nec, egestas nunc. Quisque lorem lectus, dignissim ac elementum non, </p>
								</div>
                                <div id="tabotherterms" class="tab-pane fade">
									<h4>Items</h4>
									<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque eget ante hendrerit, ultrices ligula nec, egestas nunc. Quisque lorem lectus, dignissim ac elementum non, </p>
								</div>
                                <div id="tabparty" class="tab-pane fade">
									<h4>Items</h4>
									<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque eget ante hendrerit, ultrices ligula nec, egestas nunc. Quisque lorem lectus, dignissim ac elementum non, </p>
								</div>
                                <div id="tabparty" class="tab-pane fade">
									<h4>Items</h4>
									<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque eget ante hendrerit, ultrices ligula nec, egestas nunc. Quisque lorem lectus, dignissim ac elementum non, </p>
								</div>
                                <div id="tabbuyer" class="tab-pane fade">
									<h4>Items</h4>
									<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque eget ante hendrerit, ultrices ligula nec, egestas nunc. Quisque lorem lectus, dignissim ac elementum non, </p>
								</div>
                                <div id="tabConsignee" class="tab-pane fade">
									<h4>Items</h4>
									<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque eget ante hendrerit, ultrices ligula nec, egestas nunc. Quisque lorem lectus, dignissim ac elementum non, </p>
								</div>
                                <div id="tabee" class="tab-pane fade">
									<h4>Items</h4>
									<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque eget ante hendrerit, ultrices ligula nec, egestas nunc. Quisque lorem lectus, dignissim ac elementum non, </p>
								</div>
                                <div id="tabAccounting" class="tab-pane fade">
									<h4>Items</h4>
									<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque eget ante hendrerit, ultrices ligula nec, egestas nunc. Quisque lorem lectus, dignissim ac elementum non, </p>
								</div>
                                <div id="tabReports" class="tab-pane fade">
									<h4>Items</h4>
									<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque eget ante hendrerit, ultrices ligula nec, egestas nunc. Quisque lorem lectus, dignissim ac elementum non, </p>
								</div>
							</div>
						</div>	
					</div>
				</div>
			</div>
		</div>
	</div>
    </div>    
</div>
<?php
include_once('../footer.php');
?>
	
