<?php
include_once('../header.php');

include_once('../dbFunction.php');
$funObj = new dbFunction();
if($_POST['adddrawing'])
{
	$drawing = $_POST['drawingnumber'];
	$isExist = $funObj->checkeAllreadyExist("item_drawing","drawing_value",$drawing);
	if($isExist)
	{
		echo "<script>alert('Item Drawing Number aleady exist!')</script>";
	}
	else
	{
		$city = $funObj->adddrawing($drawing);
	}
}
$drawings = $funObj->getTableData('item_drawing');
?>
<div id="page-wrapper">
    <div class="container">
	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">Drawing Number </h1>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-7">
			<div class="panel panel-default">
				<div class="panel-heading clearfix">
				  Item Drawing Number  category 
				</div>				
				<div class="panel-body">
					<div class="dataTable_wrapper">
						<table class="table table-striped table-bordered table-hover" id="dataTables-example">
							<thead>
								<tr>
									<th>Action</th>
									<th>Drawing Numbers</th>
								</tr>
							</thead>
							<tbody>
								<?php 
									while($drawing=mysql_fetch_object($drawings))
									{
										?>
											<tr class="odd gradeX">
												<td><a onclick="return confirm('Are you sure?');" href="<?php echo SITE_URL.'pages/delete.php?redirect=drawingno&table=item_drawing&field=drawing_id&id='.$drawing->drawing_id; ?>">Delete</a></td>
												<td><?php echo $drawing->drawing_value; ?></td>
											</tr>	
										<?php
									} 
								?>								
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
		<div class="col-lg-5">
			<div class="panel panel-default">
				<div class="panel-heading clearfix">
					Add Drawing Number 
				</div>
				<div style="margin:20px">	
				<form role="form" name="adddrawing" method="POST">
					<div class="form-group">
						<label>Drawing Number </label>
						<input type="text" name="drawingnumber" required class="form-control">
					</div>
					<input class="btn btn-success btn-block" type="submit" name="adddrawing" value="Add Drawing Number " />
				</form>
				</div>
			</div>
		</div>
	</div>
    </div>    
</div>
<?php
include_once('../footer.php');
?>
