<?php
include_once('../header.php');

include_once('../dbFunction.php');
$funObj = new dbFunction();
if($_POST['addSalesType'])
{
	$sales_type = $_POST['sales_type'];
	$isExist = $funObj->checkeAllreadyExist("sales_type","sales_type",$sales_type);
	if($isExist)
	{
		echo "<script>alert('Sales Type aleady exist!')</script>";
	}
	else
	{
		$quotation_type = $funObj->addSalesType($sales_type);
	}
}
$sales_type = $funObj->getTableData('sales_type');
?>
<div id="page-wrapper">
    <div class="container">
	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">Sales Type</h1>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-7">
			<div class="panel panel-default">
				<div class="panel-heading clearfix">
					Sales Type Detail
				</div>				
				<div class="panel-body">
					<div class="dataTable_wrapper">
						<table class="table table-striped table-bordered table-hover" id="dataTables-example">
							<thead>
								<tr>
									<th>Action</th>
									<th>Sales Type Type</th>
								</tr>
							</thead>
							<tbody>
								<?php 
									while($sales_types=mysql_fetch_object($sales_type))
									{
										?>
											<tr class="odd gradeX">
												<td><a onclick="return confirm('Are you sure?');" href="<?php echo SITE_URL.'pages/delete.php?redirect=sales_type&table=sales_type&field=sales_type_id&id='.$sales_types->sales_type_id; ?>">Delete</a></td>
												<td><?php echo $sales_types->sales_type; ?></td>
											</tr>	
										<?php
									} 
								?>								
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
		<div class="col-lg-5">
			<div class="panel panel-default">
				<div class="panel-heading clearfix">
					Add Sales Type
				</div>
				<div style="margin:20px">	
				<form role="form" name="addSalesType" method="POST">
					<div class="form-group">
						<label>Sales Type</label>
						<input type="text" name="sales_type" required class="form-control">
					</div>
					<input class="btn btn-success btn-block" type="submit" name="addSalesType" value="Add Sales Type" />
				</form>
				</div>
			</div>
		</div>
	</div>
    </div>    
</div>
<?php
include_once('../footer.php');
?>
