<?php
include_once('../header.php');
session_start();

include_once('../dbFunction.php');
$funObj = new dbFunction();
$employee_leave = $funObj->getTableData('user');
?>
<div id="page-wrapper">
    <div class="container">
	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">Apply Leave</h1>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-6">
			<div class="panel panel-default">
				<div class="panel-heading clearfix">
					Leave Detail
				</div>				
				<div class="panel-body">
					<div class="dataTable_wrapper">
						<table class="table table-striped table-bordered table-hover table-responsive" id="dataTables-example">
							<thead>
								<tr>
									<th>Action</th>
									<th>User Name</th> 
									<th>User Type</th> 
									<th>Designation</th>
									<th>Cost to company</th> 
								</tr>
							</thead>
							<tbody>
								<?php 
									while($row=mysql_fetch_object($employee_leave))
									{
										?>
											<tr class="odd gradeX">
												<td><a href="calculate_salary.php?calculate_salary=<?php echo $row->userid ?>">Calculate Salary</a></td>
												<td><?php echo $row->username; ?></td> 
												<td><?php echo $row->usertype; ?></td> 
												<td><?php echo $row->designation; ?></td>
												<td><?php echo $row->cost_to_company; ?></td>
											</tr>	
							<?php   }   ?>								
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
		<?php if($_GET['calculate_salary']){ ?>
		<div class="row">
		<div class="col-lg-6">
				<form method="POST">
				<p>we are calculating salary based on <b>"cost to company"</b></p>
				<div class="form-group">
					<label>Select Month</label>
					<select class="form-control" name="month"> 
						<option>Select</option> 
						<option value="01">01</option>
						<option value="02">02</option>
						<option value="03">03</option>
						<option value="04">04</option>
						<option value="05">05</option>
						<option value="06">06</option>
						<option value="07">07</option>
						<option value="08">08</option>
						<option value="09">09</option>
						<option value="10">10</option>
						<option value="11">11</option>
						<option value="12">12</option> 
					</select>
				</div>
				<div class="form-group">
					<label>Select Year</label>
						<select class="form-control" name="year"> 
							<option value="<?php echo date('Y'); ?>"><?php echo date('Y'); ?></option> 
						</select>
				</div>
				<input type="submit" name="submit" value="submit">
				</form>
							
				
		<?php } 
		
		
			
		if(isset($_POST['submit']))
		{
			$montr_year=$_POST['year'].'-'.$_POST['month'];
			 $select=mysql_query("select * from yearly_holiday where holiday_date LIKE '%".$montr_year."%'");
			//get company holiday
			$company_holiday=mysql_num_rows($select);
			
			//total no of day in month
			$number = cal_days_in_month(CAL_GREGORIAN, $_POST['month'], 2003); // 31
		 
			
			//total working day (-) company leave
			$final_working_day=$number - $company_holiday;
			$salary=mysql_fetch_array(mysql_query("select * from user where userid = '".$_GET['calculate_salary']."'"));
			$total_cost_comp=$salary['cost_to_company'];
			
			//calculate monthly perday salary
			$per_day_salary=$total_cost_comp/$final_working_day;
			
			
			//calculate SL for all current year
			
			$year=$_POST['year'];
			/**************************************Calculate SL***************************************/				
			$cal_leave=mysql_fetch_array(mysql_query("select SUM(IF(day= 'half_day',0.5,1.0)) as totalleave from employee_leave where userid = '".$_GET['calculate_salary']."' 
			and date LIKE '%".$year."-%' and leave_type='SL' and status='approve'"));
			//$total_sl=mysql_num_rows($cal_leave);
			
			$total_sl=$cal_leave['totalleave'];
			
			
			//calculate SL for current month
			echo "select SUM(IF(day= 'half_day',0.5,1.0)) as totalleave from employee_leave where userid = '".$_GET['calculate_salary']."' 
			and date LIKE '%".$montr_year."%' and leave_type='SL' and status='approve'";
			$cal_SL_current_month=mysql_fetch_array(mysql_query("select SUM(IF(day= 'half_day',0.5,1.0)) as totalleave from employee_leave where userid = '".$_GET['calculate_salary']."' 
			and date LIKE '%".$montr_year."%' and leave_type='SL' and status='approve'"));
			$month_sl=$cal_SL_current_month['totalleave']; 
			
			
			if(($total_sl-$month_sl) >= 5)
			{
				$flag='true';
			}else{
				$flag='false';
			}
			
			if($flag=='true')
			{
				$salary_working_days_SL = $final_working_day - $month_sl; 
			}
			else
			{
				if($total_sl > 5)	
				{
					$salary_working_days_SL = $final_working_day-($total_sl-5);
				}
				else
				{
					$salary_working_days_SL = $final_working_day;
				}		
			}
			$salary_working_days_SL;
			$final_amount_after_SL = $salary_working_days_SL*$per_day_salary;

			/**************************************Calculate CL***************************************/				
			//calculate CL for current Year 
			$year=$_POST['year'];
			$cal_cl_leave=mysql_fetch_array(mysql_query("select SUM(IF(day= 'half_day',0.5,1.0)) as totalleave from employee_leave where userid = '".$_GET['calculate_salary']."' 
			and date LIKE '%".$year."-%' and leave_type='CL' and status='approve'")); 
			$total_sl=$cal_cl_leave['totalleave']; 
			
			//calculate CL for current month
			$cal_SL_current_month=mysql_fetch_array(mysql_query("select SUM(IF(day= 'half_day',0.5,1.0)) as totalleave from employee_leave where userid = '".$_GET['calculate_salary']."' 
			and date LIKE '%".$montr_year."%' and leave_type='CL' and status='approve'"));
			$month_sl=$cal_SL_current_month['totalleave']; 
			
			if(($total_sl-$month_sl) >= 10)
			{
				$flag='true';
			}else{
				$flag='false';
			}
			if($flag=='true')
			{
				$salary_working_days_CL = $salary_working_days_SL - $month_sl; 
			}
			else
			{
				if($total_sl > 10)	
				{
					$salary_working_days_CL = $salary_working_days_SL-($total_sl-10);
				}
				else
				{
					$salary_working_days_CL = $salary_working_days_SL;
				}
			}
			$salary_working_days_CL;
			$final_amount_after_CL = $salary_working_days_CL*$per_day_salary;
			
			/**************************************Calculate EL/PL (carri forward)***************************************/
			//calculate EL/PL for current Year 
			
			$year=$_POST['year'];
			$cal_cl_leave=mysql_fetch_array(mysql_query("select SUM(IF(day= 'half_day',0.5,1.0)) as totalleave from employee_leave where userid = '".$_GET['calculate_salary']."' 
			and date LIKE '%".$year."-%' and leave_type='EL/PL' and status='approve'")); 
			$total_sl=$cal_cl_leave['totalleave']; 
			
			//calculate EL/PL for current month
			$cal_SL_current_month=mysql_fetch_array(mysql_query("select SUM(IF(day= 'half_day',0.5,1.0)) as totalleave from employee_leave where userid = '".$_GET['calculate_salary']."' 
			and date LIKE '%".$montr_year."%' and leave_type='EL/PL' and status='approve'"));
			$month_sl=$cal_SL_current_month['totalleave']; 
			
			if(($total_sl-$month_sl) >= 10)
			{
				$flag='true';
			}else{
				$flag='false';
			}
			if($flag=='true')
			{
				$salary_working_days_EL = $salary_working_days_CL - $month_sl; 
			}
			else
			{
				if($total_sl > 10)	
				{
					$salary_working_days_EL = $salary_working_days_CL-($total_sl-10);
				}
				else
				{
					$salary_working_days_EL = $salary_working_days_CL;
				}
			}
			$final_amount_after_CL = $salary_working_days_EL*$per_day_salary;
			$total_SL=$final_working_day-$salary_working_days_SL;
			$total_CL=$final_working_day-$salary_working_days_CL;
			$total_EL=$final_working_day-$salary_working_days_EL;
			
			echo '<p>Total Working Day <b>'.$final_working_day.'</b></p>';
			echo '<p>Per Day Salary <b>'.$per_day_salary.'</b></p>';
			echo '<p>Cost to Company after Leave Deduction <b>'.$final_amount_after_CL.'</b></p>';

			
			 
		}
		?>
		<div class="panel-body">
					<div class="dataTable_wrapper">
						<table class="table table-striped table-bordered table-hover table-responsive">
							<thead>
								<tr>
									<th>Name</th>
									<th>&nbsp;</th> 
									<th>Employee Code</th> 
									<th>#N/A</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>DESIGNATION</td>
									<td>#N/A</td>
									<td>MONTH</td>
									<td>&nbsp;</td>
								</tr>
								<tr>
									<td>Department</td>
									<td>#N/A</td>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
								</tr>
								<tr>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
								</tr>
								<tr>
									<td colspan="2">EARNINGS</td>
									<td colspan="2">DEDUCTIONS</td>
								</tr>
								<tr>
									<th>SALARY HEAD</th>
									<th>AMOUNT (Rs.)</th> 
									<th>SALARY HEAD</th> 
									<th>AMOUNT (Rs.)</th>
								</tr>
								<tr>
									<td>Basic Salary</td>
									<td>6800</td>
									<td>PF</td>
									<td>816</td>
								</tr>
								<tr>
									<td>HRA</td>
									<td>2720</td>
									<td>ESI</td>
									<td>-</td>
								</tr>
								<tr>
									<td>TA</td>
									<td>800</td>
									<td>TDS</td>
									<td>-</td>
								</tr>
								<tr>
									<td>Medical Allowance</td>
									<td>1250</td>
									<td>Loan/Advance adjustment</td>
									<td>-</td>
								</tr>
								<tr>
									<td>CEA</td>
									<td>200</td>
									<td>PT</td>
									<td>200</td>
								</tr>
								<tr>
									<td>Other Allowance</td>
									<td>3848</td>
									<td>PT</td>
									<td>200</td>
								</tr>
								<tr>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
								</tr>
								<tr>
									<td>Net Pay</td>
									<td>14602</td>
									<td></td>
									<td>&nbsp;</td>
								</tr>
								
								<tr>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
									<td>&nbsp;</td>
								</tr>
								<tr>
									<td>Prepared by</td>
									<td colspan="2">Checked by</td>
									<td>Authorised by</td>
								</tr>
								<tr>
									<td>&nbsp;</td>
									<td colspan="2">&nbsp;</td>
									<td>&nbsp;</td>
								</tr>
							</tbody>
						</table>
						
						
					</div>
				</div>
			</div>
		</div>	
		
		</div>
    </div>    
</div>
<?php
include_once('../footer.php');
?>