<?php
include_once('../header.php');
include_once('../dbFunction.php');
$funObj = new dbFunction();

$uqotid = $_GET['id'];
if($_POST['addQuotation'])
{
	$inquiry_no = $_REQUEST['inquiry_no'];
	$inquiry_date = $_REQUEST['inquiry_date'];
	$quotation_date = $_REQUEST['quotation_date'];
	$revision = $_REQUEST['revision'];
	$revision_date = $_REQUEST['revision_date'];
	$review_date = $_REQUEST['review_date'];
	$due_date = $_REQUEST['due_date'];
	$customer_code1 = $_REQUEST['customer_code1'];
	$reason = $_REQUEST['reason'];
	$reason_remarks = $_REQUEST['reason_remarks'];
	$currency = $_REQUEST['currency'];
	$conversion_rate = $_REQUEST['conversion_rate'];
	$sales = $_REQUEST['sales'];
	$quotation_type = $_REQUEST['quotation_type'];
	$action = $_REQUEST['action'];
	$status = $_REQUEST['status'];
	$sales_type = $_REQUEST['sales_type'];
	$quotation_stage = $_REQUEST['quotation_stage'];
	$rounding = $_REQUEST['rounding'];
	
	$editQuotation = $funObj->editQuotation($inquiry_no,$inquiry_date,$quotation_date,$revision,$revision_date,$review_date,$due_date,$customer_code1,$reason,$reason_remarks,$currency,$conversion_rate,$sales,$quotation_type,$action,$status,$sales_type,$quotation_stage,$rounding,$uqotid);	
	
	if($editQuotation)
	{
		?>
			<script>
               alert("Quotation updated successfully")
            </script>
		<?php
	}
	
}
$reference = $funObj->getTableData('reference');
$quotation_reason = $funObj->getTableData('quotation_reason');
$sales = $funObj->getTableData('sales');
$quotation_type = $funObj->getTableData('quotation_type');
$quotation_action = $funObj->getTableData('quotation_action');
$quotation_status = $funObj->getTableData('quotation_status');
$sales_type = $funObj->getTableData('sales_type');
$quotation_stage = $funObj->getTableData('quotation_stage');
$inquiries = $funObj->getTableData('inquiry');
$currency =  $funObj->getTableData('currency');
$agent =  $funObj->getTableData('agent');
$getQuotation = mysql_fetch_array($funObj->getDataById("quotation_master","quotation_id='".$uqotid."'"));
$customerDetail = mysql_fetch_array($funObj->getDataById("party_master","party_code='".$getQuotation['customer_code']."'"));
$quotation_billing_terms = mysql_fetch_array($funObj->getDataById("quotation_billing_terms","quotation_id='".$_GET['id']."'"));


$quotationAgent = $funObj->getDataById("quotaion_agent","quotation_id='".$uqotid."'");
$ii_status = $funObj->getTableData('industry_status');
$uoms = $funObj->getTableData('uom');
$getII = $funObj->getDataById("quotation_item","quotation_id='".$uqotid."'");
$getPartyCP = mysql_fetch_array($funObj->getDataById("contact_person","party_code='".$getQuotation['customer_code']."' ORDER BY priority ASC LIMIT 1"));
$getSpec = mysql_fetch_array($funObj->getDataById("quotation_specification","quotation_id='".$uqotid."'"));
$getQuotationNote = mysql_fetch_array($funObj->getDataById("quotation_note","quotation_id='".$uqotid."'"));
$get_tc = $funObj->getDataById("quotation_terms_condition","quotation_id='".$uqotid."'");
$term_group = $funObj->getTableData('term_group');

$users = $funObj->getTableData('user');
?>
<div id="page-wrapper">
    <div class="container">
	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">Add Quotation</h1>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-default">
				<div class="panel-body">
					<form role="form" name="addQuotation" method="POST">
						<div class="row">
							<div class="col-lg-6">
								<div class="form-group">
									<label>Inquiry No</label>
									<input type="text" value="<?php echo $getQuotation['inquiry_id'] ?>" name="inquiry_no" required class="form-control inquiry_no">
								</div>
							</div>
							<div class="col-lg-6">
								<div class="form-group">
									<label>Inquiry Date</label>
									<input type="text" name="inquiry_date" value="<?php echo $getQuotation['inquiry_date'] ?>" required class="form-control datepickers inquiry_date"> 
								</div>
							</div>
						</div>	
						<div class="row">
							<div class="col-lg-6">
								<div class="form-group">
									<label>Quotation No</label>
									<input type="text" value="<?php echo $getQuotation['quotation_id'] ?>" name="quotation_no" disabled class="form-control">
								</div>
							</div>
							<div class="col-lg-6">
								<div class="form-group">
									<label>Quotation Date</label>
									<input type="text" name="quotation_date" value="<?php echo $getQuotation['quotation_date'] ?>" required class="form-control datepickers" placeholder="dd-mm-yy"> 
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-lg-6">
								<div class="form-group">
									<label>Revision</label>
									<input type="text" name="revision" value="<?php echo $getQuotation['revision'] ?>" required class="form-control revisionJS">
								</div>
							</div>
							<div class="col-lg-6">
								<div class="form-group">
									<label>Revision Date</label>
									<input type="text" name="revision_date" value="<?php echo $getQuotation['revision_date'] ?>" required class="form-control datepickers" placeholder="dd-mm-yy"> 
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-lg-6">
								<div class="form-group">
									<label>Review Date</label>
									<input type="text" name="review_date" value="<?php echo $getQuotation['review_date'] ?>" required class="form-control datepickers" placeholder="dd-mm-yy">
								</div>
							</div>
							<div class="col-lg-6">
								<div class="form-group">
									<label>Due Date</label>
									<input type="text" name="due_date" value="<?php echo $getQuotation['due_date'] ?>" required class="form-control datepickers" placeholder="dd-mm-yy"> 
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-lg-12">
								<div class="form-group">
									<label>Customer Code (Do you want edit customer?)</label>
									<div style="display:flex">
										<input type="text" class="form-control cusromerCode" value="<?php echo $getQuotation['customer_code'] ?>" required readonly name="customer_code1">
										<div class="customer_block">
											<input type="text" class="form-control getCustomer" required value="<?php echo $customerDetail['party_name'] ?>" name="customer_code2">
											<ul class="displayCustomer" style="display: none;">
												
											</ul>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-lg-6">
								<div class="form-group">
									<label>Reason</label>
									<select class="form-control templatingSelect2" name="reason">
										<?php
											while($quotation_reasons=mysql_fetch_object($quotation_reason))
											{
												?>
												<option <?php echo ($getQuotation['reason'] == $quotation_reasons->reason_id)?'selected':''; ?> value="<?php echo $quotation_reasons->reason_id; ?>"><?php echo $quotation_reasons->reason; ?></option>
												<?php
											}
										?>
									</select>	
								</div>
							</div>
							<div class="col-lg-6">
								<div class="form-group">
									<label>Reason Remarks</label>
									<textarea class="form-control" name="reason_remarks"><?php echo $getQuotation['reason_remarks']; ?></textarea>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-lg-6">
								<div class="form-group">
									<label>Currency	</label>
									<select class="form-control templatingSelect2" name="currency">
										<?php
											while($currencys=mysql_fetch_object($currency))
											{
												?>
												<option <?php echo ($getQuotation['currency'] == $currencys->currency_id)?'selected':''; ?> value="<?php echo $currencys->currency_id; ?>"><?php echo $currencys->Currency; ?></option>
												<?php
											}
										?>
									</select>	
								</div>
							</div>
							<div class="col-lg-6">
								<div class="form-group">
									<label>Conversion Rate</label>
									<input type="text" class="form-control" value="<?php echo $getQuotation['conversion_rate']; ?>" required name="conversion_rate">
								</div>
							</div>
						</div>
						
						<div class="row">
							<div class="col-lg-6">
								<div class="form-group">
									<label>Sales</label>
									<select class="form-control templatingSelect2" name="sales">
										<option>Select Sales</option>	
										<?php
											while($saless=mysql_fetch_object($sales))
											{
												?>
												<option <?php echo ($getQuotation['sales'] == $saless->sales_id)?'selected':''; ?> value="<?php echo $saless->sales_id; ?>"><?php echo $saless->sales; ?></option>
												<?php
											}
										?>	
									</select>	
								</div>
							</div>
							<div class="col-lg-6">
								<div class="form-group">
									<label>Quotation Type</label>
									<select class="form-control templatingSelect2" name="quotation_type">
										<?php
											while($quotation_types=mysql_fetch_object($quotation_type))
											{
												?>
												<option <?php echo ($getQuotation['quotation_type'] == $quotation_types->type_id)?'selected':''; ?> value="<?php echo $quotation_types->type_id; ?>"><?php echo $quotation_types->type; ?></option>
												<?php
											}
										?>	
									</select>	
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-lg-6">
								<div class="form-group">
									<label>Action</label>
									<select class="form-control templatingSelect2" name="action">
										<?php
											while($quotation_actions=mysql_fetch_object($quotation_action))
											{
												?>
												<option <?php echo ($getQuotation['action'] == $quotation_actions->action_id)?'selected':''; ?> value="<?php echo $quotation_actions->action_id; ?>"><?php echo $quotation_actions->action; ?></option>
												<?php
											}
										?>
									</select>	
								</div>
							</div>
							<div class="col-lg-6">
								<div class="form-group">
									<label>Status</label>
									<select class="form-control templatingSelect2" name="status">
										<?php
											while($quotation_statuss=mysql_fetch_object($quotation_status))
											{
												?>
												<option <?php echo ($getQuotation['status'] == $quotation_statuss->status_id)?'selected':''; ?> value="<?php echo $quotation_statuss->status_id; ?>"><?php echo $quotation_statuss->status; ?></option>
												<?php
											}
										?>
									</select>	
								</div>
							</div>
						</div>			
						<div class="row">
							<div class="col-lg-6">
								<div class="form-group">
									<label>Sales Type</label>
									<select class="form-control templatingSelect2" name="sales_type">
										<?php
											while($sales_types=mysql_fetch_object($sales_type))
											{
												?>
												<option <?php echo ($getQuotation['sales_type'] == $sales_types->sales_type_id)?'selected':''; ?> value="<?php echo $sales_types->sales_type_id; ?>"><?php echo $sales_types->sales_type; ?></option>
												<?php
											}
										?>
									</select>	
								</div>
							</div>
							<div class="col-lg-6">
								<div class="form-group">
									<label>Quotation Stage</label>
									<select class="form-control templatingSelect2" name="quotation_stage">
										<?php
											while($quotation_stages=mysql_fetch_object($quotation_stage))
											{
												?>
												<option <?php echo ($getQuotation['quotation_stage'] == $quotation_stages->stage_id)?'selected':''; ?> value="<?php echo $quotation_stages->stage_id; ?>"><?php echo $quotation_stages->stage; ?></option>
												<?php
											}
										?>
									</select>	
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-lg-12">
								<div class="form-group">
									<label class="checkbox-inline">
										<input type="checkbox" <?php echo ($getQuotation['rounding'] == 1)?'checked':''; ?> value="1" name="rounding">Rounding
									</label>	
								</div>
							</div>
						</div>
						<div class="row" style="padding-top:30px">
							<div class="col-lg-12">
								<input class="btn btn-success" type="submit" name="addQuotation" value="Add Item" />
							</div>
						</div>		
						
					</form>
				</div>
				<div class="panel-body">
					<div class="row" style="padding-top:20px">
						<div class="col-lg-12">
							<ul class="nav nav-tabs">
								<li><a data-toggle="tab" href="#customerdetails">Customer Details</a></li>
								<li><a data-toggle="tab" href="#itemdetails">Item Details</a></li>
                                <li><a data-toggle="tab" href="#salesagent" aria-expanded="true">Sales Agent</a></li>
								<li><a data-toggle="tab" href="#termscondition" aria-expanded="true">Terms & Condition</a></li>
                                <li><a data-toggle="tab" href="#billingterms" aria-expanded="false">Billing Terms</a></li>
                                <li><a data-toggle="tab" href="#qtnspec">Qtn Spec</a></li>
								<li><a data-toggle="tab" href="#note">Note</a></li>
								<li><a data-toggle="tab" href="#login">Login</a></li>
							</ul>
							<div class="tab-content">
								<div id="attachdocument" class="tab-pane fade">
									<h4>Attach Document</h4>
									<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
								</div>
								<div id="salesagent" class="tab-pane fade">
									<h4>Sales Agent</h4>
									<div class="panel-body">
										<div class="dataTable_wrapper">
											<table class="table table-striped table-bordered table-hover" id="dataTables-example">
												<thead>
													<tr>
														<th>Action</th>
														<th>No.</th>
														<th>Sales Agent</th>
														<th>Sales Office</th>
														<th>Comm (%)</th>
													</tr>
												</thead>
												<tbody class="ajaxqAgent">
												<?php
													$i=1;
													while($quotationAgents=mysql_fetch_object($quotationAgent))
													{
														$agent_det = mysql_fetch_array($funObj->getDataById("agent","agent_id='".$quotationAgents->sales_agent_id."'"));
														?>
															<tr class="odd gradeX sadelete<?php echo $quotationAgents->quotaion_agent_id; ?>">
																<td><a class="sadelete" data-id="<?php echo $quotationAgents->quotaion_agent_id ?>" href="javascript:void(0)">Delete</a></td>
																<td><?php echo $i; ?></td>
																<td><?php echo $agent_det['agent']; ?></td>
																<td><?php echo $quotationAgents->sales_office; ?></td>
																<td><?php echo $quotationAgents->commision_agent; ?></td>
															</tr>
														<?php
														$i++;
													}
												?>
												</tbody>
											</table>
										</div>
									</div>
									<form role="form" method="post" id="addAgent" name="addAgent">
										<input type="hidden" name="ajax" value="qAgent" />
										<input type="hidden" name="quotationid" value="<?php echo $uqotid; ?>" />
										<div class="row">
											<div class="col-lg-6">
												<div class="form-group">
													<label>Sales Agent</label>
													<select class="form-control templatingSelect2" name="salesagent">
														<option>Select Agent</option>
														<?php
															while($agents=mysql_fetch_object($agent))
															{
																?>
																<option data-commision="<?php echo $agents->agent_commission; ?>" value="<?php echo $agents->agent_id; ?>"><?php echo $agents->agent; ?></option>
																<?php
															}
														?>
													</select>	
												</div>
											</div>
											<div class="col-lg-6">
												<div class="form-group">
													<label>Sales Office</label>
													<input type="text"  name="salesoffice" class="form-control">
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-lg-6">
												<div class="form-group">
													<label>Commision (%)</label>
													<input type="text"  name="commision" class="form-control">
												</div>
											</div>
											<div class="col-lg-6">
												<div class="form-group">
													<label>Commision Value</label>
													<input type="text"  name="commisionvalue" class="form-control">
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-lg-6">
												<div class="form-group">
													<label>Remark</label>
													<textarea  name="remark" class="form-control"></textarea>
												</div>
											</div>
										</div>
										<div class="row" style="padding-top:30px">
											<div class="col-lg-12">
												<input class="btn btn-success" type="submit" name="addAgent" value="Add Agent" />
											</div>
										</div>
									</form>
								</div>
								<div id="termscondition" class="tab-pane fade">
                                	<h4>Terms & Condition</h4>
                                    <div class="panel-body">
										<div class="dataTable_wrapper">
											<table class="table table-striped table-bordered table-hover" id="dataTables-example">
												<thead>
													<tr>
														<th>Action</th>
														<th>No.</th>
														<th>Terms Group</th>
														<th>Terms</th>
														<th>Created On</th>
														<th>Created By</th>
													</tr>
												</thead>
												<tbody class="ajaxtc">
													<?php 
														$i=1;
														while($get_tcs=mysql_fetch_object($get_tc))
														{
															$termsgroupid = mysql_fetch_array($funObj->getDataById('term_group',"term_group_id='".$get_tcs->group."'"));
															
															$username = mysql_fetch_array($funObj->getDataById('user',"userid='".$get_tcs->createdby."'"));
															?>
																<tr class="odd gradeX tcdelete<?php echo $get_tcs->quotation_terms_condition ?>">
																	<td><a class="tcdelete" href="javascript:void(0)" data-id="<?php echo $get_tcs->quotation_terms_condition ?>">Delete</a></td>
																	<td><?php echo $i; ?></td>
																	<td><?php echo $termsgroupid['term_group']; ?></td>
																	<td><?php echo $get_tcs->terms_condition; ?></td>
																	<td><?php echo $get_tcs->createdon; ?></td>
																	<td><?php echo $username['username']; ?></td>
																</tr>
															<?php
															$i++;
														}
													?>								
												</tbody>
											</table>
										</div>
									</div>	
									<form role="form" method="post" id="addterms" name="addterms">
										<input type="hidden" name="ajax" value="qtc" />
										<input type="hidden" name="quotationid" value="<?php echo $uqotid ?>" />
										<div class="row">
											<div class="col-lg-6">
												<div class="form-group">
													<label>Terms Group</label>
													<select class="form-control templatingSelect2" name="tc_termsGroup">
														<?php
															while($term_groups=mysql_fetch_object($term_group))
															{
																?>
																	<option value="<?php echo $term_groups->term_group_id ?>"><?php echo $term_groups->term_group; ?></option>	
																<?php
															}
														?>
													</select>	
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-lg-12">
												<div class="form-group">
													<label>Terms</label>
													<textarea name="tc_terms" class="form-control"></textarea>
												</div>
											</div>
										</div>
										<div class="row" style="padding-top:30px">
											<div class="col-lg-12">
												<input class="btn btn-success" type="submit" name="addterms" value="Add Terms" />
											</div>
										</div>
									</form>
                                </div>
                                <div id="billingterms" class="tab-pane fade">
									<h4>Billing Terms</h4>
									<form role="form" method="post" id="savebillterm" name="savebillterm">
										<input type="hidden" name="ajax" value="qtybillterm" />
										<input type="hidden" name="quotationid" value="<?php echo $uqotid; ?>" />
										<div class="row">
											<div class="col-lg-12">
												<div class="form-group">
													<label>Cal Code</label>
													
													<input type="text" name="cal_code1" value="<?php echo $quotation_billing_terms['cal_code1']; ?>" class="form-control">
													<input type="text" name="cal_code2" value="<?php echo $quotation_billing_terms['cal_code2']; ?>" class="form-control">
												</div>
											</div>	
											<div class="col-lg-12">		
												<div class="form-group">
													<label>Nerration</label>
													<input type="text" name="nerration" value="<?php echo $quotation_billing_terms['nerration']; ?>" class="form-control">
												</div>
											</div>
											<div class="col-lg-6">
												<div class="form-group">
													<label>Percentage</label>
													<input type="text" name="percentage" value="<?php echo $quotation_billing_terms['percentage']; ?>" class="form-control">										
												</div>
											</div>
											<div class="col-lg-6">
												<div class="form-group">
													<label>Value</label>
													<input type="text" name="value" value="<?php echo $quotation_billing_terms['value']; ?>" class="form-control">										
												</div>
											</div>
											<div class="col-lg-12">		
												<div class="form-group">
													<label>GL A/c</label>
													<input type="text" name="GLAC2" value="<?php echo $quotation_billing_terms['GLAC2']; ?>" class="form-control">
													<input type="text" name="GLAC2" value="<?php echo $quotation_billing_terms['GLAC2']; ?>" class="form-control">									
												</div>
											</div>
										</div>
										<div class="row" style="padding-top:30px">
											<div class="col-lg-12">
												<input class="btn btn-success" type="submit" name="savebillterm" value="Save Specification" />
											</div>
										</div>
									</form>
								</div>
								<div id="qtnspec" class="tab-pane fade">
									<h4>Quotation Spcification</h4>
									<form role="form" method="post" id="saveqtyspec" name="saveqtyspec">
										<input type="hidden" name="ajax" value="qtyspec" />
										<input type="hidden" name="quotationid" value="<?php echo $uqotid; ?>" />
										<div class="row">
											<div class="col-lg-12">
												<div class="form-group">
													<label>Spcification 1</label>
													<textarea name="spcification1" style="width:90%; height:100px;" class="form-control"><?php echo $getSpec['quotation_specification1'] ?></textarea>
												</div>
												<div class="form-group">
													<label>Spcification 2</label>
													<textarea name="spcification2" style="width:90%; height:100px;" class="form-control"><?php echo $getSpec['quotation_specification2'] ?></textarea>
												</div>
												<div class="form-group">
													<label>Spcification 3</label>
													<textarea name="spcification3" style="width:90%; height:100px;" class="form-control"><?php echo $getSpec['quotation_specification3'] ?></textarea>												
												</div>
											</div>
										</div>
										<div class="row" style="padding-top:30px">
											<div class="col-lg-12">
												<input class="btn btn-success" type="submit" name="saveqtyspec" value="Save Specification" />
											</div>
										</div>
									</form>
								</div>
								<div id="note" class="tab-pane fade">
									<h4>Note</h4>
									<form role="form" method="post" id="qutnote" name="qutnote">
										<input type="hidden" name="ajax" value="qtynote" />
										<input type="hidden" name="quotationid" value="<?php echo $uqotid; ?>" />
										<div class="row">
											<div class="col-lg-12">
												<div class="form-group">
													<label>Quotation Note</label>
													<textarea name="quotation_note" style="width:90%; height:100px;" class="form-control"><?php echo $getQuotationNote['quotation_note'] ?></textarea>
												</div>
												<div class="form-group">
													<label>Authorised By</label>
													<select class="form-control templatingSelect2" name="authorised_by">
														<?php
															while($user=mysql_fetch_object($users))
															{
																?>
																<option <?php echo ($getQuotationNote['authorised_by'] == $user->userid) ? 'selected':''; ?> value="<?php echo $user->userid; ?>"><?php echo $user->username; ?></option>
																<?php
															}
														?>	
													</select>
												</div>
												<div class="form-group">
													<label>Reference Note</label>
													<textarea name="reference_note" style="width:90%; height:100px;" class="form-control"><?php echo $getQuotationNote['reference_note'] ?></textarea>												
												</div>
												<div class="form-group">
													<label>Item Desc Title</label>
													<input type="text" name="item_desc_title" value="<?php echo $getQuotationNote['item_desc_title'] ?>" required class="form-control">
												</div>
												<div class="form-group">
													<label>Addno Title</label>
													<textarea name="addno_title" style="width:90%; height:100px;" class="form-control"><?php echo $getQuotationNote['addno_title'] ?></textarea>												
												</div>
											</div>
										</div>
										<div class="row" style="padding-top:30px">
											<div class="col-lg-12">
												<input class="btn btn-success" type="submit" name="qutnote" value="Save Note" />
											</div>
										</div>
									</form>
								</div>
								<div id="customerdetails" class="tab-pane fade">
									<h4>Customer Details</h4>
									<form role="form" method="post" id="savecustdetail" name="savecustdetail">
										<input type="hidden" class="ajax" name="ajax" value="custdetail" />
										<div class="row">
											<div class="col-lg-6">
												<div class="form-group">
													<div class="form-group">
														<label>Customer Code</label>
														<input type="text" name="customercode" readonly value="<?php echo $customerDetail['party_code']; ?>" class="form-control">
													</div>
												</div>
											</div>
											<div class="col-lg-6">
												<div class="form-group">
													<div class="form-group">
														<label>Customer Name</label>
														<input type="text" readonly name="customername" value="<?php echo $customerDetail['party_name']; ?>" class="form-control">
													</div>
												</div>
											</div>
											<div class="col-lg-6">
												<div class="form-group">
													<div class="form-group">
														<label>Referance By.{--}</label>
														<select readonly name="referance" class="form-control templatingSelect2">
														<?php while($val=mysql_fetch_object($reference)){ ?>
															<option value="<?php echo $val->referenceid; ?>"><?php echo $val->referenceby; ?></option>
														<?php } ?>
														</select>
													</div>
												</div>
											</div>
											<div class="col-lg-6">
												<div class="form-group">
													<div class="form-group">
														<label>Referance By.{--}</label>
														<input type="text" readonly class="form-control" name="ref_date" value="<?php echo date('d-m-Y'); ?>">
													</div>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-lg-4">
												<div class="form-group">
													<div class="form-group">
														<label>Address</label>
														<textarea name="customeraddress" readonly class="form-control"><?php echo $customerDetail['address']; ?></textarea>
													</div>
												</div>
											</div>
											<div class="col-lg-4">
												<div class="form-group">
													<div class="form-group">
														<label>Country</label>
														<select class="form-control selcountry templatingSelect2" readonly name="country">
															<option>Select Country</option>
															<?php
																while($country=mysql_fetch_object($countrys))
																{
																	?>
																	<option <?php echo ($customerDetail['country'] == $country->countryid) ? 'selected' : ''; ?> class="countrydisp country-<?php echo $country->countryid; ?>" value="<?php echo $country->countryid; ?>"><?php echo $country->country; ?></option>
																	<?php
																}
															?>
														</select>
													</div>
												</div>		
												<div class="form-group">
													<div class="form-group">
														<select class="form-control selstates templatingSelect2" readonly name="state">
															<option>State State</option>
															<?php
																while($state=mysql_fetch_object($states))
																{
																	?>
																	<option <?php echo ($customerDetail['state'] == $state->stateid) ? 'selected' : ''; ?> class="statedisp sCountry-<?php echo $state->countryid; ?> state-<?php echo $state->stateid; ?>" data-country="<?php echo $state->countryid; ?>" value="<?php echo $state->stateid; ?>"><?php echo $state->state; ?></option>
																	<?php
																}
															?>
														</select>
													</div>
												</div>
											</div>
											<div class="col-lg-4">
												<div class="form-group">
													<div class="form-group">
														<label>City</label>
														<select class="form-control selcity templatingSelect2" readonly name="city">
															<option>Select City</option>
															<?php
																while($city=mysql_fetch_object($citys))
																{
																	$countryid = mysql_fetch_array($funObj->getDataById('country',"countryid='".$city->stateid."'"));
																	?>
																	<option <?php echo ($customerDetail['city'] == $city->cityid) ? 'selected' : ''; ?> class="citydisp cCountry-<?php echo $countryid['countryid']; ?> cState-<?php echo $city->stateid; ?> city-<?php echo $city->cityid; ?>" data-state="<?php echo $city->stateid; ?>" data-country="<?php echo $countryid['countryid']; ?>"  value="<?php echo $city->cityid; ?>"><?php echo $city->city; ?></option>
																	<?php
																}
															?>
														</select>
													</div>
												</div>
												<div class="form-group">
													<div class="form-group">
														<label>Pincode</label>
														<input type="text" name="customername" readonly value="<?php echo $customerDetail['pincode']; ?>" class="form-control">
													</div>
												</div>
											</div>
										</div>		
										<div class="row">
											<div class="col-lg-4">
												<div class="form-group">
													<div class="form-group">
														<label>Phone No</label>
														<textarea name="customeraddress" readonly class="form-control"><?php echo $customerDetail['phones']; ?></textarea>
													</div>
												</div>
											</div>
											<div class="col-lg-4">
												<div class="form-group">
													<div class="form-group">
														<label>Fax No.</label>
														<input type="text" name="customername" readonly value="<?php echo $customerDetail['fax']; ?>" class="form-control">
													</div>
												</div>		
												<div class="form-group">
													<div class="form-group">
														<label>Email</label> 
														<input type="text" name="customername" readonly value="<?php echo $customerDetail['email']; ?>" class="form-control">
													</div>
												</div>
											</div>
											<div class="col-lg-4">
												<div class="form-group">
													<div class="form-group">
														<label>Website</label>
														<input type="text" name="customername" readonly value="<?php echo $customerDetail['website']; ?>" class="form-control">
													</div>
												</div>
											</div>
										</div>	
										<div class="row">
											<div class="col-lg-12"><h4>Kind Attn. Detail</h4></div>
											<div class="col-lg-4">
												<div class="form-group">
													<div class="form-group">
														<label>Person Name</label>
														<input type="text" name="customername" readonly value="<?php echo $getPartyCP['contact_person']; ?>" class="form-control">
													</div>
												</div>
											</div>
											<div class="col-lg-4">
												<div class="form-group">
													<div class="form-group">
														<label> Email.</label>
														<input type="text" name="customername" readonly value="<?php echo $getPartyCP['email']; ?>" class="form-control">
													</div>
												</div>		
											</div>
											<div class="col-lg-4">
												<div class="form-group">
													<div class="form-group">
														<label>Phone</label>
														<input type="text" name="customername" readonly value="<?php echo $getPartyCP['mobile']; ?>" class="form-control">
													</div>
												</div>
											</div>
										</div>	
										
									</form>
								</div>
								<div id="itemdetails" class="tab-pane fade">
									<h4>Item Details</h4>
									<div class="panel-body">
										<div class="dataTable_wrapper">
											<table class="table table-striped table-bordered table-hover" id="dataTables-example">
												<thead>
													<tr>
														<th>Action</th>
														<th>No</th>
														<th>Item Code</th>
														<th>Sample text</th>
														<th>UOM</th>
														<th>Quantity</th>
														<th>Drawing No.</th>
														<th>Rev</th>
														<th>Status</th>
													</tr>
												</thead>
												<tbody class="ajaxii">
													<?php
														$i=1;
														while($getIIs=mysql_fetch_object($getII))
														{
															$getUomById = mysql_fetch_array($funObj->getDataById("uom","uom_id='".$getIIs->uom."'"));
															$getStatusById = mysql_fetch_array($funObj->getDataById("industry_status","industry_status_id='".$getIIs->status."'"));
															?>
																<tr class="odd gradeX iidelete<?php echo $getIIs->inquiry_item_id ?>">
																	<td><a class="iidelete" href="javascript:void(0)" data-id="<?php echo $getIIs->inquiry_item_id ?>">Delete</a></td>
																	<td><?php echo $i; ?></td>
																	<td><?php echo $getIIs->item_code; ?></td>
																	<td><?php echo $getIIs->note; ?></td>
																	<td><?php echo $getUomById['uom']; ?></td>
																	<td><?php echo $getIIs->required_qty ?></td>
																	<td><?php echo $getIIs->drawing_no; ?></td>
																	<td><?php echo $getIIs->rev; ?></td>
																	<td><?php echo $getStatusById['industry_status']; ?></td>
																</tr>
															<?php
															$i++;
														}
													?>
													
																					
												</tbody>
											</table>
										</div>
									</div>	
									<form role="form" method="post" id="saveii" name="savecp">
										<input type="hidden" class="ajax" name="ajax" value="qi" />
										<input type="hidden" name="ii_inquiryid" value="<?php echo $uqotid ?>" />
										<div class="row">
											<div class="col-lg-4">
												<div class="form-group">
													<div class="form-group">
														<label>Item Code</label>
														<div class="customer_block">
															<input type="text" name="ii_code" required class="form-control ii_code">
															<ul class="getItems search-dropdown"></ul>
														</div>
													</div>
												</div>
											</div>
											<div class="col-lg-4">
												<div class="form-group">
													<div class="form-group">
														<label>Drawing No.</label>
														<input type="text" name="ii_drawing_no" required class="form-control">
													</div>
												</div>
											</div>
											<div class="col-lg-4">
												<div class="form-group">
													<div class="form-group">
														<label>Rev</label>
														<input type="text" name="ii_rev" required class="form-control">
													</div>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-lg-6">
												<div class="form-group">
													<div class="form-group">
														<label>Note</label>
														<input type="text" name="ii_note" required class="form-control">
													</div>
												</div>
											</div>
											<div class="col-lg-6">
												<div class="form-group">
													<div class="form-group">
														<label>UOM</label>
														<select class="form-control templatingSelect2" name="ii_uom">
															<?php
																while($uom=mysql_fetch_object($uoms))
																{
																	?>
																	<option value="<?php echo $uom->uom_id; ?>"><?php echo $uom->uom; ?></option>
																	<?php
																}
															?>
														</select>
													</div>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-lg-6">
												<div class="form-group">
													<div class="form-group">
														<label>Required Qty</label>
														<input type="text" name="ii_qty" required class="form-control">
													</div>
												</div>
											</div>
											<div class="col-lg-6">
												<div class="form-group">
													<div class="form-group">
														<label>Price</label>
														<input type="text" name="ii_price" required class="form-control">
													</div>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-lg-6">
												<div class="form-group">
													<div class="form-group">
														<label>Item Description (cust) {--}</label>
														<textarea id="p_phones" class="form-control" name="ii_description_cust" rows="5"></textarea>
													</div>
												</div>
											</div>
											
											<div class="col-lg-6">
												<div class="form-group">
													<div class="form-group">
														<label>Item Description</label>
														<textarea id="p_phones" class="form-control" name="ii_description" rows="5"></textarea>
													</div>
												</div>
											</div>
											
											<div class="col-lg-6">
												<div class="form-group">
													<div class="form-group">
														<label>Detail Description{--}</label>
														<textarea id="p_phones" class="form-control" name="ii_detail_description" rows="5"></textarea>
													</div>
												</div>
											</div>
											<div class="col-lg-6">
												<div class="form-group">
													<div class="form-group">
														<label>Drawing No.</label>
														<input type="text" name="ii_drawing_no" required class="form-control">
													</div>
												</div>
											</div>
											
											<div class="col-lg-6">
												<div class="form-group">
													<div class="form-group">
														<label>Drawing Revision</label>
														<input type="text" name="ii_drawing_revision" class="form-control">
													</div>
												</div>
											</div>
											
											<div class="col-lg-6">
												<div class="form-group">
													<div class="form-group">
													
														<label>Total Amount</label>
														<input type="text" name="ii_total_amount" class="form-control">
														<button class="btn btn-default">Rate</button><button class="btn btn-default">Stock</button>
													</div>
												</div>
											</div>
											
											<div class="col-lg-6">
												<div class="form-group">
													<div class="form-group">
														<label>Quentity</label>
														<input type="number" name="ii_item_quentity" class="form-control">
													</div>
												</div>
											</div>
											
											<div class="col-lg-6">
												<div class="form-group">
													<div class="form-group">
														<label>Disc(%)</label>
														<input type="text" name="ii_item_quentity" class="form-control">
													</div>
												</div>
											</div>
											
											<div class="col-lg-6">
												<div class="form-group">
													<div class="form-group">
														<label>Amount</label>
														<input type="text" name="ii_amount" class="form-control">
													</div>
												</div>
											</div>
											
											<div class="col-lg-6">
												<div class="form-group">
													<div class="form-group">
														<label>Net Amount</label>
														<input type="text" name="ii_net_amount" class="form-control">
													</div>
												</div>
											</div>
											
											
											<div class="col-lg-6">
												<div class="form-group">
													<div class="form-group">
														<label>Status</label>
														<select class="form-control templatingSelect2" name="ii_status">
															
															<?php
																while($ii_statuss=mysql_fetch_object($ii_status))
																{
																	?>
																	<option value="<?php echo $ii_statuss->industry_status_id; ?>"><?php echo $ii_statuss->industry_status; ?></option>
																	<?php
																}
															?>
														</select>
													</div>
												</div>
											</div>
											
											<div class="col-lg-6">
												<div class="form-group">
													<div class="form-group">
														<label>Cust Part No.</label>
														<input type="text" name="ii_cust_part_no" class="form-control">
													</div>
												</div>
											</div>
											
											<div class="col-lg-6">
												<div class="form-group">
													<div class="form-group">
														<label>Item Image</label>
														<input type="file" name="file" >
													</div>
												</div>
											</div>
											
										</div>
										<div class="col-lg-12">
												
										</div>
										<div class="row viewimagebtndisp">
											<div class="col-lg-12 viewimagebtn">
												
											</div>
										</div>
										<div class="row" style="padding-top:30px">
											<div class="col-lg-12">
												<input class="btn btn-success" type="submit" name="saveii" value="Add Item" />
												<button class="btn btn-default">Add Sub Items</button>
												<button class="btn btn-default">Item Spec</button>
												<button class="btn btn-default">BOM</button>
												<button class="btn btn-default">Update All Item Status</button>
											</div>
										</div>
									</form>
								</div>
								<div id="login" class="tab-pane fade">
									<h4>Login</h4>
									<!--p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p-->
									<div class="row">
										<div class="col-lg-12">
											
												<h4>Login Detail</h4>
												
													<div class="form-group">
														<div class="form-group">
															<label>Created By</label>
															<input type="text" name="created_by" placeholder="Created By" class="form-control">
														</div>
													</div>
												
												
													<div class="form-group">
														<div class="form-group">
															<label>Date & Time</label>
															<input type="text" name="date_time" placeholder="Date & Time" class="form-control">
														</div>
													</div>
												
												
													<div class="form-group">
														<div class="form-group">
															<label>Last Modified By</label>
															<input type="text" name="last_modified_by" placeholder="Last Modified By" class="form-control">
														</div>
													</div>
												
													<div class="form-group">
														<div class="form-group">
															<label>Date & Time</label>
															<input type="text" name="date_time" placeholder="Last modified Date & Time" class="form-control">
														</div>
													</div>
													
													<h4>Salse Order Stage</h4>
													
													<div class="form-group">
														<div class="form-group">
															<label>Prepared By</label>
															<input type="text" name="prepared_by" placeholder="Prepared By" class="form-control">
														</div>
													</div>
													
													
													<div class="form-group">
														<div class="form-group">
															<label>Date & Time</label>
															<input type="text" name="prepared_by_date_time" placeholder="Prepared By Date Time" class="form-control">
														</div>
													</div>
													
													<div class="form-group">
														<div class="form-group">
															<label>Approve By</label>
															<input type="text" name="approve_by" placeholder="Approved By" class="form-control">
														</div>
													</div>
													
													<div class="form-group">
														<div class="form-group">
															<label>Date Time</label>
															<input type="text" name="approve_by_date_time" placeholder="Approved By Date Time" class="form-control">
														</div>
													</div>
													<div class="form-group">
														<div class="form-group">
															<button class="btn btr-default">Approved</button>
															<button class="btn btr-default">Redo</button>
														</div>
													</div>
											
										</div>
									</div>
								</div>
							</div>
						</div>	
					</div>
				</div>
			</div>
		</div>
	</div>
    </div>    
</div>
<?php
include_once('../footer.php');
?>
<script>
$(document).ready(function(){
	
	jQuery("body").delegate(".tcdelete","click",function(){
		var co = confirm('Are you sure?');
		if (co == true) {
			var tcid = jQuery(this).attr('data-id');
			$.ajax({
				type: "POST",
				url: "<?php echo SITE_URL.'ajaxcall.php' ?>",
				data: {tcid:tcid,ajax:'qtcdelete'},
				cache: false,
					success: function(html) {
						jQuery('.tcdelete'+tcid).remove();
					}	
			});
		}
		else
		{
			return false;
		}
	})
	
		$( ".datepickers" ).datepicker({ dateFormat: 'dd-mm-yy' });
		$(".viewimagebtndisp").hide();
		
		jQuery("#addAgent").submit(function(e) {	
			var cpval = jQuery( this ).serialize();
			$.ajax({
				type: "POST",
				url: "<?php echo SITE_URL.'ajaxcall.php' ?>",
				data: cpval,
				cache: false,
					success: function(html) {
						jQuery('.ajaxqAgent').append(html);
						alert('Agent save successfully.')
						jQuery("#saveii")[0].reset();
					}	
			});
			return false;       
		});	
		
		jQuery("#qutnote").submit(function(){
			var cpval = jQuery( this ).serialize();
			$.ajax({
				type: "POST",
				url: "<?php echo SITE_URL.'ajaxcall.php' ?>",
				data: cpval,
				cache: false,
					success: function(html) {
						
					}	
			});
			return false;
		});
		
		jQuery("#savebillterm").submit(function(){
			var cpval = jQuery( this ).serialize();
			$.ajax({
				type: "POST",
				url: "<?php echo SITE_URL.'ajaxcall.php' ?>",
				data: cpval,
				cache: false,
				success: function(html) {
						
				}	
			});
			return false;
		})
		
		jQuery("#saveqtyspec").submit(function(e) {
			var cpval = jQuery( this ).serialize();
			$.ajax({
				type: "POST",
				url: "<?php echo SITE_URL.'ajaxcall.php' ?>",
				data: cpval,
				cache: false,
					success: function(html) {
						
					}	
			});
			return false;
		});
		
		jQuery("#saveii").submit(function(e) {
			var cpval = jQuery( this ).serialize();
			$.ajax({
				type: "POST",
				url: "<?php echo SITE_URL.'ajaxcall.php' ?>",
				data: cpval,
				cache: false,
					success: function(html) {
						$('.ajaxii').append(html);
						$("#saveii")[0].reset();
					}	
			});
			return false;       
		});
		
		jQuery(".dataTable_wrapper").delegate(".iidelete","click",function(){
		var co = confirm('Are you sure?');
		if (co == true) {
			var tcid = jQuery(this).attr('data-id');
			$.ajax({
				type: "POST",
				url: "<?php echo SITE_URL.'ajaxcall.php' ?>",
				data: {tcid:tcid,ajax:'qidelete'},
				cache: false,
					success: function(html) {
						jQuery('.iidelete'+tcid).remove();
					}	
			});
		}
		else
		{
			return false;
		}
	})
		
		jQuery("body").delegate(".sadelete","click",function(){
		var co = confirm('Are you sure?');
		if (co == true) {
			var agentid = jQuery(this).attr('data-id');
			$.ajax({
				type: "POST",
				url: "<?php echo SITE_URL.'ajaxcall.php' ?>",
				data: {agentid:agentid,ajax:'qAgentdelete'},
				cache: false,
					success: function(html) {
						jQuery('.sadelete'+agentid).remove();
						jQuery("#saveii")[0].reset();
					}	
			});
		}
		else
		{
			return false;
		}
	})
	
	$( ".ii_code").keyup(function(){
		var iivalue = $(this).val();
		$.ajax({
			type: "POST",
			url: "<?php echo SITE_URL.'ajaxcall.php' ?>",
			data: {iivalue:iivalue,ajax:'getItemCode'},
			cache: false,
				success: function(html) {
					$(".getItems").html(html);
					$(".getItems").show();
				}	
		})
	})
	
	$('body').delegate( ".selItemCode", "click", function() {
		$('.ii_code').val($(this).attr('data-code'));
		var itemid = $(this).attr('data-imgid');
		$(".getItems").hide();
		$.ajax({
			type: "POST",
			url: "<?php echo SITE_URL.'ajaxcall.php' ?>",
			data: {itemid:itemid,ajax:'getItemImage'},
			cache: false,
				success: function(html) {
					$('.viewimagebtn').html(html);
					$('.viewimagebtndisp').show();
				}	
		})
	})
	
	jQuery("#addterms").submit(function(e) {
		var cpval = jQuery( this ).serialize();
		$.ajax({
			type: "POST",
			url: "<?php echo SITE_URL.'ajaxcall.php' ?>",
			data: cpval,
			cache: false,
				success: function(html) {
					jQuery('.ajaxtc').append(html);
				}	
		});
		return false;       
    });
});		
</script>