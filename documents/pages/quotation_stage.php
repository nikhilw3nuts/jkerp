<?php
include_once('../header.php');

include_once('../dbFunction.php');
$funObj = new dbFunction();
if($_POST['addquotationStage'])
{
	$quotation_stage = $_POST['quotation_stage'];
	$isExist = $funObj->checkeAllreadyExist("quotation_stage","stage",$quotation_stage);
	if($isExist)
	{
		echo "<script>alert('Quotation  stage aleady exist!')</script>";
	}
	else
	{
		$quotation_stage= $funObj->addquotationStage($quotation_stage);
	}
}
$quotation_stage = $funObj->getTableData('quotation_stage');
?>
<div id="page-wrapper">
    <div class="container">
	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">Quotation Stage</h1>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-7">
			<div class="panel panel-default">
				<div class="panel-heading clearfix">
					Quotation Stage Detail
				</div>				
				<div class="panel-body">
					<div class="dataTable_wrapper">
						<table class="table table-striped table-bordered table-hover" id="dataTables-example">
							<thead>
								<tr>
									<th>Action</th>
									<th>Quotation Stage</th>
								</tr>
							</thead>
							<tbody>
								<?php 
									while($quotation_stages=mysql_fetch_object($quotation_stage))
									{
										?>
											<tr class="odd gradeX">
												<td><a onclick="return confirm('Are you sure?');" href="<?php echo SITE_URL.'pages/delete.php?redirect=quotation_stage&table=quotation_stage&field=stage_id&id='.$quotation_stages->stage_id; ?>">Delete</a></td>
												<td><?php echo $quotation_stages->stage; ?></td>
											</tr>	
										<?php
									} 
								?>								
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
		<div class="col-lg-5">
			<div class="panel panel-default">
				<div class="panel-heading clearfix">
					Add Quotation Stage
				</div>
				<div style="margin:20px">	
				<form role="form" name="addquotationStage" method="POST">
					<div class="form-group">
						<label>Quotation Stage</label>
						<input type="text" name="quotation_stage" required class="form-control">
					</div>
					<input class="btn btn-success btn-block" type="submit" name="addquotationStage" value="Add Quotation Stage" />
				</form>
				</div>
			</div>
		</div>
	</div>
    </div>    
</div>
<?php
include_once('../footer.php');
?>
<script>
jQuery(document).ready(function() {
	jQuery(".state-all").hide();
	jQuery(".state-1").show();
	jQuery(".countrycng").change(function(){
		var curt = jQuery(this).val();
		 jQuery(".state-all").hide();
		 jQuery(".state-"+curt).show();
	})
	jQuery('#dataTables-example').DataTable({
			responsive: true
	});
	
});
</script>
