<?php
include_once('../header.php');

include_once('../dbFunction.php');
$funObj = new dbFunction();
if($_POST['addDepartment'])
{
	$department = $_POST['department'];
	$checkedepartment = $funObj->checkedepartment($department);
	if($checkedepartment)
	{
		echo "<script>alert('Department aleady exist!')</script>";
	}
	else
	{
		$addDepartment = $funObj->addDepartment($department);
	}
}
$department = $funObj->getTableData('department');
?>
<div id="page-wrapper">
    <div class="container">
	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">Department</h1>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-7">
			<div class="panel panel-default">
				<div class="panel-heading clearfix">
					Department Detail
				</div>				
				<div class="panel-body">
					<div class="dataTable_wrapper">
						<table class="table table-striped table-bordered table-hover" id="dataTables-example">
							<thead>
								<tr>
									<th>Action</th>
									<th>Designation</th>
								</tr>
							</thead>
							<tbody>
								<?php 
									while($departments=mysql_fetch_object($department))
									{
										?>
											<tr class="odd gradeX">
												<td><a onclick="return confirm('Are you sure?');" href="<?php echo SITE_URL.'pages/delete.php?redirect=department&table=department&field=department_id&id='.$departments->department_id; ?>">Delete</a></td>
												<td><?php echo $departments->department; ?></td>
											</tr>	
										<?php
									} 
								?>								
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
		<div class="col-lg-5">
			<div class="panel panel-default">
				<div class="panel-heading clearfix">
					Add Department
				</div>
				<div style="margin:20px">	
				<form role="form" name="addDepartment" method="POST">
					<div class="form-group">
						<label>Department</label>
						<input type="text" name="department" required class="form-control">
					</div>
					<input class="btn btn-success btn-block" type="submit" name="addDepartment" value="Add Department" />
				</form>
				</div>
			</div>
		</div>
	</div>
    </div>    
</div>
<?php
include_once('../footer.php');
?>
<script>
jQuery(document).ready(function() {
	jQuery('#dataTables-example').DataTable({
			responsive: true
	});
});
</script>
