<?php
include_once('../header.php');
include_once('../dbFunction.php');
$funObj = new dbFunction();
if($_POST['addReference'])
{
	$reference = $_POST['reference'];
	$checkeReference = $funObj->checkeReference($reference);
	if($checkeReference)
	{
		echo "<script>alert('Reference aleady exist!')</script>";
	}
	else
	{
		$addParty = $funObj->addReference($reference);
	}
}
$reference = $funObj->getTableData('reference');
?>
<div id="page-wrapper">
    <div class="container">
	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">Party Type</h1>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-7">
			<div class="panel panel-default">
				<div class="panel-heading clearfix">
					Reference Detail
				</div>				
				<div class="panel-body">
					<div class="dataTable_wrapper">
						<table class="table table-striped table-bordered table-hover" id="dataTables-example">
							<thead>
								<tr>
									<th>Action</th>
									<th>Reference</th>
								</tr>
							</thead>
							<tbody>
								<?php 
									while($references=mysql_fetch_object($reference))
									{
										?>
											<tr class="odd gradeX">
												<td><a onclick="return confirm('Are you sure?');" href="<?php echo SITE_URL.'pages/delete.php?redirect=reference&table=reference&field=referenceid&id='.$references->referenceid; ?>">Delete</a></td>
												<td><?php echo $references->referenceby; ?></td>
											</tr>	
										<?php
									} 
								?>								
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
		<div class="col-lg-5">
			<div class="panel panel-default">
				<div class="panel-heading clearfix">
					Reference Type
				</div>
				<div style="margin:20px">	
				<form role="form" name="addReference" method="POST">
					<div class="form-group">
						<label>Reference</label>
						<input type="text" name="reference" required class="form-control">
					</div>
					<input class="btn btn-success btn-block" type="submit" name="addReference" value="Add References Type" />
				</form>
				</div>
			</div>
		</div>
	</div>
    </div>    
</div>
<?php
include_once('../footer.php');
?>
<script>
jQuery(document).ready(function() {
	jQuery('#dataTables-example').DataTable({
			responsive: true
	});
});
</script>
