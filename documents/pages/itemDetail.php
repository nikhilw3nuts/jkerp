<?php
include_once('../header.php');
include_once('../dbFunction.php');
$funObj = new dbFunction();

$itemID = $_GET['id'];
if($_POST['addItem'])
{
	$item_cate = $_POST['item_cate'];
	$isItemActive = $_POST['isItemActive'];
	$item_code1 = $_POST['item_code1'];
	$item_code2 = $_POST['item_code2'];
	$item_name = $_POST['item_name'];
	$item_uom = $_POST['item_uom'];	
	$item_cfactor  = $_POST['item_cfactor'];	
	$item_conv_qty = $_POST['item_conv_qty'];
	$item_conv_uom = $_POST['item_conv_uom'];
	$item_type = $_POST['item_type'];
	$item_mpt = $_POST['item_mpt'];
	$item_bws = $_POST['item_bws'];
	$item_group = $_POST['item_group'];
	$item_status = $_POST['item_status'];
	
	$main_dir = '../uploads/';
	if (!file_exists($main_dir)) {
		mkdir($main_dir, 0777, true);
	}
	$target_dir = $main_dir.'item/';
	if (!file_exists($target_dir)) {
		mkdir($target_dir, 0777, true);
	}

	$uploadOk = 1;
	$err ="";
	
	$valid_formats = array("doc", "docx", "txt", "xl");
	$max_file_size = 1024*300;
	$count = 0;
	$addImagestore = array();
	$i=1;
	foreach ($_FILES['uploaddoc']['name'] as $f => $name) {     
	    if ($_FILES['uploaddoc']['error'][$f] == 4) {
	        continue; // Skip file if any error found
			$uploadOk =1;
		}	       
	    if ($_FILES['uploaddoc']['error'][$f] == 0) {	           
	        if ($_FILES['uploaddoc']['size'][$f] > $max_file_size) {
	            $message[] = "$name is too large!.";
	            $uploadOk =0;
				continue; // Skip large files
	        }
			elseif( ! in_array(pathinfo($name, PATHINFO_EXTENSION), $valid_formats) ){
				$message[] = "$name is not a valid format";
				$uploadOk =0;
				continue; // Skip invalid file formats
			}
	        else{ // No error found! Move uploaded files 
	            $imageFileType = pathinfo($name,PATHINFO_EXTENSION);
				$filename = "item_".$itemID."_".time().".".$imageFileType;
				if(move_uploaded_file($_FILES["uploaddoc"]["tmp_name"][$f], $target_dir.$filename))
				{
					$addImage = $funObj->addImage('uploads/item/'.$filename,'item');
					$addImagestore[] = $addImage;
				}
				$i++;
	           
				$count++; // Number of successfully uploaded file
				$uploadOk =1;
			}
	    }
	}

	if($_FILES['uploaddoc']['name'][0] != "")
	{	
		$updateitem = $funObj->updateField('item_master','upload_doc',serialize($addImagestore),'item_id='.$itemID);
	}
	$valid_formats1 = array("jpg", "png", "gif", "zip", "bmp", "jpeg");
	$max_file_size1 = 1024*300;
	$count = 0;
	$addImagestore1 = array();
	$i=1;
	foreach ($_FILES['itemimage']['name'] as $f => $name) {

	    if ($_FILES['itemimage']['error'][$f] == 4) {
	        continue; // Skip file if any error found
			$uploadOk =1;
		}	       
	    if ($_FILES['itemimage']['error'][$f] == 0) {	           
	        if ($_FILES['itemimage']['size'][$f] > $max_file_size1) {
	            $message[] = "$name is too large!.";
	            $uploadOk =0;
				continue; // Skip large files
	        }
			elseif( ! in_array(pathinfo($name, PATHINFO_EXTENSION), $valid_formats1) ){
				$message[] = "$name is not a valid format";
				$uploadOk =0;
				continue; // Skip invalid file formats
			}
	        else{ // No error found! Move uploaded files 
	            $imageFileType = pathinfo($name,PATHINFO_EXTENSION);
				$filename = "item_".$itemID."_".time().".".$imageFileType;
				if(move_uploaded_file($_FILES["itemimage"]["tmp_name"][$f], $target_dir.$filename))
				{
					$addImage = $funObj->addImage('uploads/item/'.$filename,'item');
					$addImagestore1[] = $addImage;
				}		
				$i++;
	            $count++; // Number of successfully uploaded file
				$uploadOk =1;
			}
	    }
	}
	//print_r($addImagestore1);
	if($_FILES['itemimage']['name'][0] != "")
	{
		$updateitem = $funObj->updateField('item_master','item_image',serialize($addImagestore1),'item_id='.$itemID);
	}
	
	$addparty = $funObj->editItem($item_cate,$isItemActive,$item_code1,$item_code2,$item_name,$item_uom,$item_cfactor,$item_conv_qty,$item_conv_uom,$item_type,$item_mpt,$item_bws,$item_status,$item_group,$itemID);		
}

$item_category = $funObj->getTableData('item_category');
$uom = $funObj->getTableData('uom');
$uom1 = $funObj->getTableData('uom');
$mpt = $funObj->getTableData('material_process_type');
$industry_status = $funObj->getTableData('industry_status');
$item_type = $funObj->getTableData('item_type');
$item_statuss= $funObj->getTableData('item_stauts');
$item_group = $funObj->getTableData('item_group_code');
$getItem = mysql_fetch_array($funObj->getDataById("item_master","item_id='".$itemID."'"));
$item_main_groups = $funObj->getTableData('item_main_group');
$subitem_groups = $funObj->getTableData('item_sub_group');
$materials = $funObj->getTableData('item_material');
$make_items = $funObj->getTableData('item_make');
$drawings = $funObj->getTableData('item_drawing');
$getItemDetail = mysql_fetch_array($funObj->getDataById("Item_detail","item_id='".$itemID."'"));
$getItemPurchase = mysql_fetch_array($funObj->getDataById("item_purchase","item_id='".$itemID."'"));
$class = $funObj->getTableData('item_class');
$excise_temp = $funObj->getTableData('excise_temp');
$makeitem = $funObj->getDataById("make_item","item_id='".$itemID."'");
$itemgeneral = mysql_fetch_array($funObj->getDataById("item_general","item_id='".$itemID."'"));
?>
<div id="page-wrapper">
    <div class="container">
	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">Edit Item</h1>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-default">
				<div class="panel-body">
					<form role="form" name="addItem" method="POST" enctype="multipart/form-data">
						<div class="row">
							<div class="col-lg-6">
								<div class="form-group">
									<label>Item Category</label>
										<select class="form-control" name="item_cate">
											<?php
												while($item_categorys=mysql_fetch_object($item_category))
												{
													?>
													<option <?php echo ($getItem['item_category']==$item_categorys->item_category_id) ? 'selected' : ''; ?> value="<?php echo $item_categorys->item_category_id; ?>"><?php echo $item_categorys->item_category; ?></option>
													<?php
												}
											?>	
										</select>
									</div>
								</div>
							<div class="col-lg-6">
								<div class="form-group">
									<label>Item Group</label>
									<select class="form-control templatingSelect2" name="item_group">
										<?php
											while($item_groups=mysql_fetch_object($item_group))
											{
												?>
												<option <?php echo ($getItem['item_group']==$item_groups->item_group_code_id) ? 'selected' : ''; ?> value="<?php echo $item_groups->item_group_code_id; ?>"><?php echo $item_groups->item_group; ?></option>
												<?php
											}
										?>	
									</select>
									<label>Active?</label>
									<input type="checkbox" name="isItemActive" <?php echo ($getItem['item_active']==1) ? 'checked' : ''; ?> value="1">
								</div>
							</div>
						</div>	
						<div class="row">
							<div class="col-lg-6">
								<div class="form-group">
									<div class="form-group">
										<label>Item Code</label>
										<div>
											<input type="text" name="item_code1" value="<?php echo $getItem['item_code1']; ?>" required class="form-control input-text-sm">
											<input type="text" name="item_code2" value="<?php echo $getItem['item_code2']; ?>" required class="form-control input-text-sm">
										</div>
									</div>
								</div>
							</div>
                            <div class="col-lg-6">
								<div class="form-group">
									<div class="form-group">
										<label>Item Name</label>
										<input type="text" name="item_name" value="<?php echo $getItem['item_name']; ?>" required class="form-control">
										<a class="btn btn-default" href="#">Repeat Item (How this work?)</a>
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-lg-6">
								<div class="form-group">
									<label>UOM</label>
									<select class="form-control templatingSelect2" name="item_uom">
										<option>Select UOM</option>
										<?php
											while($uoms=mysql_fetch_object($uom))
											{
												?>
												<option <?php echo ($getItem['uom']==$uoms->uom_id) ? 'selected' : '' ; ?> value="<?php echo $uoms->uom_id	; ?>"><?php echo $uoms->uom; ?></option>
												<?php
											}
										?>
									</select>
								</div>
							</div>
							<div class="col-lg-6">
								<div class="form-group">
									<label>CFactor</label>
									<input type="text" name="item_cfactor" value="<?php echo $getItem['cfactor']; ?>" required class="form-control">
								</div>
							</div>
						</div>
                        <div class="row">
                        	<div class="col-lg-6">
								<div class="form-group">
									<label>Conv Qty</label>
									<input type="text" name="item_conv_qty" value="<?php echo $getItem['conv_qty']; ?>" required class="form-control">
								</div>
							</div>
							<div class="col-lg-6">
								<div class="form-group">
									<label>Conv. UOM</label>
									<select class="form-control templatingSelect2" name="item_conv_uom">
										<option>Select Conv. UOM</option>
										<?php
											while($uoms=mysql_fetch_object($uom1))
											{
												?>
												<option <?php echo ($getItem['conv_uom']==$uoms->uom_id) ? 'selected' : '' ; ?> value="<?php echo $uoms->uom_id; ?>"><?php echo $uoms->uom; ?></option>
												<?php
											}
										?>
									</select>
								</div>
							</div>
						</div>
                        <div class="row">
                        	<div class="col-lg-6">
								<div class="form-group">
									<label>Item Type</label>
									<select class="form-control templatingSelect2" name="item_type">
										<option>Select Item Type</option>
										<?php
											while($item_types=mysql_fetch_object($item_type))
											{
												?>
												<option <?php echo ($getItem['item_type']==$item_types->item_type_id) ? 'selected' : '' ; ?> value="<?php echo $item_types->item_type_id; ?>"><?php echo $item_types->item_type; ?></option>
												<?php
											}
										?>
									</select>
								</div>
							</div>
							<div class="col-lg-6">
								<div class="form-group">
									<label>Material Process Type</label>
									<select class="form-control templatingSelect2" name="item_mpt">
										<option>Select Material Process Type</option>
										<?php
											while($mpts=mysql_fetch_object($mpt))
											{
												?>
												<option <?php echo ($getItem['material_process_type']==$mpts->material_process_type_id) ? 'selected' : '' ; ?> value="<?php echo $mpts->material_process_type_id; ?>"><?php echo $mpts->material_process_type; ?></option>
												<?php
											}
										?>
									</select>
								</div>
							</div>
						</div>
                        <div class="row">
                        	<div class="col-lg-6">
								<div class="form-group">
									<label>Batch Wise Stock ?</label>
									<select class="form-control" name="item_bws">
										<option <?php echo ($getItem['batch_wise_stock']==0) ? 'selected' : '' ; ?> value="0">No</option>
										<option <?php echo ($getItem['batch_wise_stock']==1) ? 'selected' : '' ; ?> value="1">Yes</option>
									</select>
								</div>
							</div>
							<div class="col-lg-6">
								<div class="form-group">
									<label>Status </label>
									<select class="form-control templatingSelect2" name="item_status">
										<option>Select Status</option>
										<?php
											while($item_status=mysql_fetch_object($item_statuss))
											{
												?>
												<option <?php echo ($getItem['item_status']==$item_status->itemstatus_id) ? 'selected' : '' ; ?> value="<?php echo $item_status->itemstatus_id; ?>"><?php echo $item_status->item_status; ?></option>
												<?php
											}
										?>
									</select>
								</div>
							</div>
						</div>
						
						<div class="row">
                        	<div class="col-lg-6">
								<div class="form-group">
									<label>Upload Document </label>
									<input type="file" name="uploaddoc[]" multiple class="form-control" />
									<?php 
										$array_doc = unserialize($getItem['upload_doc']);
										foreach($array_doc as $s_doc)
										{
											$filevals = mysql_fetch_array($funObj->getDataById("jkerp_images","images_id='".$s_doc."'"));
											?>
												<a href="<?php echo SITE_URL.$filevals['image']; ?>" target="_blank"><?php echo str_replace('uploads/item/','',$filevals['image']); ?></a>
											<?php
										}
									?>
								</div>
							</div>
							<div class="col-lg-6">
								<div class="form-group">
									<label>Item image </label>
									<input type="file" name="itemimage[]" multiple class="form-control" />
									<?php 
										$array_doc = unserialize($getItem['item_image']);
										foreach($array_doc as $s_doc)
										{
											$filevals = mysql_fetch_array($funObj->getDataById("jkerp_images","images_id='".$s_doc."'"));
											?>
												<a href="<?php echo SITE_URL.$filevals['image']; ?>" target="_blank"><img width="100px" src="<?php echo SITE_URL.$filevals['image']; ?>" alt="item image"/></a>
											<?php
										}
									?>
								</div>
							</div>
						</div>
						
						<div class="row" style="padding-top:30px">
							<div class="col-lg-12">
								<input class="btn btn-success" type="submit" name="addItem" value="Edit Item" />
							</div>
						</div>		
						
					</form>
				</div>
				
				<div class="panel-body">
					<div class="row" style="padding-top:20px">
						<div class="col-lg-12">
							<ul class="nav nav-tabs">
								<li><a data-toggle="tab" href="#itemDetails" aria-expanded="false">Item Details</a></li>
								<li><a data-toggle="tab" href="#Sales" aria-expanded="true">Sales</a></li>
								<li><a data-toggle="tab" href="#purchase">Purchase</a></li>
								<li><a data-toggle="tab" href="#make">Make</a></li>
								<li><a data-toggle="tab" href="#general">General</a></li>
								<li><a data-toggle="tab" href="#excise">Excise</a></li>
								<li><a data-toggle="tab" href="#login">Login</a></li>
							</ul>
							<div class="tab-content">
								<div id="itemDetails" class="tab-pane fade">
									<h4>Inquiry Item</h4>
									<form role="form" method="post" id="itemdetailtab" name="itemdetailtab">
										<input type="hidden" class="ajax" name="ajax" value="itemDetail" />
										<input type="hidden" name="itemid" value="<?php echo $itemID ?>" />
										<div class="row">
											<div class="col-lg-4">
												<div class="form-group">
													<div class="form-group">
														<label>Description</label>
														<input type="text" value="<?php echo $getItemDetail['description'] ?>" name="item_description" class="form-control">
														<ul class="getItems">
															
														</ul>
													</div>
												</div>
											</div>
											<div class="col-lg-4">
												<div class="form-group">
													<div class="form-group">
														<label>Main Group</label>
														<select class="form-control" name="main_group">
															<?php 
																while($item_main_group=mysql_fetch_object($item_main_groups))
																{
																	?>
																		<option <?php echo ($getItemDetail['main_group'] == $item_main_group->main_group_id ) ? 'selected' : '' ; ?> value="<?php echo $item_main_group->main_group_id ?>"><?php echo $item_main_group->main_group_value ?></option>
																	<?php
																}
															?>
														</select>
													</div>
												</div>
											</div>
											<div class="col-lg-4">
												<div class="form-group">
													<label>Sub Group</label>
													<select class="form-control" name="sub_group">
														<?php 
															while($subitem_group=mysql_fetch_object($subitem_groups))
															{
																?>
																	<option <?php echo ($getItemDetail['sub_group'] == $subitem_group->sub_group_id ) ? 'selected' : '' ; ?> value="<?php echo $subitem_group->sub_group_id ?>"><?php echo $subitem_group->Sub_group_value; ?></option>
																<?php
															}
														?>
													</select>
												</div>
											</div>
										</div>
										
										<!-- Trigger the modal with a button -->
<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal">Add AddOn</button>
										<div class="row">
											<div class="col-lg-4">
												<div class="form-group">
													<div class="form-group">
														<label>AddOn...a</label><font color="red">what is use of AddOn</font>
														<textarea id="addOn" class="form-control" name="addOn"></textarea>
													</div>
												</div>
											</div>
											<div class="col-lg-4">
												<div class="form-group">
													<div class="form-group">
														<label>Note </label>
														<textarea id="note" class="form-control" name="note"><?php echo $getItemDetail['note']; ?></textarea>
													</div>
												</div>
											</div>
											<div class="col-lg-4">
												<div class="form-group">
													<div class="form-group">
														<label>Serial No Code</label>
														<input type="text" name="serialnocode" value="<?php echo $getItemDetail['serial_no_code'] ?>" class="form-control">
													</div>
													<div class="form-group">
														<div class="checkbox">
															<label>
																<input type="checkbox" <?php echo ( $getItemDetail['print_detail'] == 1 ) ? 'checked' : '' ; ?> name="print_detail" value="1">Print Detail Desc After DrgNo. in Purchase Order
															</label>
														</div>
													</div>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-lg-4">
												<div class="form-group">
													<div class="form-group">
														<label>Make </label>
														<select class="form-control templatingSelect2" name="itemmake">
															<?php 
																while($make_item=mysql_fetch_object($make_items))
																{
																	?>
																		<option <?php echo ($getItemDetail['make'] == $make_item->item_make_id ) ? 'selected' : '' ; ?> value="<?php echo $make_item->item_make_id; ?>"><?php echo $make_item->item_make_value; ?></option>
																	<?php
																}
															?>
														</select>
														
													</div>
												</div>
											</div>
											<div class="col-lg-4">
												<div class="form-group">
													<div class="form-group">
														<label>Drawing Number</label>
														<select class="form-control templatingSelect2" name="drawingno">
															<?php 
																while($drawing=mysql_fetch_object($drawings))
																{
																	?>
																		<option <?php echo ($getItemDetail['drawing_number'] == $drawing->drawing_id ) ? 'selected' : '' ; ?> value="<?php echo $drawing->drawing_id; ?>"><?php echo $drawing->drawing_value; ?></option>
																	<?php
																}
															?>
														</select>
													</div>
												</div>
											</div>
											<div class="col-lg-4">
												<div class="form-group">
													<div class="form-group">
														<label>Material Specification </label>
														<select class="form-control templatingSelect2" name="materialspecification">
															<?php
																while($material=mysql_fetch_object($materials))
																{
																	?>
																		<option <?php echo ($getItemDetail['material_specification'] == $material->material_id ) ? 'selected' : '' ; ?> value="<?php echo $material->material_id; ?>"><?php echo $material->material_value; ?></option>
																	<?php
																}
															?>
														</select>
													</div>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-lg-4">
												<div class="form-group">
													<div class="form-group">
														<label>Revision </label>
														<input type="text" id="revision" value="<?php echo $getItemDetail['revision'] ?>" class="form-control" name="revision">
													</div>
												</div>
											</div>
											<div class="col-lg-4">
												<div class="form-group">
													<div class="form-group">
														<label> </label>
														<input type="text" id="revisionnull" class="form-control" name="revisionnull">
													</div>
												</div>
											</div>
											
										</div>
										<div class="row">
											<div class="col-lg-4">
												<div class="form-group">
													<div class="form-group">
														
			<a class="btn btn-default" data-target="#myModal2" data-toggle="modal" type="button">Alternat Item</a>
													</div>
												</div>
											</div>
											<div class="col-lg-4">
												<div class="form-group">
													<div class="form-group">
														<a href="#" class="btn btn-default">BOM User Details</a>
													</div>
												</div>
											</div>
											<div class="col-lg-4">
												<div class="form-group">
													<div class="form-group">
													<a class="btn btn-default" data-target="#ItemSpacification" data-toggle="modal" type="button">Item Specification</a>
													</div>
												</div>
											</div>
										</div>
										<div class="row" style="padding-top:30px">
											<div class="col-lg-12">
												<input class="btn btn-success" type="submit" name="saveii" value="Item Detail" />
											</div>
										</div>
									</form>
								</div>
								<div id="Sales" class="tab-pane fade">
									<h4>Customer Details</h4>
									<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
								</div>
								<div id="purchase" class="tab-pane fade">
									<h4>Customer Details</h4>
									<form role="form" method="post" id="itempurchase" name="itempurchase">
										<input type="hidden" class="ajax" name="ajax" value="itemPurchase" />
										<input type="hidden" name="itemid" value="<?php echo $itemID ?>" />
										<div class="row">
											<div class="col-lg-6">
												<div class="form-group">
													<div class="form-group">
														<label>Minimum Stock Quantity</label>
														<input type="text" value="<?php echo $getItemPurchase['stock_qty_min'] ?>" name="stock_qty_min" class="form-control">
													</div>
												</div>
											</div>
											<div class="col-lg-6">
												<div class="form-group">
													<div class="form-group">
														<label>Maximum Stock Quantity</label>
														<input type="text" value="<?php echo $getItemPurchase['stock_qty_max'] ?>" name="stock_qty_max" class="form-control">
													</div>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-lg-6">
												<div class="form-group">
													<div class="form-group">
														<label>Minimum Order Quantity</label>
														<input type="text" value="<?php echo $getItemPurchase['order_qty_min'] ?>" name="order_qty_min" class="form-control">
													</div>
												</div>
											</div>
											<div class="col-lg-6">
												<div class="form-group">
													<div class="form-group">
														<label>Maximum Order Quantity</label>
														<input type="text" value="<?php echo $getItemPurchase['order_qty_max'] ?>" name="order_qty_max" class="form-control">
													</div>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-lg-6">
												<div class="form-group">
													<div class="form-group">
														<label>Lead Time</label>
														<input type="text" value="<?php echo $getItemPurchase['lead_time'] ?>" name="lead_time" class="form-control">
													</div>
												</div>
											</div>
											<div class="col-lg-6">
												<div class="form-group">
													<div class="form-group">
														<label>ReOrder Quantity</label>
														<input type="text" value="<?php echo $getItemPurchase['reorder_qty'] ?>" name="reorder_qty" class="form-control">
													</div>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-lg-6">
												<div class="form-group">
													<div class="form-group">
														<label>Class</label>
														<select class="form-control templatingSelect2" name="class">
															<option>Select Class</option>
															<?php
																while($classs=mysql_fetch_object($class))
																{
																	?>
																	<option <?php echo ($getItemPurchase['class']==$classs->class_id) ? 'selected' : '';  ?> value="<?php echo $classs->class_id; ?>"><?php echo $classs->class; ?></option>
																	<?php
																}
															?>
														</select>
													</div>
												</div>
											</div>
											<div class="col-lg-6">
												<div class="form-group">
													<div class="form-group">
														<label>Material Provided By</label>
														<select class="form-control templatingSelect2" name="material_by">
															<option>Select One</option>
															<option <?php echo ($getItemPurchase['material_by']==1) ? 'selected' : '';  ?> value="1">Vendor</option>
															<option <?php echo ($getItemPurchase['material_by']==2) ? 'selected' : '';  ?> value="2">Company</option>
															<option <?php echo ($getItemPurchase['material_by']==3) ? 'selected' : '';  ?> value="3">Provider</option>
														</select>
													</div>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-lg-12">
												<div class="form-group">
													<div class="form-group">
														<label>Purchase Officer</label>
														<input type="text" value="<?php echo $getItemPurchase['purchase_officer'] ?>" name="purchase_officer" class="form-control">
													</div>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-lg-12">
												<h4 >Tolerance</h4>
											</div>
										</div>
										<div class="row">
											<div class="col-lg-6">
												<div class="form-group">
													<div class="form-group">
														<label>Tolerance(-)</label>
														<input type="text" value="<?php echo $getItemPurchase['tolerance_minus_per'] ?>" name="tolerance_minus_per" class="form-control"> % <input type="text" value="<?php echo $getItemPurchase['tolerance_minus_val'] ?>" name="tolerance_minus_val" class="form-control"> Value 
													</div>
												</div>
											</div>
											<div class="col-lg-6">
												<div class="form-group">
													<div class="form-group">
														<label>Tolerance(+)</label>
														<input type="text" value="<?php echo $getItemPurchase['tolerance_plus_per'] ?>" name="tolerance_plus_per" class="form-control input-text-sm"> % <input type="text" value="<?php echo $getItemPurchase['tolerance_plus_val'] ?>" name="tolerance_plus_val" class="form-control input-text-sm"> Value
													</div>
												</div>
											</div>
										</div>
										<div class="row" style="padding-top:30px">
											<div class="col-lg-12">
												<input class="btn btn-success" type="submit" name="saveip" value="Save Purchase Detail" />
											</div>
										</div>
									</form>
								</div>
								<div id="make" class="tab-pane fade">
									<h4>Make</h4>
									<div class="panel-body">
										<div class="dataTable_wrapper">
											<table class="table table-striped table-bordered table-hover" id="dataTab-example">
												<thead>
													<tr>
														<th>Action</th>
														<th>Item Make</th>
													</tr>
												</thead>
												<tbody class="ajaxmake">
													<?php 
														while($makeitems=mysql_fetch_object($makeitem))
														{
															?>
																<tr class="odd gradeX mkdelete<?php echo $makeitems->make_item_id ?>">
																	<td><a class="mkdelete" href="javascript:void(0)" data-id="<?php echo $makeitems->make_item_id ?>">Delete</a></td>
																	<td><?php echo $makeitems->make_item; ?></td>
																</tr>
															<?php
														}
													?>								
												</tbody>
											</table>
										</div>
									</div>	
									<form role="form" method="post" id="addMake" name="addMake">
										<input type="hidden" name="ajax" value="makeitem" />
										<input type="hidden" name="itemid" value="<?php echo $itemID ?>" />
										<div class="row">
											<div class="col-lg-12">
												<div class="form-group">
													<label>Make</label>
													<input type="text" name="make" class="form-control"><a class="btn btn-primary">Select (we put popup here.)</a>
												</div>
											</div>
										</div>
                						<div class="row" style="padding-top:30px">
											<div class="col-lg-12">
											
												<input class="btn btn-success" type="submit" name="addMake" value="Add Make" />
											</div>
										</div>
									</form>
                                </div>
								<div id="general" class="tab-pane fade">
									<form role="form" method="post" id="itemGeneral" name="itemGeneral">
										<input type="hidden" class="ajax" name="ajax" value="itemGeneral" />
										<input type="hidden" name="item_id" value="<?php echo $itemID ?>" />
										<h4>Store Detail</h4>
                                        <div class="row">
											<div class="col-lg-6">
												<div class="form-group">
													<div class="form-group">
														<label>Location</label>
														<input type="text" value="<?php echo $itemgeneral['location'] ?>" name="location" class="form-control subheading">
													</div>
												</div>
											</div>
                                            <div class="col-lg-6">
												<div class="form-group">
													<div class="form-group">
														<label>Self Life Days</label>
														<input type="text" value="<?php echo $itemgeneral['selflifedays'] ?>" name="selflifedays" class="form-control excode">
													</div>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-lg-6">
												<div class="form-group">
													<div class="form-group">
														<label>Bin Number</label>
														<input type="text" value="<?php echo $itemgeneral['binnumber'] ?>" name="binnumber" class="form-control excode">
													</div>
												</div>
											</div>
											<div class="col-lg-6">
												<div class="form-group">
													<div class="form-group">
														<label>Warranty Period</label>
														<input type="text" value="<?php echo $itemgeneral['warratyperiod'] ?>" name="warratyperiod" class="form-control subheading" width="60%">Month (0=No Warranty)
													</div>
												</div>
											</div>
										</div>
                                        <div class="row">
											<div class="col-lg-6">
												<div class="form-group">
													<div class="form-group">
														<label>Weight</label>
														<input type="text" value="<?php echo $itemgeneral['weight'] ?>" name="weight" class="form-control excode">
													</div>
												</div>
											</div>
										</div>
                                        <h4>Quality Details</h4>
										<div class="row">
											<div class="col-lg-6">
												<div class="form-group">
													<div class="form-group">
														<label>Test Report Require</label>
														<select class="form-control templatingSelect2" name="testreport">
															<option>Select One</option>
															<option <?php echo ($itemgeneral['testreport'] == 0) ? 'selected' : '';  ?> value="0">No</option>
															<option <?php echo ($itemgeneral['testreport'] == 1) ? 'selected' : '';  ?> value="1">Yes</option>
														</select><a href="#" class="btn btn-primary">? Test certificate parameter ?</a>
													</div>
												</div>
											</div>
										</div>
                                        <h4>General Details</h4>
										<div class="row">
											<div class="col-lg-6">
												<div class="form-group">
													<div class="form-group">
														<label>Module Number</label>
														<input type="text" value="<?php echo $itemgeneral['modulenumber'] ?>" name="modulenumber" class="form-control excode">
													</div>
												</div>
											</div>
										</div>
                                        <div class="row" style="padding-top:30px">
											<div class="col-lg-12">
												<input class="btn btn-success" type="submit" name="itemGeneral" value="Save Purchase Detail" />
											</div>
										</div>
									</form>
								</div>
								<div id="excise" class="tab-pane fade">
									<h4>Excise</h4>
									<form role="form" method="post" id="itemExcise" name="itemExcise">
										<input type="hidden" class="ajax" name="ajax" value="itemExcise" />
										<input type="hidden" name="itemid" value="<?php echo $itemID ?>" />
										<input type="hidden" name="addstatus" id="addstatus" value="Yes" />
										<div class="row">
											<div class="col-lg-6">
												<div class="form-group">
													<div class="form-group">
														<label>Excisable Item</label>
														<select class="form-control" name="excisableitem">
															<option>Select One</option>
															<option  value="0">No</option>
															<option  value="1">Yes</option>
														</select>
													</div>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-lg-6">
												<div class="form-group">
													<div class="form-group">
														<label>Description Of Excisable Commodity</label>
														<br>
														<input type="text" value="" name="excode" readonly class="form-control excode" width="10%">
														<input type="text" value="" name="exdesc" class="form-control exdesc input-text-sm">
													</div>
												</div>
											</div>
											<div class="col-lg-6">
												<div class="form-group">
													<div class="form-group">
														<label>Sub Heading Number</label>
														<br>
														<input type="text" value="" name="subheading" class="form-control subheading">
													</div>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-lg-6">
												<div class="form-group">
													<a style="background: #dfdfdf none repeat scroll 0 0; padding: 10px;" class="findExcise"><i class="fa fa-binoculars"></i></a>
												</div>	
											</div>	
										</div>
										<div class="row" style="padding-top:30px">
											<div class="col-lg-12">
												<input class="btn btn-success" type="submit" name="saveip" value="Save Purchase Detail" />
											</div>
										</div>
									</form>
								</div>
								<div id="login" class="tab-pane fade">
									<h4>Login</h4>
									<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
								</div>
							</div>
						</div>	
					</div>
				</div>
			</div>
		</div>
	</div>
    </div>    
</div>

<div class="modal fade in" id="myModalexcise" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
				<h4 class="modal-title" id="myModalLabel">Excise</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-lg-12">
						<div class="panel panel-default">
							<div style="margin: 20px 10px">	
								<form role="form" id="addalert" name="addalert" method="POST">
									<div class="row">
										<div class="col-lg-12">
											<table class="table table-striped" id="dataTables-example">
												<thead>
													<tr>
														<th>Excise Code</th>
														<th>Excise Description</th>
														<th>Sub Heading</th>
													</tr>
												</thead>
												<tbody>
													<?php
														while($excise_temps=mysql_fetch_object($excise_temp))
														{
															?>
																<tr class="selectExcise" data-code="<?php echo $excise_temps->excise_code; ?>" data-desc="<?php echo $excise_temps->ecdesc; ?>" data-sub="<?php echo $excise_temps->subheading; ?>">
																	<td><?php echo $excise_temps->excise_code; ?></td>
																	<td><?php echo $excise_temps->ecdesc; ?></td>
																	<td><?php echo $excise_temps->subheading; ?></td>
																</tr>
															<?php
														}
													?>
												</tbody>
											</table>
										</div>
									</div>
									
								</form>    
							</div>
						</div>
					</div>
				</div>
				
			</div>
		</div>
	</div>
</div>

<!--Modal For Item spacification-->
<div id="ItemSpacification" class="modal fade" role="dialog">
	<div class="modal-dialog">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Item spacification</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-lg-12">
					
						<div class="form-group">
							<div class="form-group">
								<label>Spacification-1</label>
								<textarea placeholder="Spacification-1" class="form-control" name="Spacification-1"></textarea>
							</div>
						</div>
						<div class="form-group">
							<div class="form-group">
								<label>Spacification-2</label>
								<textarea placeholder="Spacification-2" class="form-control" name="Spacification-2"></textarea>
							</div>
						</div>
						<div class="form-group">
							<div class="form-group">
								<label>Spacification-3</label>
								<textarea placeholder="Spacification-3" class="form-control" name="Spacification-3"></textarea>
							</div>
						</div>
						
					</div>
				</div>
				<div class="modal-footer">
					<div class="col-lg-6">
						<button type="button" class="btn btn-primary">Save Change</button>
						
					</div>
					<div class="col-lg-6">
						<button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- Modal For Item spacification End-->



<!-- Modal For alternate item-->
<div id="myModal2" class="modal fade" role="dialog">
	<div class="modal-dialog">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Alternat Item</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-lg-12">
					<table class="table table-striped table-bordered table-hover" id="dataTables-example">
					<thead>						
						<tr>
							<th>NO</th>
							<th>Alt. Item Code</th>
							<th>Item Description</th>
							<th>Drag No.</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>1</td>
							<td>104</td>
							<td>this is dummy description</td>
							<td>341</td>
						</tr>
						<tr>
							<td>2</td>
							<td>104</td>
							<td>this is dummy description</td>
							<td>341</td>
						</tr>
					</tbody>
					</table>
						<div class="form-group">
							<div class="form-group">
								<label>Item Code</label>
								<input type="text" name="text" class="form-control" placeholder="Item Code" name="ItemCode"></textarea>
							</div>
						</div>
						<div class="form-group">
							<div class="form-group">
								<label>Item Description</label>
								<textarea  placeholder="Item Description" class="form-control" name="ItemDescription"></textarea>
							</div>
						</div>
						<div class="form-group">
							<div class="form-group">
								<label>Add Description</label>
								<textarea  placeholder="Add Description" class="form-control" name="AddDescription"></textarea>
							</div>
						</div>
						<div class="form-group">
							<div class="form-group">
								<label>Detail Description</label>
								<textarea  placeholder="Detail Description" class="form-control" name="DetailDescription"></textarea>
							</div>
						</div>
						<div class="form-group">
							<div class="form-group">
								<label>Drawing Number</label>
								<input type="text" placeholder="Drawing Number" class="form-control" name="DrawingNumber">
							</div>
						</div>
						<div class="form-group">
							<div class="form-group">
								<label>Drawing Revision</label>
								<input type="text" placeholder="Drawing Revision" class="form-control" name="DrawingRevision">
							</div>
						</div>
						<div class="form-group">
							<div class="form-group">
								<label>UMO</label>
								<input type="text" placeholder="Drawing Revision" class="form-control" name="DrawingRevision">
							</div>
						</div>
						<div class="form-group">
							<div class="form-group">
								<label>Remark</label>
								<textarea placeholder="Drawing Revision" class="form-control" name="Remark"></textarea>
							</div>
						</div>
						
					</div>
				</div>
				<div class="modal-footer">
					<div class="col-lg-8">
						<button type="button" class="btn btn-primary">Add</button>
						<button type="button" class="btn btn-primary">Cancel</button>
						<button type="button" class="btn btn-primary">Delete</button>
					</div>
					<div class="col-lg-4">
						<button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- Modal For alternate item END-->

<!-- Modal For Add On-->
<div id="myModal" class="modal fade" role="dialog">
	<div class="modal-dialog">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Item Master AddOn</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-lg-12">
						<div class="form-group">
							<div class="form-group">
								<label>Title</label>
								<input type="text" name="text" class="form-control" placeholder="Title" name="addOn_title"></textarea>
							</div>
						</div>
						<div class="form-group">
							<div class="form-group">
								<label>AddOn...</label>
								<textarea id="addOn" placeholder="AddOn" class="form-control" name="addOn"></textarea>
							</div>
						</div>
						<div class="form-group">
							<div class="form-group">
							<label>IsPrint</label>
								<select name="isprint" class="form-control">
								<option value="1">YES</option>
								<option value="0">NO</option>
								</select>
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<div class="col-lg-8">
						<button type="button" class="btn btn-primary">Add</button>
						<button type="button" class="btn btn-primary">Cancel</button>
						<button type="button" class="btn btn-primary">Get From AddOn Master</button>
					</div>
					<div class="col-lg-4">
						<button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- Modal For Add On END-->
<?php
include_once('../footer.php');
?>
<script>
jQuery(document).ready(function() {
	
	jQuery('.selectExcise').click(function(){
		jQuery('#addstatus').val('No')
		jQuery('.excode').val(jQuery(this).attr('data-code'));
		jQuery('.exdesc').val(jQuery(this).attr('data-desc'));
		jQuery('.subheading').val(jQuery(this).attr('data-sub'));
		$('#myModalexcise').modal("hide");
	})
	
	jQuery(".findExcise").click(function(){
		$('#myModalexcise').modal("show");
	})
	
	jQuery("#itemdetailtab").submit(function(e) {
		var cpval = jQuery( this ).serialize();
		$.ajax({
			type: "POST",
			url: "<?php echo SITE_URL.'ajaxcall.php' ?>",
			data: cpval,
			cache: false,
				success: function(html) {
					alert("Add item detail successfully")
				}	
		});
		return false;       
    });
	
	jQuery("#itempurchase").submit(function(e) {
		var cpval = jQuery( this ).serialize();
		$.ajax({
			type: "POST",
			url: "<?php echo SITE_URL.'ajaxcall.php' ?>",
			data: cpval,
			cache: false,
				success: function(html) {
					alert("Save Purchase Detail successfully")
				}	
		});
		return false;       
    });
	
	jQuery("#itemExcise").submit(function(e) {
		var cpval = jQuery( this ).serialize();
		$.ajax({
			type: "POST",
			url: "<?php echo SITE_URL.'ajaxcall.php' ?>",
			data: cpval,
			cache: false,
				success: function(html) {
					alert("Save Excise");
					jQuery('.excode').val(html);
				}	
		});
		return false;       
    });
	
	
	jQuery("body").delegate(".mkdelete","click",function(){
		var co = confirm('Are you sure?');
		if (co == true) {
			var mkid = jQuery(this).attr('data-id');
			$.ajax({
				type: "POST",
				url: "<?php echo SITE_URL.'ajaxcall.php' ?>",
				data: {mkid:mkid,ajax:'mkdelete'},
				cache: false,
					success: function(html) {
						jQuery('.mkdelete'+mkid).remove();
					}	
			});
		}
		else
		{
			return false;
		}
	})
	
	jQuery("#addMake").submit(function(e) {
		var cpval = jQuery( this ).serialize();
		$.ajax({
			type: "POST",
			url: "<?php echo SITE_URL.'ajaxcall.php' ?>",
			data: cpval,
			cache: false,
				success: function(html) {
					if(html=='2')
						alert("Make item allready exist");
					else if(html=='1')
						alert("Make item not add! please try agine");
					else			
						jQuery('.ajaxmake').append(html);
				}	
		});
		return false;       
    });
	
	jQuery("#itemGeneral").submit(function(e) {
		var cpval = jQuery( this ).serialize();
		$.ajax({
			type: "POST",
			url: "<?php echo SITE_URL.'ajaxcall.php' ?>",
			data: cpval,
			cache: false,
			success: function(html) {
				alert(html);
			}	
		});
		return false;       
    });		
	
	jQuery('#dataTables-example').DataTable({
			responsive: true
	});
	
	
	
});



</script>
	
