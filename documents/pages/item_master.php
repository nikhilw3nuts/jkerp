<?php
include_once('../header.php');

include_once('../dbFunction.php');
$funObj = new dbFunction();
$items = $funObj->getTableData('item_master');
?>
<div id="page-wrapper">
    <div class="container">
	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">Item Master</h1>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-default">
				<div class="panel-heading clearfix">
					Item Master Detail
					<a class="btn btn-primary" style="float:right" href="<?php echo SITE_URL ?>pages/addItem.php">Add Item</a>
				</div>				
				<div class="panel-body">
					<div class="dataTable_wrapper">
						<table class="table table-striped table-bordered table-hover" id="dataTables-example">
							<thead>
								<tr>
									<th>Action</th>
									<th>Item Name</th>
									<th>Item Code</th>
								</tr>
							</thead>
							<tbody>
								<?php 
									while($item=mysql_fetch_object($items))
									{
										?>
											<tr class="odd gradeX">
												<td><a onclick="return confirm('Are you sure?');" href="<?php echo SITE_URL.'pages/delete.php?redirect=item_master&table=item_master&field=item_id&id='.$item->item_id; ?>">Delete</a></td>
												<td><a href="<?php echo SITE_URL.'pages/itemDetail.php?id='.$item->item_id ?>"><?php echo $item->item_name; ?></a></td>
												<td><?php echo $item->item_code1; ?><?php echo $item->item_code2; ?></td>
											</tr>	
										<?php
									} 
								?>								
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
    </div>   
</div>
<?php
include_once('../footer.php');
?>
<script>
jQuery(document).ready(function() {
	jQuery('#dataTables-example').DataTable({
			responsive: true
	});
});
</script>
