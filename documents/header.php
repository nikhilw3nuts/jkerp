<?php 
include_once('dbFunction.php');
if(!$_SESSION['login'])
	header("location:".SITE_URL);
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>SB Admin 2 - Bootstrap Admin Theme</title>
    <link href="<?php echo SITE_URL; ?>bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo SITE_URL; ?>bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">
    <link href="<?php echo SITE_URL; ?>dist/css/timeline.css" rel="stylesheet">
    <link href="<?php echo SITE_URL; ?>dist/css/sb-admin-2.css" rel="stylesheet">
    <link href="<?php echo SITE_URL; ?>bower_components/morrisjs/morris.css" rel="stylesheet">
    <link href="<?php echo SITE_URL; ?>bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="<?php echo SITE_URL; ?>bower_components/jquery-ui/jquery-ui.css" rel="stylesheet" type="text/css">
	<link href="<?php echo SITE_URL; ?>bower_components/lightbox/lightbox.min.css" rel="stylesheet" type="text/css">
	<!----AUTO DROP DOWN---->
	<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.1/css/select2.min.css">
	
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>
<body>
    <div id="wrapper">
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.html">Jay Khodiyar</a>
            </div>

            <ul class="nav navbar-top-links navbar-right">
				<li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-user fa-fw"></i>  <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <li><a href="<?php echo SITE_URL; ?>pages/changepassword.php"><i class="fa fa-user fa-fw"></i> Change Password</a></li>
                        <li class="divider"></li>
                        <li><a href="<?php echo SITE_URL; ?>logout.php"><i class="fa fa-sign-out fa-fw"></i> Logout</a></li>
                    </ul>
                </li>
            </ul>

            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                        <li>
                            <a href="<?php echo SITE_URL; ?>dashbord.php"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
                        </li>
                        <li>
                         	<a href="javascript:void(0)"><i class="fa fa-users fa-fw"></i> Master<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
								<li>
									<a href="<?php echo SITE_URL; ?>pages/company_detail.php">Company Detail</a>
								</li>
                            	<li>
                                	<a href="javascript:void(0)">Party Master<span class="fa arrow"></span></a>
   									<ul class="nav nav-third-level" aria-expanded="false">
                                		<li>
                                            <a href="<?php echo SITE_URL; ?>pages/party.php">Party</a>
                                        </li>
                                         <li>
                                            <a href="<?php echo SITE_URL; ?>pages/country.php">Country</a>
                                        </li>
                                        <li>
                                            <a href="<?php echo SITE_URL; ?>pages/state.php">State</a>
                                        </li>
                                        <li>
                                            <a href="<?php echo SITE_URL; ?>pages/city.php">City</a>
                                        </li>
                                        <li>
                                            <a href="<?php echo SITE_URL; ?>pages/partytype1.php">Party type1</a>
                                        </li>
                                        <li>
                                            <a href="<?php echo SITE_URL; ?>pages/partytype2.php">Party type2</a>
                                        </li>
                                        <li>
                                            <a href="<?php echo SITE_URL; ?>pages/reference.php">Reference</a>
                                        </li>
                                        <li>
                                            <a href="<?php echo SITE_URL; ?>pages/designation.php">Designation</a>
                                        </li>
                                        <li>
                                            <a href="<?php echo SITE_URL; ?>pages/department.php">Department</a>
                                        </li>
                                        <li>
                                            <a href="<?php echo SITE_URL; ?>pages/termsgroup.php">Terms Group</a>
                                        </li>
                                    </ul>    
                                </li>
                                <li>
                                	<a href="javascript:void(0)">Item<span class="fa arrow"></span></a>
   									<ul class="nav nav-third-level" aria-expanded="false">
                                		<li>
                                            <a href="<?php echo SITE_URL; ?>pages/item_master.php">Item Master</a>
                                        </li>
                                        <li>
                                            <a href="<?php echo SITE_URL; ?>pages/item_category.php">Item category</a>
                                        </li>
                                        <li>
                                            <a href="<?php echo SITE_URL; ?>pages/item_group_code.php">Item Group Code</a>
                                        </li>
                                        <li>
                                            <a href="<?php echo SITE_URL; ?>pages/item_type.php">Item Type</a>
                                        </li>
                                        <li>
                                            <a href="<?php echo SITE_URL; ?>pages/item_status.php">Item Status	</a>
                                        </li>
                                        <li>
                                            <a href="<?php echo SITE_URL; ?>pages/material_process_type.php">Material Process Type</a>
                                        </li> 
                                        <li>
                                            <a href="<?php echo SITE_URL; ?>pages/main_group.php">Main Group</a>
                                        </li>  
                                         <li>
                                            <a href="<?php echo SITE_URL; ?>pages/sub_group.php">Sub Group</a>
                                        </li>  
                                         <li>
                                            <a href="<?php echo SITE_URL; ?>pages/item_make.php">Make</a>
                                        </li>  
                                        <li>
                                            <a href="<?php echo SITE_URL; ?>pages/drawingno.php">Drawing Number </a>
                                        </li>  
                                        <li>
                                            <a href="<?php echo SITE_URL; ?>pages/item_material.php">Material Specification </a>
                                        </li>                           
										<li>
                                            <a href="<?php echo SITE_URL; ?>pages/item_class.php">Class</a>
                                        </li>
										<li>
                                            <a href="<?php echo SITE_URL; ?>pages/excise.php">Excise</a>
                                        </li>
										<li>
                                            <a href="<?php echo SITE_URL; ?>pages/AddOnMaster.php">AddOn Master</a>
                                        </li>										
                                    </ul>    
                                </li>	
                                <li>
                                    <a href="<?php echo SITE_URL; ?>pages/user.php">user</a>
                                </li>
                                <li>
                                    <a href="<?php echo SITE_URL; ?>pages/agent.php">Agent</a>
                                </li>
                                <li>
                                	<a href="javascript:void(0)">Sales<span class="fa arrow"></span></a>
   									<ul class="nav nav-third-level" aria-expanded="false">
                                		<li>
                                             <a href="<?php echo SITE_URL; ?>pages/reference.php">Reference</a>
                                        </li>
                                        <li>
                                            <a href="<?php echo SITE_URL; ?>pages/industry_stage.php">Inquiry Stage</a>
                                        </li>
                                        <li>
                                            <a href="<?php echo SITE_URL; ?>pages/industry_type.php">Industry Type</a>
                                        </li>
                                        <li>
                                            <a href="<?php echo SITE_URL; ?>pages/industry_status.php">Inquiry Status</a>
                                        </li>
                                        <li>
                                            <a href="<?php echo SITE_URL; ?>pages/priority.php">Priority</a>
                                        </li>
                                        <li>
                                            <a href="<?php echo SITE_URL; ?>pages/lead_provider.php">Lead Provider</a>
                                        </li>
                                        <li>
                                            <a href="<?php echo SITE_URL; ?>pages/uom.php">UOM</a>
                                        </li>
                                        <li>
                                            <a href="<?php echo SITE_URL; ?>pages/quotation_type.php">Quotation Type</a>
                                        </li>
                                        <li>
                                            <a href="<?php echo SITE_URL; ?>pages/sales_type.php">Sales Type</a>
                                        </li>
                                        <li>
                                            <a href="<?php echo SITE_URL; ?>pages/quotation_reason.php">Quotation Reason</a>
                                        </li>
                                         <li>
                                            <a href="<?php echo SITE_URL; ?>pages/currency.php">Currency</a>
                                        </li>
                                        <li>
                                            <a href="<?php echo SITE_URL; ?>pages/sales.php">Sales</a>
                                        </li>
                                        <li>
                                            <a href="<?php echo SITE_URL; ?>pages/action.php">Action</a>
                                        </li>
                                        <li>
                                            <a href="<?php echo SITE_URL; ?>pages/quotation_stage.php">Stage</a>
                                        </li>
                                        <li>
                                            <a href="<?php echo SITE_URL; ?>pages/status.php">Status</a>
                                        </li>
                                        <li>
                                            <a href="<?php echo SITE_URL; ?>pages/inquery_conform.php">Inquiry Confirmation</a>
                                        </li>
                                     </ul>    
                                </li>
                                <li>
                                	<a href="javascript:void(0)">Terms & condition<span class="fa arrow"></span></a>
   									<ul class="nav nav-third-level" aria-expanded="false">
                                		<li>
                                             <a href="<?php echo SITE_URL; ?>pages/terms_condition_group.php">Terms & Condition Group</a>
                                        </li>
                                        <li>
                                             <a href="<?php echo SITE_URL; ?>pages/terms_condition_detail.php">Terms & Condition Details</a>
                                        </li>
                                        <li>
                                             <a href="<?php echo SITE_URL; ?>pages/default_term.php">Default Terms & Condition</a>
                                        </li>
                                        <li>
                                             <a href="<?php echo SITE_URL; ?>pages/terms_condition_template.php">Terms & Condtion Template</a>
                                        </li>
                                     </ul>    
                                </li>
                                <li>
                                	<a href="javascript:void(0)">General Master<span class="fa arrow"></span></a>
   									<ul class="nav nav-third-level" aria-expanded="false">
                                		<li>
                                             <a href="<?php echo SITE_URL; ?>pages/branch.php">Branch</a>
                                        </li>
										<li>
                                             <a href="<?php echo SITE_URL; ?>pages/project.php">Project</a>
                                        </li>
                                        <li>
                                             <a href="<?php echo SITE_URL; ?>pages/billingtemplate.php">Billing Template</a>
                                        </li>
                                     </ul>    
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a href="<?php echo SITE_URL; ?>dashbord.php"><i class="fa fa-shopping-cart fa-fw"></i> Purchase</a>
                            <ul class="nav nav-second-level">
                            	<li>
                                	 
								</li>
                            </ul>
                        </li>
                        <li>
                            <a href="javascript:void(0)"><i class="fa fa-line-chart fa-fw" ></i> Sales <span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                            	<li>
	                                <a href="<?php echo SITE_URL; ?>pages/salse_opportunities_leads.php">Sales Opportunities/Leads</a>
                                </li>
								<li>
	                                <a href="<?php echo SITE_URL; ?>pages/inquiry.php">Inquiry</a>
                                </li>
								<li>
	                                <a href="<?php echo SITE_URL; ?>pages/addInquiry.php">Add Inquiry</a>
                                </li>
                                <li>
	                                <a href="<?php echo SITE_URL; ?>pages/quotation.php">Quotation</a>
                                </li>
								<li>
	                                <a href="<?php echo SITE_URL; ?>pages/salesorder.php">Sales Order</a>
                                </li>
								<li>
	                                <a href="<?php echo SITE_URL; ?>pages/sales_challan.php">Challan</a>
                                </li>
								<li>
	                                <a href="<?php echo SITE_URL; ?>pages/sales_invoice.php">Invoice</a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a href="<?php echo SITE_URL; ?>dashbord.php"><i class="fa fa-shopping-bag fa-fw"></i> Store</a>
                        </li>
                        <li>
                            <a href="<?php echo SITE_URL; ?>dashbord.php"><i class="fa fa-truck fa-fw"></i> Dispatch</a>
                        </li>
                        <li>
                            <a href="<?php echo SITE_URL; ?>dashbord.php"><i class="fa fa-money fa-fw"></i> Finance</a>
                        </li>
                        <li>
                            <a href="<?php echo SITE_URL; ?>dashbord.php"><i class="fa fa-file-text fa-fw"></i> Reports</a>
                        </li>
                        <li>
                            <a href="javascript:void(0)"><i class="fa fa-cog fa-fw"></i> General</a>
							<ul class="nav nav-third-level">
								<li>
									<a href="<?php echo SITE_URL; ?>pages/dailyworkentry.php">Daily Work Entry</a>
								</li> 
								<li>
									<a href="<?php echo SITE_URL; ?>pages/alert.php">Alert </a>
								</li>		
								<li>
									<a href="<?php echo SITE_URL; ?>pages/alerttypemaster.php">Alert Type Master</a>
								</li>
							</ul>   
                        </li>
						
						  <li>
                            <a href="javascript:void(0)"><i class="fa fa-cog fa-fw"></i>HR</a>
							<ul class="nav nav-third-level">
								<li>
									<a href="<?php echo SITE_URL; ?>pages/employee_master.php">Employee Master</a>
									<ul class="nav nav-third-level" aria-expanded="false">
                                		<li>
                                            <a href="<?php echo SITE_URL; ?>pages/employee_master.php">Manage Employee</a>
										</li>
										<li>
                                            <a href="<?php echo SITE_URL; ?>pages/calculate_salary.php">Calculate Salary</a>
										</li>
									</ul>
								</li>
								<li>
									<a href="<?php echo SITE_URL; ?>pages/leave_master.php">Leave Master</a>
									<ul class="nav nav-third-level" aria-expanded="false">
                                		<li><a href="<?php echo SITE_URL; ?>pages/leave_master.php">Total Free Leaves</a></li>
										<li><a href="<?php echo SITE_URL; ?>pages/weekly_holiday.php">Weekly Leaves</a></li>
										<li><a href="<?php echo SITE_URL; ?>pages/yearly_leave.php">Yearly Leaves</a></li>	
									</ul>
								</li>
								<li>
									<a href="<?php echo SITE_URL; ?>pages/expected_interview.php">Expected Interview</a>
								</li>
								<li>
									<a href="<?php echo SITE_URL; ?>pages/employe_leave.php">Apply For Leave</a>
								</li>
								<li>
									<a href="#">Add Letters</a>
									<ul class="nav nav-third-level" aria-expanded="false">
                                		<li>
                                            <a href="<?php echo SITE_URL; ?>pages/hr_latters.php?select_latter=offer_letter">Offer Letter</a>
										</li>
										<li>
                                            <a href="<?php echo SITE_URL; ?>pages/hr_latters.php?select_latter=appointment_letter">Appoinment Letter</a>
										</li>
										<li>
                                            <a href="<?php echo SITE_URL; ?>pages/hr_latters.php?select_latter=confirmation_letter">Confirmation Letter</a>
										</li>
										<li>
                                            <a href="<?php echo SITE_URL; ?>pages/hr_latters.php?select_latter=experience_letter">Experiance Letter</a>
										</li>
										<li>
                                            <a href="<?php echo SITE_URL; ?>pages/hr_latters.php?select_latter=noc_letter">NOC Letter</a>
										</li>
									</ul>
								</li>
								
																
																
							</ul>   
                        </li>
                        <li>
                            <a href="<?php echo SITE_URL; ?>dashbord.php"><i class="fa fa-cogs fa-fw"></i> Settings</a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
