<?php
include_once('../header.php');

include_once('../dbFunction.php');
$funObj = new dbFunction();
$countrylogs = $funObj->getTableData('z_country','ORDER BY id DESC ');
?>
<div id="page-wrapper">
    <div class="container">
	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">Country Log</h1>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-default">
				<div class="panel-heading clearfix">
					Country Detail
				</div>				
				<div class="panel-body">
					<div class="dataTable_wrapper">
						<table class="table table-striped table-bordered table-hover" id="dataTables-example">
							<thead>
								<tr>
									<th>Action</th>
									<th>Country</th>
									<th>Old Country</th>
                                    <th>date</th>
                                    <th>user</th>
								</tr>
							</thead>
							<tbody>
								<?php 
									while($countrylog=mysql_fetch_object($countrylogs))
									{
										$user = mysql_fetch_array($funObj->getDataById('user','userid="'.$countrylog->user_id.'"'));
										?>
											<tr class="odd gradeX">
	                                            <td><?php if($countrylog->action == 1) { echo "ADD"; } elseif($countrylog->action == 2) {  echo "EDIT"; } else { echo "DELETE"; }  ?></td>
												<td><?php echo $countrylog->country; ?></td>
                                                <td><?php echo $countrylog->oldcountry; ?></td>
                                                <td><?php echo $countrylog->date; ?></td>
                                                <td><?php echo $user['username']; ?></td>
											</tr>	
										<?php
									} 
								?>								
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
    </div>
</div>
<?php
include_once('../footer.php');
?>

