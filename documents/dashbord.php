<?php 
include_once('header.php');
include_once('dbFunction.php');
$funObj = new dbFunction();
	?>
	<div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Dashboard</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-3 col-md-6">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-user fa-5x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class="huge">26</div>
                                    <div>Party!</div>
                                </div>
                            </div>
                        </div>
                        <a href="#">
                            <div class="panel-footer">
                                <span class="pull-left">View Details</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="panel panel-green">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-tasks fa-5x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class="huge">12</div>
                                    <div>New Tasks!</div>
                                </div>
                            </div>
                        </div>
                        <a href="#">
                            <div class="panel-footer">
                                <span class="pull-left">View Details</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="panel panel-yellow">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-shopping-cart fa-5x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class="huge">124</div>
                                    <div>New Orders!</div>
                                </div>
                            </div>
                        </div>
                        <a href="#">
                            <div class="panel-footer">
                                <span class="pull-left">View Details</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="panel panel-red">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-support fa-5x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class="huge">13</div>
                                    <div>Support Tickets!</div>
                                </div>
                            </div>
                        </div>
                        <a href="#">
                            <div class="panel-footer">
                                <span class="pull-left">View Details</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-6">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-bell fa-fw"></i> SALES AND DISTRIBUTION
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="list-group">
                                <a href="#" class="list-group-item">
                                    <i class="fa fa-bell fa-fw""></i> Pending Enquiries for Quotation
                                    <span class="pull-right text-muted small"><em>0</em>
                                    </span>
                                </a>
                                <a href="#" class="list-group-item">
                                    <b><i class="fa fa-bell fa-fw"></i> Pending Quotes for Approval
                                    <span class="pull-right text-muted small"><em>12</em>
                                    </span></b>
                                </a>
                                <a href="#" class="list-group-item">
                                    <b><i class="fa fa-bell fa-fw"></i> Pending Quotes for Today's Follo
                                    <span class="pull-right text-muted small"><em>2</em>
                                    </span></b>
                                </a>
                                <a href="#" class="list-group-item">
                                    <b><i class="fa fa-bell fa-fw"></i> Overdue Quotes for Follow up
                                    <span class="pull-right text-muted small"><em>4</em>
                                    </span></b>
                                </a>
                                <a href="#" class="list-group-item">
                                    <b><i class="fa fa-bell fa-fw"></i> Due Despatch For Todays
                                    <span class="pull-right text-muted small"><em>1</em>
                                    </span></b>
                                </a>
                                <a href="#" class="list-group-item">
                                     <i class="fa fa-bell fa-fw"></i> OverDue Despatches
                                    <span class="pull-right text-muted small"><em>0</em>
                                    </span> 
                                </a>
                                <a href="#" class="list-group-item">
                                    <i class="fa fa-bell fa-fw"></i> Upcoming Despatches
                                    <span class="pull-right text-muted small"><em>0</em>
                                    </span>
                                </a>
                                <a href="#" class="list-group-item">
                                   <b> <i class="fa fa-bell fa-fw"></i> Pending SO For Approval
                                    <span class="pull-right text-muted small"><em>2</em>
                                    </span></b>
                                </a>
                                <a href="#" class="list-group-item">
                                   <b> <i class="fa fa-bell fa-fw"></i> Today's Leads For Follow Up
                                    <span class="pull-right text-muted small"><em>3</em>
                                    </span></b>
                                </a>
                                <a href="#" class="list-group-item">
                                   <b> <i class="fa fa-bell fa-fw"></i> OverDue Leads For Follow Up
                                    <span class="pull-right text-muted small"><em>2</em>
                                    </span></b>
                                </a>
                                
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                	<div class="panel panel-default">
                        <div class="panel-heading">
                            <i class="fa fa-bell fa-fw"></i> SERVICE & MAINTENANCE
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="list-group">
                                <a href="#" class="list-group-item">
                                    <i class="fa fa-bell fa-fw""></i> Pending Service Calls
                                </a>
                                <a href="#" class="list-group-item">
                                    <i class="fa fa-bell fa-fw"></i> Due Service Contracts
                                </a>
                                <a href="#" class="list-group-item">
                                    <i class="fa fa-bell fa-fw"></i> Due Asset Maintenance
                                </a>
                                <a href="#" class="list-group-item">
                                    <i class="fa fa-bell fa-fw"></i> Due Asset Insurance
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.row -->
        
        	<div class="row">
            	<div class="col-lg-8">
                	<div class="panel panel-default">
                        <div class="panel-heading">
                            <h5 style="color:red">Hi Mr. User Name! You Have 3 Pending Activities</h5>
                        </div>
                        <div class="panel-body">
                            <div class="table-responsive">
                                 
                                 <table class="table table-striped">
                                    <thead>
                                        <tr>
                                            <th>NO</th>
                                            <th>Date</th>
                                            <th>ACTIVITIES ctivities</th>
                                            <th>ASSIGNED By</th>
                                            <th>ASSIGNED TO</th>
                                            <th>CATEGORY</th>
                                            <th>DESCRIPTION</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>1</td>
                                            <td>30-7-2016</td>
                                            <td>Activity name</td>
                                            <td>Admin</td>
                                            <td>user1</td>
                                            <td>Activities Category</td>
                                            <td>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</td>
                                        </tr>
                                        <tr>
                                            <td>2</td>
                                            <td>30-7-2016</td>
                                            <td>Activity name</td>
                                            <td>user1</td>
                                            <td>user1</td>
                                            <td>Activities Category</td>
                                            <td>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</td>
                                        </tr>
                                        <tr>
                                            <td>3</td>
                                            <td>30-7-2016</td>
                                            <td>Activity name</td>
                                            <td>user2</td>
                                            <td>user1</td>
                                            <td>Activities Category</td>
                                            <td>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        
        </div>
        <!-- /#page-wrapper -->
<?php 
include_once('footer.php');
	?>