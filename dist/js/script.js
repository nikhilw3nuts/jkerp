function loader()
{
	$(".loader").show();
}
function unloader()
{
	$(".loader").hide();
}

$(document).ready(function()
{
	if($(".btnpermsave").length)
	{
		$(".btnpermsave").on("click",function(e)
		{
			e.preventDefault();
			loader();

			if($("#userid").val()=='')
			{
				alert("Provide User ID");
				unloader();
				return false;
			}



			$.ajax(
			{
				type:"POST",
				url:"ajaxcall.php?ajax=saveperm",
				data: $("#frmperm").serialize(),
				success:function(data)
				{
					unloader();
					alert(data);
				}
			});
		});
	}
});


function callUsers()
{
	loader();
	$(".pcheck").prop("checked",false)
	var usertype= $("#usertype").val();
	$.ajax({
	type:"post",
	url:"ajaxcall.php?ajax=loaduser",
	data:"usertype="+usertype,
	success:function(data)
	{
		$("#userid").html(data);
		unloader();
	}
	});
}

function callPerm()
{
	loader();
	$(".pcheck").prop("checked",false)
	var userid =  $("#userid").val();
	$.ajax({
	type:"post",
	url:"ajaxcall.php?ajax=loadperm",
	data: "userid="+userid,
	success:function(data)
	{
		eval(data);
		unloader();
	}
	});
}