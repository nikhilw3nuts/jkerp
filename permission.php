<?php 
require_once 'header.php';  
include_once('dbFunction.php');
$funObj = new dbFunction();
?>
<div id="page-wrapper">
    <div class="container">
		<div class="row">
		&nbsp;
			<form name="frmperm" id="frmperm" action="" method="post">
			<div class="col-lg-12">
				<h1 class="page-header" style="position:relative;">Permission
				
				<div style="position:absolute;right:5px; top:-5px;">
					<input type="button" name="btnpermsave" id="btnpermsave" value="Save Permission"  class="btn btn-primary btnpermsave">
				</div>
				</h1>
			</div>
			<!-- /.col-lg-12 -->
			<div class="row">
				<div class="col-md-6" >
				<label class="col-md-2">Usertype</label>
				<div class="col-md-8">
				<select class="form-control" id="usertype" name="usertype" onchange="callUsers()">
				<option value="">Select Usertype</option>
		<?php
			$users = $funObj->getTableData("usertype");
			while($row=mysql_fetch_assoc($users))
			{
				extract($row);
		?>
			<option value="<?php echo $usertypeid; ?>"><?php echo $usertype; ?></option>
		<?php
			}
		?>

				</select></div>
				</div>
				<div class="col-md-6">
				
								<label class="col-md-2">User</label>
				<div class="col-md-8">
				<select class="form-control" id="userid" name="userid" onchange="callPerm()">
				<option value="">Select User</option>
				</select></div>
				</div>
			</div>
			
				<div class="col-lg-12">
					<table class="table table-striped">
					<thead>
						<tr>
							<th width="*">Module Name</th>
							<th width="8%" align="center">Add</th>
							<th width="8%" align="center">Edit</th>
							<th width="8%" align="center">Delete</th>
							<th width="8%" align="center">View</th>
						</tr>
					</thead>
					<tbody>
					<?php
					  draw_permission_list(0, 20);
					?>
					</tbody>
					</table>
				</div>
				<div style="margin-bottom:35px; text-align:center;">
					<input type="button" name="btnpermsave" id="btnpermsave" value="Save Permission"  class="btn btn-primary btnpermsave">
				</div>
			</form>
		</div>
	</div>
</div>
<div class="loader"></div>
<?php
require_once 'footer.php';
function draw_permission_list($parent_id=0, $margin=0)
{
	global $funObj;
	$item_category = $funObj->getTableData('module_master'," where parent_module='".$parent_id."'");
	while($res=mysql_fetch_assoc($item_category))
	{
		extract($res);
?>
<tr>
<td>
<input type="hidden" id="listmodule" name="listmodule[]" value="<?php echo $page_id;?>">
<p style="padding-left:<?php echo $margin; ?>px"><?php echo $module_name; ?></p>
</td>
<td align="center"><input type="checkbox" id="module_add<?php echo $page_id; ?>" name="module_add<?php echo $page_id; ?>" value="Y" class="pcheck" /></td>
<td align="center"><input type="checkbox" id="module_edit<?php echo $page_id; ?>" name="module_edit<?php echo $page_id; ?>" value="Y"  class="pcheck" /></td>
<td align="center"><input type="checkbox" id="module_delete<?php echo $page_id; ?>" name="module_delete<?php echo $page_id; ?>" value="Y"  class="pcheck" /></td>
<td align="center"><input type="checkbox" id="module_view<?php echo $page_id; ?>" name="module_view<?php echo $page_id; ?>" value="Y"  class="pcheck" /></td>
</tr>
<?php
		draw_permission_list($page_id, $margin+40);
	}
}
?>