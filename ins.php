<?php
session_start();
date_default_timezone_set('Asia/Kolkata');
require "dbFunction.php";

$db= new dbFunction();
$perm_list= array("master"=>"",
	"Company Detail"=>"company_detail.php",
	"Party Master"=>Array(
		"Party"=>"party.php",
		"Country"=>"country.php",
		"State"=>"state.php",
		"City"=>"city.php",
		"Party Type1"=>"partytype1.php",
		"Party Type2"=>"partytype2.php",
		"Reference"=>"reference.php",
		"Designation"=>"designation.php",
		"Department"=>"department.php",
		"Terms Group"=>"termsgroup.php"),
	"Item"=>Array(
		"Item Master"=>"item_master.php",
		"Item Category"=>"item_category.php",
		"Item Group_code"=>"item_group_code.php",
		"Item Type"=>"item_type.php",
		"Item Status"=>"item_status.php",
		"Material Process Type"=>"material_process_type.php",
		"Main Group"=>"main_group.php",
		"Sub Group"=>"sub_group.php",
		"Make"=>"item_make.php",
		"Drawing Number"=>"drawingno.php",
		"Material Specification"=>"item_material.php",
		"Class"=>"item_class.php",
		"Excise"=>"excise.php"),
	"User"=>"user.php",
	"Agent"=>"agent.php",
	"Sales"=>Array(
		"Reference"=>"reference.php",
		"Inquiry Stage"=>"industry_stage.php",
		"Industry Type"=>"industry_type.php",
		"Inquiry Status"=>"industry_status.php",
		"Priority"=>"priority.php",
		"Lead Provider"=>"lead_provider.php",
		"Uom"=>"uom.php",
		"Quotation Type"=>"quotation_type.php",
		"Sales Type"=>"sales_type.php",
		"Quotation Reason"=>"quotation_reason.php",
		"Currency"=>"currency.php",
		"Sales"=>"sales.php",
		"Action"=>"action.php",
		"Stage"=>"quotation_stage.php",
		"Status"=>"status.php",
		"Inquiry Confirmation"=>"inquery_conform.php"),
	"Terms Condition"=>Array(
		"Terms Condition Group"=>"terms_condition_group.php",
		"Terms Condition Details"=>"terms_condition_detail.php",
		"Default Terms Condition"=>"default_term.php",
		"Terms Condtion Template"=>"terms_condition_template.php"),
	"General Master"=>Array(
		"Branch"=>"branch.php",
		"Project"=>"project.php",
		"Billing Template"=>"billingtemplate.php"),
"Purchase"=>"dashbord.php",
"Sales"=>Array(
	"Sales Opportunities Leads"=>"salse_opportunities_leads.php",
	"Inquiry"=>"inquiry.php",
	"Add Inquiry"=>"addInquiry.php",
	"Quotation"=>"quotation.php",
	"Sales Order"=>"salesorder.php"),
"Store" =>"",
"Dispatch"=>"",
"Finance"=>"",
"Reports"=>"",
"General"=>Array(
	"Daily Work Entry"=>"dailyworkentry.php",
	"Alert"=>"alert.php",
	"Alert Type Master"=>"alerttypemaster.php"),
"Hr"=>Array("Employee Master"=>Array(
		"Manage Employee"=>"employee_master.php",
		"Calculate Salary"=>"calculate_salary.php"),
	"Leave Master"=>Array(
		"Total Free Leaves"=>"leave_master.php",
		"Weekly Leaves"=>"weekly_holiday.php",
		"Yearly Leaves"=>"yearly_leave.php"),
	"Expected Interview"=>"expected_interview.php",
	"Apply For Leave"=>"employe_leave.php",
	"Add Letters"=>Array(
		"Offer Letter"=>"hr_latters.php?select_latter=offer_letter",
		"Appoinment Letter"=>"hr_latters.php?select_latter=appointment_letter",
		"Confirmation Letter"=>"hr_latters.php?select_latter=confirmation_letter",
		"Experiance Letter"=>"hr_latters.php?select_latter=experience_letter",
		"Noc Letter"=>"hr_latters.php?select_latter=noc_letter")),
"Settings"=>"");

loopinsert($perm_list, "0");

function loopinsert($array=array(), $parent_id=0, $type='page')
{
	foreach($array as $key => $module)
	{
		if(is_array($module))
		{
			$q="insert into module_master (module_name, parent_module, type,file_name) values ('".$key."','".$parent_id."','".$type."','--')";
			if(mysql_query($q))
			{
				$p_id = mysql_insert_id();
				loopinsert($module, $p_id);
			}
			else
			{ echo " --- ".mysql_error()."<BR>"; }
		}
		else
		{
			$qry="insert into module_master (module_name, parent_module, type,file_name)values ('".$key."','".$parent_id."','".$type."','".$module."')";
			if(mysql_query($qry))
			{ echo $module." ---- Inserted successfully <br>"; }
			else
			{ echo $module." --- ".mysql_error()."<BR>"; }
		}
	}
}




?>