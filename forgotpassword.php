<?php 
include_once('dbFunction.php'); 

$funObj = new dbFunction();
$chnpass = false;
if($_POST['sendemail']){  
        $emailid = $_POST['email'];  
        $checkmail = $funObj->isMailExist($emailid);  
        if (!$checkmail) {  
			echo "<script>alert('Invalid email address')</script>";
        }  
        else
        {
			echo "<script>alert('Mail send successfully.')</script>";
			echo $checkmail;
		}
    }
   
	
	$usercode = $_GET['usercode'];

	if($_GET['changepass']=="true")
	{
		if($_SESSION['usercode'] == $_SESSION['chemail'].'_'.$usercode)
		{
			$chnpass = true;
		}
		else
		{
			$chnpass = false;
			echo "<script>alert('Wrong url! please try again.')</script>";
			header("location:forgotpassword.php");
			unset($_SESSION['usercode']);
			unset($_SESSION['sendmailtrue']);
		}
	}
	
	if($_POST['changePassword'])
	{
		$pass = $_POST['password'];  
        $pass1 = $_POST['repassword'];
        $checkmail = $funObj->changepassword($pass,$pass1);  
	}
?>
<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Jay Khodiyar</title>
    <link href="bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">
    <link href="dist/css/sb-admin-2.css" rel="stylesheet">
    <link href="bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                <div class="login-panel panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Forgot Password</h3>
                    </div>
                    <div class="panel-body">
						<?php if($chnpass){
							?>
								<form role="form" action="" name="changepass" method="POST">
									<fieldset>
										<div class="form-group">
											<input class="form-control" placeholder="Password" name="password" type="password" value="">
										</div>
										<div class="form-group">
											<input class="form-control" placeholder="confirm Password" name="repassword" type="password" value="">
										</div>
										<input class="btn btn-lg btn-success btn-block" type="submit" name="changePassword" value="Change Password" />
									</fieldset>
								</form>
							<?php
						}
						else
						{ ?>
							<form role="form" action="" name="forgetpass" method="POST">
								<fieldset>
									<div class="form-group">
										<input class="form-control" placeholder="E-mail" name="email" type="email" autofocus>
									</div>
									<input class="btn btn-lg btn-success btn-block" type="submit" name="sendemail" value="Send Mail" />
								</fieldset>
							</form>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script src="bower_components/jquery/dist/jquery.min.js"></script>
    <script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="bower_components/metisMenu/dist/metisMenu.min.js"></script>
    <script src="dist/js/sb-admin-2.js"></script>

</body>

</html>
