<?php
include_once('../header.php');

include_once('../dbFunction.php');
$funObj = new dbFunction();
$cp_log = $funObj->getTableData('z_contact_person','ORDER BY id DESC ');
?>
<div id="page-wrapper">
    <div class="container">
	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">Contact Person Log</h1>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-default">
				<div class="panel-heading clearfix">
					Country Detail
				</div>				
				<div class="panel-body">
					<div class="dataTable_wrapper">
						<table class="table table-striped table-bordered table-hover" id="dataTables-example">
							<thead>
								<tr>
									<th>Action</th>
									<th>New values</th>
									<th>Old values</th>
                                    <th>date</th>
                                    <th>user</th>
								</tr>
							</thead>
							<tbody>
								<?php 
									while($cp_logs=mysql_fetch_object($cp_log))
									{
										$user = mysql_fetch_array($funObj->getDataById('user','userid="'.$cp_logs->user_id.'"'));
										?>
											<tr class="odd gradeX">
	                                            <td><?php if($cp_logs->action == 1) { echo "ADD"; } elseif($cp_logs->action == 2) {  echo "EDIT"; } else { echo "DELETE"; }  ?></td>
												<td>
													<?php
														$newvalue_array = unserialize($cp_logs->new_values);
														$department = mysql_fetch_array($funObj->getDataById('department','department_id="'.
														$newvalue_array['department'].'"'));
														$designation = mysql_fetch_array($funObj->getDataById('designation','designation_id="'.
														$newvalue_array['designation'].'"'));
														$user1 = mysql_fetch_array($funObj->getDataById('user','userid="'.$newvalue_array['user_id'].'"'));
														echo "<p>Contact Person Name : ".$newvalue_array['contact_person']."</p>";
														echo "<p>Priority : ".$newvalue_array['priority']."</p>";
														echo "<p>Designation : ".$designation['designation']."</p>";
														echo "<p>Department : ".$department['department']."</p>";
														echo "<p>Mobile : ".$newvalue_array['mobile']."</p>";
														echo "<p>Fax : ".$newvalue_array['fax']."</p>";
														echo "<p>Email : ".$newvalue_array['email']."</p>";
														echo "<p>Note : ".$newvalue_array['note']."</p>";
														$statuse = ($newvalue_array['status'] == 1) ? "Active" : "Deactive";
														echo "<p>Status : ".$statuse."</p>";
														echo "<p>user : ".$user1['username']."</p>";
													?>
												</td>
                                                <td>
													<?php 
														$oldvalue_array = unserialize($cp_logs->old_values);
														if($oldvalue_array != "")
														{
															$department1 = mysql_fetch_array($funObj->getDataById('department','department_id="'.
															$oldvalue_array['department'].'"'));
															$designation1 = mysql_fetch_array($funObj->getDataById('designation','designation_id="'.
															$oldvalue_array['designation'].'"'));
															$user2 = mysql_fetch_array($funObj->getDataById('user','userid="'.$oldvalue_array['user_id'].'"'));
															echo "<p>Contact Person Name : ".$oldvalue_array['contact_person']."</p>";
															echo "<p>Priority : ".$oldvalue_array['priority']."</p>";
															echo "<p>Designation : ".$designation1['designation']."</p>";
															echo "<p>Department : ".$department1['department']."</p>";
															echo "<p>Mobile : ".$oldvalue_array['mobile']."</p>";
															echo "<p>Fax : ".$oldvalue_array['fax']."</p>";
															echo "<p>Email : ".$oldvalue_array['email']."</p>";
															echo "<p>Note : ".$oldvalue_array['note']."</p>";
															$statuse = ($oldvalue_array['status'] == 1) ? "Active" : "Deactive";
															echo "<p>Status : ".$statuse."</p>";
															echo "<p>user : ".$user2['username']."</p>";
														}
													?>
												</td>
                                                <td><?php echo $cp_logs->date; ?></td>
                                                <td><?php echo $user['username']; ?></td>
											</tr>	
										<?php
									} 
								?>								
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
    </div>
</div>
<?php
include_once('../footer.php');
?>

